//
//  GroupTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 3/5/22.
//

import XCTest
@testable import Bureau

class GroupTest: XCTestCase {
    
    var groups: Group?
    let id = "61a7c2d21a7fb92f9aefd8ba"
    let groupName = "Security"
    let quotaName = "Balcony"
    let quotaNumber = 3
    let bookForOthers = true
    let recurrenceBookings = false
    let createdAt = "2021-10-23T20:25:30.688Z"
    let updatedAt = "2022-04-27T21:59:16.574Z"
    let version = 1

    override func setUpWithError() throws {
        let group = Data("""
            {
                "id": "\(id)",
                "name": "\(groupName)",
                "quota": {"\(quotaName)": \(quotaNumber)},
                "bookForOthers": \(bookForOthers),
                "recurrenceBookings": \(recurrenceBookings),
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "version": \(version),
            }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            groups = try decoder.decode(Group.self, from: group)
        } catch {
            print(error)
        }
    }

    func testGroupModelIsInitializedCorrectly() throws {
        XCTAssertEqual(groups?.id, id)
        XCTAssertEqual(groups?.name, groupName)
        XCTAssertEqual(groups?.quota?.first?.key, quotaName)
        XCTAssertEqual(groups?.quota?.first?.value, quotaNumber)
        XCTAssertEqual(groups?.bookForOthers, bookForOthers)
        XCTAssertEqual(groups?.recurrenceBookings, recurrenceBookings)
        XCTAssertEqual(groups?.createdAt, createdAt)
        XCTAssertEqual(groups?.updatedAt, updatedAt)
        XCTAssertEqual(groups?.version, version)
    }
}
