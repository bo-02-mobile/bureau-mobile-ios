//
//  AreaTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/3/21.
//

import XCTest
@testable import Bureau

class AreaTest: XCTestCase {
    var area: Area?
    let code = "3A"
    let typeDescription = "3A Teams"
    let id = "60a807f1dc0ba843b1ac4064"
    let areaName = "3A"
    let published = true
    var markers: [Marker] = []
    let thumbnailId = "eneh2stibz6prd7oaxka"
    let thumbnailUrl = "https://res.cloudinary.com/dbjicvy8t/image/upload/v1626054622/eneh2stibz6prd7oaxka.jpg"
    let backgroundId = "ecuz2zdbuiseeryvzomk"
    let backgroundUrl = "https://res.cloudinary.com/dbjicvy8t/image/upload/v1626054622/ecuz2zdbuiseeryvzomk.jpg"

    override func setUpWithError() throws {
        let areaData = Data("""
        {
          "id": "\(id)",
          "code": "\(code)",
          "name": "\(areaName)",
          "description": "\(typeDescription)",
          "background": {
            "id": "\(backgroundId)",
            "url": "\(backgroundUrl)"
          },
          "thumbnail": {
            "id": "\(thumbnailId)",
            "url": "\(thumbnailUrl)"
          },
          "published": \(published),
          "markers": \(markers)
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            area = try decoder.decode(Area.self, from: areaData)
        } catch {
            print(error)
        }
    }

    func testAreaIsInitializeCorrectly() throws {
        XCTAssertEqual(area?.id, id)
        XCTAssertEqual(area?.code, code)
        XCTAssertEqual(area?.name, areaName)
        XCTAssertEqual(area?.markers?.count, markers.count)
        XCTAssertEqual(area?.published, published)
        XCTAssertEqual(area?.thumbnail?.id, thumbnailId)
        XCTAssertEqual(area?.thumbnail?.url, thumbnailUrl)
        XCTAssertEqual(area?.background?.id, backgroundId)
        XCTAssertEqual(area?.background?.url, backgroundUrl)
    }
}
