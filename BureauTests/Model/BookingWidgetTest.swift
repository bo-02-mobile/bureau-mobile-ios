//
//  BookingWidgetTest.swift
//  BureauTests
//
//  Created by User on 1/4/22.
//

import XCTest
@testable import Bureau

class BookingWidgetTest: XCTestCase {
    
    var bookingWidget: BookingWidget?
    let areaName = "3A"
    let assetCode = ""
    let startTime = "2021-06-19T22:39:16.022Z"
    let endTime = "2021-06-19T22:39:16.022Z"
    let date = "2021-06-19T22:39:16.022Z"
    var isConfirmed = true

    override func setUpWithError() throws {
        bookingWidget = BookingWidget(
            areaName: areaName,
            assetCode: assetCode,
            startTime: startTime,
            endTime: endTime,
            date: date,
            isConfirmed: isConfirmed
        )
    }

    func testBookingWidgetIsInitializeCorrectly() throws {
        XCTAssertEqual(bookingWidget?.areaName, areaName)
        XCTAssertEqual(bookingWidget?.assetCode, assetCode)
        XCTAssertEqual(bookingWidget?.startTime, startTime)
        XCTAssertEqual(bookingWidget?.endTime, endTime)
        XCTAssertEqual(bookingWidget?.date, date)
        XCTAssertEqual(bookingWidget?.isConfirmed, isConfirmed)
    }
}
