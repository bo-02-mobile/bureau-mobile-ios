//
//  TimeHourTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/5/21.
//

import XCTest
@testable import Bureau

class TimeHourTest: XCTestCase {
    var timeHour: TimeHour?
    var hours = 10
    var minute = 30

    func testInitWithoutParameters() throws {
        timeHour = TimeHour()
        hours = 0
        minute = 0
        XCTAssertEqual(hours, timeHour?.hour)
        XCTAssertEqual(minute, timeHour?.minute)
    }
    
    func testGetHourTextAndMinuteLowerThanTen() throws {
        timeHour = TimeHour(hour: 9, minute: 9)
        XCTAssertEqual(timeHour?.getHourText(), "09")
        XCTAssertEqual(timeHour?.getMinuteText(), "09")
    }
    
    func testGetHourTextAndMinuteEqualToTen() throws {
        timeHour = TimeHour(hour: 10, minute: 10)
        XCTAssertEqual(timeHour?.getHourText(), "10")
        XCTAssertEqual(timeHour?.getMinuteText(), "10")
    }
    
    func testGetHourTextAndMinuteBiggerThanTen() throws {
        timeHour = TimeHour(hour: 23, minute: 20)
        XCTAssertEqual(timeHour?.getHourText(), "23")
        XCTAssertEqual(timeHour?.getMinuteText(), "20")
    }
}
