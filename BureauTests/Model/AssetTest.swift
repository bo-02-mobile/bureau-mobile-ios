//
//  AssetTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/3/21.
//

import XCTest
@testable import Bureau

class AssetTest: XCTestCase {
    var asset: Asset?
    let id = "60b14364b74284b5f27fface"
    let buildingId = "60a807c5dc0ba843b1ac4063"
    let areaId = "60b13fe3b74284b5f27ffacb"
    let assetName = "1C001"
    let code = "1C001"
    let assetDescription = ""
    let published = true
    let statsCount = 0
    let statsAverage = 0
    let bookingWindow = 3
    let isRecurrent = true
    let labels = ["Power", "Wifi", "Mouse", "Ethernet"]
    let type = "Conference Room"

    override func setUpWithError() throws {
        let assetData = Data("""
        {
                "id": "\(id)",
                "buildingId": "\(buildingId)",
                "areaId": "\(areaId)",
                "name": "\(assetName)",
                "code": "\(code)",
                "description": "\(assetDescription)",
                "public": \(published),
                "stats": {
                    "count": \(statsCount),
                    "average": \(statsAverage)
                },
                "properties": {
                    "labels": \(labels),
                    "type": "\(type)",
                    "owner": null
                },
                "policies": {
                    "bookingWindow": \(bookingWindow),
                    "isRecurrent": \(isRecurrent),
                    "min": null,
                    "max": null
                },
                "createdAt": "2021-07-26T14:28:10.294Z",
                "updatedAt": "2021-07-26T14:28:10.294Z",
                "version": 1,
            }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            asset = try decoder.decode(Asset.self, from: assetData)
        } catch {
            print(error)
        }
    }

    func testAssetIsInitializeProperly() throws {
        XCTAssertEqual(asset?.areaId, areaId)
        XCTAssertEqual(asset?.buildingID, buildingId)
        XCTAssertEqual(asset?.id, id)
        XCTAssertEqual(asset?.code, code)
        XCTAssertEqual(asset?.name, assetName)
        XCTAssertEqual(asset?.properties?.labels?.count, labels.count)
        XCTAssertEqual(asset?.properties?.type, type)
        XCTAssertEqual(asset?.policies?.bookingWindow, bookingWindow)
        XCTAssertEqual(asset?.assetDescription, assetDescription)
        XCTAssertEqual(asset?.assetPublic, published)
        XCTAssertEqual(asset?.stats?.count, statsCount)
        XCTAssertEqual(asset?.stats?.average, statsAverage)
    }
}
