//
//  PropertiesTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/5/21.
//

import XCTest
@testable import Bureau

class PropertiesTest: XCTestCase {
    var properties: Properties?
    let labels = ["mouse", "wifi", "keyboard"]
    let type = "Workstation"

    override func setUpWithError() throws {
        let propertiesData = Data("""
        {
            "labels": \(labels),
            "type": "\(type)"
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            properties = try decoder.decode(Properties.self, from: propertiesData)
        } catch {
            print(error)
        }
    }

    func testPropertiesIsInitializeProperly() throws {
        XCTAssertEqual(properties?.labels?.count, labels.count)
        XCTAssertEqual(properties?.type, type)
    }
}
