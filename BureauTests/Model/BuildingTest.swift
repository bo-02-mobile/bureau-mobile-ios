//
//  BuildingTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/3/21.
//

import XCTest
@testable import Bureau

class BuildingTest: XCTestCase {
    var building: Building?
    let id = "60a807c5dc0ba843b1ac4063"
    let buildingName = "Jala Building Cochabamba"
    let code = "JALA-COCHA"
    let address = "Av Melchor Pérez de Olguín 2643, Cochabamba"
    let timezone = "UTC−05=00"
    let published = true

    override func setUpWithError() throws {
        let buildingData = Data("""
        {
            "id": "\(id)",
            "name": "\(buildingName)",
            "code": "\(code)",
            "address": "\(address)",
            "timezone": "\(timezone)",
            "public": \(published),
            "createdAt": "2021-07-26T14:28:04.971Z",
            "updatedAt": "2021-07-26T14:28:04.971Z",
            "version": 1
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            building = try decoder.decode(Building.self, from: buildingData)
        } catch {
            print(error)
        }
    }

    func testBuildingIsInitializeProperly() throws {
        XCTAssertEqual(building?.id, id)
        XCTAssertEqual(building?.name, buildingName)
        XCTAssertEqual(building?.code, code)
        XCTAssertEqual(building?.address, address)
        XCTAssertEqual(building?.timezone, timezone)
        XCTAssertEqual(building?.published, published)
    }
}
