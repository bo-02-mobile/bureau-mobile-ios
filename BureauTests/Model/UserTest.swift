//
//  UserTest.swift
//  BureauTests
//
//  Created by Victor Arana on 6/6/21.
//

import XCTest
@testable import Bureau

class UserTest: XCTestCase {
    var user: User?
    let userName = "Victor Andres Arana Aireyu"
    let userEmail = "3av.arana@gmail.com"
    let userPicturePath = "wwwpicturecom/path"

    override func setUpWithError() throws {
        let userData = Data("""
        {
          "name" : "\(userName)",
          "email" : "\(userEmail)",
          "picture" : "\(userPicturePath)"
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            user = try decoder.decode(User.self, from: userData)
        } catch {
            print(error)
        }
    }

    func testUserIsInitializeCorrectly() throws {
        XCTAssertEqual(user?.name, userName)
        XCTAssertEqual(user?.email, userEmail)
        XCTAssertEqual(user?.picture, userPicturePath)
    }
}
