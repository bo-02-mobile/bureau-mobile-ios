//
//  MakerTest.swift
//  BureauTests
//
//  Created by Victor Arana on 6/19/21.
//

import XCTest
@testable import Bureau

class MarkerTest: XCTestCase {
    var marker: Marker?
    let assetId = "60c0d1cc8f0d73a745f4d46e"
    let published = false
    let xPos = 0.22609825237935588
    let yPos  = 0.45081967213114754

    override func setUpWithError() throws {
        let markerData = Data("""
        {
          "assetId": "\(assetId)",
          "public": \(published),
          "x": \(xPos),
          "y": \(yPos),
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            marker = try decoder.decode(Marker.self, from: markerData)
        } catch {
            print(error)
        }
    }

    func testMarkerIsInitializeCorrectly() throws {
        XCTAssertEqual(marker?.workstationId, assetId)
        XCTAssertEqual(marker?.isAvailable, false)
        XCTAssertEqual(marker?.xPos, xPos)
        XCTAssertEqual(marker?.yPos, yPos)
    }
}
