//
//  StatsTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/5/21.
//

import XCTest
@testable import Bureau
class StatsTest: XCTestCase {
    var stats: Stats?
    let count = 5
    let average = 9

    override func setUpWithError() throws {
        let statsData = Data("""
        {
            "count": \(count),
            "average": \(average)
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            stats = try decoder.decode(Stats.self, from: statsData)
        } catch {
            print(error)
        }
    }

    func testStatsIsInitializeProperly() throws {
        XCTAssertEqual(stats?.count, count)
        XCTAssertEqual(stats?.average, average)
    }
}
