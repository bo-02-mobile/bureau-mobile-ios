//
//  AssetTypeTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/3/21.
//

import XCTest
@testable import Bureau

class AssetTypeTest: XCTestCase {
    var assetType: AssetType?
    let id = "60b14364b74284b5f27fface"
    let assetTypeName = "Workstation"
    let image = "https://res.cloudinary.com/dbjicvy8t/image/upload/v1626054622/ecuz2zdbuiseeryvzomk.jpg"
    let index = 0

    override func setUpWithError() throws {
        let assetTypeData = Data("""
        {
          "id": "\(id)",
          "name": "\(assetTypeName)",
          "image": "\(image)",
          "index": \(index),
          "isSelected": false
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            assetType = try decoder.decode(AssetType.self, from: assetTypeData)
        } catch {
            print(error)
        }
    }

    func testAssetTypeIsInitializeProperly() throws {
        XCTAssertEqual(assetType?.id, id)
        XCTAssertEqual(assetType?.name, assetTypeName)
        XCTAssertEqual(assetType?.image, image)
        XCTAssertEqual(assetType?.index, index)
        XCTAssertEqual(assetType?.isSelected, false)
    }
}
