//
//  OwnerTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/5/21.
//

import XCTest
@testable import Bureau

class OwnerTest: XCTestCase {
    var owner: Owner?
    let id = "60a807c5dc0ba843b1ac4063"
    let ownerName = "Víctor Andrés Arana Aireyu"
    let email = "3av.arana@gmail.com"

    override func setUpWithError() throws {
        let ownerData = Data("""
        {
            "id": "\(id)",
            "preferredUsername": "\(ownerName)",
            "email": "\(email)"
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            owner = try decoder.decode(Owner.self, from: ownerData)
        } catch {
            print(error)
        }
    }

    func testOwnerIsInitializeProperly() throws {
        XCTAssertEqual(owner?.id, id)
        XCTAssertEqual(owner?.preferredUsername, ownerName)
        XCTAssertEqual(owner?.email, email)
    }
}
