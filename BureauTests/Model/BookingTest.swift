//
//  BookingTest.swift
//  BureauTests
//
//  Created by Victor Arana on 7/2/21.
//

import XCTest
@testable import Bureau

class BookingTest: XCTestCase {
    var bookingConfirmation: Booking?
    let id = "60c0d1cc8f0d73a745f4d46e"
    let assetId = "d73a460c0d1cc8f0d4745f6e"
    let ownerId = "1"
    let ownerEmail = "3av.arana@gmail.com"
    let ownerName = "Andres"
    let areaName = "3A"
    let workstationCode = "1C002"
    let startTime = "2021-06-19T22:39:16.022Z"
    let endTime = "2021-06-19T22:39:16.022Z"
    let isRecurrent = false
    let recurrenceRule = "none"
    let isPermanent = false
    let isConfirmed = true
    let state = "UserCancelled"
    let cancellationDate = "2022-05-18T12:46:45.938Z"
    let reason = ""

    override func setUpWithError() throws {
        let booking = Data("""
        {
          "id": "\(id)",
          "assetId": "\(assetId)",
          "owner": {
            "id": "\(ownerId)",
            "preferredUsername": "\(ownerName)",
            "email": "\(ownerEmail)"
          },
          "areaName": "\(areaName)",
          "workstationCode": "\(workstationCode)",
          "ownerName": "\(ownerName)",
          "startTime": "\(startTime)",
          "endTime": "\(endTime)",
          "isRecurrent": \(isRecurrent),
          "recurrenceRule": "\(recurrenceRule)",
          "isPermanent": \(isPermanent),
          "isConfirmed": \(isConfirmed),
          "status": {
             "state": "\(state)",
             "cancellationDate": "\(cancellationDate)",
             "reason": "\(reason)"
           },
        }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            bookingConfirmation = try decoder.decode(Booking.self, from: booking)
        } catch {
            print(error)
        }
    }

    func testBookingIsInitializeCorrectly() throws {
        XCTAssertEqual(bookingConfirmation?.id, id)
        XCTAssertEqual(bookingConfirmation?.assetId, assetId)
        XCTAssertEqual(bookingConfirmation?.owner?.id, ownerId)
        XCTAssertEqual(bookingConfirmation?.owner?.email, ownerEmail)
        XCTAssertEqual(bookingConfirmation?.owner?.preferredUsername, ownerName)
        XCTAssertEqual(bookingConfirmation?.startTime, startTime)
        XCTAssertEqual(bookingConfirmation?.endTime, endTime)
        XCTAssertEqual(bookingConfirmation?.isRecurrent, isRecurrent)
        XCTAssertEqual(bookingConfirmation?.isPermanent, isPermanent)
        XCTAssertEqual(bookingConfirmation?.isConfirmed, isConfirmed)
        XCTAssertEqual(bookingConfirmation?.areaName, areaName)
        XCTAssertEqual(bookingConfirmation?.status?.state, state)
        XCTAssertEqual(bookingConfirmation?.status?.cancellationDate, cancellationDate)
        XCTAssertEqual(bookingConfirmation?.status?.reason, reason)
    }
}
