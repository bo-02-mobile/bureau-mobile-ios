//
//  WorkstationFeatureTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/5/21.
//

import XCTest
@testable import Bureau

class WorkstationFeatureTest: XCTestCase {
    var workstationFeature: WorkstationFeature?
    let id = "60a807f1dc0ba843b1ac4064"
    let workstationFeatureName = "Keyboard"
    let index = 1
    var isSelected = false

    override func setUpWithError() throws {
        let workstationFeatureData = Data("""
        {
                "id": "\(id)",
                "name": "\(workstationFeatureName)",
                "index": \(index),
                "isSelected": \(isSelected)
            }
        """.utf8)
        let decoder = JSONDecoder()
        do {
            workstationFeature = try decoder.decode(WorkstationFeature.self, from: workstationFeatureData)
        } catch {
            print(error)
        }
    }

    func testWorkstationFeatureIsInitializeProperly() throws {
        XCTAssertEqual(workstationFeature?.id, id)
        XCTAssertEqual(workstationFeature?.name, workstationFeatureName)
        XCTAssertEqual(workstationFeature?.index, index)
        XCTAssertEqual(workstationFeature?.isSelected, isSelected)
    }
}
