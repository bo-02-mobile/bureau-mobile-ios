//
//  ConstantsTest.swift
//  BureauTests
//
//  Created by admin on 3/14/22.
//

import XCTest
@testable import Bureau

class ConstantsTest: XCTestCase {

    func testProductionValues() throws {
        if CI.productionFlag == "YES" {
            XCTAssertFalse(Constants.Demo.isInDemoMode)
        } else {
            XCTAssertTrue(Constants.Demo.isInDemoMode)
        }
    }
}
