//
//  HelpersTest.swift
//  BureauTests
//
//  Created by Fernando Guerrero on 2/2/22.
//

import XCTest
@testable import Bureau

class HelpersTest: XCTestCase {
    
    let startTime = "2021-07-26T04:00:00Z"
    let endTime = "2021-07-27T03:59:00Z"
    
    func testConvertDateFormatUTC() throws {
        let calendar = Calendar.current
        let startComponents = DateComponents(calendar: calendar, timeZone: TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat), year: 2021, month: 7, day: 26, hour: 4, minute: 0, second: 0)
        let startDate = calendar.date(from: startComponents)!
        let startString = Helpers.convertDateFormatUTC(inputDate: startDate, inputHour: startDate)
        let endComponents = DateComponents(calendar: calendar, timeZone: TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat), year: 2021, month: 7, day: 27, hour: 3, minute: 59, second: 0)
        let endDate = calendar.date(from: endComponents)!
        let endString = Helpers.convertDateFormatUTC(inputDate: endDate, inputHour: endDate)
        XCTAssertEqual(startTime, startString)
        XCTAssertEqual(endTime, endString)
    }
    
    func testConvertDateFormatWithTimeZone() throws {
        let calendar = Calendar.current
        let startComponents = DateComponents(calendar: calendar, timeZone: TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat), year: 2021, month: 7, day: 26, hour: 8, minute: 0, second: 0)
        let startDate = calendar.date(from: startComponents)!
        let startString = Helpers.convertDateFormatWithTimeZone(inputDate: startDate, inputHour: startDate, timezone: "UTC-04:00")
        let endComponents = DateComponents(calendar: calendar, timeZone: TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat), year: 2021, month: 7, day: 27, hour: 2, minute: 59, second: 0)
        let endDate = calendar.date(from: endComponents)!
        let endString = Helpers.convertDateFormatWithTimeZone(inputDate: endDate, inputHour: endDate, timezone: "UTC+01:00")
        XCTAssertEqual(startTime, startString)
        XCTAssertEqual(endTime, endString)
    }
    
    func testIsTimePlusOneMinuteFunctionReturnsFalse() throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        let startTimeDate = dateFormatter.date(from: startTime)!
        let endTimeDate = dateFormatter.date(from: endTime)!
        XCTAssertFalse(Helpers.isTimePlusOneMinute(startTimeDate, equalsTo: endTimeDate))
    }
    
    func testIsTimePlusOneMinuteFunctionReturnsTrue() throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        let startTimeDate = dateFormatter.date(from: startTime)!
        let endTimeDate = dateFormatter.date(from: endTime)!
        XCTAssertTrue(Helpers.isTimePlusOneMinute(endTimeDate, equalsTo: startTimeDate))
    }
    
    func testConvertToLiteralDateFormat() throws {
        let calendar = Calendar.current
        let timezone = TimeZone.current
        let startComponents = DateComponents(calendar: calendar, timeZone: timezone, year: 2022, month: 4, day: 6, hour: 8, minute: 0, second: 0)
        let startDate = calendar.date(from: startComponents)!
        let endComponents = DateComponents(calendar: calendar, timeZone: timezone, year: 2022, month: 4, day: 6, hour: 16, minute: 30, second: 0)
        let endDate = calendar.date(from: endComponents)!
        let description = Helpers.convertToLiteralDateFormat(currentDate: startDate, startHour: startDate, endHour: endDate)
        XCTAssertEqual("Wed, Apr. 6 from 08:00 AM to 04:30 PM", description)
    }
    
    func testConvertLocalToUTCFormat() throws {
        var hour = 12
        var day = 26
        let dateString = Helpers.convertLocalToUTCFormat(stringDate: "2021-07-\(day)T\(hour):00Z", isForQueryParam: true)
        // Operation to offset hour from local timezone to UTC+00:00
        hour -= (TimeZone.current.secondsFromGMT()/60/60)
        // Validations for timezones that overflow current day
        if hour >= 24 {
            hour = hour % 24
            day = 27
        } else if hour < 0 {
            hour += 24
            day = 25
        }
        XCTAssertEqual(dateString, "2021-07-\(day)T\(String(format: "%02d", hour)):00:00Z")
    }
    
    func testConvertDatetimeToUTCFormat() throws {
        var dateString = Helpers.convertDatetimeToUTCFormat(stringDate: "2021-07-26T12:00Z", isForQueryParam: true, timezone: "UTC-04:00")
        XCTAssertEqual(dateString, "2021-07-26T16:00:00Z")
        dateString = Helpers.convertDatetimeToUTCFormat(stringDate: "2021-07-26T12:00Z", isForQueryParam: true, timezone: "UTC+04:00")
        XCTAssertEqual(dateString, "2021-07-26T08:00:00Z")
        dateString = Helpers.convertDatetimeToUTCFormat(stringDate: "2021-07-26T12:00Z", isForQueryParam: true, timezone: Constants.DateTimeFormat.utcFormat)
        XCTAssertEqual(dateString, "2021-07-26T12:00:00Z")
        dateString = Helpers.convertDatetimeToUTCFormat(stringDate: "2021-07-26T12:00Z", isForQueryParam: true, timezone: "UTC-12:00")
        XCTAssertEqual(dateString, "2021-07-27T00:00:00Z")
        dateString = Helpers.convertDatetimeToUTCFormat(stringDate: "2021-07-26T12:00Z", isForQueryParam: true, timezone: "UTC+12:00")
        XCTAssertEqual(dateString, "2021-07-26T00:00:00Z")
    }
    
    func testOffsetUTCDateToGivenTimezone() throws {
        let calendar = Calendar.current
        let components = DateComponents(calendar: calendar, timeZone: TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat), year: 2021, month: 7, day: 26, hour: 12, minute: 0, second: 0)
        let utcDate = calendar.date(from: components)!
        let localOffset = TimeZone.current.secondsFromGMT()
        let localDate = utcDate.addingTimeInterval(Double(localOffset))
        var timezone = "UTC-04:00"
        var diff = TimeZone(abbreviation: timezone)!.secondsFromGMT() - localOffset
        var newDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: utcDate, timezoneAbbreviation: timezone)
        XCTAssertEqual(localDate.addingTimeInterval(Double(diff)).description, newDate.addingTimeInterval(Double(localOffset)).description)
        timezone = "UTC+01:00"
        diff = TimeZone(abbreviation: timezone)!.secondsFromGMT() - localOffset
        newDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: utcDate, timezoneAbbreviation: timezone)
        XCTAssertEqual(localDate.addingTimeInterval(Double(diff)).description, newDate.addingTimeInterval(Double(localOffset)).description)
        timezone = "UTC+12:00"
        diff = TimeZone(abbreviation: timezone)!.secondsFromGMT() - localOffset
        newDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: utcDate, timezoneAbbreviation: timezone)
        XCTAssertEqual(localDate.addingTimeInterval(Double(diff)).description, newDate.addingTimeInterval(Double(localOffset)).description)
        timezone = "UTC-12:00"
        diff = TimeZone(abbreviation: timezone)!.secondsFromGMT() - localOffset
        newDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: utcDate, timezoneAbbreviation: timezone)
        XCTAssertEqual(localDate.addingTimeInterval(Double(diff)).description, newDate.addingTimeInterval(Double(localOffset)).description)
    }
    
    func testIsBookingInTimeForValidation() throws {
        
    }
}
