//
//  ListWorkstationDetail.swift
//  BureauTests
//
//  Created by Cristian Misael Almendro Lazarte on 20/6/21.
//

import XCTest
@testable import Bureau

class WorkstationDetailViewModelTest: XCTestCase {
    var viewModel = WorkstationDetailsViewModel()
    var errorViewModel = ErrorViewModel()
    var mockApiService: MockApiService!
    
    let assetWithSchedule: Asset! = Asset(
        id: "A123",
        buildingID: "",
        areaId: "321",
        name: "123",
        code: "123",
        assetDescription: "",
        assetPublic: true,
        stats: Stats(count: 5, average: 5),
        properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "", email: "")),
        policies: Policies(bookingWindow: 5, isRecurrent: true, min: 120, max: 180, schedule: [
            Schedule(id: "", startTime: "2021-11-05T14:00:00Z", endTime: "2021-11-05T16:00:00Z"),
            Schedule(id: "", startTime: "2021-11-05T20:00:00Z", endTime: "2021-11-05T23:00:00Z")]
        ),
        createdAt: "2021-07-26T14:28:08.118Z",
        updatedAt: "2021-09-02T00:47:59.956Z",
        version: 1
    )
    
    let assetWithoutSchedule: Asset! = Asset(
        id: "A123",
        buildingID: "",
        areaId: "321",
        name: "123",
        code: "123",
        assetDescription: "",
        assetPublic: true,
        stats: Stats(count: 5, average: 5),
        properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "", email: "")),
        policies: Policies(bookingWindow: 5, isRecurrent: true, min: 120, max: 180, schedule: [
            Schedule(id: "", startTime: "2021-11-05T03:00:00Z", endTime: "2021-11-06T02:59:00Z")]
        ),
        createdAt: "2021-07-26T14:28:08.118Z",
        updatedAt: "2021-09-02T00:47:59.956Z",
        version: 1
    )
    
    let dates: [String]! = [
        "05:00:00",
        "14:00:00",
        "16:00:00",
        "20:00:00",
        "23:00:00",
        "04:59:59"
    ]
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
        viewModel.selectedAsset = assetWithSchedule
    }
    
    func testLoadWorkstationDetails() throws {
        viewModel.loadWorkstationDetails()
        XCTAssertNotNil(viewModel.bindWorkstationToController)
    }
    
    func testGetUserEmail() throws {
        viewModel.getUserEmail { (result) in
            switch (result) {
            case .success(let userData):
                XCTAssertNotNil(userData)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testCallFuncToCreateABooking() throws {
        mockApiService.apiToCreateABooking(assetId: "1233", buildingId: "1234") { (result) in
            switch (result) {
            case .success(let booking):
                XCTAssertNotNil(booking)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testGetDate() throws {
        XCTAssertNoThrow(viewModel.getDate())
        XCTAssertEqual(viewModel.getDate(), mockApiService.currentDate)
    }
    
    func testGetStartTime() throws {
        XCTAssertNoThrow(viewModel.getStartTime())
        XCTAssertEqual(viewModel.getStartTime(), mockApiService.startHourDate)
    }
    
    func testGetStartHour() throws {
        XCTAssertNoThrow(viewModel.getStartHour())
    }
    
    func testGetEndTime() throws {
        XCTAssertNoThrow(viewModel.getEndTime())
        XCTAssertEqual(viewModel.getEndTime(), mockApiService.endHourDate)
    }
    
    func testCheckBookingWindow() throws {
        XCTAssertNoThrow(viewModel.checkBookingWindow())
        XCTAssertEqual("", viewModel.checkBookingWindow())
    }
    
    func testCheckAssetMaxMinUsageValidHour() throws {
        mockApiService.endHourDate = Calendar.current.date(byAdding: .hour, value: 1, to: mockApiService.endHourDate) ?? Date()
        XCTAssertNoThrow(viewModel.checkAssetMaxMinUsage())
        XCTAssertEqual("", viewModel.checkAssetMaxMinUsage())
    }
        
    func testCheckAssetMaxMinUsageMinutesLessThanMinimumRequired() throws {
        XCTAssertNoThrow(viewModel.checkAssetMaxMinUsage())
        XCTAssertEqual("The minimum booking time for this asset is 02:00 hours.", viewModel.checkAssetMaxMinUsage())
    }
        
    func testCheckAssetMaxMinUsageMinutesMoreThanMaximumRequired() throws {
        mockApiService.endHourDate = Calendar.current.date(byAdding: .hour, value: 3, to: mockApiService.endHourDate) ?? Date()
        XCTAssertNoThrow(viewModel.checkAssetMaxMinUsage())
        XCTAssertEqual("The maximum booking time for this asset is 03:00 hours.", viewModel.checkAssetMaxMinUsage())
    }
    
    func testCheckBookingPermanent() throws {
        XCTAssertNoThrow(viewModel.checkBookingPermanent())
        XCTAssertEqual("", viewModel.checkBookingPermanent())
    }
    
    func testGetSchedules() throws {
        var workstation = viewModel.selectedAsset
        XCTAssertNoThrow(viewModel.getSchedules(workstation))
        workstation = assetWithoutSchedule
        XCTAssertNoThrow(viewModel.getSchedules(workstation))
        XCTAssertTrue(viewModel.scheduleBookings.isEmpty)
        workstation = assetWithSchedule
        XCTAssertNoThrow(viewModel.getSchedules(workstation))
        let resultDates = viewModel.getSchedules(workstation)
        let firstHour = resultDates[1].description.components(separatedBy: " ")[1]
        let secondHour = resultDates[2].description.components(separatedBy: " ")[1]
        let thirdHour = resultDates[3].description.components(separatedBy: " ")[1]
        let fourthHour = resultDates[4].description.components(separatedBy: " ")[1]
        XCTAssertEqual(dates[1], firstHour)
        XCTAssertEqual(dates[2], secondHour)
        XCTAssertEqual(dates[3], thirdHour)
        XCTAssertEqual(dates[4], fourthHour)
    }
    
    func testSetScheduleBookings() throws {
        XCTAssertTrue(viewModel.scheduleBookings.isEmpty)
        XCTAssertNoThrow(viewModel.setScheduleBookings(dates: [], buildingId: ""))
        XCTAssertTrue(viewModel.scheduleBookings.isEmpty)
        XCTAssertNoThrow(viewModel.getSchedules(assetWithSchedule))
        let schedules = viewModel.scheduleBookings
        XCTAssertFalse(schedules.isEmpty)
        XCTAssertEqual(schedules.count, 3)
        XCTAssertEqual(schedules[0].endTime?.split(separator: "T")[1], "14:00:00.000Z")
        XCTAssertEqual(schedules[1].startTime?.split(separator: "T")[1], "16:00:00.000Z")
        XCTAssertEqual(schedules[1].endTime?.split(separator: "T")[1], "20:00:00.000Z")
        XCTAssertEqual(schedules[2].startTime?.split(separator: "T")[1], "23:00:00.000Z")
    }
    
    func testResetRecurrenceRules() throws {
        viewModel.resetRecurrenceRules()
        XCTAssertEqual(mockApiService.isRecurrent, false)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "never")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onMonth, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onWeek, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDate, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, nil)
        XCTAssertNoThrow(viewModel.resetRecurrenceRules())
    }
    
    func testLoadUserWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetUserData()
        XCTAssertNil(self.mockApiService.sessionUser)
    }
}
