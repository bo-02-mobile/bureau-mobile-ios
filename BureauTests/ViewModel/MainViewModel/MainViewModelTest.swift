//
//  MainViewModelTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 3/5/22.
//

import XCTest
@testable import Bureau

class MainViewModelTest: XCTestCase {
    var viewModel = MainViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedServices = mockApiService
    }
    
    func testCallFuncToGetUserProfile() throws {
        let userProfile = UserProfileMock()
        viewModel.callFuncToGetUserProfile()
        XCTAssertEqual(viewModel.userProfile?.id, userProfile.user.id)
        XCTAssertEqual(viewModel.userProfile?.externalID, userProfile.user.externalID)
        XCTAssertEqual(viewModel.userProfile?.name, userProfile.user.name)
        XCTAssertEqual(viewModel.userProfile?.givenName, userProfile.user.givenName)
        XCTAssertEqual(viewModel.userProfile?.familyName, userProfile.user.familyName)
        XCTAssertEqual(viewModel.userProfile?.preferredUsername, userProfile.user.preferredUsername)
        XCTAssertEqual(viewModel.userProfile?.email, userProfile.user.email)
        XCTAssertEqual(viewModel.userProfile?.role, userProfile.user.role)
        XCTAssertEqual(viewModel.userProfile?.picture, userProfile.user.picture)
        XCTAssertEqual(viewModel.userProfile?.emailVerified, false)
        XCTAssertEqual(viewModel.userProfile?.groups, userProfile.user.groups)
        XCTAssertEqual(viewModel.userProfile?.favoriteAssets, userProfile.user.favoriteAssets)
        XCTAssertEqual(viewModel.userProfile?.createdAt, userProfile.user.createdAt)
        XCTAssertEqual(viewModel.userProfile?.updatedAt, userProfile.user.updatedAt)
        XCTAssertEqual(viewModel.userProfile?.version, userProfile.user.version)
    }

    func testCallFuncToGetUserGroups() throws {
        let userProfileGroupId = ["61a7c2d21a7fb92f9aefd8ba"]
        viewModel.callFuncToGetUserGroups(userProfileGroups: userProfileGroupId)
        XCTAssertTrue(UserDefaultsService.shared.getRecurrentBookingsPermission())
        XCTAssertTrue(UserDefaultsService.shared.getBookForOthersPermission())
    }
    
    func testSetStartTimeAndEndTimeWithValidStartTimeAndEndTime() throws {
        let startTime = Calendar.current.date(bySettingHour: 10, minute: 20, second: 0, of: Date())!
        let endTime = Calendar.current.date(bySettingHour: 11, minute: 50, second: 0, of: Date())!
        viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime)
        let filterStartTime = mockApiService.startHourDate
        let filterEndTime = mockApiService.endHourDate
        XCTAssertEqual(filterStartTime, startTime)
        XCTAssertEqual(filterEndTime, endTime)
        XCTAssertNoThrow(viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime))
    }
    
    func testSetStartTimeAndEndTimeWhenStartTimeAndEndTimeAreEquals() throws {
        let startTime = Calendar.current.date(bySettingHour: 15, minute: 30, second: 0, of: Date())!
        let endTime = Calendar.current.date(bySettingHour: 15, minute: 30, second: 0, of: Date())!
        let expectedEndTime = Calendar.current.date(byAdding: .minute, value: 1, to: startTime)!
        viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime)
        let filterStartTime = mockApiService.startHourDate
        let filterEndTime = mockApiService.endHourDate
        XCTAssertEqual(filterStartTime, startTime)
        XCTAssertEqual(filterEndTime, expectedEndTime)
        XCTAssertNoThrow(viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime))
    }
    
    func testSetStartTimeAndEndTimeWhenStartTimeIsGreaterThanEndTime() throws {
        let startTime = Calendar.current.date(bySettingHour: 16, minute: 00, second: 0, of: Date())!
        let endTime = Calendar.current.date(bySettingHour: 14, minute: 00, second: 0, of: Date())!
        viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime)
        let filterStartTime = mockApiService.startHourDate
        let filterEndTime = mockApiService.endHourDate
        XCTAssertEqual(filterStartTime, endTime)
        XCTAssertEqual(filterEndTime, startTime)
        XCTAssertNoThrow(viewModel.setStartTimeAndEndTime(startHourTime: startTime, endHourTime: endTime))
    }
}
