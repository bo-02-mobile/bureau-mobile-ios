//
//  BuildingViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class BuildingViewModelTest: XCTestCase {
    var viewModel: BuildingViewModel!
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel = .init(apiService: mockApiService)
    }

    func testLoadBuildingsProperly() throws {
        viewModel.bindBuildingViewModelToController = {
            XCTAssertEqual(self.viewModel.dataBuilding.count, 1)
        }
        viewModel.callFuncToGetBuildingData()
    }
    
    func testLoadAreasNumberOfBuildingProperly() throws {
        viewModel.bindAreasNumberViewModelToController = {
            XCTAssertEqual(self.viewModel.dataBuilding[0].areasNumber, 2)
        }
        self.viewModel.callFuncToGetBuildingData()
        self.viewModel.callFuncToGetAreasNumberOfBuilding(buildingId: self.viewModel.dataBuilding[0].id)
    }
    
    func testLoadBuildingsWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetBuildingData()
        XCTAssertNil(viewModel.dataBuilding)
    }
    
    func testSetBuilding() {
        let building = BuildingMock().building
        viewModel.setBuilding(building: building)
        XCTAssertEqual(mockApiService.selectedBuilding.id, building.id)
    }
}
