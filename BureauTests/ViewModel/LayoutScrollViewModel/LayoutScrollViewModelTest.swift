//
//  LayoutScrollViewModelTest.swift
//  BureauTests
//
//  Created by Julio Gabriel Tobares on 11/01/2022.
//

import Foundation
@testable import Bureau
import XCTest

class LayoutScrollViewModelTest: XCTestCase {
    var viewModel = LayoutScrollViewModel()
    var apiShared: MockApiService!
    
    override func setUpWithError() throws {
        apiShared = MockApiService()
        viewModel.apiSharedService = apiShared
    }
    
    func testShowOnlyAvailablePlaces() throws {
        XCTAssertEqual(viewModel.getShowOnlyAvailableSpaces(), apiShared.showOnlyAvailableSpaces)
    }
}
