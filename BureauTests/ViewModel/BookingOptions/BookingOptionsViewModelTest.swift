//
//  BookingOptionsViewModel.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 4/3/22.
//

import XCTest
@testable import Bureau

class BookingOptionsViewModelTest: XCTestCase {
    var viewModel = BookingOptionsViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
        viewModel.mainViewModel.apiSharedServices = mockApiService
    }
    
    func testGetStartTime() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/09/20 17:30")!
        mockApiService.startHourDate = date
        let startTime = viewModel.getStartTime()
        let hour = Calendar.current.component(.hour, from: mockApiService.startHourDate)
        let minute = Calendar.current.component(.minute, from: mockApiService.startHourDate)

        XCTAssertEqual(startTime.hour, hour)
        XCTAssertEqual(startTime.minute, minute)
        XCTAssertNoThrow(viewModel.getStartTime())
    }
    
    func testGetEndTime() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/10/20 18:50")!
        
        mockApiService.endHourDate = date
        let endTime = viewModel.getEndTime()
        let hour = Calendar.current.component(.hour, from: mockApiService.endHourDate)
        let minute = Calendar.current.component(.minute, from: mockApiService.endHourDate)

        XCTAssertEqual(endTime.hour, hour)
        XCTAssertEqual(endTime.minute, minute)
        XCTAssertNoThrow(viewModel.getEndTime())
    }

    func testGetDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/11/20 14:40")!
        mockApiService.currentDate = date
        
        XCTAssertEqual(viewModel.getDate(), date)
        XCTAssertNoThrow(viewModel.getDate())
    }
    
    func testSetDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/11/20 14:40")!
        viewModel.setDate(date: date)
        
        XCTAssertEqual(mockApiService.currentDate, date)
        XCTAssertNoThrow(viewModel.setDate(date: date))
    }
    
    func testGetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/12/20 10:10")!
        mockApiService.tempCurrentDate = date
        
        XCTAssertEqual(viewModel.getTempDate(), date)
        XCTAssertNoThrow(viewModel.getTempDate())
    }
    
    func testSetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2010/12/20 10:10")!
        mockApiService.currentDate = date
        viewModel.setTempDate()
        XCTAssertEqual(mockApiService.tempCurrentDate, date)
        XCTAssertNoThrow(viewModel.setTempDate())
    }
    
    func testGetTempRecurrenceType() throws {
        let recurrenceType = "daily"
        mockApiService.tempRecurrenceRule.type = recurrenceType
        XCTAssertEqual(viewModel.getTempRecurrencetype(), recurrenceType)
        XCTAssertNoThrow(viewModel.getTempRecurrencetype())
    }
    
    func testSetRecurrenceRuleWhenNoRecurrenceRuleHasBeenSelected() throws {
        let recurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: false, type: "never", endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
        mockApiService.tempRecurrenceRule = recurrenceRule
        viewModel.setRecurrenceRule()
        XCTAssertEqual(mockApiService.isRecurrent, false)
        XCTAssertEqual(mockApiService.recurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(mockApiService.recurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(mockApiService.recurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertNoThrow(viewModel.setRecurrenceRule())
    }
    
    func testSetRecurrenceRuleWhenRecurrenceRuleHasBeenSelected() throws {
        let recurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: true, type: "yearly", endDate: Date(), onDays: [], onMonth: "5", onWeek: "2", onDate: "1", every: "0")
        mockApiService.tempRecurrenceRule = recurrenceRule
        viewModel.setRecurrenceRule()
        XCTAssertEqual(mockApiService.isRecurrent, true)
        XCTAssertEqual(mockApiService.recurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(mockApiService.recurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(mockApiService.recurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertNoThrow(viewModel.setRecurrenceRule())
    }
    
    func testSetRecurrenceWhenDismiss() {
        let recurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: true, type: "yearly", endDate: Date(), onDays: [], onMonth: "5", onWeek: "2", onDate: "1", every: "0")
        mockApiService.recurrenceRule = recurrenceRule
        viewModel.setRecurrenceWhenDismiss()
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertNoThrow(viewModel.setRecurrenceWhenDismiss())
    }
    
    func testGetUserName() {
        let user = mockApiService.user
        mockApiService.sessionUser = user
        XCTAssertEqual(viewModel.getUserName(), user.name)
        XCTAssertNoThrow(viewModel.getUserName())
    }
    
    func testGetSelectedUser() {
        let user = mockApiService.user
        mockApiService.selectedUser = user
        let localUser = viewModel.getSelectedUser()
        XCTAssertEqual(localUser.name, user.name)
        XCTAssertNoThrow(viewModel.getSelectedUser())
    }
    
    func testSetStartTimeAndEndTimeWorksProperly() throws {
        let startTime = Calendar.current.date(bySettingHour: 10, minute: 30, second: 0, of: Date())!
        let endTime = Calendar.current.date(bySettingHour: 13, minute: 00, second: 0, of: Date())!
        viewModel.setStartTimeAndEndTime(startHour: startTime, endHour: endTime)
        let filterStartTime = mockApiService.startHourDate
        let filterEndTime = mockApiService.endHourDate
        XCTAssertEqual(filterStartTime, startTime)
        XCTAssertEqual(filterEndTime, endTime)
        XCTAssertNoThrow(viewModel.setStartTimeAndEndTime(startHour: startTime, endHour: endTime))
    }
}
