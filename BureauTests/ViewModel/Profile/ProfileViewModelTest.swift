//
//  ProfileViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class ProfileViewModelTest: XCTestCase {
    
    var viewModel =  ProfileViewModel()
    var apiService: MockAuthService!
    
    override func setUpWithError() throws {
        apiService = MockAuthService()
        viewModel.apiSharedService = apiService
    }
    
    func testLoadUserDataWorksProperly() throws {
        viewModel.bindUserToViewController = { (user) in
            XCTAssertNotNil(user)
        }
        viewModel.callFuncToGetUserData()
    }
    
    func testCallFuncToLogOut() throws {
        viewModel.callFuncToLogOut()
        apiService.setAuthState(nil) { result in
            switch (result) {
            case .success(let wasLogoutSuccessful):
                XCTAssertNotNil(wasLogoutSuccessful)
                XCTAssertNotNil(self.viewModel.logOutSuccessfully)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}
