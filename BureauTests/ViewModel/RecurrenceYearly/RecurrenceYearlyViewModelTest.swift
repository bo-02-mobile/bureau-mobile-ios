//
//  RecurrenceYearlyViewModelTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 13/3/22.
//

import XCTest
@testable import Bureau

class RecurrenceYearlyViewModelTest: XCTestCase {
    var viewModel = RecurrenceYearlyViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }

    func testGetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/02/11 18:00")!
        
        mockApiService.tempCurrentDate = date
        XCTAssertEqual(viewModel.getTempDate(), date)
        XCTAssertNoThrow(viewModel.getTempDate())
    }
    
    func testGetEndDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        XCTAssertEqual(viewModel.getEndDate(), date)
        XCTAssertNoThrow(viewModel.getEndDate())
    }
    
    func testGetTempRecurrence() throws {
        let recurrenceRule = RecurrenceRule(isCustom: true, type: "yearly", endDate: Date(), onDays: ["Thursday"], onMonth: "9", onWeek: "1", onDate: "20", every: "0")
        mockApiService.tempRecurrenceRule = recurrenceRule
        let tempRecurrenceRule = viewModel.getTempRecurrenceRule()
        XCTAssertEqual(tempRecurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(tempRecurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(tempRecurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertEqual(tempRecurrenceRule.onMonth, recurrenceRule.onMonth)
        XCTAssertEqual(tempRecurrenceRule.onWeek, recurrenceRule.onWeek)
        XCTAssertEqual(tempRecurrenceRule.onDate, recurrenceRule.onDate)
        XCTAssertEqual(tempRecurrenceRule.every, recurrenceRule.every)
        XCTAssertNoThrow(viewModel.getTempRecurrenceRule())
    }
    
    func testSetRecurrenceRulesWhenOnDaySelected() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.setRecurrenceRules(onDate: "12", onWeek: "3", onDays: ["Tuesday"], onMonth: "7", onSelected: true)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "yearly")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, "0")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.endDate, date)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDate, "12")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onMonth, "7")
    }
    
    func testSetRecurrenceRulesWhenOnTheSelected() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.setRecurrenceRules(onDate: "12", onWeek: "3", onDays: ["Tuesday"], onMonth: "7", onSelected: false)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "yearly")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, "0")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.endDate, date)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onWeek, "3")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDays, ["Tuesday"])
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onMonth, "7")
    }
}
