//
//  RecurrenceDailyViewModelTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 13/3/22.
//

import XCTest
@testable import Bureau

class RecurrenceDailyViewModelTest: XCTestCase {
    var viewModel = RecurrenceDailyViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }

    func testGetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2020/02/10 13:00")!
        
        mockApiService.tempCurrentDate = date
        XCTAssertEqual(viewModel.getTempDate(), date)
        XCTAssertNoThrow(viewModel.getTempDate())
    }
    
    func testGetTempRecurrence() throws {
        let recurrenceRule = RecurrenceRule(isCustom: true, type: "yearly", endDate: Date(), onDays: ["Wednesday"], onMonth: "5", onWeek: "2", onDate: "1", every: "0")
        mockApiService.tempRecurrenceRule = recurrenceRule
        let tempRecurrenceRule = viewModel.getTempRecurrenceRule()
        XCTAssertEqual(tempRecurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(tempRecurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(tempRecurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertEqual(tempRecurrenceRule.onMonth, recurrenceRule.onMonth)
        XCTAssertEqual(tempRecurrenceRule.onWeek, recurrenceRule.onWeek)
        XCTAssertEqual(tempRecurrenceRule.onDate, recurrenceRule.onDate)
        XCTAssertEqual(tempRecurrenceRule.every, recurrenceRule.every)
        XCTAssertNoThrow(viewModel.getTempRecurrenceRule())
    }
    
    func testGetEndDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        XCTAssertEqual(viewModel.getEndDate(), date)
        XCTAssertNoThrow(viewModel.getEndDate())
    }
    
    func testSetRecurrenceRules() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.setRecurrenceRules(every: "10")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, "10")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "daily")
    }
}
