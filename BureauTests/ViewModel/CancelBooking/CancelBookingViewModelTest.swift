//
//  CancelBookingViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class CancelBookingViewModelTest: XCTestCase {
    var viewModel = CancelBookingViewModel()
    var mockApiService: MockApiService!
    let workstationCode = "123"
    let bookingId = "456"

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService
        viewModel.initViewModel(workstationCode: workstationCode, bookingID: bookingId)
    }

    func testCancelBookingViewModelIsInitializeProperly() throws {
        XCTAssertEqual(viewModel.workstationCode, workstationCode)
        XCTAssertEqual(viewModel.bookingID, bookingId)
    }
    
    func testCancelBookingWorksProperly() throws {
        var functionCalled = 0
        viewModel.bindCancelBookingResponseToController = {
            functionCalled += 1
        }
        viewModel.callFuncToDeleteBooking()
        XCTAssertGreaterThan(functionCalled, 0)
    }
    
    func testCancelBookingWithError() throws {
        mockApiService.successResponses = false
        var functionCalled = 0
        viewModel.bindErrorViewModelToController = { _,_ in
            functionCalled += 1
        }
        viewModel.callFuncToDeleteBooking()
        XCTAssertGreaterThan(functionCalled, 0)
    }
}
