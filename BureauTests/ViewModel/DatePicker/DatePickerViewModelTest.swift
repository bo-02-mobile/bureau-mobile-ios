//
//  DatePickerViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class DatePickerViewModelTest: XCTestCase {
    var viewModel = DatePickerViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService
    }

    func testGetDateWorksProperly() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/20 13:00")!
        
        mockApiService.currentDate = date
        XCTAssertEqual(viewModel.getDate(), date)
    }
    
    func testGetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/30 18:00")!
        
        mockApiService.tempCurrentDate = date
        XCTAssertEqual(viewModel.getTempDate(), date)
        XCTAssertNoThrow(viewModel.getTempDate())
    }
    
    func testGetEndDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/20 15:00")!
        
        mockApiService.tempRecurrenceRule.endDate = date
        XCTAssertEqual(viewModel.getEndDate(), date)
        XCTAssertNoThrow(viewModel.getEndDate())
    }
    
    func testSetDateWhenDateSelected() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/20 13:00")!
        
        viewModel.setDate(date: date, pickerSetType: Constants.DatePickerSetType.date)
        XCTAssertEqual(mockApiService.currentDate, date)
        XCTAssertEqual(mockApiService.tempCurrentDate, date)
    }
    
    func testSetDateWhenTempDateSelected() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/20 13:00")!
        
        viewModel.setDate(date: date, pickerSetType: Constants.DatePickerSetType.tempDate)
        XCTAssertEqual(mockApiService.tempCurrentDate, date)
    }
    
    func testSetDateWhenEndDateSelected() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "1992/06/20 13:00")!
        
        viewModel.setDate(date: date, pickerSetType: Constants.DatePickerSetType.endDate)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.endDate, date)
    }
}
