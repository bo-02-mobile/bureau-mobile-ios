//
//  QrReaderViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 6/19/21.
//
import Foundation
import XCTest
@testable import Bureau

class QrReaderViewModelTest: XCTestCase {
    var viewModel = QrReaderViewModel()
    var apiService: MockApiService!
    let id = "60788742d66b184ee89fa7a0"
    let assetId = "12390183123oad"

    override func setUpWithError() throws {
        apiService = MockApiService()
        viewModel.apiSharedService = apiService
    }

    func testBookingConfirmationIsSuccesfully() throws {
        viewModel.bindConfirmateBookingToController = { [weak self] (confirmationResult) in
            XCTAssertEqual(confirmationResult.id, self?.id)
            XCTAssertEqual(confirmationResult.assetId, self?.assetId)
        }
        viewModel.callFuncToConfirmateABooking(bookingId: "assetId")
    }
    
    func testAutoBookingValidationIsSuccesfully() throws {
        viewModel.bindQRBookingValidateToController = { [weak self] (confirmationResult) in
            XCTAssertEqual(confirmationResult.id, self?.id)
            XCTAssertEqual(confirmationResult.assetId, self?.assetId)
        }
        viewModel.callFuncToValidateBookingAutomatically(bookingId: "assetId")
    }
    
    func testSetStartAndEndTime() {
        let calendar = Calendar.current
        let normalizedDate = Helpers.normalizeDate(currentDate: Date())
        let normalizedHour = calendar.component(.hour, from: normalizedDate)
        
        viewModel.setStartAndEndTime(startHour: Date())
        
        let startHour = calendar.component(.hour, from: apiService.startHourDate)
        let endHour = calendar.component(.hour, from: apiService.endHourDate)
        XCTAssertEqual(startHour, normalizedHour)
        XCTAssertEqual(endHour, (normalizedHour + 1) % 24)
    }
    
    func testGetTimeValidatedBookingWhenBookingIsNotInValidationTimeRange() throws {
        let bookingArray = [Booking(
            id: "A123",
            areaName: "Demo Area",
            startTime: "2022-01-25T02:00:00Z",
            endTime: "2022-01-25T04:00:00Z",
            isRecurrent: false,
            isPermanent: false,
            isConfirmed: false,
            buildingId: "60b556b5374d31f8e5489750",
            areaId: "60b556da37d31f8e5489751",
            assetId: "60c008878c15bbc5d0705e17",
            owner: Owner(id: "id4568", preferredUsername: "Pedro", email: "pedro@gmail.com"),
            assetCode: "BCBC",
            assetType: "Workstation",
            createdAt: "2022-0126T21:31:40.392Z",
            updatedAt: "2022-01-26T21:40:00Z",
            confirmedAt: nil,
            reservationMessage: "New Booking",
            status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: nil
        )]
        viewModel.bookingsList = bookingArray
        let assetId = "60c008878c15bbc5d0705e17"
        XCTAssertNoThrow(viewModel.getTimeValidatedBooking(assetId: assetId))
        XCTAssertEqual(nil, viewModel.getTimeValidatedBooking(assetId: assetId)?.status)
        XCTAssertEqual(nil, viewModel.getTimeValidatedBooking(assetId: assetId)?.booking.assetId)
    }
    
    func testCallFuncToGetBookingsData() {
        viewModel.callFuncToGetBookingsData(email: "user@gmail.com")
        XCTAssertNoThrow(viewModel.callFuncToGetBookingsData(email: "user@gmail.com"))
        XCTAssertEqual(viewModel.bookingsList?.count, 1)
    }
}
