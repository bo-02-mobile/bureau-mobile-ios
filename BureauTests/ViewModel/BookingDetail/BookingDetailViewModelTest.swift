//
//  BookingDetailViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class BookingDetailViewModelTest: XCTestCase {
    var viewModel = BookingDetailViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService
    }

    func testLoadBookings() throws {
        viewModel.bindBookingDetailViewModelToController = {
            XCTAssertEqual(self.viewModel.dataBooking.count, 1)
        }
        viewModel.callFuncToGetAssetBookings(assetId: "123", startTime: "10:00", endTime: "12:00")
    }
    
    func testLoadAssetDetail() throws {
        viewModel.bindBookingDetailViewModelToController = {
            XCTAssertEqual(self.viewModel.dataBooking.count, 1)
        }
        viewModel.callFuncToGetAssetWithID(assetId: "123")
    }
    
    func testLoadBookingsWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetAssetBookings(assetId: "123", startTime: "10:00", endTime: "12:00")
        XCTAssertNil(viewModel.asset)
    }
    
    func testLoadAssetDetailWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetAssetWithID(assetId: "123")
        XCTAssertNil(viewModel.asset)
    }
}
