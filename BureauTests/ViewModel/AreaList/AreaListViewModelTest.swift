//
//  AreaListViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class AreaListViewModelTest: XCTestCase {
    var viewModel = AreaListViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService
        viewModel.userServiceShared = mockApiService
    }
    
    func testSetArea() throws {
        viewModel.callFuncToGetLayoutData()
        let indexTest = 0
        viewModel.setArea(index: indexTest)
        XCTAssertNotNil(viewModel.apiServiceShared.selectedArea)
        XCTAssertEqual(viewModel.apiServiceShared.selectedArea.id, viewModel.dataArea[indexTest].area?.id)
    }
    
    func testGetAvailableSpaceForArea() throws {
        viewModel.apiServiceShared.startHourDate = Calendar.current.date(bySettingHour: 12, minute: 30, second: 0, of: Date()) ?? Date()
        viewModel.apiServiceShared.endHourDate = Calendar.current.date(bySettingHour: 13, minute: 30, second: 0, of: Date()) ?? Date()
        viewModel.callFuncToGetLayoutData()
        let indexTest = 0
        let count = viewModel.getAvaliableSpaceFromArea(index: indexTest)
        XCTAssertEqual(count, 1)
    }
    
    func testGetArea() {
        viewModel.apiServiceShared.selectedArea = AreasMock(buildingID: "123").area
        XCTAssertNotNil(viewModel.getArea())
        XCTAssertEqual(viewModel.getArea().id, "123")
        XCTAssertEqual(viewModel.getArea().code, "3DA")
    }
    
    func testLoadLayoutData() throws {
        viewModel.bindAreaViewModelToController = {
            XCTAssertEqual(self.viewModel.dataLayout.count, 3)
        }
        viewModel.callFuncToGetLayoutData()
    }
    
    func testCallFuncToGetLayoutDataWithError() {
        mockApiService.successResponses = false
        viewModel.callFuncToGetLayoutData()
        XCTAssertNil(self.viewModel.dataLayout)
    }
    
    func testCallFuncToGetAreaDataWithError() {
        mockApiService.successResponses = false
        viewModel.callFuncToGetAreaData()
        XCTAssertNil(self.viewModel.dataArea)
    }
    
    func testLoadUsersProperly() throws {
        viewModel.callFuncToGetUserData()
        XCTAssertNotNil(self.mockApiService.sessionUser)
    }
    
    func testLoadUserWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetUserData()
        XCTAssertNil(self.mockApiService.sessionUser)
    }
    
    func testCallFuncToGetOverlappedBookings() throws {
        viewModel.callFuncToGetOverlappedBookings()
        
        XCTAssertEqual(viewModel.overlappedBookings.count, 3)
    }
}
