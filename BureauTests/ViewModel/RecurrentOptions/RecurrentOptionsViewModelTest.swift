//
//  RecurrentOptionsViewModelTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 13/3/22.
//

import XCTest
@testable import Bureau

class RecurrenceOptionsViewModelTest: XCTestCase {
    var viewModel = RecurrenceOptionsViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }
    
    func testGetTempRecurrenceRule() throws {
        let recurrenceRule = RecurrenceRule(isCustom: true, type: "weekly", endDate: Date(), onDays: ["Tuesday"], onMonth: "11", onWeek: "1", onDate: "17", every: "2")
        mockApiService.tempRecurrenceRule = recurrenceRule
        let tempRecurrenceRule = viewModel.getTempRecurrenceRule()
        XCTAssertEqual(tempRecurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(tempRecurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(tempRecurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertEqual(tempRecurrenceRule.onMonth, recurrenceRule.onMonth)
        XCTAssertEqual(tempRecurrenceRule.onWeek, recurrenceRule.onWeek)
        XCTAssertEqual(tempRecurrenceRule.onDate, recurrenceRule.onDate)
        XCTAssertEqual(tempRecurrenceRule.every, recurrenceRule.every)
        XCTAssertNoThrow(viewModel.getTempRecurrenceRule())
    }
    
    func testSetRecurrenceRules() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2021/04/22 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.setRecurrenceRules()
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "never")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onMonth, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onWeek, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDate, nil)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, nil)
        XCTAssertNoThrow(viewModel.setRecurrenceRules())
    }
    
    func testUpdateRecurrentEndDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2021/04/22 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.updateRecurrenceEndDate()
        XCTAssertGreaterThan(viewModel.getTempRecurrenceRule().endDate, viewModel.apiSharedService.tempCurrentDate)
    }
}
