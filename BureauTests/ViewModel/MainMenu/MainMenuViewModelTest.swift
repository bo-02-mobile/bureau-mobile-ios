//
//  MainMenuViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class MainMenuViewModelTest: XCTestCase {
    var viewModel = MainMenuViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }

    func testGetDate() throws {
        XCTAssertNoThrow(viewModel.getDate())
        XCTAssertEqual(viewModel.getDate(), mockApiService.currentDate)
    }
    
    func testGetStartTime() throws {
        XCTAssertNoThrow(viewModel.getStartTime())
        XCTAssertEqual(viewModel.getStartTime(), mockApiService.startHourDate)
    }
    
    func testGetEndTime() throws {
        XCTAssertNoThrow(viewModel.getEndTime())
        XCTAssertEqual(viewModel.getEndTime(), mockApiService.endHourDate)
    }
    
    func testGetEnabledFilters() throws {
        XCTAssertEqual(viewModel.getEnabledFiltersNumber(), 0)
    }
}
