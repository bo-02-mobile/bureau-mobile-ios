//
//  TimePickerViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class TimePickerViewModelTest: XCTestCase {
    var viewModel = TimePickerViewModel()
    var apiService: MockApiService!
    
    override func setUpWithError() throws {
        apiService = MockApiService()
        viewModel.apiSharedService = apiService
        viewModel.mainViewModel.apiSharedServices = apiService
    }

    func testGetStartTimeWorksProperly() throws {
        let timeHour = viewModel.getStartTime()
        let helperHour = Helpers.getHourAndMinuteFromDate(date: apiService.startHourDate)
        XCTAssertEqual(timeHour.hour, helperHour.hour)
        XCTAssertEqual(timeHour.minute, helperHour.minute)
        XCTAssertNoThrow(viewModel.getStartTime())
    }
    
    func testGetEndTimeWorksProperly() throws {
        let timeHour = viewModel.getEndTime()
        let helperHour = Helpers.getHourAndMinuteFromDate(date: apiService.endHourDate)
        XCTAssertEqual(timeHour.hour, helperHour.hour)
        XCTAssertEqual(timeHour.minute, helperHour.minute)
        XCTAssertNoThrow(viewModel.getEndTime())
    }
    
    func testSetStartTimeAndEndTimeWorksProperly() throws {
        let startTime = Calendar.current.date(bySettingHour: 16, minute: 10, second: 0, of: Date())!
        let endTime = Calendar.current.date(bySettingHour: 18, minute: 20, second: 0, of: Date())!
        viewModel.setStartTimeAndEndTime(startHour: startTime, endHour: endTime)
        let filterStartTime = apiService.startHourDate
        let filterEndTime = apiService.endHourDate
        XCTAssertEqual(filterStartTime, startTime)
        XCTAssertEqual(filterEndTime, endTime)
        XCTAssertNoThrow(viewModel.setStartTimeAndEndTime(startHour: startTime, endHour: endTime))
    }
}
