//
//  FiltersViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 8/30/21.
//

import XCTest

@testable import Bureau
class FiltersViewModelTest: XCTestCase {
    var viewModel = FiltersViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService

        viewModel.callFuncToGetAssetTypes()
        viewModel.callFuncToGetWorkstationFeatures()
    }

    func testLoadAssetTypesCorrectly() {
        XCTAssertEqual(viewModel.assetTypes.count, 7)
    }
    
    func testLoadWorkstationFeaturesCorrectly() {
        XCTAssertEqual(viewModel.workstationFeatures.count, 5)
    }
    
    func testGetShowOnlyAvailableSpacesValue() {
        XCTAssertEqual(viewModel.showOnlyAvailableSpaces, false)
    }
    
    func testGetFiltersEnableNumber() {
        XCTAssertEqual(viewModel.getEnabledFiltersNumber(), 0)
    }
    func testUpdateFiltersEnableNumber() {
        let assetTypesMock = AssetTypesMock()
        viewModel.setSelectedAssetType(assetType: assetTypesMock.assetTypes[1])
        XCTAssertEqual(viewModel.getEnabledFiltersNumber(), 1)
    }
    
    func testShowOnlyAvailableSpacesWhenisOn() {
        let isOn = true
        viewModel.setShowOnlyAvailableSpacesValue(isOn: isOn)
        viewModel.applyFilters()
        let result = viewModel.getShowOnlyAvailableSpacesValue()
        XCTAssertTrue(result)
    }
    
    func testGetAssetTypesLenght() {
        let result = viewModel.getAssetTypesLenght()
        XCTAssertEqual(result, 7)
    }
    func testGetAssetTypesLenghtByIndex() {
        let result = viewModel.getAssetTypeByIndex(index: 1)
        XCTAssertEqual(result.id, "1")
    }
    
    func testApplyfilters() {
        viewModel.applyFilters()
        XCTAssertNotNil(mockApiService.selectedAssetType)
        XCTAssertEqual(mockApiService.enabledFilters, 0)
    }
    
    func testGetWorkstationFeaturesLenght() {
        let result = viewModel.getWorkstationFeaturesLenght()
        XCTAssertEqual(result, 5)
    }
    
    func testGetWorkstationFeaturesLenghtByIndex() {
        let result = viewModel.getWorkstationFeatureByIndex(index: 0)
        XCTAssertEqual(result.id, "1")
    }
    
    func testSelectWorkstation() {
        let workstationFeature = WorkstationFeaturesMock().workstationFeatures[0]
        viewModel.setWorkstationFeature(workstationFeature: workstationFeature)
        XCTAssertEqual(viewModel.getEnabledFiltersNumber(), 1)
    }
    
    func testClearFilters() {
        let assetTypesMock = AssetTypesMock()
        let workstationFeature = WorkstationFeaturesMock().workstationFeatures[0]
        viewModel.setWorkstationFeature(workstationFeature: workstationFeature)
        viewModel.setSelectedAssetType(assetType: assetTypesMock.assetTypes[1])
        viewModel.applyFilters()
        XCTAssertEqual(mockApiService.enabledFilters, 2)
        viewModel.clearFilters()
        XCTAssertEqual(mockApiService.selectedAssetType.name, "All")
        XCTAssertFalse(mockApiService.showOnlyAvailableSpaces)
        XCTAssertEqual(mockApiService.selectedWorkstationFeatures.count, 0)
        XCTAssertEqual(mockApiService.enabledFilters, 0)
    }
    
    func testGetFeaturesByAssetType() {
        let features = WorkstationFeaturesMock().features
        mockApiService.selectedAssetType = AssetTypesMock().assetTypes[2]
        viewModel.callFuncToGetAssetTypes()
        let labels = viewModel.getFeaturesByAssetType(features: features)
        XCTAssertEqual(labels[0].id, "3")
        XCTAssertEqual(labels[1].id, "4")
    }

}
