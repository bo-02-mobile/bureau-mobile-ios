//
//  OverlayViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 6/21/21.
//

import XCTest
@testable import Bureau

class OverlayViewModelTest: XCTestCase {
    var viewModel = OverlayViewModel()
    var apiService: MockApiService!
    
    let id = "60c0d1cc8f0d73a745f4d46e"
    let areaName = "3A"
    let startTime = "2021-06-19T12:00:00.022Z"
    let endTime = "2021-06-19T13:00:00.022Z"
    let isRecurrent = false
    let recurrenceRule = "none"
    let isPermanent = false
    let isConfirmed = true
    let bookingDate = "Jun 19, 2021 from 12:00 PM to 13:00 PM"
    let buildingId = "0812308123"
    let areaId = "1203123"
    let assetId = "12390183123oad"
    let owner: Owner? = nil
    let assetCode = "1239A"
    let assetType = "Workstation"
    let createdAt = "2021-04-16T12:00:00.484Z"
    let updatedAt = "2021-04-16T12:00:00.484Z"
    let confirmedAt = "2021-04-16T12:00:00.484Z"
    let reservationMessage = "New Booking"
    let recurrenceId = "3fbce922-f7eb-4ad8-bbe9-1bf9f55a36c9"
    let status = Status(state: "Pending", cancellationDate: nil, reason: nil)

    override func setUpWithError() throws {
        apiService = MockApiService()
        viewModel.apiSharedService = apiService
        
        let bookingConfirmation = Booking(id: id,
                                          areaName: areaName,
                                          startTime: startTime,
                                          endTime: endTime,
                                          isRecurrent: isRecurrent,
                                          isPermanent: isPermanent,
                                          isConfirmed: isConfirmed,
                                          buildingId: buildingId,
                                          areaId: areaId,
                                          assetId: assetId,
                                          owner: owner,
                                          assetCode: assetCode,
                                          assetType: assetType,
                                          createdAt: createdAt,
                                          updatedAt: updatedAt,
                                          confirmedAt: confirmedAt,
                                          reservationMessage: reservationMessage,
                                          status: status, recurrenceId: recurrenceId)
        
        viewModel.setBookingConfirmation(bookingConfirmation: bookingConfirmation)
    }

    func testViewModelIsInitializeCorrectly() throws {
        XCTAssertNotNil(viewModel.bookingConfirmation)
    }

    func testBookingDateConstruction() throws {
        viewModel.bindBookingDateViewModelToController = { [weak self] (bookingDate) in
            XCTAssertEqual(self?.bookingDate, bookingDate)
        }
    }

    func testConstructBookingDateLabel() throws {
        viewModel.constructBookingDateLabel()
        XCTAssertEqual(viewModel.bookingDate, reservationMessage.uppercasingFirst)
    }
    
    func testConstructWorkstationCodeLabel() throws {
        viewModel.constructWorkstationCodeLabel()
        XCTAssertNotNil(viewModel.workstationCode)
    }
}
