//
//  FindUserViewModelTest.swift
//  BureauTests
//
//  Created by Julio Gabriel Tobares on 24/03/2022.
//

import Foundation
import XCTest
@testable import Bureau

class FindUserViewModelTest: XCTestCase {
    
    var viewModel: FindUserViewModel!
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel = .init(apiService: mockApiService)
    }
    
    func testLoadUsersProperly() throws {
        viewModel.bindFindUserViewModelToController = {
            XCTAssertEqual(self.viewModel.dataUser.count, 2)
        }
        viewModel.callFuncToGetUserData()
        XCTAssertNotNil(self.viewModel.dataUser)
    }
    
    func testLoadUsersWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetUserData()
        XCTAssertNil(viewModel.dataUser)
    }
    
    func testSetUser() {
        let localUser = mockApiService.user
        viewModel.setUser(user: localUser)
        XCTAssertEqual(mockApiService.selectedUser.name, localUser.name)
    }
}
