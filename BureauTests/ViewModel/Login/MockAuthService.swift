//
//  MockAuthService.swift
//  BureauTests
//
//  Created by Victor Arana on 6/7/21.
//

import Foundation
import AppAuth
@testable import Bureau

class MockAuthService: AuthServiceProtocol {
    func signOut(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void) {
        completionHandler(.success(true))
    }
    
    func getIslogoutPendingFlag() -> Bool {
        return true
    }
    
    func setAuthState(_ authState: OIDAuthState?, _ completionHandler: @escaping (Result<Bool, CustomError>) -> Void) {
        completionHandler(.success(true))
    }
    func saveState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void) {
        completionHandler(.success(true))
    }
    func loadState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void) {
        completionHandler(.success(true))
    }
    func getOIDAuthorizationRequest(completion: @escaping (Result<OIDAuthorizationRequest, CustomError>) -> Void) {
        guard let endpoint = URL(string: Constants.JalaLogin.issuer) else {
            return
        }
        let configuration = OIDServiceConfiguration(authorizationEndpoint: endpoint, tokenEndpoint: endpoint)
        let request = OIDAuthorizationRequest(configuration: configuration,
                                              clientId: Constants.JalaLogin.clientId,
                                              clientSecret: Constants.JalaLogin.clientId,
                                              scopes: [],
                                              redirectURL: endpoint,
                                              responseType: OIDResponseTypeCode,
                                              additionalParameters: nil)
        completion(.success(request))
    }
    func getAuthState() -> OIDAuthState? {
        return nil
    }
    func getUserInformationEndpoint() -> URL? {
        guard let endpoint = URL(string: Constants.JalaLogin.issuer) else {
            return nil
        }
        return endpoint
    }
}
