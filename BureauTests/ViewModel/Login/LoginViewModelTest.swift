//
//  LoginViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 6/6/21.
//

import XCTest
import AppAuth
@testable import Bureau

class LoginViewModelTest: XCTestCase {
    var viewModel = LoginViewModel()
    var errorViewModel = ErrorViewModel()
    var apiService: MockAuthService!
    var authState: OIDAuthState?
    
    override func setUpWithError() throws {
        apiService = MockAuthService()
        viewModel.apiSharedService = apiService
    }
    
    func testLoginIsSuccesfully() {
        viewModel.bindLoginViewModelToController = { (wasLoginSuccessful) in
            XCTAssertEqual(wasLoginSuccessful, true)
        }
        viewModel.callFuncToSetAuthState(authState: authState)
    }
    
    func testCallFuncToGetAuthorizationRequest() throws {
        viewModel.callFuncToGetAuthorizationRequest()
        apiService.getOIDAuthorizationRequest { (result) in
            switch (result) {
            case .success(let authorizationRequest):
                XCTAssertNotNil(authorizationRequest)
            case .failure(let error):
                XCTAssertThrowsError(self.errorViewModel.displayErrorInView(error: .internalError))
                XCTAssertEqual(error, CustomError.internalError)
            }
        }
    }
    
    func testCallFuncToSetAuthState() throws {
        viewModel.callFuncToSetAuthState(authState: authState)
        apiService.setAuthState(authState) { (result) in
            switch (result) {
            case .success(let wasLoginSuccessful):
                XCTAssertNotNil(wasLoginSuccessful)
                if !wasLoginSuccessful {
                    XCTAssertThrowsError(self.errorViewModel.displayErrorInView(error: .internalError))
                }
            case .failure(let error):
                XCTAssertThrowsError(self.errorViewModel.displayErrorInView(error: .internalError))
                XCTAssertEqual(error, CustomError.internalError)
            }
        }
    }
    
    func testCallFuncToGetUserData() throws {
        viewModel.callFuncToGetUserData()
        XCTAssertNotNil(viewModel.bindLoginViewModelToController)
    }

    func testTryLogOut() {
        guard let endpoint = URL(string: Constants.JalaLogin.issuer) else {
            return
        }
        let configuration = OIDServiceConfiguration(authorizationEndpoint: endpoint, tokenEndpoint: endpoint)
        let request = OIDAuthorizationRequest(configuration: configuration,
                                              clientId: Constants.JalaLogin.clientId,
                                              clientSecret: Constants.JalaLogin.clientId,
                                              scopes: [],
                                              redirectURL: endpoint,
                                              responseType: OIDResponseTypeCode,
                                              additionalParameters: nil)
        viewModel.bindAuthRequestViewModelToController = { (authRequest) in
            XCTAssertEqual(authRequest, request)
        }
        viewModel.tryLogout(thenLogin: false)
    }
}
