//
//  FavoriteViewModelTest.swift
//  BureauTests
//
//  Created by Christian Torrico Avila on 31/3/22.
//

import XCTest
@testable import Bureau

class FavoriteViewModelTest: XCTestCase {
    
    var viewModel: FavoriteViewModel = FavoriteViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }

    func testbindUserProfileInfoToViewController() throws {
        viewModel.bindUserProfileInfoToViewController = { userInfo in
            XCTAssertNotNil(userInfo)
            XCTAssertEqual(userInfo.id, "60fec6831f9lf1fa8a07057c")
            XCTAssertEqual(userInfo.externalID, "b6ab57c9-d986-1FA4-9dbe-01df51d3c4cd")
        }
        viewModel.getUserProfileInfo()
        XCTAssertNotNil(viewModel.userProfileInfo)
        XCTAssertEqual(viewModel.userProfileInfo.favoriteAssets?.count ?? 0, 1)
    }
    
    func testbindAssetsToViewController() throws {
        viewModel.bindAssetsToViewController = { assets in
            XCTAssertNotNil(assets)
            XCTAssertEqual(assets[0].id, "A231")
            XCTAssertEqual(assets.count, 3)
        }
        viewModel.getAllUserFavorites(favoriteAssets: ["60fec6831f9lf1fa8a07057c"])
        XCTAssertNotNil(viewModel.assets)
        XCTAssertEqual(viewModel.assets.count, 3)
    }
    
    func testDeleteFavorites() throws {
        viewModel.deleteFavoriteAsset(assetId: "60fec6831f9lf1fa8a07057c")
        XCTAssertFalse(viewModel.userProfileInfo.favoriteAssets?.contains { $0 == "60fec6831f9lf1fa8a07057c" } ?? true)
    }
    
    func testInsideBookingWindow() throws {
        let asset = AssetsMock().assetWithPolicies
        let fourDaysFromNow = Calendar.current.date(byAdding: .day, value: 4, to: Date())!
        mockApiService.currentDate = fourDaysFromNow
        
        let check = viewModel.checkBookingWindow(workstation: asset)
        XCTAssertTrue(check)
    }
    
    func testOutSideBookingWindow() throws {
        let asset = AssetsMock().assetWithPolicies
        let fiveDaysFromNow = Calendar.current.date(byAdding: .day, value: 10, to: Date())!
        mockApiService.currentDate = fiveDaysFromNow
        
        let check = viewModel.checkBookingWindow(workstation: asset)
        XCTAssertFalse(check)
    }
    
    func testCorrectMaxMinUsage() throws {
        let asset = AssetsMock().assetWithPolicies
        let start = Calendar.current.date(bySettingHour: 9, minute: 30, second: 0, of: Date())!
        let end = Calendar.current.date(bySettingHour: 15, minute: 00, second: 0, of: Date())!
        mockApiService.startHourDate = start
        mockApiService.endHourDate = end
        
        let check = viewModel.checkAssetMaxMinUsage(workstation: asset)
        XCTAssertTrue(check)
    }
    
    func testIncorrectMaxMinUsage() throws {
        let asset = AssetsMock().assetWithPolicies
        let start = Calendar.current.date(bySettingHour: 9, minute: 30, second: 0, of: Date())!
        let end = Calendar.current.date(bySettingHour: 22, minute: 00, second: 0, of: Date())!
        mockApiService.startHourDate = start
        mockApiService.endHourDate = end
        
        let check = viewModel.checkAssetMaxMinUsage(workstation: asset)
        XCTAssertFalse(check)
    }
    
    func testCheckBookingPermanent() {
        let asset = AssetsMock().assetWithPolicies
        let check = viewModel.checkBookingPermanent(workstation: asset)
        XCTAssertFalse(check)
    }
    
    func testcheckWorkstationInsideSchedules() throws {
        let asset = AssetsMock().assetWithPolicies
        let timeZone = TimeZone(abbreviation: "UTC")
        let start = Date().setTime(hour: 13, min: 30, sec: 0, timeZone: timeZone)!
        let end = Date().setTime(hour: 17, min: 0, sec: 0, timeZone: timeZone)!
        mockApiService.startHourDate = start
        mockApiService.endHourDate = end
        
        let check = viewModel.checkWorkstationSchedules(workstation: asset)
        XCTAssertTrue(check)
    }
    
    func testcheckWorkstationOutsideSchedules() throws {
        let asset = AssetsMock().assetWithPolicies
        let timeZone = TimeZone(abbreviation: "UTC")
        let start = Date().setTime(hour: 13, min: 30, sec: 0, timeZone: timeZone)!
        let end = Date().setTime(hour: 2, min: 0, sec: 0, timeZone: timeZone)!
        mockApiService.startHourDate = start
        mockApiService.endHourDate = end
        
        let check = viewModel.checkWorkstationSchedules(workstation: asset)
        XCTAssertFalse(check)
    }
}
