//
//  CalendarDayViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class CalendarDayViewModelTest: XCTestCase {
    var viewModel = CalendarDayViewModel()
    var mockApiService: MockApiService!

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiServiceShared = mockApiService
    }

    func testGetDate() throws {
        XCTAssertNoThrow(viewModel.getDate())
    }
    
    func testGetStartTime() throws {
        XCTAssertNoThrow(viewModel.getStartTime())
    }
    
    func testGetEndTime() throws {
        XCTAssertNoThrow(viewModel.getEndTime())
    }
    
    func testGetStartHour() throws {
        XCTAssertNoThrow(viewModel.getStartHour())
    }
    
    func testGetUserName() throws {
        viewModel.getUserName { (result) in
            switch (result) {
            case .success(let userData):
                XCTAssertNotNil(userData)
            case .failure(let error):
                XCTAssertEqual(error, CustomError.internalError)
            }
        }
    }
    
    func testGetUserEmail() throws {
        viewModel.getUserEmail { (result) in
            switch (result) {
            case .success(let userData):
                XCTAssertNotNil(userData)
            case .failure(let error):
                XCTAssertEqual(error, CustomError.internalError)
            }
        }
    }
    
}
