//
//  MyBookingsViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 7/2/21.
//

import XCTest
@testable import Bureau

class MyBookingsViewModelTest: XCTestCase {
    var viewModel = MyBookingsViewModel()
    var apiService: MockApiService!

    override func setUpWithError() throws {
        apiService = MockApiService()
        viewModel.apiSharedService = apiService
    }

    func testBookingConfirmationIsSuccesfully() throws {
        viewModel.bindBookingsViewModelToController = { [weak self] () in
            XCTAssertEqual(self?.viewModel.dataBookings.count, 3)
        }
    }
    
    func testCallFuncToGetAllBookings() {
        let buildingMock = BuildingMock()
        apiService.buildingData = [buildingMock.building4]
        viewModel.callFuncToGetAllBookings(email: "user@gmail.com")
        XCTAssertNoThrow(viewModel.callFuncToGetAllBookings(email: "user@gmail.com"))
        XCTAssertEqual(viewModel.todayBookingsList.count, 1)
        XCTAssertEqual(viewModel.upcomingBookingsList.count, 2)
        XCTAssertEqual(viewModel.permanentBookingsList.count, 1)
    }
    
    func testCallFuncToGetPermanentAssetsInfo() {
        let bookingsMock = BookingsMock()
        let area = AreasMock(buildingID: "").area
        viewModel.permanentBookingsList = [bookingsMock.booking]
        viewModel.callFuncToGetPermanentAssetsInfo()
        XCTAssertNoThrow(viewModel.callFuncToGetPermanentAssetsInfo())
        XCTAssertEqual(viewModel.permanentBookingsList[0].areaName, area.name)
    }
    
    func testCheckBookingBuildingExists() {
        let buildingMock = BuildingMock()
        let bookingMock = BookingsMock()
        apiService.buildingData = [buildingMock.building4]
        let bookings = viewModel.checkBookingBuildingExists(bookings: bookingMock.bookingData)
        XCTAssertNoThrow(viewModel.checkBookingBuildingExists(bookings: bookingMock.bookingData))
        XCTAssertEqual(bookings.count, 3)
    }
    
    func testCheckBookingBuildingExistWhenBuildingsAreaUnavailable() {
        let buildingMock = BuildingMock()
        let bookingMock = BookingsMock()
        apiService.buildingData = []
        let bookings = viewModel.checkBookingBuildingExists(bookings: bookingMock.bookingData)
        XCTAssertNoThrow(viewModel.checkBookingBuildingExists(bookings: bookingMock.bookingData))
        XCTAssertEqual(bookings.count, 0)
    }
    
    func testSetAssetsAsPermanentBookings() {
        let assetsMock = AssetsMock()
        let assetItem = assetsMock.permanentAssetsList[0]
        let permanentBookings = viewModel.setAssetsAsPermanentBookings(assets: assetsMock.permanentAssetsList)
        XCTAssertEqual(permanentBookings.count, 1)
        XCTAssertEqual(permanentBookings[0].isPermanent, true)
        XCTAssertEqual(permanentBookings[0].buildingId, assetItem.buildingID)
        XCTAssertEqual(permanentBookings[0].areaId, assetItem.areaId)
        XCTAssertEqual(permanentBookings[0].assetId, assetItem.id)
        XCTAssertEqual(permanentBookings[0].owner?.id, assetItem.properties?.owner?.id)
        XCTAssertEqual(permanentBookings[0].owner?.preferredUsername, assetItem.properties?.owner?.preferredUsername)
        XCTAssertEqual(permanentBookings[0].owner?.email, assetItem.properties?.owner?.email)
    }
}
