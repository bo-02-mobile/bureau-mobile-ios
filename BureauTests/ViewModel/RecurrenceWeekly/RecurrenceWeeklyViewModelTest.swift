//
//  RecurrenceWeeklyViewModelTest.swift
//  BureauTests
//
//  Created by Juan Pablo Lozada Chambilla on 13/3/22.
//

import XCTest
@testable import Bureau

class RecurrenceWeeklyViewModelTest: XCTestCase {
    var viewModel = RecurrenceWeeklyViewModel()
    var mockApiService: MockApiService!
    
    override func setUpWithError() throws {
        mockApiService = MockApiService()
        viewModel.apiSharedService = mockApiService
    }

    func testGetTempDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/07/20 11:00")!
        
        mockApiService.tempCurrentDate = date
        XCTAssertEqual(viewModel.getTempDate(), date)
        XCTAssertNoThrow(viewModel.getTempDate())
    }
    
    func testGetEndDate() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        XCTAssertEqual(viewModel.getEndDate(), date)
        XCTAssertNoThrow(viewModel.getEndDate())
    }
    
    func testGetTempRecurrence() throws {
        let recurrenceRule = RecurrenceRule(isCustom: true, type: "weekly", endDate: Date(), onDays: ["Monday"], onMonth: "10", onWeek: "3", onDate: "18", every: "1")
        mockApiService.tempRecurrenceRule = recurrenceRule
        let tempRecurrenceRule = viewModel.getTempRecurrenceRule()
        XCTAssertEqual(tempRecurrenceRule.type, recurrenceRule.type)
        XCTAssertEqual(tempRecurrenceRule.isCustom, recurrenceRule.isCustom)
        XCTAssertEqual(tempRecurrenceRule.onDays, recurrenceRule.onDays)
        XCTAssertEqual(tempRecurrenceRule.onMonth, recurrenceRule.onMonth)
        XCTAssertEqual(tempRecurrenceRule.onWeek, recurrenceRule.onWeek)
        XCTAssertEqual(tempRecurrenceRule.onDate, recurrenceRule.onDate)
        XCTAssertEqual(tempRecurrenceRule.every, recurrenceRule.every)
        XCTAssertNoThrow(viewModel.getTempRecurrenceRule())
    }
    
    func testSetRecurrenceRules() throws {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let date = formatter.date(from: "2019/11/14 10:10")!
        mockApiService.tempRecurrenceRule.endDate = date
        viewModel.setRecurrenceRules(every: "15", onDays: ["Monday", "Tuesday", "Friday"])
        XCTAssertEqual(mockApiService.tempRecurrenceRule.type, "weekly")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.every, "15")
        XCTAssertEqual(mockApiService.tempRecurrenceRule.endDate, date)
        XCTAssertEqual(mockApiService.tempRecurrenceRule.onDays, ["Monday", "Tuesday", "Friday"])
    }
}
