//
//  AreaLayoutViewModelTest.swift
//  BureauTests
//
//  Created by Victor Arana on 9/6/21.
//

import XCTest
@testable import Bureau

class AreaLayoutViewModelTest: XCTestCase {
    var viewModel = AreaLayoutViewModel()
    var mockApiService: MockApiService!
    
    let assetWithSchedule: Asset! = Asset(
        id: "A123",
        buildingID: "",
        areaId: "321",
        name: "123",
        code: "123",
        assetDescription: "",
        assetPublic: true,
        stats: Stats(count: 5, average: 5),
        properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "", email: "")),
        policies: Policies(bookingWindow: 1, isRecurrent: true, min: 120, max: 180, schedule: [
            Schedule(id: "", startTime: "2021-11-05T06:00:00Z", endTime: "2021-11-05T18:00:00Z")]
        ),
        createdAt: "2021-07-26T14:28:08.118Z",
        updatedAt: "2021-09-02T00:47:59.956Z",
        version: 1
    )

    override func setUpWithError() throws {
        mockApiService = MockApiService()
        mockApiService.selectedBuilding = Building(id: "123", code: "code", name: "building name", address: "urb las palmas", timezone: "UTC-2:00", published: true)
        viewModel.apiServiceShared = mockApiService
    }
    
    func testLoadAllWorkstationAreaAndLayout() throws {
        viewModel.bindLayoutViewModelToController = { (areaLayout) in
            XCTAssertNotNil(areaLayout)
            XCTAssertEqual(areaLayout.id, "123")
            XCTAssertEqual(areaLayout.code, "3DA")
        }
        viewModel.callFuncToGetAllWorkstationForArea()
        XCTAssertEqual(self.viewModel.dataWorkstation.count, 3)
        XCTAssertEqual(self.viewModel.dataBookings.count, 3)
        XCTAssertNotNil(self.viewModel.layout)
    }
    
    func testLoadFilteredWorkstationAreaAndLayout() throws {
        viewModel.bindLayoutViewModelToController = { (areaLayout) in
            XCTAssertNotNil(areaLayout)
            XCTAssertEqual(areaLayout.id, "123")
            XCTAssertEqual(areaLayout.code, "3DA")
        }
        viewModel.callFuncToGetFilteredWorkstationData()
        XCTAssertEqual(self.viewModel.dataBookings.count, 3)
        XCTAssertNotNil(self.viewModel.layout)
    }

    func testLoadAssetsSuccess() throws {
        viewModel.callFuncToGetAllWorkstationForArea()
        XCTAssertEqual(self.viewModel.dataWorkstation.count, 3)
    }
    
    func testLoadFilteredAssetsWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetAllWorkstationForArea()
        XCTAssertNil(self.viewModel.dataWorkstation)
    }
    
    func testLoadBookingsWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetAreaBookings()
        XCTAssertNil(self.viewModel.dataBookings)
    }
    
    func testLoadAreaLayoutWithError() throws {
        mockApiService.successResponses = false
        viewModel.callFuncToGetLayoutData()
        XCTAssertNil(self.viewModel.dataBookings)
    }
    
    func testGetSelectedArea() throws {
        viewModel.apiServiceShared.selectedArea = AreasMock(buildingID: "123").area
        XCTAssertNotNil(viewModel.getArea())
        XCTAssertEqual(viewModel.getArea().id, "123")
        XCTAssertEqual(viewModel.getArea().code, "3DA")
    }
    
    func testGetSelectedAreaCodeWhenNotNull() {
        viewModel.apiServiceShared.selectedArea = AreasMock(buildingID: "123").area
        let areaCode = viewModel.getSelectedAreaCode()
        XCTAssertNotEqual(areaCode, "")
        XCTAssertEqual(areaCode, "3DA")
    }
    
    func testGetSelectedAreaWhenCodeNull() {
        let areaCode = viewModel.getSelectedAreaCode()
        XCTAssertEqual(areaCode, "")
        XCTAssertNotEqual(areaCode, "3DA")
    }
    
    func testCheckWorkstationBookingWindows() {
        var markers = AreasMock(buildingID: "").area.markers
        markers![0].isAvailable = true
        let date = viewModel.apiServiceShared.currentDate
        XCTAssertNoThrow(viewModel.checkWorkstationBookingWindows(dataMarkers: &markers!, [assetWithSchedule]))
        XCTAssertTrue(markers![0].isAvailable)
        viewModel.apiServiceShared.currentDate = viewModel.apiServiceShared.currentDate.addingTimeInterval(Double(2*Constants.DateTimeFormat.oneDayInSeconds))
        XCTAssertNoThrow(viewModel.checkWorkstationBookingWindows(dataMarkers: &markers!, [assetWithSchedule]))
        XCTAssertFalse(markers![0].isAvailable)
        viewModel.apiServiceShared.currentDate = date
    }
    
    func testCheckWorkstationSchedules() {
        var markers = AreasMock(buildingID: "").area.markers
        markers?[0].isAvailable = true
        viewModel.apiServiceShared.startHourDate = Date().setTime(hour: 10, min: 0, sec: 0)!
        viewModel.apiServiceShared.endHourDate = Date().setTime(hour: 10, min: 30, sec: 0)!
        viewModel.checkWorkstationSchedules(dataMarkers: &markers!, [assetWithSchedule])
        XCTAssertNoThrow(viewModel.checkWorkstationSchedules(dataMarkers: &markers!, [assetWithSchedule]))
        XCTAssertTrue(markers?[0].isAvailable ?? false)
    }
    
    func testGetEnabledFiltersNumber() {
        let expected = viewModel.apiServiceShared.enabledFilters
        let actual = viewModel.getEnabledFiltersNumber()
        XCTAssertEqual(expected, actual)
    }
}
