//
//  MockAPIService.swift
//  BureauTests
//
//  Created by user on 6/3/21.
//

import Foundation
import AppAuth
@testable import Bureau
final class MockApiService: APIServiceProtocol, UserServiceProtocol {

    var currentDate = Date()
    var startHourDate = Date()
    var endHourDate = Date()
    var selectedBuilding: Building!
    var buildingData: [Building]! = []
    var successResponses: Bool = true
    var selectedArea: Area!
    var tempCurrentDate = Date() {
        didSet {
             self.setRecurrentEndDate()
        }
   }
    
    var recurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: false, type: "never", endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
    var tempRecurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: false, type: "never", endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
    var isRecurrent: Bool = false
    var buildingId: String!
    var selectedAssetType: AssetType!
    var selectedWorkstationFeatures: [WorkstationFeature] = []
    var showOnlyAvailableSpaces: Bool = false
    var enabledFilters = 0
    let date = "2021-04-16T12:00:00.484Z"
    var selectedUser: User!
    var sessionUser: User!
    var user: User = User(name: "John Test", email: "john@test.com", picture: "Pic")
    
    var selectedWorkstation: Asset! = Asset(
        id: "A123",
        buildingID: "",
        areaId: "321",
        name: "123",
        code: "123",
        assetDescription: "",
        assetPublic: true,
        stats: Stats(count: 5, average: 5),
        properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "", email: "")),
        policies: Policies(bookingWindow: 5, isRecurrent: true, min: 120, max: 180, schedule: nil),
        createdAt: "2021-07-26T14:28:08.118Z",
        updatedAt: "2021-09-02T00:47:59.956Z",
        version: 1
    )
    
    init() {
         startHourDate = Helpers.normalizeDate(currentDate: Date())
         if let safeEndHour = Calendar.current.date(byAdding: .hour, value: 1, to: startHourDate) {
              endHourDate = safeEndHour
         }
    }
    
    func setRecurrentEndDate() {
         let diff = Calendar.current.dateComponents([.day], from: self.tempRecurrenceRule.endDate, to: tempCurrentDate)
         if diff.day ?? -1 >= 0 {
              self.tempRecurrenceRule.endDate = Calendar.current.date(byAdding: .month, value: 1, to: tempCurrentDate) ?? Date()
         }
    }
    
    func apiToGetAssetTypes(completion: @escaping (Result<[TypeData], CustomError>) -> Void) {
         let assetTypesMock = AssetTypesMock()
         completion(.success(assetTypesMock.types))
    }
    
    func apiToGetWorkstationFeatures(completion: @escaping (Result<[Feature], CustomError>) -> Void) {
         let worksationFeaturesMock = WorkstationFeaturesMock()
         completion(.success(worksationFeaturesMock.features))
    }
    
    func apiToGetUserBookings(email: String, bookingsTime: BookingsTime, completion: @escaping (Result<[Booking], CustomError>) -> Void) {
        let bookingsMock = BookingsMock()
        if (bookingsTime == .today) {
            completion(.success([bookingsMock.booking]))
        } else if (bookingsTime == .upcoming) {
            completion(.success([bookingsMock.booking2, bookingsMock.booking3]))
        }
    }
    
    func apiToDeleteBooking(bookingID: String, completion: @escaping (Result<Bool, CustomError>) -> Void) {
        if successResponses {
            completion(.success(true))
        } else {
            completion(.failure(.internalError))
        }
    }
    
    func apiToCreateABooking(assetId: String, buildingId: String, completion: @escaping (Result<Booking, CustomError>) -> Void) {
        let booking = Booking(id: "60788742d66b184ee89fa7a0",
                              areaName: "",
                              startTime: date,
                              endTime: date,
                              isRecurrent: false,
                              isPermanent: false,
                              isConfirmed: true,
                              buildingId: "0812308123",
                              areaId: "1203123",
                              assetId: "12390183123oad",
                              owner: nil,
                              assetCode: "1239A",
                              assetType: "Workstation",
                              createdAt: date,
                              updatedAt: date,
                              confirmedAt: date,
                              reservationMessage: "New Booking",
                              status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: nil)

        completion(.success(booking))
    }
    
    func apiToGetUsers(completion: @escaping (Result<[User], CustomError>) -> Void) {
        if successResponses {
            let users: [User] = [User(name: "Juan Perez",
                                          email: "Juan@Perez.com",
                                          picture: "Pic"),
                                     User(name: "Antonio Garcia",
                                          email: "Tony@Test.com",
                                          picture: "Pic")]
            completion(.success(users))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetAreaData(completion: @escaping (Result<[Area], CustomError>) -> Void) {
        if successResponses {
            let buildingID = "123"
            let areasMock: AreasMock = AreasMock.init(buildingID: buildingID)
            completion(.success(areasMock.areaData))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetAreasNumberOfBuilding(buildingId: String?, completion: @escaping (Result<Int, CustomError>) -> Void) {
        if successResponses {
            completion(.success(2))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }

    func apiToGetBuildingData(completion: @escaping (Result<[Building], CustomError>) -> Void) {
        if successResponses {
            let buildingMock: BuildingMock = BuildingMock.init()
            completion(.success(buildingMock.buildingData))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }

    func apiToGetUserData(completion: @escaping (Result<User, CustomError>) -> Void) {
        if successResponses {
            let user: User = User(name: "Victor Andres Arana Aireyu", email: "3av.arana@gmail.com", picture: "user.picture.path")
            completion(.success(user))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }

    func apiToGetWorkstationDataFromAnArea(completion: @escaping (Result<[Asset], CustomError>) -> Void) {
        if (successResponses) {
            let featuresMock = AssetsMock()
            completion(.success(featuresMock.assetList))
        } else {
            completion(.failure(CustomError.internalError))
        }        
    }
    
    func apiToGetAllWorkstationDataFromAnArea(completion: @escaping (Result<[Asset], CustomError>) -> Void) {
        if (successResponses) {
            let featuresMock = AssetsMock()
            completion(.success(featuresMock.assetList))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetWorkstationDataFromABuilding(idList: String, completion: @escaping (Result<[Asset], CustomError>) -> Void) {
        if successResponses {
            let featuresMock = AssetsMock()
            completion(.success(featuresMock.assetList))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }

    func apiToGetLayoutDataFromAnArea(completion: @escaping (Result<Area, CustomError>) -> Void) {
        if (successResponses) {
            let areasMock: AreasMock = AreasMock.init(buildingID: "123")
            completion(.success(areasMock.area))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }

    func apiToConfirmABooking(bookingId: String, completion: @escaping (Result<Booking, CustomError>) -> Void) {
        let bookingConfirmation = Booking(id: "60788742d66b184ee89fa7a0",
                                          areaName: "",
                                          startTime: date,
                                          endTime: date,
                                          isRecurrent: false,
                                          isPermanent: false,
                                          isConfirmed: true,
                                          buildingId: "0812308123",
                                          areaId: "1203123",
                                          assetId: "12390183123oad",
                                          owner: nil,
                                          assetCode: "1239A",
                                          assetType: "Workstation",
                                          createdAt: date,
                                          updatedAt: date,
                                          confirmedAt: date,
                                          reservationMessage: "New Booking",
                                          status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: nil)

        completion(.success(bookingConfirmation))
    }

    func apiToGetWorkstationBookings(workstationID: String?, completion: @escaping (Result<[Booking], CustomError>) -> Void) {
        let bookingsMock = BookingsMock()
        completion(.success(bookingsMock.bookingData))
    }

    func apiToCreatetABooking(workstationID: String, startTime: String, endTime: String, isRecurrent: String, recurrenceRule: String, completion: @escaping (Result<Booking, CustomError>) -> Void) {
        let bookingCreated = Booking(id: "60788742d66b184ee89fa7a0",
                                     areaName: "",
                                     startTime: date,
                                     endTime: date,
                                     isRecurrent: false,
                                     isPermanent: false,
                                     isConfirmed: true,
                                     buildingId: "0812308123",
                                     areaId: "1203123",
                                     assetId: "12390183123oad",
                                     owner: nil,
                                     assetCode: "1239A",
                                     assetType: "Workstation",
                                     createdAt: date,
                                     updatedAt: date,
                                     confirmedAt: date,
                                     reservationMessage: "New Booking",
                                     status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: nil)

        completion(.success(bookingCreated))
    }
    
    func apiToGetAssetBookings(assetId: String, startTime: String, endTime: String, completion: @escaping (Result<[Booking], CustomError>) -> Void) {
        if successResponses {
            let bookingCreated = Booking(id: "60788742d66b184ee89fa7a0",
                                         areaName: "",
                                         startTime: date,
                                         endTime: date,
                                         isRecurrent: false,
                                         isPermanent: false,
                                         isConfirmed: true,
                                         buildingId: "0812308123",
                                         areaId: "1203123",
                                         assetId: "12390183123oad",
                                         owner: nil,
                                         assetCode: "1239A",
                                         assetType: "Workstation",
                                         createdAt: date,
                                         updatedAt: date,
                                         confirmedAt: date,
                                         reservationMessage: "New Booking",
                                         status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: nil)
            completion(.success([bookingCreated]))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetAssetWithID(assetId: String, completion: @escaping (Result<Asset, CustomError>) -> Void) {
        if successResponses {
            let assetCreated = Asset(id: "60b14364b74284b5f27fface",
                                      buildingID: "60a807c5dc0ba843b1ac4063",
                                      areaId: "60b13fe3b74284b5f27ffacb",
                                      name: "1C001",
                                      code: "1C001",
                                      assetDescription: "",
                                      assetPublic: true,
                                      stats: nil,
                                      properties: nil,
                                      policies: nil,
                                      createdAt: "2021-07-26T14:28:10.294Z",
                                      updatedAt: "2021-07-26T14:28:10.294Z",
                                     version: 1)
            completion(.success( assetCreated))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetAreaBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void) {
        if (successResponses) {
            let bookingsMock = BookingsMock()
            completion(.success(bookingsMock.bookingData))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetUserProfileInfo(completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
        if (successResponses) {
            let userInfo = UserProfileMock()
            completion(.success(userInfo.user))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetUserFavorites(assets: [String], completion: @escaping (Result<[Asset], CustomError>) -> Void) {
        if (successResponses) {
            let featuresMock = AssetsMock()
            completion(.success(featuresMock.assetList))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToDeletefavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
        if (successResponses) {
            let userInfo = UserProfileMock()
            completion(.success(userInfo.user))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToCreateFavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
        if (successResponses) {
            let userInfo = UserProfileMock()
            completion(.success(userInfo.user))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetUserGroups(groupIdArray: [String], completion: @escaping (Result<[Group], CustomError>) -> Void) {
        if successResponses {
            let group: Group = Group(
                id: "61a7c2d21a7fb92f9aefd8ba",
                name: "Security",
                quota: ["Balcony": 3],
                bookForOthers: true,
                recurrenceBookings: true,
                createdAt: "2021-10-23T20:25:30.688Z",
                updatedAt: "2022-04-27T21:59:16.574Z",
                version: 1
            )
            completion(.success([group]))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetOverlappedBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void) {
        if (successResponses) {
            let bookingsMock = BookingsMock()
            completion(.success(bookingsMock.bookingData))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetUserPermanentAssets(email: String, completion: @escaping (Result<[Asset], CustomError>) -> Void) {
        if (successResponses) {
            let featuresMock = AssetsMock()
            completion(.success(featuresMock.permanentAssetsList))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
    
    func apiToGetAreaInfo(areaId: String, completion: @escaping (Result<Area, CustomError>) -> Void) {
        if successResponses {
            let areasMock: AreasMock = AreasMock.init(buildingID: "")
            completion(.success(areasMock.area))
        } else {
            completion(.failure(CustomError.internalError))
        }
    }
}
