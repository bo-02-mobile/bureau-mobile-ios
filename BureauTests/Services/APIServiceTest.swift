//
//  APIServiceTest.swift
//  BureauTests
//
//  Created by User on 1/12/22.
//

import XCTest
@testable import Bureau

class APIServiceTest: XCTestCase {
    
    let service = ApiService.shared

    func testInitStartHour() throws {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: service.startHourDate)
        let minute = calendar.component(.minute, from: service.startHourDate)
        let second = calendar.component(.second, from: service.startHourDate)
        XCTAssertEqual(hour, 8)
        XCTAssertEqual(minute, 30)
        XCTAssertEqual(second, 0)
        
    }
    
    func testInitEndHour() throws {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: service.endHourDate)
        let minute = calendar.component(.minute, from: service.endHourDate)
        let second = calendar.component(.second, from: service.endHourDate)
        XCTAssertEqual(hour, 18)
        XCTAssertEqual(minute, 30)
        XCTAssertEqual(second, 0)
    }

}
