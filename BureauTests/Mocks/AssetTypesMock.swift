//
//  AssetTypesMock.swift
//  Bureau
//
//  Created by Victor Arana on 8/26/21.
//

import Foundation

class AssetTypesMock {
    
    // MARK: - Variables
    var types: [TypeData] = []
    var assetTypes: [AssetType]
    
    // MARK: - Constants
    let assetTypeOne = TypeData(id: "1", name: "Test", text: "Test", value: "Test", labels: ["1", "2"])
    let assetTypeTwo = TypeData(id: "2", name: "Workstation",text: "All", value: "All", labels: ["3", "4"])
    let assetTypeThree = TypeData(id: "3", name: "Conference Room", text: "All", value: "All", labels: ["1", "3"])
    let assetTypeFour = TypeData(id: "4", name: "Office", text: "All", value: "All", labels: ["2", "4"])
    let assetTypeFive = TypeData(id: "5", name: "Leisure", text: "All", value: "All")
    let assetTypeSix = TypeData(id: "6", name: "Parking",  text: "All", value: "All")
    
    // MARK: - Constructors
    init() {
        types.append(assetTypeOne)
        types.append(assetTypeTwo)
        types.append(assetTypeThree)
        types.append(assetTypeFour)
        types.append(assetTypeFive)
        types.append(assetTypeSix)
        
        assetTypes = [AssetType(id: "", name: "All", text: "All", value: "All", image: "allAsset", isSelected: false, index: 0)]
        var index = 1
        for item in types {
            assetTypes.append(AssetType(id: item.id, name: item.name, text: item.text, value: item.value, image: item.value, isSelected: false, index: index, labels: item.labels))
            index += 1
        }
    }
}
