//
//  UserProfileMock.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 31/3/22.
//

import Foundation

class UserProfileMock {
    
    var user = UserProfile(id: "60fec6831f9lf1fa8a07057c",
                           externalID: "b6ab57c9-d986-1FA4-9dbe-01df51d3c4cd",
                           name: "Alvaro Perez Salvatierra",
                           givenName: "Alvaro Perez", familyName: "Salvatierra", preferredUsername: "Alvaro Perez Salvatierra", email: "roperoendemoniado1@gmail.com", role: "administrator",
                           picture: "https://lh3.googleusercontent.com/a/AATXAJxCSLKOFfReAMkwk2oy05qNWm4jmBsQmasw16iZ=s96-c",
                           emailVerified: false, groups: ["61746fbaec33bf74b4d89a87"],
                           favoriteAssets: ["61746fbaec33bf74b4d89a87"] ,
                           createdAt: "2021-05-27T14:44:58.296Z", updatedAt: "2022-03-30T14:42:12.131Z", version: "1.2")
    
}
