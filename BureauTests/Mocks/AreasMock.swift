//
//  AreaMock.swift
//  Bureau
//
//  Created by user on 6/8/21.
//

import Foundation

class AreasMock {
    // MARK: - Variables
    var areaData: [Area] = []
    
    // MARK: - Constants
    var area = Area(code: "3DA", typeDescription: "Meetings Room description1", id: "123",
                    name: "Meetings Room1", published: true, thumbnail: Thumbnail(id: "1", url: "url"),
                    background: Background(id: "2", url: "url"), markers: [])
    let area1 = Area(code: "3DA", typeDescription: "Meetings Room description2", id: "231",
                     name: "Meetings Room2", published: true, thumbnail: Thumbnail(id: "1", url: "url"),
                     background: Background(id: "2", url: "url"), markers: [])
    let area2 = Area(code: "3DA", typeDescription: "Meetings Room description3", id: "312",
                     name: "Meetings Room3", published: true, thumbnail: Thumbnail(id: "1", url: "url"),
                     background: Background(id: "2", url: "url"), markers: [])
    let area3 = Area(code: "3DA", typeDescription: "Meetings Room description4", id: "321",
                     name: "Meetings Room4", published: true, thumbnail: Thumbnail(id: "1", url: "url"),
                     background: Background(id: "2", url: "url"), markers: [])
    
    let marker1 = Marker(workstationId: "A123", isPublic: true, xPos: 0, yPos: 0)
    let marker2 = Marker(workstationId: "A231", isPublic: false, xPos: 0, yPos: 0)
    let marker3 = Marker(workstationId: "A113", isPublic: true, xPos: 0, yPos: 0)
    let marker4 = Marker(workstationId: "A333", isPublic: true, xPos: 0, yPos: 0)
    // MARK: - Constructors
    init(buildingID:String) {
        if buildingID == "123" {
            areaData.append(area)
            areaData.append(area1)
            areaData.append(area2)
            areaData.append(area3)
        }
        self.area.markers = [marker1, marker2, marker3, marker4]
    }
}
