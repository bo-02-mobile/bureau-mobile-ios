//
//  BookingsMock.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 17/6/21.
//

import Foundation

let workstationId = "607884ffd66b184ee89fa79e"
let date = "2021-04-16T12:00:00.484Z"
let ownerEmail = "cristian1@gmail.com"

class BookingsMock {

    // MARK: - Variables
    var bookingData: [Booking] = []

    // MARK: - Constants
    let booking = Booking(id: "1", areaName: "3A", startTime: date, endTime: date,
                          isRecurrent: false, isPermanent: false, isConfirmed: false, buildingId: "628d71b6e060f1c961d70a64",
                          areaId: "01", assetId: "A123", owner: nil, assetCode: "1239A",
                          assetType: "Workstation", createdAt: date, updatedAt: date, confirmedAt: date, reservationMessage: "",
                          status: Status(state: "Validated", cancellationDate: nil, reason: nil), recurrenceId: "3fbce922-f7eb-4ad8-bbe9-1bf9f55a36c9")
    let booking2 = Booking(id: "2", areaName: "4A", startTime: date, endTime: date,
                           isRecurrent: false, isPermanent: false, isConfirmed: false, buildingId: "628d71b6e060f1c961d70a64",
                           areaId: "A231", assetId: "02", owner: nil, assetCode: "1239A",
                           assetType: "Workstation", createdAt: date, updatedAt: date, confirmedAt: date, reservationMessage: "",
                           status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: "3fbce922-f7eb-4ad8-bbe9-1bf9f55a36c9")
    let booking3 = Booking(id: "3", areaName: "5A", startTime: date, endTime: date,
                           isRecurrent: false, isPermanent: false, isConfirmed: false, buildingId: "628d71b6e060f1c961d70a64",
                           areaId: "03", assetId: nil, owner: nil, assetCode: "1239A",
                           assetType: "Workstation", createdAt: date, updatedAt: date, confirmedAt: date, reservationMessage: "",
                           status: Status(state: "Pending", cancellationDate: nil, reason: nil), recurrenceId: "3fbce922-f7eb-4ad8-bbe9-1bf9f55a36c9")

    // MARK: - Constructors
    init() {
        bookingData.append(booking)
        bookingData.append(booking2)
        bookingData.append(booking3)
    }

}
