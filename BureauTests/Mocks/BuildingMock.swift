//
//  BuildingMock.swift
//  Bureau
//
//  Created by user on 6/8/21.
//

import Foundation

class BuildingMock {

    // MARK: - Variables
    var buildingData: [Building] = []

    // MARK: - Constants
    let building: Building = Building(id: "6241b891df2ecd958d259cd5", code: "JALA-HDQC", name: "JalaSoft Headquarter Cochabamba",
                                      address: "Diagonal 24 # 02- 12, Cochabamba, Bolivia", timezone: "UTC−05:00", published: false)
    let building2: Building = Building(id: "6141b891df2ecd958d259ctt5", code: "Jala Foundation Cochabamba", name: "JALA-FUNDATION", address: "Avenida 6", timezone: "UTC−12:00", published: false)
    let building3 = Building(id: "6441b891df2ecd958d259ch7", code: "JALA-LPB", name: "Jala Building La Paz", address: "Carrera 15 Norte # 5 - 15, La Paz, Bolivia", timezone: "UTC−05:00", published: true)
    let building4 = Building(id: "628d71b6e060f1c961d70a64", code: "JALA-LPB", name: "Jala Building La Paz", address: "Carrera 15 Norte # 5 - 15, La Paz, Bolivia", timezone: "UTC−05:00", published: true)
    // MARK: - Constructors
    init() {
        buildingData.append(building)
        buildingData.append(building2)
        buildingData.append(building3)
    }
}
