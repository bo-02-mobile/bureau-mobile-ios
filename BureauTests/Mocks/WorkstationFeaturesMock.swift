//
//  AssetFeaturesMock.swift
//  Bureau
//
//  Created by Victor Arana on 8/27/21.
//

import Foundation

class WorkstationFeaturesMock {

    // MARK: - Variables
    var features: [Feature] = []
    var workstationFeatures: [WorkstationFeature] = []
    // MARK: - Constants
    let workstationFeatureOne = Feature(id: "1", name: "Wifi", text: "",value: "")
    let workstationFeatureTwo = Feature(id: "2", name: "Power", text: "", value: "")
    let workstationFeatureThree = Feature(id: "3", name: "Mouse", text: "",value: "")
    let workstationFeatureFour = Feature(id: "4", name: "Keyboard", text: "",value: "")
    let workstationFeatureFive = Feature(id: "5", name: "Ethernet", text: "",value: "")
    
    // MARK: - Constructors
    init() {
        features.append(workstationFeatureOne)
        features.append(workstationFeatureTwo)
        features.append(workstationFeatureThree)
        features.append(workstationFeatureFour)
        features.append(workstationFeatureFive)
        
        var index = 0
        for item in features {
            workstationFeatures.append(WorkstationFeature(id: item.id, name: item.name, text: item.text, value: item.value, index: index))
            index += 1
        }
    }
}
