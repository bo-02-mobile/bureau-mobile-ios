//
//  UserMock.swift
//  Bureau
//
//  Created by Victor Arana on 9/10/21.
//

import Foundation

class UserMock {
    static func getUser() -> User {
        let user = User(name: "Demo User",
                        email: "test.bureau@gmail.com",
                        picture: "https://lh3.googleusercontent.com/a/AATXAJwKdcAc9MrJRCm3gOOvlSyNVULudlkukKhTH6T_=s96-c")
        return user
    }
}
