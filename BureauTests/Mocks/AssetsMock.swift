//
//  FeaturesMock.swift
//  Bureau
//
//  Created by user on 6/7/21.
//

import Foundation
class AssetsMock {

    // MARK: - Variables
    var feature1: [String] = ["Monitor","HDMI", "Camera", "Mouse", "Keyboard", "Desktop", "Network", "Wifi", "Power Strip", "Jabra", "Chair"]
    
    var policies = Policies(bookingWindow: 0, isRecurrent: false, min: 30, max: 720, schedule: [Schedule(id: nil, startTime: "2022-05-18T04:00:00Z", endTime: "2022-05-19T03:59:00Z")])
    
    var asset = Asset(id: "A123", buildingID: "6441b891df2ecd958d259ch7", areaId: "321", name: "123",
                      code: "123", assetDescription: "", assetPublic: true,
                      stats: Stats(count: 5, average: 5),
                      properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "Oliver", email: "test@email.com")), policies: nil,
                      createdAt: "2021-07-26T14:28:08.118Z", updatedAt: "2021-09-02T00:47:59.956Z", version: 1)
    var asset2 = Asset(id: "A231", buildingID: "6441b891df2ecd958d259ch7", areaId: "", name: "32",
                       code: "32", assetDescription: "", assetPublic: true,
                      stats: Stats(count: 5, average: 5),
                       properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion"), policies: nil,
                       createdAt: "2021-07-26T14:28:08.118Z", updatedAt: "2021-09-02T00:47:59.956Z", version: 1)

    var asset3 = Asset(id: "A333", buildingID: "6441b891df2ecd958d259ch7", areaId: "123", name: "528",
                       code: "528", assetDescription: "The best workstation", assetPublic: true,
                      stats: Stats(count: 5, average: 5),
                       properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion"),
                                   policies: Policies(bookingWindow: 0, isRecurrent: false, min: 30, max: 720,
                                   schedule: [Schedule(id: nil, startTime: "2022-05-18T05:00:00Z", endTime: "2022-05-19T18:00:00Z")]),
                       createdAt: "2021-07-26T14:28:08.118Z", updatedAt: "2021-09-02T00:47:59.956Z", version: 1)
    var assetList: [Asset] = []
    var assetWithPolicies: Asset
    var permanentAssetsList: [Asset] = []
    
    // MARK: - Constructors
    init() {
        let schedule = Schedule(id: "10", startTime: "2021-09-02T12:00:00Z", endTime: "2021-09-02T22:00:00Z")
        let policies = Policies(bookingWindow: 5, isRecurrent: false, min: 60, max: 360, schedule: [schedule])

        assetWithPolicies = Asset(id: "A123", buildingID: "628d71b6e060f1c961d70a64", areaId: "321", name: "123", code: "123",
                                  assetDescription: "", assetPublic: true, stats: Stats(count: 5, average: 5),
                                  properties: Properties(labels: ["Monitor","HDMI", "Camera"], type: "Workstaion", owner: Owner(id: "O123", preferredUsername: "Oliver", email: "test@email.com")),
                                  policies: policies, createdAt: "2021-07-26T14:28:08.118Z", updatedAt: "2021-09-02T00:47:59.956Z", version: 1)
        permanentAssetsList.append(assetWithPolicies)
        assetList.append(contentsOf: [asset, asset2, asset3])
    }

}
