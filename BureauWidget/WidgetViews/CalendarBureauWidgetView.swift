//
//  CalendarBureauWidgetView.swift
//  BureauWidgetExtension
//
//  Created by Julio Gabriel Tobares on 28/03/2022.
//

import WidgetKit
import SwiftUI

extension DateFormatter {
    static var month: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale.autoupdatingCurrent
        formatter.setLocalizedDateFormatFromTemplate("MMMM")
        return formatter
    }

    static var monthAndYear: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale.autoupdatingCurrent
        formatter.setLocalizedDateFormatFromTemplate("MMMM yyyy")
        return formatter
    }
}

extension Calendar {
    func generateDates(
        inside interval: DateInterval,
        matching components: DateComponents
    ) -> [Date] {
        var dates: [Date] = []
        dates.append(interval.start)

        enumerateDates(
            startingAfter: interval.start,
            matching: components,
            matchingPolicy: .nextTime
        ) { date, _, stop in
            if let date = date {
                if date < interval.end {
                    dates.append(date)
                } else {
                    stop = true
                }
            }
        }

        return dates
    }
}

struct CalendarGridView<DateView>: View where DateView: View {
    // MARK: Lifecycle

    init(
        interval: DateInterval,
        showHeaders: Bool = false,
        @ViewBuilder content: @escaping (Date) -> DateView
    ) {
        self.interval = interval
        self.showHeaders = showHeaders
        self.content = content
    }

    // MARK: Internal

    @Environment(\.calendar) var calendar

    let interval: DateInterval
    let showHeaders: Bool
    let content: (Date) -> DateView

    @ViewBuilder
    var body: some View {
        weekDaysView()
        daysGridView()
    }

    // MARK: Private

    private var months: [Date] {
        calendar.generateDates(
            inside: interval,
            matching: DateComponents(day: 1, hour: 0, minute: 0, second: 0)
        )
    }

    private var weeks: [Date] {
        guard let monthInterval = calendar.dateInterval(of: .month, for: Date()) else { return [] }

        return calendar.generateDates(
            inside: monthInterval,
            matching: DateComponents(hour: 0, minute: 0, second: 0, weekday: calendar.firstWeekday)
        )
    }
    
    private func weekDaysView() -> some View {
        HStack() {
            ForEach(0 ..< 7) { index in
                Text(getWeekDaysSorted()[index].localizedUppercase)
                    .font(.system(size: 14))
                    .frame(maxWidth: .infinity)
            }
        }.frame(maxWidth: .infinity)
    }

    private func daysGridView() -> some View {
        LazyVGrid(columns: Array(repeating: GridItem(), count: 7), spacing: 5) {
            ForEach(months, id: \.self) { month in
                ForEach(days(for: month), id: \.self) { date in
                    if calendar.isDate(date, equalTo: month, toGranularity: .month) {
                        content(date).id(date)
                    } else {
                        content(date).hidden()
                    }
                }
            }
        }
    }

    private func days(for month: Date) -> [Date] {
        guard
            let monthInterval = calendar.dateInterval(of: .month, for: month),
            let monthFirstWeek = calendar.dateInterval(of: .weekOfMonth, for: monthInterval.start),
            let monthLastWeek = calendar.dateInterval(of: .weekOfMonth, for: monthInterval.end)
        else { return [] }

        return calendar.generateDates(
            inside: DateInterval(start: monthFirstWeek.start, end: monthLastWeek.end),
            matching: DateComponents(hour: 0, minute: 0, second: 0)
        )
    }

    private func getWeekDaysSorted() -> [String] {
        let weekDays = Calendar.autoupdatingCurrent.veryShortWeekdaySymbols
        let sortedWeekDays = Array(weekDays[Calendar.autoupdatingCurrent.firstWeekday - 1 ..< Calendar.autoupdatingCurrent.shortWeekdaySymbols.count] + weekDays[0 ..< Calendar.autoupdatingCurrent.firstWeekday - 1])
        return sortedWeekDays
    }
}

struct CalendarBureauWidgetView: View {
    var entry: BureauProvider.Entry
    @Environment(\.widgetFamily) var family
    var body: some View {
        switch family {
        case .systemLarge:
            EmptyView()
        case .systemMedium:
            CalendarBookingView(entry: entry)
        case .systemSmall:
            EmptyView()
        @unknown default:
            EmptyView()
        }
    }
}

struct CalendarBookingView: View {
    var entry: BureauProvider.Entry
    @State private var date = Date()
    var body: some View {
        HStack {
                VStack(spacing: 8) {
                    if(entry.bookingsWidget.isEmpty) {
                        Text("None")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(.system(size: 18, weight: .semibold))
                            .foregroundColor(Color.black)
                        Spacer()
                    } else {
                        VStack() {
                            Text(entry.bookingsWidget[0].date ?? "")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(Color.black)
                            Text(entry.bookingsWidget[0].assetCode ?? "")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(.system(size: 20, weight: .bold))
                                .foregroundColor(Color.red)
                                .padding(.bottom, 0)
                            if (entry.bookingsWidget[0].isPermanent == true) {
                                Text("Permanent booking")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .font(.system(size: 12, weight: .medium))
                                    .foregroundColor(Color.black)
                                    .opacity(0.5)
                            } else {
                                Text("\(entry.bookingsWidget[0].startTime!) - \(entry.bookingsWidget[0].endTime!)")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .font(.system(size: 12, weight: .medium))
                                    .foregroundColor(Color.black)
                                    .opacity(0.5)
                            }
                        }
                    }
                    if(entry.bookingsWidget.first?.assetCode?.count ?? 0 < 8) {Spacer()}
                    HStack {
                        Link(destination: URL(string: "scheme://booking")!, label: {
                            Image("addIcon")
                                .resizable()
                                .frame(width: 32, height: 32)
                            
                        }).frame(maxWidth: .infinity, alignment: .leading)
                    }.widgetURL(URL(string: "scheme://booking")!)
                }.frame(maxWidth: 110, maxHeight: .infinity)
                HStack {
                    MediumCalendarGridView(entry: entry)
                }.frame(maxWidth: .infinity, maxHeight: .infinity)
            }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.horizontal, 16).padding([.bottom, .top], 16)
            .background(ContainerRelativeShape().fill(Color.white))
    }
}

struct MediumCalendarGridView: View {
    let entry: BureauProvider.Entry
    
    var dateTest = Date()
    
    var body: some View {
            VStack(alignment: .center, spacing: 5) {
                CalendarGridView(interval: Calendar.autoupdatingCurrent.dateInterval(of: .month, for: Date())!) { date in
                    if let dateWidget = entry.bookingsWidget.first?.date, dateWidget == Helpers.getWidgetDateLabel(date: date) {
                        Text("\(Calendar.current.dateComponents([.day], from: date).day!)")
                            .minimumScaleFactor(0.5)
                            .foregroundColor(Color.white)
                            .frame(width: 26, height: 16)
                            .multilineTextAlignment(.trailing)
                            .padding(.all, 1.2)
                            .background(Color.red)
                            .clipShape(Circle())
                    } else {
                           Text("\(Calendar.current.dateComponents([.day], from: date).day!)")
                                .minimumScaleFactor(0.5)
                                .foregroundColor(Calendar.autoupdatingCurrent.isDateInToday(date) ? .red : .gray)
                                .frame(width: 26, height: 16)
                                .multilineTextAlignment(.trailing)
                    }
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.all, 5)
    }
}

struct CalendarBureauWidgetView_Previews: PreviewProvider {
    static let myCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        return calendar
    }()
    
    static let bookingWidgets = {
        return [BookingWidget(areaName: "Area",
                                           assetCode: "Workstation2",
                                           startTime: "08:30 PM",
                                           endTime: "09:30 PM",
                                           date: Helpers.getWidgetDateLabel(date: "2022-05-15T23:30:00Z"),
                                           isConfirmed: nil)]
    }()
    
    static var previews: some View {
        Group {
            CalendarBureauWidgetView(
                entry: BureauEntry(bookingWidgets: bookingWidgets)
            )
            .environment(\.calendar, myCalendar)
            .previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}
