//
//  BureauWidgetView.swift
//  BureauWidgetExtension
//
//  Created by Carlos Alcala on 13/9/21.
//

import WidgetKit
import SwiftUI

struct BureauWidgetView : View {
    var entry: BureauProvider.Entry
    @Environment(\.widgetFamily) var family
    var body: some View {
        switch family {
        case .systemLarge:
            EmptyView()
        case .systemMedium:
            MyBookingView(entry: entry)
        case .systemSmall:
            TodayView(entry: entry)
        @unknown default:
            EmptyView()
        }
    }
}

struct TodayView: View {
     var entry: BureauProvider.Entry
     var body: some View {
        VStack {
            if(entry.bookingsWidget.isEmpty) {
                HStack(alignment: .top) {
                    Text("None")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(.system(size: 18, weight: .semibold))
                        .foregroundColor(Color.white)
                    Image("bureauLogo")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 22, height: 22, alignment: .trailing)
                        .padding(.top, 5)
                }
            } else {
                HStack(alignment: .top) {
                    VStack {
                        if (entry.bookingsWidget[0].isPermanent == true) {
                            Text("Today")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(Color("textYellow"))
                        } else {
                            Text(entry.bookingsWidget[0].date ?? "")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(Color("textYellow"))
                        }
                        Text(entry.bookingsWidget[0].assetCode ?? "")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(.system(size: 20, weight: .bold))
                            .foregroundColor(Color.white)
                    }
                    Image("bureauLogo")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 22, height: 22, alignment: .trailing)
                        .padding(.top, 5)
                }
                HStack {
                    if (entry.bookingsWidget[0].isPermanent == true) {
                        Text("Permanent booking")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(.system(size: 12, weight: .medium))
                            .foregroundColor(Color.black)
                            .opacity(0.5)
                    } else {
                        Text("\(entry.bookingsWidget[0].startTime!) - \(entry.bookingsWidget[0].endTime!)")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(.system(size: 12, weight: .medium))
                            .foregroundColor(Color.black)
                            .opacity(0.5)
                    }
                }
            }
            Spacer()
            HStack {
                Link(destination: URL(string: "scheme://booking")!, label: {
                    Image("addIcon")
                        .resizable()
                        .frame(width: 32, height: 32, alignment: .trailing)
                        .foregroundColor(.red)
                })
            }
            .frame(maxWidth: .infinity, alignment: .trailing)
            .widgetURL(URL(string: "scheme://booking")!)
        }
        .padding(.all, 16)
        .background(ContainerRelativeShape().fill(Color("primaryMain")))
     }
}

struct MyBookingView: View {
    var entry: BureauProvider.Entry
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                VStack {
                    Text("My Bookings")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(Color("textYellow"))
                }
                Image("bureauLogo")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 22, height: 22, alignment: .trailing)
            }
            HStack(alignment: .bottom) {
                VStack(spacing: 8) {
                    if(entry.bookingsWidget.isEmpty) {
                        Text("None")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(.system(size: 18, weight: .semibold))
                            .foregroundColor(Color.white)
                        Spacer()
                    } else {
                        ForEach(entry.bookingsWidget, id:\.id) { wid in
                            VStack(spacing: 2) {
                                Text("\(wid.assetCode!) - \(wid.areaName!)")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .font(.system(size: 18, weight: .medium))
                                    .foregroundColor(Color.white)
                                if (wid.isPermanent == true) {
                                    Text("Permanent booking")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .font(.system(size: 12, weight: .medium))
                                        .foregroundColor(Color.black)
                                        .opacity(0.5)
                                } else {
                                    Text("\(wid.date!) from \(wid.startTime!) to \(wid.endTime!)")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .font(.system(size: 12, weight: .medium))
                                        .foregroundColor(Color.black)
                                        .opacity(0.5)
                                }
                            }
                        }
                    }
                    if(entry.bookingsWidget.count == 1) {Spacer()}
                }.frame(maxWidth: .infinity, maxHeight: .infinity)
                Link(destination: URL(string: "scheme://booking")!, label: {
                    Image("addIcon")
                        .resizable()
                        .frame(width: 32, height: 32, alignment: .trailing)
                        .foregroundColor(.red)
                })
            }.frame(maxWidth: .infinity, maxHeight: .infinity)
        }
        .padding(.all, 16)
        .background(ContainerRelativeShape().fill(Color("primaryMain")))
    }
}

struct WidgetPreview: PreviewProvider {
    static var previews: some View {
        Group {
            TodayView(entry: BureauProvider.Entry())
                .previewContext(WidgetPreviewContext(family: .systemSmall))
            MyBookingView(entry: BureauProvider.Entry())
                .previewContext(WidgetPreviewContext(family: .systemMedium))
            CalendarBookingView(entry: BureauProvider.Entry()).previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}
