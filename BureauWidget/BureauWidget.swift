//
//  BureauWidget.swift
//  BureauWidget
//
//  Created by Carlos Alcala on 13/9/21.
//

import WidgetKit
import SwiftUI

@main
struct BureauWidgetBundle: WidgetBundle {
    
    @WidgetBundleBuilder
    var body: some Widget {
        BureauWidget()
        CalendarBureauWidget()
    }
}

struct BureauWidget: Widget {
    let kind: String = "Bureau Widget"

    var body: some WidgetConfiguration {
        StaticConfiguration(
            kind: kind,
            provider: BureauProvider(),
            content: { BureauWidgetView(entry: $0) }
        )
        .configurationDisplayName("Bureau Widget")
        .description("Your nearest reservation!")
        .supportedFamilies([.systemMedium, .systemSmall])
    }
}

struct CalendarBureauWidget: Widget {
    let kind: String = "Calendar Bureau Widget"

    var body: some WidgetConfiguration {
        StaticConfiguration(
            kind: kind,
            provider: BureauProvider(),
            content: { CalendarBureauWidgetView(entry: $0) }
        )
        .configurationDisplayName("Bureau Widget")
        .description("Your nearest reservation!")
        .supportedFamilies([.systemMedium])
    }
}

struct BureauWidget_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            BureauWidgetView(
                entry: BureauEntry()
            )
            .previewContext(WidgetPreviewContext(family: .systemMedium))

            BureauWidgetView(
                entry: BureauEntry()
            )
            .previewContext(WidgetPreviewContext(family: .systemSmall))
            
            CalendarBureauWidgetView(
                entry: BureauEntry()
            )
            .previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}
