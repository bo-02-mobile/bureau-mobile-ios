//
//  BureauProvider.swift
//  BureauWidgetExtension
//
//  Created by Carlos Alcala on 13/9/21.
//

import WidgetKit

struct BureauProvider: TimelineProvider {

    private let placeholderEntry = BureauEntry()

    func placeholder(in context: Context) -> BureauEntry {
        return placeholderEntry
    }

    func getSnapshot(in context: Context, completion: @escaping (BureauEntry) -> Void) {
        completion(placeholderEntry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<BureauEntry>) -> Void) {
        let urlBase = UserDefaultsForWidget.shared.defaults?.value(forKey: Constants.UserDefaultsKeys.currentURL) ?? ""
        let email = UserDefaultsForWidget.shared.defaults?.value(forKey: Constants.UserDefaultsKeys.userEmail) ?? ""
        let userAuth = UserDefaultsForWidget.shared.defaults?.value(forKey: Constants.UserDefaultsKeys.userAuth) ?? ""
        let services = ApiService()
        var entries: [BureauEntry] = []
        let bureauent = BureauEntry()
        var buildingList: [Building] = []
        var todayBookingsList:  [Booking] = []
        var upcomingbookingsList:  [Booking] = []
        var permanentBookingsList: [Booking] = []
        var dataBookings: [Booking] = []
        services.apiToGetBuildingData(userAuth: userAuth as? String ?? "", urlBase: urlBase as? String ?? "") { result in
            switch result {
            case .success(let buildings):
                buildingList = buildings
                let groupBookings: DispatchGroup = DispatchGroup()
                todayBookingsList.removeAll()
                upcomingbookingsList.removeAll()
                permanentBookingsList.removeAll()
                groupBookings.enter()
                services.getbookings(userAuth: userAuth as? String ?? "", bookingsTime: .today, urlBase: urlBase as? String ?? "", email: email as? String ?? "") { (result) in
                    switch (result) {
                    case .success(let todayBookings):
                        todayBookingsList = todayBookings.sorted(by: { ($0.createdAt ?? "") < ($1.createdAt ?? "") })
                    case .failure(_):
                        entries.append(bureauent)
                        let timeline = Timeline(entries: entries, policy: .atEnd)
                        completion(timeline)
                    }
                    groupBookings.leave()
                }
                groupBookings.enter()
                services.getbookings(userAuth: userAuth as? String ?? "", bookingsTime: .upcoming, urlBase: urlBase as? String ?? "", email: email as? String ?? "") { (result) in
                    switch (result) {
                    case .success(let upcomingBookings):
                        upcomingbookingsList = upcomingBookings.sorted(by: { ($0.createdAt ?? "") < ($1.createdAt ?? "") })
                    case .failure(_):
                        entries.append(bureauent)
                        let timeline = Timeline(entries: entries, policy: .atEnd)
                        completion(timeline)
                    }
                    groupBookings.leave()
                }
                groupBookings.enter()
                services.getPermanentAssets(userAuth: userAuth as? String ?? "", urlBase: urlBase as? String ?? "", email: email as? String ?? "") { (result) in
                    switch (result) {
                    case .success(let assets):
                        permanentBookingsList = Helpers.setAssetsAsPermanentBookings(assets: assets)
                    case .failure(_):
                        entries.append(bureauent)
                        let timeline = Timeline(entries: entries, policy: .atEnd)
                        completion(timeline)
                    }
                    groupBookings.leave()
                }
                groupBookings.notify(queue: DispatchQueue.main) {
                    let groupAssets: DispatchGroup = DispatchGroup()
                    for index in permanentBookingsList.indices {
                        groupAssets.enter()
                        services.getAreaInfo(userAuth: userAuth as? String ?? "", urlBase: urlBase as? String ?? "", areaId: permanentBookingsList[index].areaId ?? "") { (result) in
                            switch (result) {
                            case .success(let areaInfo):
                                permanentBookingsList[index].areaName = areaInfo.name
                            case .failure(_):
                                permanentBookingsList[index].areaName = Constants.AreasInfo.areaNotFound 
                            }
                            groupAssets.leave()
                        }
                    }
                    groupAssets.notify(queue: DispatchQueue.main) {
                        dataBookings = todayBookingsList + upcomingbookingsList + permanentBookingsList
                        var bookingsShown = 0
                        let reloadDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date())!
                        var apientry = BureauEntry()
                        apientry.bookingsWidget = []
                        for booking in dataBookings {
                            if let startTime = booking.startTime, let endTime = booking.endTime, bookingsShown < 2 {
                                if let timezone = buildingList.first(where: { $0.id == booking.buildingId })?.timezone {
                                    let startDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: startTime))
                                    let endDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: endTime))
                                    let startTimeBooking = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startDatetUTC, timezoneAbbreviation: timezone)
                                    let endTimeBooking = Helpers.offsetUTCDateToGivenTimezone(dateUTC: endDatetUTC, timezoneAbbreviation: timezone)
                                    let booked = BookingWidget(areaName: booking.areaName,
                                                               assetCode: booking.assetCode,
                                                               startTime: Helpers.convertDateToHour(initDate: startTimeBooking),
                                                               endTime: Helpers.convertDateToHour(initDate: endTimeBooking),
                                                               date: Helpers.getWidgetDateLabel(date: booking.startTime ?? ""),
                                                               isConfirmed: booking.isConfirmed,
                                                               isPermanent: booking.isPermanent)
                                    apientry.bookingsWidget.append(booked)
                                    bookingsShown += 1
                                }
                            } else {
                                break
                            }
                        }
                        apientry.date = Date()
                        entries.append(apientry)
                        entries.append(bureauent)
                        let timeline = Timeline(entries: entries, policy: .after(reloadDate))
                        completion(timeline)
                    }
                }
            case .failure(_):
                entries.append(bureauent)
                let timeline = Timeline(entries: entries, policy: .atEnd)
                completion(timeline)
            }
        }
    }
}
