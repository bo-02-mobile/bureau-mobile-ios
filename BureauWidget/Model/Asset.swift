//
//  Asset.swift
//  BureauWidgetExtension
//
//  Created by Juan Pablo Lozada Chambilla on 4/7/22.
//

import Foundation

struct Asset: Codable {
    var id: String = ""
    let buildingID: String?
    let areaId: String?
    let name: String?
    let code: String?
    let assetDescription: String?
    let assetPublic: Bool?
    let stats: Stats?
    var properties: Properties?
    let policies: Policies?
    let createdAt: String?
    let updatedAt: String?
    let version: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case buildingID = "buildingId"
        case areaId = "areaId"
        case name, code
        case assetDescription = "description"
        case assetPublic = "public"
        case stats, properties, policies, createdAt, updatedAt, version
    }
}

// MARK: - Policies
struct Policies: Codable {
    let bookingWindow: Int?
    let isRecurrent: Bool?
    let min: Int?
    let max: Int?
    let schedule: [Schedule]?
}

// MARK: - Schedule
struct Schedule: Codable {
    let id: String?
    let startTime: String?
    let endTime: String?
}

// MARK: - Stats
struct Stats: Codable {
    let count: Int?
    let average: Int?
}

// MARK: - Properties
struct Properties: Codable {
    var labels: [String]?
    let type: String?
    var owner: Owner?
}
