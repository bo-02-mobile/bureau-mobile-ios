//
//  Area.swift
//  BureauWidgetExtension
//
//  Created by Juan Pablo Lozada Chambilla on 5/7/22.
//

import Foundation

struct Area: Codable {
    let code: String?
    let typeDescription: String?
    let id: String?
    let name: String?
    let published: Bool?
    let thumbnail: Thumbnail?
    let background: Background?
    var markers: [Marker]?
}

struct AreaSpace {
    let area: Area?
    let space: Int?
}

struct Thumbnail: Codable {
    let id: String?
    let url: String?
}

struct Background: Codable {
    let id: String?
    let url: String?
}

struct Marker: Codable {
    var workstationId: String = ""
    var isAvailable: Bool = false
    var isPublic: Bool?
    var xPos: Double?
    var yPos: Double?
    enum CodingKeys: String, CodingKey {
        case workstationId = "assetId", xPos = "x", yPos = "y", isPublic = "public"
    }
}
