//
//  Building.swift
//  BureauWidgetExtension
//
//  Created by Christian Torrico Avila on 16/4/22.
//

import Foundation

struct Building: Codable {
    let id : String?
    let code : String?
    let name : String?
    let address : String?
    let timezone : String?
    let published: Bool?
    enum CodingKeys: String, CodingKey {
        case id, code, name, address, timezone, published = "public"
    }
    var areasNumber: Int = -1
}
