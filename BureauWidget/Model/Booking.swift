//
//  Booking.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 23/3/22.
//

import Foundation

struct Booking: Codable {
    var id: String = ""
    var areaName: String?
    let startTime: String?
    let endTime: String?
    let isRecurrent: Bool?
    let isPermanent: Bool?
    let isConfirmed: Bool?
    let buildingId: String?
    let areaId: String?
    let assetId: String?
    let owner: Owner?
    let assetCode: String?
    let assetType: String?
    let createdAt: String?
    let updatedAt: String?
    let confirmedAt: String?
}

struct Owner: Codable {
    let id: String?
    let name : String?
    let email: String?
}
