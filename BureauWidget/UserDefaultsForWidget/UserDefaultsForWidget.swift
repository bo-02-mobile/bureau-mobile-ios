//
//  UserDefaultsForWidget.swift
//  BureauWidgetExtension
//
//  Created by Carlos Alcala on 16/9/21.
//

import Foundation

class UserDefaultsForWidget {

    // MARK: - Constants
    static let shared = UserDefaultsForWidget()
    let defaults = UserDefaults(suiteName: "group.com.jalasoft.bureauapps")

    open func setStructArray<T: Codable>(_ value: [T], forKey defaultName: String) {
        let data = value.map { try? JSONEncoder().encode($0) }
        guard let defaults = defaults else {
            return
        }
        defaults.set(data, forKey: defaultName)
    }

    open func getStructArray<T>(_ type: T.Type, forKey defaultName: String) -> [T] where T : Decodable {
        guard let defaults = defaults, let encodedData = defaults.array(forKey: defaultName) as? [Data] else {
            return []
        }

        // swiftlint:disable force_try
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
}
