//
//  ApiService.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 23/3/22.
//

import Foundation

class ApiService {
    
    func getbookings(userAuth: String, bookingsTime: BookingsTime, urlBase: String, email: String, completion : @escaping (Result<[Booking], Error>) -> Void ) {
        guard var url = URL(string: "\(urlBase)bookings/groupByRecurrence?filters=owner.email%20eq%20string(\(email))") else { return }

        let calendar = Calendar.current
        if (bookingsTime == .today) {
             let startOfDate: Date = calendar.startOfDay(for: Date())
             let endOfDate: Date = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) ?? Date()
             let startOfDateFormat: String = Helpers.convertDateTimeToDateString(date: startOfDate)
             let endOfDateFormat: String = Helpers.convertDateTimeToDateString(date: endOfDate)
             let queryStartOfDay = [URLQueryItem(name: "filters", value: "startTime gte date(\(startOfDateFormat))")]
             let queryEndOfDay = [URLQueryItem(name: "filters", value: "startTime lte date(\(endOfDateFormat))")]
             url.appending(queryStartOfDay)
             url.appending(queryEndOfDay)
        } else if (bookingsTime == .upcoming) {
             var dayComponent = DateComponents()
             dayComponent.day = 1
             let nextDay: Date = calendar.date(byAdding: dayComponent, to: Date()) ?? Date()
             let startDate: Date = calendar.startOfDay(for: nextDay)
             let startDateFormat: String = Helpers.convertDateTimeToDateString(date: startDate)
             let queryStartDay = [URLQueryItem(name: "filters", value: "startTime gte date(\(startDateFormat))")]
             url.appending(queryStartDay)
        }
        let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
        let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
        url.appending(queryNotUserCancelled)
        url.appending(queryNotSystemCancelled)
       
        var request = URLRequest(url: url)
        request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(userAuth)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
        
        URLSession.shared.dataTask(with: request) { (data, _ , error) in
            if let error = error {
                completion(.failure(error))
            }
            
            if let data = data {
                do {
                    let object = try JSONDecoder().decode([Booking].self, from: data)
                    completion(.success(object))
                } catch let decoderError {
                    completion(.failure(decoderError))
                }
            }
            
        }.resume()
    }
    
    func apiToGetBuildingData(userAuth: String, urlBase: String, completion : @escaping (Result<[Building], Error>) -> Void ) {
        guard let url = URL(string: "\(urlBase)buildings") else { return }
        var request = URLRequest(url: url)
        request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(userAuth)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
        
        URLSession.shared.dataTask(with: request) { (data, _, error) in
            if let error = error {
                completion(.failure(error))
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let buildingList = try decoder.decode([Building].self, from: data)
                    completion(.success(buildingList))
                } catch let decoderError {
                    completion(.failure(decoderError))
                }
            }
        }.resume()
    }
    
    func getPermanentAssets(userAuth: String, urlBase: String, email: String, completion : @escaping (Result<[Asset], Error>) -> Void ) {
        guard var url = URL(string: "\(urlBase)assets") else { return }

        let permanentAssets = [URLQueryItem(name: "filters", value: "properties.owner.email eq string(\(email)")]
        url.appending(permanentAssets)
       
        var request = URLRequest(url: url)
        request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(userAuth)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
        
        URLSession.shared.dataTask(with: request) { (data, _ , error) in
            if let error = error {
                completion(.failure(error))
            }
            
            if let data = data {
                do {
                    let object = try JSONDecoder().decode([Asset].self, from: data)
                    completion(.success(object))
                } catch let decoderError {
                    completion(.failure(decoderError))
                }
            }
            
        }.resume()
    }
    
    func getAreaInfo(userAuth: String, urlBase: String, areaId: String, completion : @escaping (Result<Area, Error>) -> Void ) {
        guard let url = URL(string: "\(urlBase)areas/\(areaId)") else { return }
       
        var request = URLRequest(url: url)
        request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(userAuth)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
        
        URLSession.shared.dataTask(with: request) { (data, _ , error) in
            if let error = error {
                completion(.failure(error))
            }
            
            if let data = data {
                do {
                    let object = try JSONDecoder().decode(Area.self, from: data)
                    completion(.success(object))
                } catch let decoderError {
                    completion(.failure(decoderError))
                }
            }
            
        }.resume()
    }
    
}
