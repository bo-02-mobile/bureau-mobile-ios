//
//  BureauEntry.swift
//  BureauWidgetExtension
//
//  Created by Carlos Alcala on 13/9/21.
//

import WidgetKit

struct BureauEntry: TimelineEntry {
    var date: Date
    var bookingsWidget: [BookingWidget]

    init() {
        self.date = Date()
        self.bookingsWidget = UserDefaultsForWidget.shared.getStructArray(BookingWidget.self, forKey: Constants.UserDefaultsKeys.bookingInfo)
    }
    
    init(bookingWidgets: [BookingWidget]) {
        self.date = Date()
        self.bookingsWidget = bookingWidgets
    }
}
