//
//  Constants.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 23/3/22.
//

import Foundation

struct Constants {
    struct UserDefaultsKeys {
        static let bookingInfo = "bookingInfo"
        static let currentURL = "currentURL"
        static let userEmail = "userEmail"
        static let userAuth = "userAuth"
    }
    struct AuthorizationHeader {
        static let fieldName = "Authorization"
        static let fieldValue = "Bearer"
    }
    
    struct DateTimeFormat {
        static let dateFormatForAPI = "yyyy-MM-dd"
        static let hourFormatForAPI = "HH:mm"
        static let dateFormat = "EEE, MMM. d"
        static let completeDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
    
    struct LocalIdentifier {
        static let enUS = "en_US"
    }
    
    struct QueryParameters {
        static let excludeUserCancelled = "status.state ne string(UserCancelled)"
        static let excludeSystemCancelled = "status.state ne string(SystemCancelled)"
    }
    
    struct AreasInfo {
        static let areaNotFound = "Area not found"
    }
}
