//
//  Helpers.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 23/3/22.
//

import Foundation

struct Helpers {
     static func constructDate(hour: String, minute: String, isAm: Bool) -> Date {
         var components = DateComponents()
         var safeHour = Int(hour) ?? 0
         if safeHour == 12 {
             safeHour = 0
         }
         if isAm {
             components.hour = Int(safeHour)
         } else {
             components.hour = safeHour + 12
         }
         components.minute = Int(minute)
         let date = Calendar.current.date(from: components) ?? Date()
         return date
     }
    
    static func convertDateFormat(inputDate: Date, inputHour: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormatForAPI
        let day = dateFormatter.string(from: inputDate)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormatForAPI
        let hour = dateFormatter.string(from: inputHour)
        return "\(day)T\(hour)Z"
    }
    
    static func getDateToHourLabel(date: String) -> String {
        let dateFormatted = convertDateStringToDate(dateString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormatForAPI
        return dateFormatter.string(from: dateFormatted)
    }
    
    static func getWidgetDateLabel(date: String) -> String {
        let dateFormatted = convertDateStringToDate(dateString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        var dateLabel = ""
        if dateFormatter.string(from: dateFormatted).elementsEqual(dateFormatter.string(from: Date())) {
            dateLabel = "Today"
        } else {
            dateLabel = dateFormatter.string(from: dateFormatted)
        }
        return dateLabel
    }
    
    static func getWidgetDateLabel(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        var dateLabel = ""
        if dateFormatter.string(from: date).elementsEqual(dateFormatter.string(from: Date())) {
            dateLabel = "Today"
        } else {
            dateLabel = dateFormatter.string(from: date)
        }
        return dateLabel
    }
    
    static func convertDateStringToDate(dateString: String) -> Date {
        let correctedDate = self.correctDateFormatFromApi(date: dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from:correctedDate)!
        return date
    }
    
    static func correctDateFormatFromApi(date: String) -> String {
        var dateArray = date.split(separator: ".")
        if dateArray.count == 1 {
            dateArray[0].remove(at: dateArray[0].index(before: dateArray[0].endIndex))
        }
        return dateArray[0] + ".000Z"
    }
    
    // Change a UTC Date hour based on difference betweent given timezone and local timezone
    static func offsetUTCDateToGivenTimezone(dateUTC: Date, timezoneAbbreviation: String) -> Date {
        guard let timezone = TimeZone(abbreviation: timezoneAbbreviation) else {
            return dateUTC
        }
        let current = TimeZone.current
        let diff: Double = Double((timezone.secondsFromGMT()) - (current.secondsFromGMT()))
        return dateUTC.addingTimeInterval(diff)
    }
    
    static func convertDateToHour(initDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: initDate)
    }
    
    static func setAssetsAsPermanentBookings(assets: [Asset]) -> [Booking] {
        var permanentBookings: [Booking] = []
        assets.forEach { asset in
            let newBooking = Booking(id: asset.id, areaName: "", startTime: asset.createdAt, endTime: asset.createdAt, isRecurrent: false, isPermanent: true,
                             isConfirmed: false, buildingId: asset.buildingID, areaId: asset.areaId, assetId: asset.id, owner: asset.properties?.owner, assetCode: asset.code,
                             assetType: asset.properties?.type, createdAt: asset.createdAt, updatedAt: asset.updatedAt, confirmedAt: asset.createdAt)
            permanentBookings.append(newBooking)
        }
        return permanentBookings
    }
    
    static func convertDateTimeToDateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}
