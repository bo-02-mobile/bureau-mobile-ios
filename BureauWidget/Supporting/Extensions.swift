//
//  Extensions.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 23/3/22.
//

import Foundation

extension URL {
    mutating func appending(_ queryItems: [URLQueryItem]) {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return
        }
        urlComponents.queryItems = (urlComponents.queryItems ?? []) + queryItems
        self = urlComponents.url!
    }
}
