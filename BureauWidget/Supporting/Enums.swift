//
//  Enums.swift
//  BureauWidgetExtension
//
//  Created by Juan Pablo Lozada Chambilla on 20/10/22.
//

import Foundation

enum BookingsTime {
    case today
    case upcoming
}
