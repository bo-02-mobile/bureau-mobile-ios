//
//  Properties.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 3/8/21.
//

import Foundation

struct Properties: Codable {
    var labels: [String]?
    let type: String?
    var owner: Owner?
}
