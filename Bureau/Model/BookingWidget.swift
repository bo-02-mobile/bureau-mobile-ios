//
//  BookingWidget.swift
//  Bureau
//
//  Created by Carlos Alcala on 16/9/21.
//

import Foundation

struct BookingWidget: Codable {
    var id = UUID()
    var areaName: String?
    var assetCode: String?
    var startTime: String?
    var endTime: String?
    var date: String?
    var isConfirmed: Bool?
    var isPermanent: Bool?
}
