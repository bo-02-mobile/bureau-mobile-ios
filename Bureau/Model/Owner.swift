//
//  Owner.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 1/8/21.
//

import Foundation

struct Owner: Codable {
    let id: String?
    let preferredUsername : String?
    let email: String?
}
