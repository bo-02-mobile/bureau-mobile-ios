//
//  RecurrenceRule.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 10/3/22.
//

import Foundation
struct RecurrenceRule: Codable {
    var isCustom: Bool
    var type: String
    var endDate: Date
    var onDays: [String]?
    var onMonth: String?
    var onWeek: String?
    var onDate: String?
    var every: String?
}
