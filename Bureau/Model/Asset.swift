//
//  Asset.swift
//  Bureau
//
//  Created by Victor Arana on 9/3/21.
//

import Foundation

// MARK: - Asset
struct Asset: Codable {
    var id: String = ""
    let buildingID: String?
    let areaId: String?
    let name: String?
    let code: String?
    let assetDescription: String?
    let assetPublic: Bool?
    let stats: Stats?
    var properties: Properties?
    let policies: Policies?
    let createdAt: String?
    let updatedAt: String?
    let version: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case buildingID = "buildingId"
        case areaId = "areaId"
        case name, code
        case assetDescription = "description"
        case assetPublic = "public"
        case stats, properties, policies, createdAt, updatedAt, version
    }
}

// MARK: - Policies
struct Policies: Codable {
    let bookingWindow: Int?
    let isRecurrent: Bool?
    let min: Int?
    let max: Int?
    let schedule: [Schedule]?
}

// MARK: - Schedule
struct Schedule: Codable {
    let id: String?
    let startTime: String?
    let endTime: String?
}
