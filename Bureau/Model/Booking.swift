//
//  Booking.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 17/6/21.
//

import Foundation

struct Booking: Codable {
    var id: String = ""
    var areaName: String?
    var startTime: String?
    var endTime: String?
    let isRecurrent: Bool?
    let isPermanent: Bool?
    let isConfirmed: Bool?
    let buildingId: String?
    let areaId: String?
    let assetId: String?
    let owner: Owner?
    let assetCode: String?
    let assetType: String?
    let createdAt: String?
    let updatedAt: String?
    let confirmedAt: String?
    let reservationMessage: String?
    let status: Status?
    let recurrenceId: String?
}

struct Status: Codable {
    let state: String?
    let cancellationDate: String?
    let reason: String?
}
