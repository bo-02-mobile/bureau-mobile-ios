//
//  Groups.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 3/5/22.
//

import Foundation

struct Group: Codable {
    let id: String?
    let name: String?
    let quota: [String: Int]?
    let bookForOthers: Bool?
    let recurrenceBookings: Bool?
    let createdAt: String?
    let updatedAt: String?
    let version: Int?
}
