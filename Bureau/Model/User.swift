//
//  User.swift
//  Bureau
//
//  Created by Victor Arana on 6/6/21.
//

import Foundation

struct User: Codable {
    var name: String?
    var email: String?
    var picture: String?
}
