//
//  TimeHour.swift
//  Bureau
//
//  Created by Victor Arana on 7/19/21.
//

import Foundation

class TimeHour: Codable {
    var hour: Int
    var minute: Int
    
    init() {
        self.hour = 0
        self.minute = 0
    }
    
    init(hour: Int, minute: Int) {
        self.hour = hour
        self.minute = minute
    }
    
    func getHourText() -> String {
        if self.hour < 10 {
             return "0" + String(hour)
        }
        return String(hour)
    }
    
    func getMinuteText() -> String {
        if self.minute < 10 {
             return "0" + String(minute)
        }
        return String(minute)
    }
}
