//
//  Marker.swift
//  Bureau
//
//  Created by Victor Arana on 6/15/21.
//

import Foundation

struct Marker: Codable {
    var workstationId: String = ""
    var isAvailable: Bool = false
    var isPublic: Bool?
    var xPos: Double?
    var yPos: Double?
    enum CodingKeys: String, CodingKey {
        case workstationId = "assetId", xPos = "x", yPos = "y", isPublic = "public"
    }
}
