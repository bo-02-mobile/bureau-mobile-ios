//
//  ErrorMessage.swift
//  Bureau
//
//  Created by Julio Gabriel Tobares on 21/01/2022.
//

import Foundation

struct ErrorMessage: Codable {
    let error: String

    enum CodingKeys: String, CodingKey {
        case error = "Error"
    }
}
