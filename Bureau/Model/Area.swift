//
//  List.swift
//  Bureau
//
//  Created by user on 6/1/21.
//

import Foundation

struct Area: Codable {
    let code: String?
    let typeDescription: String?
    let id: String?
    let name: String?
    let published: Bool?
    let thumbnail: Thumbnail?
    let background: Background?
    var markers: [Marker]?
}

struct AreaSpace {
    let area: Area?
    let space: Int?
}

struct Thumbnail: Codable {
    let id: String?
    let url: String?
}

struct Background: Codable {
    let id: String?
    let url: String?
}
