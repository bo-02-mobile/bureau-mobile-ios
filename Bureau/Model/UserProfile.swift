//
//  UserProfile.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 24/3/22.
//

import Foundation

// MARK: - UserProfile
struct UserProfile: Codable {
    let id: String?
    let externalID: String?
    let name: String?
    let givenName: String?
    let familyName: String?
    let preferredUsername: String?
    let email: String?
    let role: String?
    let picture: String?
    let emailVerified: Bool?
    let groups: [String]?
    let favoriteAssets: [String]?
    let createdAt: String?
    let updatedAt: String?
    let version: String?

    enum CodingKeys: String, CodingKey {
        case id
        case externalID = "externalId"
        case name, givenName, familyName, preferredUsername, email, role, picture, emailVerified, groups, favoriteAssets, createdAt, updatedAt, version
    }
}
