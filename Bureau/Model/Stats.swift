//
//  Stats.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 3/8/21.
//

import Foundation

struct Stats: Codable {
    let count: Int?
    let average: Int?
}
