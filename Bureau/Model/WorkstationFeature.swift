//
//  WorkstationFeature.swift
//  Bureau
//
//  Created by Victor Arana on 8/26/21.
//

import Foundation

struct Feature: Codable {
    let id: String?
    let name : String?
    let text : String?
    let value : String?
}

struct WorkstationFeature: Codable {
    let id: String?
    let name : String?
    let text : String?
    let value : String?
    let index: Int?
    var isSelected: Bool = false
}
