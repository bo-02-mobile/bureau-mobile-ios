//
//  BookingConfiguration.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 1/4/22.
//

import Foundation

// MARK: - BookingConfiguration
struct BookingConfiguration: Codable {
    let id: String?
    let limitAfterDeadLine: Int?
    let limitBeforeDeadLine: Int?
}

enum BookingDeadLine {
    case beforeDeadLine
    case afterDeadLine
}
