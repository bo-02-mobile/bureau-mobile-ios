//
//  AssetType.swift
//  Bureau
//
//  Created by Victor Arana on 8/26/21.
//

import Foundation

struct TypeData: Codable {
    var id: String?
    var name : String?
    var text : String?
    var value : String?
    var labels: [String]?
}

struct AssetType: Codable {
    var id: String?
    var name : String?
    var text : String?
    var value : String?
    var image: String?
    var isSelected: Bool = false
    var index: Int?
    var labels: [String]?
}
