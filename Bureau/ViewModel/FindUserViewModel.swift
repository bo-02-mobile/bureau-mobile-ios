//
//  FindUserViewModel.swift
//  Bureau
//
//  Created by Julio Gabriel Tobares on 17/03/2022.
//

import Foundation

class FindUserViewModel: ErrorViewModel {
    
    // MARK: - Variables
    var bindFindUserViewModelToController : (() -> Void) = {
        // will reatreave user info and executed on its view controller
    }
    var apiServiceShared: APIServiceProtocol
    
    // MARK: - Functions
    override init() {
        self.apiServiceShared = ApiService.shared
    }

    init(apiService: APIServiceProtocol) {
        self.apiServiceShared = apiService
    }
    
    private(set) var dataUser : [User]! {
        didSet {
            self.bindFindUserViewModelToController()
        }
    }
    
    func setUser(user: User) {
        apiServiceShared.selectedUser = user
        self.bindFindUserViewModelToController()
    }
    
    func callFuncToGetUserData() {
        apiServiceShared.apiToGetUsers {(result) in
            switch (result) {
            case .success(let userData):
                self.dataUser = userData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
}
