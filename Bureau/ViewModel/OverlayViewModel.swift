//
//  OverlayViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 6/21/21.
//

import Foundation

class OverlayViewModel: ErrorViewModel {
    
    // MARK: - Variables
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var bindWorkstationCodeViewModelToController : ((String) -> Void) = { _ in }
    var bindBookingDateViewModelToController : ((String) -> Void) = { _ in }
    var selectedAsset: Asset!
    // MARK: - Functions
    var bookingConfirmation : Booking?
    private(set) var workstationCode : String! {
        didSet {
            self.bindWorkstationCodeViewModelToController(workstationCode)
        }
    }

    private(set) var bookingDate : String! {
        didSet {
            self.bindBookingDateViewModelToController(bookingDate)
        }
    }
    
    func setBookingConfirmation(bookingConfirmation: Booking) {
        self.bookingConfirmation = bookingConfirmation
    }

    func constructWorkstationCodeLabel() {
        let workstationCodeLabel = Constants.BookingConfirmationModal.workstationCodeLabel
        if let workstation = selectedAsset, let code = workstation.code {
            workstationCode = "\(workstationCodeLabel) \(code)"
            return
        }
        workstationCode = workstationCodeLabel + " \(bookingConfirmation?.assetCode ?? "")"
    }

    func constructBookingDateLabel() {
        if let bookingMessage = bookingConfirmation?.reservationMessage {
            bookingDate = bookingMessage.uppercasingFirst
            return
        }
        bookingDate = Helpers.convertToLiteralDateFormat(currentDate: apiSharedService.currentDate,
                                                         startHour: apiSharedService.startHourDate,
                                                         endHour: apiSharedService.endHourDate)
    }
}
