//
//  ErrorViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import Foundation

class ErrorViewModel: NSObject {
    
    // MARK: - Variables
    var bindErrorViewModelToController : ((String, String) -> Void) = {_,_ in }
    
    // MARK: - Functions
    func displayErrorInView(error: CustomError) {
        let displayError = Helpers.getErrorMessages(error: error)
        self.bindErrorViewModelToController(displayError.title, displayError.message)
    }
}
