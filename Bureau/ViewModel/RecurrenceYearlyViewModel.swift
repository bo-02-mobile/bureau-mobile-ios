//
//  RecurrenceYearlyViewModel.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 9/3/22.
//

import Foundation
class RecurrenceYearlyViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getTempDate() -> Date {
        return apiSharedService.tempCurrentDate
    }
    
    func getEndDate() -> Date {
        return apiSharedService.tempRecurrenceRule.endDate
    }
    
    func getTempRecurrenceRule() -> RecurrenceRule {
        return apiSharedService.tempRecurrenceRule
    }
    
    func setRecurrenceRules(onDate: String, onWeek: String, onDays: [String], onMonth: String, onSelected: Bool) {
        let endDate = getEndDate()
        if(onSelected) {
            apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: true, type: Constants.RecurrentOptions.yearly, endDate: endDate, onDays: [], onMonth: onMonth, onWeek: nil, onDate: onDate, every: "0")
        } else {
            apiSharedService.tempRecurrenceRule = RecurrenceRule(
                isCustom: true, type: Constants.RecurrentOptions.yearly, endDate: endDate, onDays: onDays, onMonth: onMonth, onWeek: onWeek, onDate: nil, every: "0")
        }
    }
}
