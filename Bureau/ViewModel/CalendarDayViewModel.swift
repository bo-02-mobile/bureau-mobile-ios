//
//  CalendarDayViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 8/13/21.
//

import Foundation

class CalendarDayViewModel: ErrorViewModel {
    
    var apiServiceShared: APIServiceProtocol = ApiService.shared

    // MARK: - Functions
    func getDate() -> Date {
        return apiServiceShared.currentDate
    }
    
    func getStartTime() -> Date {
        return apiServiceShared.startHourDate
    }

    func getEndTime() -> Date {
        return apiServiceShared.endHourDate
    }
    
    func getStartHour() -> Float {
        let date = apiServiceShared.startHourDate
        return Float(Calendar.current.component(.hour, from: date))
    }
    
    func getUserName(completion: @escaping (Result<String, CustomError>) -> Void) {
        UserDefaultsService.shared.getUserInfo { (result) in
            switch (result) {
            case .success(let userData):
                guard let safeUserName = userData.name else {
                    completion(.failure(.internalError))
                    return
                }
                completion(.success(safeUserName))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getUserEmail(completion: @escaping (Result<String, CustomError>) -> Void) {
        UserDefaultsService.shared.getUserInfo { (result) in
            switch (result) {
            case .success(let userData):
                guard let safeUserEmail = userData.email else {
                    completion(.failure(.internalError))
                    return
                }
                completion(.success(safeUserEmail))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
