//
//  FiltersViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 8/26/21.
//

import Foundation

class FiltersViewModel: ErrorViewModel {
    
    // MARK: - Variables
    var bindAssetTypesToViewController : (() -> Void) = { }
    var bindWorstationFeaturesToViewController : (() -> Void) = { }
    
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    
    private(set) var assetTypes : [AssetType]! {
        didSet {
            self.bindAssetTypesToViewController()
        }
    }
    
    private(set) var workstationFeatures : [WorkstationFeature]! {
        didSet {
            self.bindWorstationFeaturesToViewController()
        }
    }
    
    var showOnlyAvailableSpaces = false
    
    // MARK: - Functions
    func getShowOnlyAvailableSpacesValue() -> Bool {
        self.showOnlyAvailableSpaces = apiServiceShared.showOnlyAvailableSpaces
        return self.showOnlyAvailableSpaces
    }
    
    func setShowOnlyAvailableSpacesValue(isOn: Bool) {
        self.showOnlyAvailableSpaces = isOn
    }
    
    func callFuncToGetAssetTypes() {
        apiServiceShared.apiToGetAssetTypes { (result) in
            switch (result) {
            case .success(let typesData):
                var assetTypesData = self.converToAssetTypeArray(typesData: typesData)
                self.getAssetTypeSelection(assetTypesData: &assetTypesData)
                self.assetTypes = assetTypesData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func converToAssetTypeArray(typesData: [TypeData]) -> [AssetType] {
        var assetTypeArray: [AssetType] = [AssetType(id: "", name: "All", text: "All", value: "All", image: "allAsset", isSelected: false, index: 0)]
        var index = 1
        for item in typesData {
            assetTypeArray.append(AssetType(id: item.id, name: item.name, text: item.text,
                                            value: item.value, image: item.value, isSelected: false,
                                            index: index, labels: item.labels))
            index += 1
        }
        return assetTypeArray
    }
    
    func getAssetTypeSelection(assetTypesData: inout [AssetType]) {
        guard let selectedId = apiServiceShared.selectedAssetType?.id else {
            assetTypesData[0].isSelected = true
            return
        }
        for index in 0..<assetTypesData.count where assetTypesData[index].id == selectedId {
            assetTypesData[index].isSelected = true
            break
        }
    }
    
    func callFuncToGetWorkstationFeatures() {
        apiServiceShared.apiToGetWorkstationFeatures { (result) in
            switch (result) {
            case .success(let featuresData):
                let filteredFeatures = self.getFeaturesByAssetType(features: featuresData)
                var workstationFeaturesData = self.converToWorkstationFeatureArray(feauteresData: filteredFeatures)
                self.getWorkstationFeaturesSelection(workstationFeaturesData: &workstationFeaturesData)
                self.workstationFeatures = workstationFeaturesData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getFeaturesByAssetType(features: [Feature]) -> [Feature] {
        // If asset type 'All' is selected don't filter
        if (self.assetTypes[0].isSelected) {
            return features
        }
        let assetLabels = assetTypes.filter { $0.isSelected == true }.first?.labels
        let filtered = features.filter { feature in assetLabels?.contains(feature.id ?? "") ?? false }
        return filtered
    }
    
    func converToWorkstationFeatureArray(feauteresData: [Feature]) -> [WorkstationFeature] {
        var workstationFeatureArray: [WorkstationFeature] = []
        var index = 0
        for item in feauteresData {
            workstationFeatureArray.append(WorkstationFeature(id: item.id, name: item.name, text: item.text, value: item.value, index: index))
            index += 1
        }
        return workstationFeatureArray
    }
    
    func getWorkstationFeaturesSelection(workstationFeaturesData: inout [WorkstationFeature]) {
        let selectedWorkstationFeatures = apiServiceShared.selectedWorkstationFeatures
        for workstationFeature in selectedWorkstationFeatures {
            for index in 0..<workstationFeaturesData.count where workstationFeature.id == workstationFeaturesData[index].id {
                workstationFeaturesData[index].isSelected = true
                break
            }
        }
    }
    
    func getAssetTypesLenght() -> Int {
        if assetTypes == nil {
            return 0
        }
        return assetTypes.count
    }
    
    func getAssetTypeByIndex(index: Int) -> AssetType {
        return assetTypes[index]
    }
    
    func setSelectedAssetType(assetType: AssetType) {
        for index in 0..<self.assetTypes.count {
            if self.assetTypes[index].id == assetType.id {
                self.assetTypes[index].isSelected = true
            } else {
                self.assetTypes[index].isSelected = false
            }
        }
    }
    
    func getWorkstationFeaturesLenght() -> Int {
        if workstationFeatures == nil {
            return 0
        }
        return workstationFeatures.count
    }
    
    func getWorkstationFeatureByIndex(index: Int) -> WorkstationFeature {
        return workstationFeatures[index]
    }
    
    func setWorkstationFeature(workstationFeature: WorkstationFeature) {
        guard let index = workstationFeature.index else { return }
        self.workstationFeatures[index].isSelected = !self.workstationFeatures[index].isSelected
    }
    
    func applyFilters() {
        apiServiceShared.showOnlyAvailableSpaces = self.showOnlyAvailableSpaces
        for assetType in self.assetTypes where assetType.isSelected {
            apiServiceShared.selectedAssetType = assetType
        }
        apiServiceShared.selectedWorkstationFeatures.removeAll()
        for workstationFeature in self.workstationFeatures where workstationFeature.isSelected {
            apiServiceShared.selectedWorkstationFeatures.append(workstationFeature)
        }
        apiServiceShared.enabledFilters = self.getEnabledFiltersNumber()
    }
    
    func clearFilters() {
        self.setShowOnlyAvailableSpacesValue(isOn: false)
        for index in 0..<self.assetTypes.count {
            self.assetTypes[index].isSelected = false
        }
        self.assetTypes[0].isSelected = true
        for index in 0..<self.workstationFeatures.count {
            self.workstationFeatures[index].isSelected = false
        }
        self.applyFilters()
    }
    
    func getSelectedAssetType() -> AssetType? {
        for assetType in self.assetTypes where assetType.isSelected {
            return assetType
        }
        return nil
    }
    
    func getEnabledFiltersNumber() -> Int {
        var value = self.showOnlyAvailableSpaces ? 1 : 0
        value += self.assetTypes[0].isSelected ? 0 : 1
        for workstationFeature in self.workstationFeatures where workstationFeature.isSelected {
            value += 1
        }
        return value
    }
}
