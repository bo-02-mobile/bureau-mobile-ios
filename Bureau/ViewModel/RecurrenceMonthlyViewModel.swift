//
//  RecurrenceMonthlyOptionViewModel.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 9/3/22.
//

import Foundation
class RecurrenceMonthlyViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getTempDate() -> Date {
        return apiSharedService.tempCurrentDate
    }
    
    func getEndDate() -> Date {
        return apiSharedService.tempRecurrenceRule.endDate
    }
    
    func getTempRecurrenceRule() -> RecurrenceRule {
        return apiSharedService.tempRecurrenceRule
    }
    
    func setRecurrenceRules(every: String, onDate: String, onWeek: String, onDays: [String], onSelected: Bool) {
        let endDate = getEndDate()
        if(onSelected) {
            apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: true, type: Constants.RecurrentOptions.monthly, endDate: endDate, onDays: [], onMonth: nil, onWeek: nil, onDate: onDate, every: every)
        } else {
            apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: true, type: Constants.RecurrentOptions.monthly, endDate: endDate, onDays: onDays,
                                                  onMonth: nil, onWeek: onWeek, onDate: nil, every: every)
        }
    }
}
