//
//  BookingDetailsViewModel.swift
//  Bureau
//
//  Created by user on 7/2/21.
//

import Foundation

class BookingDetailViewModel: ErrorViewModel {
    
    var bindBookingDetailViewModelToController : (() -> Void) = {}
    var bindAssetViewModelToController : (() -> Void) = {}
    
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    
    // MARK: - Functions
    private(set) var dataBooking : [Booking]! {
        didSet {
            self.bindBookingDetailViewModelToController()
        }
    }
    
    private(set) var asset : Asset! {
        didSet {
            self.bindAssetViewModelToController()
        }
    }
    
    func callFuncToGetAssetWithID(assetId: String) {
        apiServiceShared.apiToGetAssetWithID(assetId: assetId) { (result) in
            switch (result) {
            case .success(let asset):
                self.asset = asset
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToGetAssetBookings(assetId: String, startTime: String, endTime: String) {
        apiServiceShared.apiToGetAssetBookings(assetId: assetId, startTime: startTime, endTime: endTime) { (result) in
            switch (result) {
            case .success(let dataBooking):
                self.dataBooking = dataBooking
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
}
