//
//  AreasListViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import Foundation

class AreaListViewModel: ErrorViewModel {
    
    // MARK: - Variables
    var bindAreaViewModelToController : (() -> Void) = {}
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    var userServiceShared: UserServiceProtocol = UserService.shared
    
    // MARK: - Functions
    private(set) var dataArea : [AreaSpace]! {
        didSet {
            self.bindAreaViewModelToController()
        }
    }
    
    private(set) var dataLayout: [Asset]! {
        didSet {
            self.callFuncToGetAreaData()
        }
    }
    
    var overlappedBookings: [Booking] = [] {
         didSet {
             self.callFuncToGetLayoutData()
         }
    }
    
    func setArea(index: Int) {
        apiServiceShared.selectedArea = dataArea[index].area
    }
    
    func getAvaliableSpaceFromArea(index: Int) -> Int {
        var calendar = Calendar.current
        let timezone = UserDefaultsService().getBuildingTimezone() ?? Constants.DateTimeFormat.utcFormat
        calendar.timeZone = TimeZone.init(abbreviation: timezone) ?? .current
        let hourStartTime = Calendar.current.component(.hour, from: apiServiceShared.startHourDate)
        let minuteStartTime = Calendar.current.component(.minute, from: apiServiceShared.startHourDate)
        let hourEndTime = Calendar.current.component(.hour, from: apiServiceShared.endHourDate)
        let minuteEndTime = Calendar.current.component(.minute, from: apiServiceShared.endHourDate)
        guard let deviceStartTime = calendar.date(bySettingHour: hourStartTime, minute: minuteStartTime, second: 0, of: Date()) else { return 0 }
        guard let deviceEndTime = calendar.date(bySettingHour: hourEndTime, minute: minuteEndTime, second: 0, of: Date()) else { return 0 }
        let assets = dataLayout.filter {$0.areaId == dataArea[index].area!.id}
        var availableSpaces = 0
        for asset in assets where asset.properties?.owner == nil {
            let assetBookingWindow = asset.policies?.bookingWindow ?? 0
            var dayComponent = DateComponents()
            dayComponent.day = assetBookingWindow
            let fromDate = Calendar.current.startOfDay(for: Date())
            let toDate = Calendar.current.startOfDay(for: apiServiceShared.currentDate)
            let dateDifference = Calendar.current.dateComponents([.day], from: fromDate, to:  toDate).day ?? 0
            if(dateDifference >= assetBookingWindow && assetBookingWindow != 0) {
                continue
            }
            guard let schedules = asset.policies?.schedule else { return 0 }
            availableSpaces += self.countAvailableSpaces(schedules: schedules, calendar: calendar, deviceStartTime: deviceStartTime, deviceEndTime: deviceEndTime)
        }
        return availableSpaces
    }

    private func countAvailableSpaces(schedules: [Schedule], calendar: Foundation.Calendar, deviceStartTime: Date, deviceEndTime: Date) -> Int {
        var availableSpaces = 0
        for schedule in schedules {
            let scheduleStartTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.startTime ?? ""))
            let scheduleEndTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.endTime ?? ""))
            let hourScheduleStartTime = calendar.component(.hour, from: scheduleStartTime)
            let minuteScheduleStartTime = calendar.component(.minute, from: scheduleStartTime)
            let hourScheduleEndTime = calendar.component(.hour, from: scheduleEndTime)
            let minuteScheduleEndTime = calendar.component(.minute, from: scheduleEndTime)
            let scheduleStartDate = calendar.date(bySettingHour: hourScheduleStartTime, minute: minuteScheduleStartTime, second: 0, of: Date())
            let scheduleEndDate = calendar.date(bySettingHour: hourScheduleEndTime, minute: minuteScheduleEndTime, second: 0, of: Date())
            if let startDate = scheduleStartDate, let endDate = scheduleEndDate {
                let validStartTimeHour = deviceStartTime.isBetween(startDate, and: endDate)
                let validEndTimeHour = deviceEndTime.isBetween(startDate, and: endDate)
                if (validStartTimeHour && validEndTimeHour) {
                    availableSpaces += 1
                    break
                }
            }
        }
        return availableSpaces
    }
    
    func getAvaliableSpaceFromAreaById(areaId: String) -> Int {
        return dataLayout.filter {$0.areaId == areaId}.count
    }
    
    func getArea() -> Area {
       return apiServiceShared.selectedArea
    }

    func callFuncToGetAreaData() {
        apiServiceShared.apiToGetAreaData { (result) in
            switch (result) {
            case .success(let areaData):
                self.dataArea = self.convertAreaToAreaSpace(areaData: areaData)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func convertAreaToAreaSpace(areaData: [Area]) -> [AreaSpace] {
        var areaSpaces = [AreaSpace]()
        for item in areaData {
            areaSpaces.append(AreaSpace(area: item, space: getAvaliableSpaceFromAreaById(areaId: item.id!)))
        }
        let areaSpacesAux = areaSpaces
        for item in areaSpacesAux where item.space! == 0 {
                areaSpaces.removeAll(where: {
                    $0.area?.id == item.area?.id
                })
                areaSpaces.append(item)
        } 
        return areaSpaces
    }
    
    func callFuncToGetOverlappedBookings() {
        apiServiceShared.apiToGetOverlappedBookings {(result) in
            switch (result) {
            case .success(let bookings):
                self.overlappedBookings = bookings
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToGetLayoutData() {
        let idList = overlappedBookings.map { $0.assetId ?? "" }.convertToIdString()
        apiServiceShared.apiToGetWorkstationDataFromABuilding(idList: idList) { (result) in
            switch (result) {
            case .success(let layoutData):
                self.dataLayout = layoutData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToGetUserData() {
        userServiceShared.apiToGetUserData {(result) in
            switch (result) {
            case .success(let userData):
                self.apiServiceShared.sessionUser = userData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
}
