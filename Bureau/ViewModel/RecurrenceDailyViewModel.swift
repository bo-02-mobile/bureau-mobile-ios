//
//  RecurrenceDailyOption.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 11/3/22.
//

import Foundation

class RecurrenceDailyViewModel {
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getTempDate() -> Date {
        return apiSharedService.tempCurrentDate
    }
    
    func getTempRecurrenceRule() -> RecurrenceRule {
        return apiSharedService.tempRecurrenceRule
    }
    
    func getEndDate() -> Date {
        return apiSharedService.tempRecurrenceRule.endDate
    }
    
    func setRecurrenceRules(every: String) {
        let endDate = getEndDate()
        apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: true, type: Constants.RecurrentOptions.daily, endDate: endDate, onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: every)
    }
}
