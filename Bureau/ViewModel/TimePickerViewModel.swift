//
//  TimePickerViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/15/21.
//

import Foundation

class TimePickerViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var mainViewModel: MainViewModel = MainViewModel()
    
    func getStartTime() -> TimeHour {
        return Helpers.getHourAndMinuteFromDate(date: apiSharedService.startHourDate)
    }

    func getEndTime() -> TimeHour {
        return Helpers.getHourAndMinuteFromDate(date: apiSharedService.endHourDate)
    }

    func setStartTimeAndEndTime(startHour: Date, endHour: Date) {
        mainViewModel.setStartTimeAndEndTime(startHourTime: startHour, endHourTime: endHour)
    }
}
