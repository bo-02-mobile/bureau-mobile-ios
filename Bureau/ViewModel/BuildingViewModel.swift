//
//  BuildingViewModel.swift
//  Bureau
//
//  Created by user on 7/18/21.
//

import Foundation

class BuildingViewModel: ErrorViewModel {

    // MARK: - Variables
    var bindBuildingViewModelToController : (() -> Void) = {}
    var bindAreasNumberViewModelToController : (() -> Void) = {
        // will be executed every time the number of areas changes
    }
    
    var apiServiceShared: APIServiceProtocol
    
    // MARK: - Functions
 
    override init() {
        self.apiServiceShared = ApiService.shared
    }

    init(apiService: APIServiceProtocol) {
        self.apiServiceShared = apiService
    }
    
    private(set) var dataBuilding : [Building]! {
        didSet {
            if !dataBuilding.isEmpty {
                apiServiceShared.selectedBuilding = dataBuilding[0]
                ApiService.shared.buildingData = self.dataBuilding
            }
            self.bindBuildingViewModelToController()
        }
    }
    
    func setBuilding(building: Building) {
        apiServiceShared.selectedBuilding = building
    }
    
    func setBuildingId(buildingID: String) {
        apiServiceShared.buildingId = buildingID
    }
    
    func callFuncToGetBuildingData() {
        apiServiceShared.apiToGetBuildingData {(result) in
            switch (result) {
            case .success(let buildingData):
                self.dataBuilding = buildingData.filter { $0.published == true }
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToGetAreasNumberOfBuilding(buildingId: String?) {
        apiServiceShared.apiToGetAreasNumberOfBuilding(buildingId: buildingId) { (result) in
            guard let index = self.dataBuilding?.firstIndex(where: { $0.id == buildingId }) else {
                self.displayErrorInView(error: .internalError)
                return
            }
            switch (result) {
            case .success(let areasNumber):
                self.dataBuilding[index].areasNumber = areasNumber
                self.bindAreasNumberViewModelToController()
            case .failure(let error):
                self.dataBuilding[index].areasNumber = 0
                self.bindAreasNumberViewModelToController()
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getBuildingTimezone(buildingId: String) -> String? {
        guard let safeBuildings = self.dataBuilding else {
            return nil
        }
        for building in safeBuildings where building.id == buildingId {
            return building.timezone
        }
        return nil
    }
}
