//
//  LayoutScrollViewModel.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 7/9/21.
//

import Foundation

class LayoutScrollViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getShowOnlyAvailableSpaces() -> Bool {
        return apiSharedService.showOnlyAvailableSpaces
    }
}
