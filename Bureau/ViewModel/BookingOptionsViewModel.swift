//
//  BookingOptionsViewModel.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 3/3/22.
//

import Foundation

class BookingOptionsViewModel: ErrorViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var mainViewModel: MainViewModel = MainViewModel()
    var bindFIndUserViewModelToController : (() -> Void) = {
        // will reatreave user info and executed on its view controller
    }
    
    func getStartTime() -> TimeHour {
        return Helpers.getHourAndMinuteFromDate(date: apiSharedService.startHourDate)
    }

    func getEndTime() -> TimeHour {
        return Helpers.getHourAndMinuteFromDate(date: apiSharedService.endHourDate)
    }
    
    func getDate() -> Date {
        return apiSharedService.currentDate
    }
    
    func setDate(date: Date) {
        apiSharedService.currentDate = date
    }
    
    func getTempDate() -> Date {
        return apiSharedService.tempCurrentDate
    }
    
    func setTempDate() {
        apiSharedService.tempCurrentDate = apiSharedService.currentDate
    }
    
    func getTempRecurrencetype() -> String {
        return apiSharedService.tempRecurrenceRule.type
    }
    
    func setRecurrenceRule() {
        if(apiSharedService.tempRecurrenceRule.type == Constants.RecurrentOptions.never) {
            apiSharedService.isRecurrent = false
        } else {
            apiSharedService.isRecurrent = true
        }
        apiSharedService.recurrenceRule = apiSharedService.tempRecurrenceRule
    }
    
    func setRecurrenceWhenDismiss() {
        apiSharedService.tempRecurrenceRule = apiSharedService.recurrenceRule
    }
    
    func getUserName() -> String {
        return apiSharedService.sessionUser?.name ?? Constants.UserValidation.emptyName
    }
    
    func getSelectedUser() -> User {
        return apiSharedService.selectedUser ?? apiSharedService.sessionUser
    }
    
    func setStartTimeAndEndTime(startHour: Date, endHour: Date) {
        mainViewModel.setStartTimeAndEndTime(startHourTime: startHour, endHourTime: endHour)
    }
}
