//
//  LoginViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 6/5/21.
//

import Foundation
import AppAuth

class LoginViewModel: ErrorViewModel {

    // MARK: - Variables
    var apiSharedService: AuthServiceProtocol = AuthService.shared
    var bindLoginViewModelToController : ((Bool) -> Void) = { _ in }
    var bindAuthRequestViewModelToController : ((OIDAuthorizationRequest) -> Void) = { _ in }

    // MARK: - Functions
    func callFuncToSetAuthState(authState: OIDAuthState?) {
        apiSharedService.setAuthState(authState) { (result) in
            switch (result) {
            case .success(let wasLoginSuccessful):
                if wasLoginSuccessful {
                    self.callFuncToGetUserData()
                    return
                }
                self.displayErrorInView(error: .internalError)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func tryLogout(thenLogin: Bool) {
        if(apiSharedService.getIslogoutPendingFlag()) {
            apiSharedService.signOut({ (result) in
                switch (result) {
                case .success(_): break
                case .failure(let error):
                    self.displayErrorInView(error: error)
                }
                if thenLogin {
                    self.callFuncToGetAuthorizationRequest()
                }
            })
        } else if thenLogin {
            self.callFuncToGetAuthorizationRequest()
        }
    }

    func callFuncToGetAuthorizationRequest() {
        apiSharedService.getOIDAuthorizationRequest { (result) in
            switch (result) {
            case .success(let authorizationRequest):
                self.bindAuthRequestViewModelToController(authorizationRequest)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToGetUserData() {
        UserService.shared.apiToGetUserData {(result) in
            switch (result) {
            case .success(let userData):
                UserDefaultsService.shared.saveUserInfo(user: userData) { (response) in
                    switch (response) {
                    case .success(_):
                        self.bindLoginViewModelToController(true)
                    case .failure(let error):
                        self.displayErrorInView(error: error)
                    }
                }
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
}
