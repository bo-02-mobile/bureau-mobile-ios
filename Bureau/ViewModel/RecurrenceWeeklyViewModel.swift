//
//  RecurrenceWeeklyViewModel.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 11/3/22.
//

import Foundation
class RecurrenceWeeklyViewModel {
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getTempDate() -> Date {
        return apiSharedService.tempCurrentDate
    }
    
    func getEndDate() -> Date {
        return apiSharedService.tempRecurrenceRule.endDate
    }
    
    func getTempRecurrenceRule() -> RecurrenceRule {
        return apiSharedService.tempRecurrenceRule
    }
    
    func setRecurrenceRules(every: String, onDays: [String]) {
        let endDate = getEndDate()
        apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: true, type: Constants.RecurrentOptions.weekly, endDate: endDate, onDays: onDays, onMonth: nil, onWeek: nil, onDate: nil, every: every)
    }
}
	
