//
//  ProfileViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/20/21.
//

import Foundation
import WidgetKit

class ProfileViewModel: ErrorViewModel {

    var apiSharedService: AuthServiceProtocol = AuthService.shared
    var bindLogOutToViewController: (() -> Void) = { }
    var bindUserToViewController : ((User) -> Void) = { _ in }

    private(set) var logOutSuccessfully : Bool! {
        didSet {
            self.bindLogOutToViewController()
        }
    }

    private(set) var dataUser : User! {
        didSet {
            self.bindUserToViewController(dataUser)
        }
    }

    func callFuncToLogOut() {
        apiSharedService.signOut { (result) in
            switch (result) {
            case .success(let wasLogoutSuccessful):
                if wasLogoutSuccessful {
                    self.logOutSuccessfully = wasLogoutSuccessful
                    self.clearWidgetData()
                    return
                }
                self.displayErrorInView(error: CustomError.internalError)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }

    func callFuncToGetUserData() {
        UserDefaultsService.shared.getUserInfo { (result) in
            switch (result) {
            case .success(let userData):
                self.dataUser = userData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func clearWidgetData() {
        UserDefaultsService.shared.defaultsWidget?.setValue([], forKey: Constants.UserDefaultsKeys.bookingInfo)
        UserDefaultsService.shared.defaultsWidget?.setValue("", forKey: Constants.UserDefaultsKeys.currentURL)
        UserDefaultsService.shared.defaultsWidget?.setValue("", forKey: Constants.UserDefaultsKeys.userEmail)
        UserDefaultsService.shared.defaultsWidget?.setValue("", forKey: Constants.UserDefaultsKeys.userAuth)
        WidgetCenter.shared.reloadAllTimelines()
    }
}
