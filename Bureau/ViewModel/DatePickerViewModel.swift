//
//  DatePickerViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/14/21.
//

import Foundation

class DatePickerViewModel {
    
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    // MARK: - Functions
    func getDate() -> Date {
        return apiServiceShared.currentDate
    }
    
    func getTempDate() -> Date {
        return apiServiceShared.tempCurrentDate
    }
    
    func getEndDate() -> Date {
        return apiServiceShared.tempRecurrenceRule.endDate
    }
    
    func setDate(date: Date, pickerSetType: String) {
        switch(pickerSetType) {
        case Constants.DatePickerSetType.date:
            apiServiceShared.currentDate = date
            apiServiceShared.tempCurrentDate = date
        case Constants.DatePickerSetType.tempDate:
            apiServiceShared.tempCurrentDate = date
        case Constants.DatePickerSetType.endDate:
            apiServiceShared.tempRecurrenceRule.endDate = date
        default:
            return
        }
    }

}
