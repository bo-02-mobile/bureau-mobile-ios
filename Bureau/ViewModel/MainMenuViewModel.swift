//
//  MainMenuViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/12/21.
//

import Foundation

class MainMenuViewModel {

    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    // MARK: - Functions
    func getDate() -> Date {
        return apiSharedService.currentDate
    }

    func getStartTime() -> Date {
        return apiSharedService.startHourDate
    }

    func getEndTime() -> Date {
        return apiSharedService.endHourDate
    }
    
    func getEnabledFiltersNumber() -> Int {
        return apiSharedService.enabledFilters
    }
}
