//
//  CancelBookingViewModel.swift
//  Bureau
//
//  Created by user on 7/6/21.
//

import Foundation

class CancelBookingViewModel: ErrorViewModel {

    // MARK: - Variables
    var bindCancelBookingResponseToController : (() -> Void) = { }
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    
    var workstationID: String?
    var workstationCode: String?
    var bookingID: String?
    // MARK: - Functions
    
    func initViewModel(workstationCode:String, bookingID: String) {
        self.workstationCode = workstationCode
        self.bookingID = bookingID
    }
    
    func callFuncToDeleteBooking() {
        apiServiceShared.apiToDeleteBooking(bookingID: bookingID!) { (result) in
            switch (result) {
            case .success:
                self.bindCancelBookingResponseToController()
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
}
