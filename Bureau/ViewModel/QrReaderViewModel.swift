//
//  QrReaderViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 6/19/21.
//

import Foundation

class QrReaderViewModel: ErrorViewModel {

    var apiSharedService: APIServiceProtocol = ApiService.shared
    var bookingsList: [Booking]? = []
    
    // MARK: - Variables
    var bindConfirmateBookingToController : ((Booking) -> Void) = { _ in
        // will be executed on tis view controller with its booking detail
    }
    var bindQRBookingValidateToController: ((Booking) -> Void) = { _ in
        // will be executed on its view controller when a booking validation is done
    }

    // MARK: - Functions
    func callFuncToConfirmateABooking(bookingId: String) {
        apiSharedService.apiToConfirmABooking(bookingId: bookingId) { (result) in
            switch (result) {
            case .success(let bookingConfirmation):
                self.bindConfirmateBookingToController(bookingConfirmation)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func callFuncToValidateBookingAutomatically(bookingId: String) {
        apiSharedService.apiToConfirmABooking(bookingId: bookingId) { (result) in
            switch (result) {
            case .success(let bookingValidated):
                self.bindQRBookingValidateToController(bookingValidated)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func setWorkstation(workstationId: String, completion: @escaping(Asset?) -> Void) {
        apiSharedService.apiToGetAssetWithID(assetId: workstationId) { result in
            switch (result) {
            case .success(let workstation):
                completion(workstation)
            case .failure(_):
                completion(nil)
            }
        }
    }
    
    func setStartAndEndTime(startHour: Date) {
        let startTimeNormalized = Helpers.normalizeDate(currentDate: startHour)
        apiSharedService.startHourDate = startTimeNormalized
        
        if let safeEndHour = Calendar.current.date(byAdding: .hour, value: 1, to: startTimeNormalized) {
            apiSharedService.endHourDate = safeEndHour
        }
    }
    
    func callFuncToGetBookingsData(email: String) {
        apiSharedService.apiToGetUserBookings(email: email, bookingsTime: .today) { (result) in
            switch (result) {
            case .success(let bookings):
                self.bookingsList = bookings.sorted(by: { ($0.startTime ?? "") < ($1.startTime ?? "") })
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getTimeValidatedBooking(assetId: String) -> (booking: Booking, status: String)? {
        guard let safeBookings = self.bookingsList else {
            return nil
        }
        if safeBookings.isEmpty {
            return nil
        }
        for booking in safeBookings where booking.assetId == assetId {
            if Helpers.checkIfBookingTimeForValidation(booking: booking) {
                return (booking, Constants.QRBookingValidation.bookingToValidate)
            }
            if Helpers.checkForNextReservation(booking: booking) {
                return (booking, Constants.QRBookingValidation.nextBooking)
            }
        }
        return nil
    }
}
