//
//  WorkstationDetailsViewModel.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 17/6/21.
//

import Foundation

class WorkstationDetailsViewModel: ErrorViewModel {

    // MARK: - Variables
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var bindWorkstationToController: ((Asset?) -> Void) = { _ in }
    var bindWorkstationDetailVMToController : (() -> Void) = {}
    var bindCreateBookingToController : ((Booking) -> Void) = { _ in }
    var bindFavoritesToController : ((Bool) -> Void) = { _ in
        // will be executed on its view controller to check if the current asset is user's favorite asset
    }
    var userBookings:[Booking] = []
    var scheduleBookings: [Booking] = []
    var isCurrentAssetFavorite = false
    var selectedAsset: Asset?

    // MARK: - Constants
    let defaults = UserDefaults.standard

    // MARK: - Functions

    private(set) var dataBooking : [Booking]! {
        didSet {
            dataBooking.append(contentsOf: scheduleBookings)
            self.bindWorkstationDetailVMToController()
            self.checkBookingExists()
        }
    }
    
    func loadWorkstationDetails() {
        var workstation = selectedAsset
        let features = workstation?.properties?.labels?.map {
            $0.uppercasingFirst
        }
        workstation?.properties?.labels = features
        self.bindWorkstationToController(workstation)
        _ = self.getSchedules(workstation)
    }

    func callFuncToGetWorkstationBookings() {
        apiSharedService.apiToGetWorkstationBookings(workstationID: selectedAsset?.id) { (result) in
            switch (result) {
            case .success(let dataBooking):
                self.dataBooking = dataBooking
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getSchedules(_ workstation: Asset?) -> [Date] {
        self.scheduleBookings = []
        guard let schedules = workstation?.policies?.schedule, let buildingId = workstation?.buildingID else {
            return []
        }
        let buildingTimezone = ApiService.shared.buildingData
                .first(where: { $0.id == buildingId })?.timezone ?? Constants.DateTimeFormat.utcFormat
        let timezone = TimeZone.init(abbreviation: buildingTimezone) ?? .current
        guard let startOfDay = apiSharedService.currentDate.setTime(hour: 0, min: 0, sec: 0, timeZone: timezone) else {
            return []
        }
        guard let endOfDay = apiSharedService.currentDate.setTime(hour: 23, min: 59, sec: 59, timeZone: timezone) else {
                return []
        }
    
        var calendar = Calendar.current
        calendar.timeZone = timezone
        var dates: [Date] = [startOfDay]
        schedules.forEach { schedule in
            let scheduleStartTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.startTime ?? ""))
            let scheduleEndTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.endTime ?? ""))
            let hourScheduleStartTime = calendar.component(.hour, from: scheduleStartTime)
            let minuteScheduleStartTime = calendar.component(.minute, from: scheduleStartTime)
            let hourScheduleEndTime = calendar.component(.hour, from: scheduleEndTime)
            let minuteScheduleEndTime = calendar.component(.minute, from: scheduleEndTime)
            let scheduleDateStart = apiSharedService.currentDate.setTime(hour: hourScheduleStartTime, min: minuteScheduleStartTime, sec: 0, timeZone: timezone) ?? Date()
            let scheduleDateEnd = apiSharedService.currentDate.setTime(hour: hourScheduleEndTime, min: minuteScheduleEndTime, sec: 0, timeZone: timezone) ?? Date()
            if scheduleDateStart < scheduleDateEnd {
                dates.append(scheduleDateStart)
                dates.append(scheduleDateEnd)
            }
        }
        dates.append(endOfDay)
        if dates.count <= 2 {
            return []
        }
        if Helpers.isTimePlusOneMinute(dates[2], equalsTo: dates[1]) {
            return []
        }
        setScheduleBookings(dates: dates, buildingId: buildingId)
        return dates
    }
    
    func setScheduleBookings(dates: [Date], buildingId: String) {
        var schedules: [Booking] = []
        for index in stride(from: 0, to: dates.count, by: 2) {
            let startString = Helpers.convertDateTimeToDateString(date: dates[index])
            let endString = Helpers.convertDateTimeToDateString(date: dates[index + 1])
            schedules.append(Booking(
                id: Constants.BookingPolicies.scheduleBookingId, areaName: "",
                startTime: startString, endTime: endString, isRecurrent: false,
                isPermanent: false, isConfirmed: false, buildingId: buildingId,
                areaId: "", assetId: "", owner: Owner(id: "", preferredUsername: "", email: ""),
                assetCode: "", assetType: "", createdAt: "", updatedAt: "", confirmedAt: nil, reservationMessage: "", status: nil, recurrenceId: nil))
        }
        self.scheduleBookings = schedules
    }
    
    func checkBookingExists() {
        var currentUserEmail: String!
        getUserEmail { (result) in
            switch (result) {
            case .success(let userEmail):
                currentUserEmail = userEmail
            case .failure(_):
                return
            }
        }
        self.userBookings.removeAll()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        for booking in self.dataBooking {
            let currentStartTime = dateFormatter.date(from: booking.startTime ?? "")
            let currentEndTime = dateFormatter.date(from: booking.endTime ?? "")
            let selectedStartTime = ApiService.shared.startHourDate
            let selectedEndTime = ApiService.shared.endHourDate
            if let startTime = currentStartTime, let endTime = currentEndTime,
               booking.owner?.email == currentUserEmail {
                let validSelectedStartTime = selectedStartTime.isBetween(startTime, and: endTime)
                let validSelectedEndTime = selectedEndTime.isBetween(startTime, and: endTime)
                if validSelectedStartTime || validSelectedEndTime {
                    userBookings.append(booking)
                } else {
                    let validScheduleStartTime = startTime.isBetween(selectedStartTime, and: selectedEndTime)
                    let validScheduleEndTime = endTime.isBetween(selectedStartTime, and: selectedEndTime)
                    if validScheduleStartTime || validScheduleEndTime {
                        userBookings.append(booking)
                    }
                }
            }
        }
    }
    
    func getUserEmail(completion: @escaping (Result<String, CustomError>) -> Void) {
        UserDefaultsService.shared.getUserInfo { (result) in
            switch (result) {
            case .success(let userData):
                guard let safeUserName = userData.email else {
                    completion(.failure(.internalError))
                    return
                }
                completion(.success(safeUserName))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func callFuncToCreateABooking() {
        apiSharedService.apiToCreateABooking(assetId: selectedAsset?.id ?? "", buildingId: selectedAsset?.buildingID ?? "") { (result) in
            switch (result) {
            case .success(let booking):
                self.bindCreateBookingToController(booking)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getDate() -> Date {
        return apiSharedService.currentDate
    }

    func getStartTime() -> Date {
        return apiSharedService.startHourDate
    }
    
    func getStartHour() -> Float {
        let date = apiSharedService.startHourDate
        return Float(Calendar.current.component(.hour, from: date))
    }

    func getEndTime() -> Date {
        return apiSharedService.endHourDate
    }
    
    func setUserBooking(booking: Booking) {
        self.userBookings.removeAll()
        self.userBookings.append(booking)
    }
    
    func checkBookingWindow() -> String {
        let assetBookingWindow = selectedAsset?.policies?.bookingWindow ?? 0
        var dayComponent = DateComponents()
        dayComponent.day = assetBookingWindow
        let fromDate = Calendar.current.startOfDay(for: Date())
        let toDate = Calendar.current.startOfDay(for: getDate())
        let dateDifference = Calendar.current.dateComponents([.day], from: fromDate, to:  toDate).day ?? 0
        if(dateDifference >= assetBookingWindow && assetBookingWindow != 0) {
            return "The booking policy of this asset only allows booking up to \(assetBookingWindow) day(s) later."
        }
        return ""
    }
    
    // Function is not being use. Remains here in case of future need
    func checkAssetMaxMinUsage() -> String {
        guard let assetMin = selectedAsset?.policies?.min else { return "" }
        guard let assetMax = selectedAsset?.policies?.max else { return "" }
        guard let minutesDifference = Calendar.current.dateComponents([.minute], from: getStartTime(), to: getEndTime()).minute else { return "" }
        if (minutesDifference < assetMin && minutesDifference >= 0) {
            return "The minimum booking time for this asset is \(Helpers.convertMinutesToHourMinuteString(minutes: assetMin)) hours."
        } else if (minutesDifference > assetMax && minutesDifference >= 0) {
            return "The maximum booking time for this asset is \(Helpers.convertMinutesToHourMinuteString(minutes: assetMax)) hours."
        }
        return ""
    }
        
    func checkBookingPermanent() -> String {
        if let ownerName = selectedAsset?.properties?.owner?.preferredUsername, !ownerName.isEmpty {
            return ownerName
        }
        if let ownerName = selectedAsset?.properties?.owner?.email, !ownerName.isEmpty {
            return ownerName
        }
        return ""
    }
        
    func resetRecurrenceRules() {
        let recurrenceRules = RecurrenceRule(isCustom: false, type: Constants.RecurrentOptions.never, endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
        apiSharedService.isRecurrent = false
        apiSharedService.tempRecurrenceRule = recurrenceRules
        apiSharedService.recurrenceRule = recurrenceRules
    }
    
    func callFuncToGetUserData() {
        UserService.shared.apiToGetUserData {(result) in
            switch (result) {
            case .success(let userData):
                self.apiSharedService.sessionUser = userData
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func getFavoriteAsset() {
        apiSharedService.apiToGetUserProfileInfo { (result) in
            switch (result) {
            case .success(let userProfileInfo):
                let assetId = self.selectedAsset?.id
                let isFavorite = userProfileInfo.favoriteAssets?.contains(assetId ?? "") ?? false
                self.bindFavoritesToController(isFavorite)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            
        }
    }
    
    func deleteFavoriteAsset() {
        let assetId = selectedAsset?.id
        apiSharedService.apiToDeletefavorite(assetId: assetId ?? "") { (result) in
            switch result {
            case .success(let userProfileInfo):
                let isFavorite = userProfileInfo.favoriteAssets?.contains(assetId ?? "") ?? false
                self.bindFavoritesToController(isFavorite)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func createFavoriteAsset() {
        apiSharedService.apiToCreateFavorite(assetId: selectedAsset?.id ?? "") { result in
            switch result {
            case .success(let userProfileInfo):
                let assetId = self.selectedAsset?.id
                let isFavorite = userProfileInfo.favoriteAssets?.contains(assetId ?? "") ?? false
                self.bindFavoritesToController(isFavorite)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func createOrDeleteFavorite() {
        if isCurrentAssetFavorite {
            deleteFavoriteAsset()
        } else {
            createFavoriteAsset()
        }
    }
    
}
