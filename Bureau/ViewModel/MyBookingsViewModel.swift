//
//  MyBookingsViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/2/21.
//

import Foundation
import WidgetKit

class MyBookingsViewModel: ErrorViewModel {

    // MARK: - Variables
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var bindBookingsViewModelToController : (() -> Void) = {}
    var bindUserToViewController : ((User) -> Void) = { _ in }
    var dataUserPublic: User!
    // MARK: - Functions
    var dataBookings : [Booking]! {
        didSet {
            self.bindBookingsViewModelToController()
        }
    }
    var todayBookingsList: [Booking] = []
    var upcomingBookingsList: [Booking] = []
    var permanentBookingsList: [Booking] = []
    
    private(set) var dataUser : User! {
        didSet {
            self.bindUserToViewController(dataUser)
        }
    }
    
    func callFuncToGetAllBookings(email: String) {
        let group: DispatchGroup = DispatchGroup()
        self.todayBookingsList.removeAll()
        self.upcomingBookingsList.removeAll()
        self.permanentBookingsList.removeAll()
        group.enter()
        apiSharedService.apiToGetUserBookings(email: email, bookingsTime: .today) { (result) in
            switch (result) {
            case .success(let todayBookings):
                self.todayBookingsList = self.checkBookingBuildingExists(bookings: todayBookings)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            group.leave()
        }
        group.enter()
        apiSharedService.apiToGetUserBookings(email: email, bookingsTime: .upcoming) { (result) in
            switch (result) {
            case .success(let upcomingBookings):
                self.upcomingBookingsList = self.checkBookingBuildingExists(bookings: upcomingBookings)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            group.leave()
        }
        group.enter()
        apiSharedService.apiToGetUserPermanentAssets(email: email) { (result) in
            switch (result) {
            case .success(let assets):
                let bookings = self.setAssetsAsPermanentBookings(assets: assets)
                self.permanentBookingsList = self.checkBookingBuildingExists(bookings: bookings)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            group.leave()
        }
        group.notify(queue: DispatchQueue.main) {
            self.callFuncToGetPermanentAssetsInfo()
        }
    }
    
    func callFuncToGetPermanentAssetsInfo() {
        let groupAssets: DispatchGroup = DispatchGroup()
        for index in self.permanentBookingsList.indices {
            groupAssets.enter()
            self.apiSharedService.apiToGetAreaInfo(areaId: self.permanentBookingsList[index].areaId ?? "") { (result) in
                switch (result) {
                case .success(let areaInfo):
                    self.permanentBookingsList[index].areaName = areaInfo.name
                case .failure(let error):
                    self.displayErrorInView(error: error)
                }
                groupAssets.leave()
            }
        }
        groupAssets.notify(queue: DispatchQueue.main) {
            self.dataBookings = self.permanentBookingsList + self.todayBookingsList + self.upcomingBookingsList
            self.setWidgetData()
        }
    }
    
    func checkBookingBuildingExists(bookings: [Booking]) -> [Booking] {
        var filteredBookings: [Booking] = []
        let buildingsList = apiSharedService.buildingData
        buildingsList?.forEach { building in
            filteredBookings.append(contentsOf: bookings.filter { booking in booking.buildingId == building.id })
        }
        return filteredBookings.sorted(by: { ($0.startTime ?? "") < ($1.startTime ?? "") })
    }
    
    func setAssetsAsPermanentBookings(assets: [Asset]) -> [Booking] {
        var permanentBookings: [Booking] = []
        assets.forEach { asset in
            let newBooking = Booking(id: asset.id, areaName: "", startTime: asset.createdAt, endTime: asset.createdAt, isRecurrent: false, isPermanent: true,
                             isConfirmed: false, buildingId: asset.buildingID, areaId: asset.areaId, assetId: asset.id, owner: asset.properties?.owner, assetCode: asset.code,
                             assetType: asset.properties?.type, createdAt: asset.createdAt, updatedAt: asset.updatedAt, confirmedAt: nil, reservationMessage: nil, status: nil, recurrenceId: nil)
            permanentBookings.append(newBooking)
        }
        return permanentBookings
    }
    
    func callFuncToGetUserData() {
        if self.dataUser != nil {
            self.bindUserToViewController(self.dataUser)
            return
        }
        UserService.shared.apiToGetUserData {(result) in
            switch (result) {
            case .success(let userData):
                self.dataUser = userData
                self.dataUserPublic = self.dataUser
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }

    private func setWidgetData() {
        let arraySlice = self.dataBookings.prefix(2)
        let newArray = Array(arraySlice)
        var bookingWidgetArray: [BookingWidget] = []
        newArray.forEach { booking in
            let booked = BookingWidget(
                areaName: booking.areaName,
                assetCode: booking.assetCode,
                startTime: Helpers.getDateToHourLabel(date: booking.startTime ?? ""),
                endTime: Helpers.getDateToHourLabel(date: booking.endTime ?? ""),
                date: Helpers.getWidgetDateLabel(date: booking.startTime ?? ""),
                isConfirmed: booking.isConfirmed,
                isPermanent: booking.isPermanent
            )
            bookingWidgetArray.append(booked)
        }
    
        UserDefaultsService.shared.setStructArray(bookingWidgetArray, forKey: Constants.UserDefaultsKeys.bookingInfo)
        WidgetCenter.shared.reloadAllTimelines()
    }
}
