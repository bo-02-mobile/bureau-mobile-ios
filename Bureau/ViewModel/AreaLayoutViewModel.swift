//
//  AreaLayoutViewModel.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import Foundation

class AreaLayoutViewModel: ErrorViewModel {
    
    // MARK: - Variables
    var bindLayoutViewModelToController : ((Area) -> Void) = {_ in }
    var apiServiceShared: APIServiceProtocol = ApiService.shared
    // MARK: - Functions
    private(set) var dataWorkstation: [Asset]! {
        didSet {
            self.callFuncToGetFilteredWorkstationData()
        }
    }
    
    private var filteredWorkstationsData: [Asset]! {
        didSet {
            self.callFuncToGetAreaBookings()
        }
    }
    
    private(set) var dataBookings: [Booking]! {
        didSet {
            self.callFuncToGetLayoutData()
        }
    }
    
    private(set) var layout: Area! {
        didSet {
            self.bindLayoutViewModelToController(layout)
        }
    }
    
    func getArea() -> Area! {
        return apiServiceShared.selectedArea
    }
    
    func getSelectedAreaCode() -> String {
        guard let safeSelectedAreaCode = apiServiceShared.selectedArea?.code else {
            return ""
        }
        return safeSelectedAreaCode
    }
    
    // Get all assets
    func callFuncToGetAllWorkstationForArea() {
        apiServiceShared.apiToGetAllWorkstationDataFromAnArea { (result) in
            switch (result) {
            case .success(let assets):
                self.dataWorkstation = assets.sorted(by: { $0.id < $1.id})
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }

    // Get filtered assets
    func callFuncToGetFilteredWorkstationData() {
        apiServiceShared.apiToGetWorkstationDataFromAnArea { (result) in
            switch (result) {
            case .success(let assets):
                self.filteredWorkstationsData = assets.sorted(by: { $0.id < $1.id})
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }

    func callFuncToGetLayoutData() {
        apiServiceShared.apiToGetLayoutDataFromAnArea { (result) in
            switch (result) {
            case .success(var dataLayout):
                guard var safeMarkers = dataLayout.markers else {
                    return
                }
                self.dataWorkstation?.forEach { workstation in
                    if let isPublic = workstation.assetPublic, !isPublic {
                        let id: Int? = safeMarkers.firstIndex(where: { $0.workstationId == workstation.id })
                        if let safeId = id {
                            safeMarkers[safeId].isPublic = false
                        }
                    }
                }
                safeMarkers = safeMarkers.filter({ $0.isPublic == true })
                self.checkWorkstationBookings(dataMarkers: &safeMarkers)
                self.checkWorkstationBookingWindows(dataMarkers: &safeMarkers, self.dataWorkstation)
                self.checkWorkstationSchedules(dataMarkers: &safeMarkers, self.dataWorkstation)
                safeMarkers = safeMarkers.sorted(by: { $0.workstationId < $1.workstationId })
                dataLayout.markers = safeMarkers
                self.layout = dataLayout
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }

    func getWorkstationAvailability(dataMarkers: inout [Marker]) {
        if dataMarkers.isEmpty {
            return
        }
        dataMarkers = dataMarkers.filter({ self.isAvailable(marker: $0) })
    }

    func isAvailable(marker: Marker) -> Bool {
        for workstation in filteredWorkstationsData where workstation.id == marker.workstationId {
            if workstation.properties?.owner == nil {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    func callFuncToGetAreaBookings() {
        apiServiceShared.apiToGetAreaBookings { (result) in
            switch (result) {
            case .success(let bookings):
                self.dataBookings = bookings.sorted(by: { $0.id < $1.id})
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func checkWorkstationBookings(dataMarkers: inout [Marker]) {
        var bookingIds: Set<String> = []
        for booking in self.dataBookings {
            bookingIds.insert(booking.assetId ?? "")

        }
        for index in 0..<dataMarkers.count {
            let isBooked = bookingIds.contains(dataMarkers[index].workstationId)
            let isAvailable = self.isAvailable(marker: dataMarkers[index])
            dataMarkers[index].isAvailable = !isBooked && isAvailable
        }
    }
    
    func checkWorkstationBookingWindows(dataMarkers: inout [Marker], _ dataWorkstation: [Asset]?) {
        guard let now = Date().setTime(hour: 0, min: 0, sec: 0) else { return }
        guard let date = apiServiceShared.currentDate.setTime(hour: 0, min: 0, sec: 0) else { return }
        guard let workstations = dataWorkstation else { return }
        for workstation in workstations {
            guard let window = workstation.policies?.bookingWindow, window > 0 else { continue }
            guard let markerIndex = dataMarkers.firstIndex(where: { $0.workstationId == workstation.id }) else { continue }
            if !dataMarkers[markerIndex].isAvailable {
                continue
            }
            if date >= now.addingTimeInterval(Double(window * Constants.DateTimeFormat.oneDayInSeconds)) {
                dataMarkers[markerIndex].isAvailable = false
            }
        }
    }
    
    func checkWorkstationSchedules(dataMarkers: inout [Marker], _ dataWorkstation: [Asset]?) {
        let today = apiServiceShared.currentDate
        let startString = Helpers.convertDateFormat(inputDate: today, inputHour: apiServiceShared.startHourDate)
        let endString = Helpers.convertDateFormat(inputDate: today, inputHour: apiServiceShared.endHourDate)
        let buildingTimezone = apiServiceShared.selectedBuilding.timezone ?? Constants.DateTimeFormat.utcFormat
        let start = Helpers.convertDateStringToDateTimezone(dateString: startString, timezone: buildingTimezone)
        let end = Helpers.convertDateStringToDateTimezone(dateString: endString, timezone: buildingTimezone)
        guard let workstations: [Asset] = dataWorkstation else { return }
        for workstation in workstations {
            guard let schedules = workstation.policies?.schedule else { continue }
            guard let markerIndex = dataMarkers.firstIndex(where: { $0.workstationId==workstation.id}) else { continue }
            if !dataMarkers[markerIndex].isAvailable {
                continue
            }
            setScheduledMarkersAvailability(dataMarkers: &dataMarkers, schedules: schedules, markerIndex: markerIndex, start: start, end: end)
        }
    }
    
    func setScheduledMarkersAvailability(dataMarkers: inout [Marker], schedules: [Schedule], markerIndex: Int, start: Date, end: Date) {
        var calendar = Calendar.current
        let timezone = UserDefaultsService().getBuildingTimezone() ?? Constants.DateTimeFormat.utcFormat
        calendar.timeZone = TimeZone.init(abbreviation: timezone) ?? .current
        let hourStartTime = Calendar.current.component(.hour, from: apiServiceShared.startHourDate)
        let minuteStartTime = Calendar.current.component(.minute, from: apiServiceShared.startHourDate)
        let hourEndTime = Calendar.current.component(.hour, from: apiServiceShared.endHourDate)
        let minuteEndTime = Calendar.current.component(.minute, from: apiServiceShared.endHourDate)
        guard let deviceStartTime = calendar.date(bySettingHour: hourStartTime, minute: minuteStartTime, second: 0, of: Date()) else { return }
        guard let deviceEndTime = calendar.date(bySettingHour: hourEndTime, minute: minuteEndTime, second: 0, of: Date()) else { return }
        for schedule in schedules {
            dataMarkers[markerIndex].isAvailable = false
            let scheduleStartTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.startTime ?? ""))
            let scheduleEndTime = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: schedule.endTime ?? ""))
            let hourScheduleStartTime = calendar.component(.hour, from: scheduleStartTime)
            let minuteScheduleStartTime = calendar.component(.minute, from: scheduleStartTime)
            let hourScheduleEndTime = calendar.component(.hour, from: scheduleEndTime)
            let minuteScheduleEndTime = calendar.component(.minute, from: scheduleEndTime)
            let scheduleStartDate = calendar.date(bySettingHour: hourScheduleStartTime, minute: minuteScheduleStartTime, second: 0, of: Date())
            let scheduleEndDate = calendar.date(bySettingHour: hourScheduleEndTime, minute: minuteScheduleEndTime, second: 0, of: Date())
            if let startDate = scheduleStartDate, let endDate = scheduleEndDate {
                let validStartTimeHour = deviceStartTime.isBetween(startDate, and: endDate)
                let validEndTimeHour = deviceEndTime.isBetween(startDate, and: endDate)
                if (validStartTimeHour && validEndTimeHour) {
                    dataMarkers[markerIndex].isAvailable = true
                    break
                }
            }
        }
    }
    
    func getEnabledFiltersNumber() -> Int {
        return apiServiceShared.enabledFilters
    }
}
