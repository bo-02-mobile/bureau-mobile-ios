//
//  RecurrentOptionsController.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 11/3/22.
//

import Foundation

class RecurrenceOptionsViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    
    func getTempRecurrenceRule() -> RecurrenceRule {
        return apiSharedService.tempRecurrenceRule
    }
    
    func setRecurrenceRules() {
        apiSharedService.tempRecurrenceRule = RecurrenceRule(isCustom: false, type: Constants.RecurrentOptions.never, endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
    }
    
    func updateRecurrenceEndDate() {
        apiSharedService.setRecurrentEndDate()
    }
}
