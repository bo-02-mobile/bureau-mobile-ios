//
//  MainViewModel.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 3/5/22.
//

import Foundation

class MainViewModel: ErrorViewModel {
    
    var apiSharedServices: APIServiceProtocol = ApiService.shared
    var userProfile: UserProfile? {
         didSet {
             callFuncToGetUserGroups(userProfileGroups: userProfile?.groups ?? [])
         }
    }
    
    func callFuncToGetUserProfile() {
        apiSharedServices.apiToGetUserProfileInfo { (result) in
            switch (result) {
            case .success(let userProfileInfo):
                self.userProfile = userProfileInfo
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            
        }
    }
    
    func callFuncToGetUserGroups(userProfileGroups: [String]) {
        apiSharedServices.apiToGetUserGroups(groupIdArray: userProfileGroups) {(result) in
            switch (result) {
            case .success(let userGroups):
                let bookingPermission = userGroups.filter { $0.recurrenceBookings == true }.count
                let bookForOtherPermission = userGroups.filter { $0.bookForOthers == true }.count
                if(bookingPermission > 0) {
                    UserDefaultsService.shared.saveRecurrentBookingsPermission(true)
                } else {
                    UserDefaultsService.shared.saveRecurrentBookingsPermission(false)
                }
                if(bookForOtherPermission > 0) {
                    UserDefaultsService.shared.saveBookForOthersPermission(true)
                } else {
                    UserDefaultsService.shared.saveBookForOthersPermission(false)
                }
                
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func setStartTimeAndEndTime(startHourTime: Date, endHourTime: Date) {
        let calendar = Calendar.current
        let startTimeHour = calendar.component(.hour, from: startHourTime)
        let startTimeMinute = calendar.component(.minute, from: startHourTime)
        let endTimeHour = calendar.component(.hour, from: endHourTime)
        let endTimeMinute = calendar.component(.minute, from: endHourTime)
        let isEqual = validateEqualHours(startTimeHour: startTimeHour, startTimeMinute: startTimeMinute, endTimeHour: endTimeHour, endTimeMinute: endTimeMinute)
        if isEqual {
            return
        }
        let isInvested = validateInvestedHours(startTimeHour: startTimeHour, startTimeMinute: startTimeMinute, endTimeHour: endTimeHour, endTimeMinute: endTimeMinute)
        if isInvested {
            return
        }
        self.apiSharedServices.startHourDate = startHourTime
        self.apiSharedServices.endHourDate = endHourTime        
    }
    
    func validateEqualHours(startTimeHour: Int, startTimeMinute: Int, endTimeHour: Int, endTimeMinute: Int) -> Bool {
        if(startTimeHour == endTimeHour && startTimeMinute == endTimeMinute) {
            let startTime = Calendar.current.date(bySettingHour: endTimeHour, minute: endTimeMinute, second: 0, of: Date())!
            let endTime = Calendar.current.date(byAdding: .minute, value: 1, to: startTime)!
            self.apiSharedServices.startHourDate = startTime
            self.apiSharedServices.endHourDate = endTime
            return true
        }
        return false
    }
    
    func validateInvestedHours(startTimeHour: Int, startTimeMinute: Int, endTimeHour: Int, endTimeMinute: Int) -> Bool {
        if (startTimeHour > endTimeHour || startTimeHour >= endTimeHour && startTimeMinute >= endTimeMinute) {
            let startTime = Calendar.current.date(bySettingHour: endTimeHour, minute: endTimeMinute, second: 0, of: Date())!
            let endTime = Calendar.current.date(bySettingHour: startTimeHour, minute: startTimeMinute, second: 0, of: Date())!
            self.apiSharedServices.startHourDate = startTime
            self.apiSharedServices.endHourDate = endTime
            return true
        }
        return false
    }
}
