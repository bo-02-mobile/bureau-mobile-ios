//
//  ConstantsLocal.swift
//  Bureau
//
//  Created by User on 2/16/22.
//

import Foundation
extension Constants {
    struct Demo {
        static let isInDemoMode = CI.productionFlag != "YES"
        static let isASimulator = false
    }
}
