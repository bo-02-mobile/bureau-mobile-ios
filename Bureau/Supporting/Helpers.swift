//
//  Helpers.swift
//  Bureau
//
//  Created by user on 6/9/21.
//

import UIKit
import FirebaseCrashlytics

struct Helpers {

    // MARK: - Functions
    static func convertDateFormat(inputDate: Date, inputHour: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormatForAPI
        let day = dateFormatter.string(from: inputDate)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormatForAPI
        let hour = dateFormatter.string(from: inputHour)
        return "\(day)T\(hour)Z"
    }
    
    // Returns DateTime string in UTC TimeZone for API from a given date and hour (both Date objects)
    static func convertDateFormatUTC(inputDate: Date, inputHour: Date) -> String {
        return convertDateFormatWithTimeZone(inputDate: inputDate, inputHour: inputHour, timezone: Constants.DateTimeFormat.utcFormat)
    }
    
    // Returns DateTime string in given UTC TimeZone String for API from a given date and hour (both Date objects)
    static func convertDateFormatWithTimeZone(inputDate: Date, inputHour: Date, timezone: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormatForAPI
        let day = dateFormatter.string(from: inputDate)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormatWithSeconds
        let hour = dateFormatter.string(from: inputHour)
        return "\(day)T\(hour)Z"
    }
    
    // Checks if first time plus 1 minute equals second time (both Date objects)
    static func isTimePlusOneMinute(_ first: Date, equalsTo second: Date) -> Bool {
        var calendar = Calendar.current
        guard let timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat) else {
            return false
        }
        calendar.timeZone = timeZone
        var firstHour = calendar.component(.hour, from: first)
        let secondHour = calendar.component(.hour, from: second)
        var firstMinutes = calendar.component(.minute, from: first)
        let secondMinutes = calendar.component(.minute, from: second)
        firstMinutes += 1
        if firstMinutes == 60 {
            firstHour = (firstHour + 1) % 24
            firstMinutes = 0
        }
        return firstHour == secondHour && firstMinutes == secondMinutes
    }

    // From selected time window to string: "Tue, Feb. 01 from 05:32 PM to 07:33 PM"
    static func convertToLiteralDateFormat(currentDate: Date, startHour: Date, endHour: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        let date = dateFormatter.string(from: currentDate)
        let dateFormmaterHour = DateFormatter()
        dateFormmaterHour.dateFormat = Constants.DateTimeFormat.timeFormat
        let initHour = dateFormmaterHour.string(from: startHour)
        let finishHour = dateFormmaterHour.string(from: endHour)
        return "\(date) from \(initHour) to \(finishHour)"
    }

    // Transforms local datetime (in string form) to UTC datetime (in string form)
    static func convertLocalToUTCFormat(stringDate:String, isForQueryParam: Bool) -> String {
        guard let currentTimezone = TimeZone.current.abbreviation() else {
            return stringDate
        }
        return convertDatetimeToUTCFormat(stringDate: stringDate, isForQueryParam: isForQueryParam, timezone: currentTimezone)
    }
    
    // Transforms datetime with given timezone (in string form) to UTC datetime (in string form)
    static func convertDatetimeToUTCFormat(stringDate:String, isForQueryParam: Bool, timezone: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormatToConvert
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        var formattedString = stringDate.replacingOccurrences(of: "Z", with: "")
        if let lowerBound = formattedString.range(of: ".")?.lowerBound {
            formattedString = "\(formattedString[..<lowerBound])"
        }
        guard let date = dateFormatter.date(from: formattedString) else {
            return stringDate
        }
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let dateString = dateFormatter.string(from: date)
        if isForQueryParam {
            return dateString.replacingOccurrences(of: "+0000", with: "Z")
        }
        return dateString
    }

    static func getCurrentDate() -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormatForAPI
        let date = Date()
        return dateFormatter.string(from: date)
    }

    static func convertDateStringToDate(dateString: String) -> Date {
        let correctedDate = self.correctDateFormatFromApi(date: dateString)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let date = dateFormatter.date(from:correctedDate)!
        return date
    }
    
    static func convertDateStringToDateTimezone(dateString: String, timezone: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        dateFormatter.dateFormat = Constants.DateTimeFormat.areaLayoutDateTime
        let date = dateFormatter.date(from: dateString)!
        return date
    }
    
    static func convertDateToDateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    static func convertDateTimeToDateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        let dateString = dateFormatter.string(from: date)
        return dateString
    }

    static func dateLocalToUTC(dateLocal: Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        return dateFormatter.date(from: dateFormatter.string(from:  dateLocal))!
    }

    static func dateUTCToLocal(dateUTC: Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        return dateFormatter.date(from: dateFormatter.string(from: dateUTC))!
    }
    
    // Change a UTC Date hour based on difference betweent given timezone and local timezone
    static func offsetUTCDateToGivenTimezone(dateUTC: Date, timezoneAbbreviation: String) -> Date {
        guard let timezone = TimeZone(abbreviation: timezoneAbbreviation) else {
            return dateUTC
        }
        let current = TimeZone.current
        let diff: Double = Double((timezone.secondsFromGMT()) - (current.secondsFromGMT()))
        return dateUTC.addingTimeInterval(diff)
    }
    
    static func offsetUTCDateToScheduledTimezone(dateUTC: Date, timezoneAbbreviation: String) -> Date {
        guard let timezone = TimeZone(abbreviation: timezoneAbbreviation) else {
            return dateUTC
        }
        let current = TimeZone.current
        let diff: Double = Double((timezone.secondsFromGMT()) - (current.secondsFromGMT()))
        return dateUTC.addingTimeInterval(-diff)
    }

    static func correctDateFormatFromApi(date: String) -> String {
        var dateArray = date.split(separator: ".")
        if dateArray.count == 1 {
            dateArray[0].remove(at: dateArray[0].index(before: dateArray[0].endIndex))
        }
        return dateArray[0] + ".000Z"
    }
    
    static func utcToLocal(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let correctDateStr = correctDateFormatFromApi(date: dateStr)
        if let date = dateFormatter.date(from: correctDateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    static func calendarFormatDate(dateStr: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let correctDateStr = correctDateFormatFromApi(date: dateStr)
        if let date = dateFormatter.date(from: correctDateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
            return dateFormatter.date(from: dateFormatter.string(from: date))
        }
        return nil
    }
    
    static func calendarFormatDateWithTimezone(dateStr: String, timezone: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let correctDateStr = correctDateFormatFromApi(date: dateStr)
        if let date = dateFormatter.date(from: correctDateStr) {
            return Helpers.offsetUTCDateToGivenTimezone(dateUTC: date, timezoneAbbreviation: timezone)
        }
        return nil
    }
    
    static func calendarFormatDateTime(dateStr: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.date(from: dateStr)
    }
    
    static func calendarFormatDateTimeString(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: Constants.DateTimeFormat.utcFormat)
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateTimeFormat
        return dateFormatter.string(from: date!)
    }
    
    static func getErrorMessages(error: CustomError) -> (title: String, message: String) {
        switch error {
        case .noInternetConnection:
            return (Constants.NoConnectedToNetwork.title, Constants.NoConnectedToNetwork.message)
        case .internalError:
            return (Constants.InternalError.title, Constants.InternalError.message)
        case .connectionError:
            return (Constants.ServerConnectionError.title, Constants.ServerConnectionError.message)
        case .bookingConfirmationError:
            return (Constants.BookingConfirmationError.title, Constants.BookingConfirmationError.message)
        case .authenticationFailed:
            return (Constants.AuhtenticationFailed.title, Constants.AuhtenticationFailed.message)
        case .cameraPermissionDenied:
            return (Constants.CameraPermissionDenied.title, Constants.CameraPermissionDenied.message)
        case .scannerNotSupported:
            return (Constants.ScannerNotSupported.title, Constants.ScannerNotSupported.message)
        case .readQrCodeError:
            return (Constants.ReadQrCodeError.title, Constants.ReadQrCodeError.message)
        case .emptyWorkstationId:
            return (Constants.EmptyWorkstationId.title, Constants.EmptyWorkstationId.message)
        case .invalidTime:
            return (Constants.InvalidTime.title, Constants.InvalidTime.message)
        case .notAvailableSpaces:
            return (Constants.NotAvailableSpaces.title, Constants.NotAvailableSpaces.message)
        case .bookingNotFound:
            return (Constants.BookingNotFound.title, Constants.BookingNotFound.message)
        case .serverError(message: let message):
            return (Constants.ServerError.title, message)
        case .noBookingAvailable:
            return (Constants.NoBookingAvailable.title, Constants.NoBookingAvailable.message)
        }
    }

    static func redirectToLoginPage(isForcedLogout: Bool) {
        DispatchQueue.main.async {
            let viewController = SceneDelegate.mainStoryboard.instantiateViewController(withIdentifier: Constants.StoryBoard.loginViewControllerID) as? LoginViewController
            UIApplication.shared.windows.first?.rootViewController = viewController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
            if isForcedLogout {
                viewController?.showErrorMessage(Constants.ForcedLogout.title, Constants.ForcedLogout.message)
            } else {
                viewController?.showErrorMessage(Constants.NormalLogout.title, Constants.NormalLogout.message)
            }
        }
    }

    static func normalizeDate(currentDate: Date) -> Date {
        var startHours = Calendar.current.component(.hour, from: currentDate)
        var startMinutes = Calendar.current.component(.minute, from: currentDate)
        switch startMinutes {
        case 1...30:
            startMinutes = 30
        case 31...59:
            startHours += 1
            startMinutes = 0
        default:
            startMinutes = 0
        }
        if let safeStartHour = Calendar.current.date(bySettingHour: startHours, minute: startMinutes, second: 0, of: Date()) {
            return safeStartHour
        }
        return currentDate
    }

    static func getDateLabel(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        var dateLabel = ""
        if dateFormatter.string(from: date).elementsEqual(dateFormatter.string(from: Date())) {
            dateLabel = "Today"
        } else {
            dateLabel = dateFormatter.string(from: date)
        }
        return dateLabel
    }
    
    static func getSecondDateLabel(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateBookingFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        var dateLabel = ""
        if dateFormatter.string(from: date).elementsEqual(dateFormatter.string(from: Date())) {
            dateLabel = "Today"
        } else {
            dateLabel = dateFormatter.string(from: date)
        }
        return dateLabel
    }
    
    static func getTimeLabel(startDate: Date, endDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.timeFormat
        var timeLabel = ""
        timeLabel = dateFormatter.string(from: startDate)
        timeLabel += " - "
        timeLabel += dateFormatter.string(from: endDate)
        return timeLabel
    }
    
    static func getDateAndTimeLabel(startDate: Date, endDate: Date, date: Date) -> String {
        let dateFormatter = DateFormatter()
        let date = getDateLabel(date: date)
        dateFormatter.dateFormat = Constants.DateTimeFormat.timeFormat
        var timeLabel = ""
        timeLabel = date
        timeLabel += " - "
        timeLabel += dateFormatter.string(from: startDate)
        timeLabel += " to "
        timeLabel += dateFormatter.string(from: endDate)
        return timeLabel
    }
    
    static func getHourAndMinuteFromDate(date: Date) -> TimeHour {
        let startHour = Calendar.current.component(.hour, from: date)
        let startMinute = Calendar.current.component(.minute, from: date)
        let timeHour = TimeHour(hour: startHour, minute: startMinute)
        return timeHour
    }

    static func constructDate(hour: String, minute: String, isAm: Bool) -> Date {
        var components = DateComponents()
        var safeHour = Int(hour) ?? 0
        if safeHour == 12 {
            safeHour = 0
        }
        if isAm {
            components.hour = Int(safeHour)
        } else {
            components.hour = safeHour + 12
        }
        components.minute = Int(minute)
        let date = Calendar.current.date(from: components) ?? Date()
        return date
    }
    
    static func getDatePickerHeight() -> CGFloat {
        var datePickerHeight = ModalType.datePicker.rawValue
        if UIScreen.main.bounds.height > 1100 {
            datePickerHeight += 30
        }
        if UIScreen.main.bounds.height < 800 {
            datePickerHeight -= 30
        }
        if UIScreen.main.bounds.height < 700 {
            datePickerHeight -= 20
        }
        return CGFloat(datePickerHeight)
    }
    
    static func getTimePickerHeight() -> CGFloat {
        var timePickerHeight = ModalType.timePicker.rawValue
        if UIScreen.main.bounds.height < 800 {
            timePickerHeight -= 60
        }
        if UIScreen.main.bounds.height < 700 {
            timePickerHeight -= 40
        }
        return CGFloat(timePickerHeight)
    }
    
    static func isValidTimeHour(hour: String, minute: String) -> Bool {
        guard let safeHour = Int(hour), let safeMinute = Int(minute) else {
            return false
        }
        if safeHour < 1 || safeHour > 12 {
            return false
        }
        if safeMinute < 0 || safeMinute > 59 {
            return false
        }
        return true
    }
    
    static func formatDate(startTime: String, endTime: String, format: String) -> String {
        var dateBookingFormat = format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        let startDate = Helpers.convertDateStringToDate(dateString: startTime)
        let endDate = Helpers.convertDateStringToDate(dateString: endTime)
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        let bookingDay = dateFormatter.string(from: startDate)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormat
        let startHour = dateFormatter.string(from: startDate)
        let endHour = dateFormatter.string(from: endDate)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.date, with: bookingDay)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.startHour, with: startHour)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.endHour, with: endHour)
        
        return dateBookingFormat
    }
    
    static func formatDate(startTime: String, endTime: String, format: String, timezone: String) -> String {
        var dateBookingFormat = format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = Constants.DateTimeFormat.completeDateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        let startDate = Helpers.convertDateStringToDate(dateString: startTime)
        let endDate = Helpers.convertDateStringToDate(dateString: endTime)
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        let bookingDay = dateFormatter.string(from: startDate)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormat
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        let startHour = dateFormatter.string(from: startDate)
        let endHour = dateFormatter.string(from: endDate)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.date, with: bookingDay)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.startHour, with: startHour)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.endHour, with: endHour)
        
        return dateBookingFormat
    }
    
    static func formatDateTime(startTime: String, endTime: String, format: String) -> String {
        var dateBookingFormat = format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
        let startDate: Date? = calendarFormatDateTime(dateStr: startTime)
        let endDate: Date? = calendarFormatDateTime(dateStr: endTime)
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        let bookingDay = dateFormatter.string(from: startDate!)
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourFormat
        let startHour = dateFormatter.string(from: startDate!)
        let endHour = dateFormatter.string(from: endDate!)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.date, with: bookingDay)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.startHour, with: startHour)
        dateBookingFormat = dateBookingFormat.replacingOccurrences(of: Constants.BookingConfirmationModal.endHour, with: endHour)
        
        return dateBookingFormat
    }
    
    // Check if booking is in range of validation
    static func isBookingInTimeForValidation(booking: Booking) -> Bool {
        if booking.confirmedAt != nil {
            return false
        }
        guard let safeStartDate = booking.startTime else { return false }
        let timezone = TimeZone.current.abbreviation() ?? Constants.DateTimeFormat.utcFormat
        let beforeDeadLine = UserDefaultsService.shared.getLimitBeforeDeadLine()
        let afterDeadLine = UserDefaultsService.shared.getLimitAfterDeadLine()
        var startBookDate = Helpers.convertDateStringToDate(dateString: safeStartDate)
        startBookDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startBookDate, timezoneAbbreviation: timezone)
        let startDate = Calendar.current.date(byAdding: .minute, value: beforeDeadLine * -1, to: startBookDate)!
        let endDate = Calendar.current.date(byAdding: .minute, value: afterDeadLine, to: startBookDate)!
        let currentDate = Date()
        if currentDate.isBetween(startDate, and: endDate) {
            return true
        }
        return false
    }
    
    static func dateStringIsToday(dateString: String) -> Bool {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .medium
        let stringN = dateFormatter.string(from: now)
        let date = Helpers.calendarFormatDate(dateStr: dateString)
        let stringS = dateFormatter.string(from: date!)
        return stringS.elementsEqual(stringN)
    }
    
    static func addMinutesToDate(date: Date, minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: date)!
    }
    
    static func constructLabelForValidation(initDate: Date, endDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.timeFormat
        var timeLabel = Constants.BookingConfirmationModal.validableLabel
        timeLabel = timeLabel.replacingOccurrences(of: Constants.BookingConfirmationModal.startHour,
                                                   with: dateFormatter.string(from: initDate))
        timeLabel = timeLabel.replacingOccurrences(of: Constants.BookingConfirmationModal.endHour,
                                                   with: dateFormatter.string(from: endDate))
        return timeLabel
    }
    
    static func getAssetTypeLabel(label: String?) -> String {
        if let safeAssetType = label, !safeAssetType.isEmpty {
            return safeAssetType.uppercasingFirst
        } else {
            return Constants.AssetTypeDefault.label
        }
    }
    
    static func convertMinutesToHourMinuteString(minutes: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourMinuteTimeFormat
        var dateComponents = DateComponents()
        dateComponents.hour = minutes / 60
        dateComponents.minute = minutes % 60
        guard let bookingUsage = Calendar.current.date(from: dateComponents) else { return "" }
        let bookingUsageString = dateFormatter.string(from: bookingUsage)
        return bookingUsageString
    }
    
    static func checkIfBookingTimeForValidation(booking: Booking) -> Bool {
        guard let safeStartDate = booking.startTime else { return false }
        var startDate = self.convertDateStringToDate(dateString: safeStartDate)
        var endDate = self.convertDateStringToDate(dateString: safeStartDate)
        let beforeDeadLine = UserDefaultsService.shared.getLimitBeforeDeadLine() * -1
        let afterDeadLine = UserDefaultsService.shared.getLimitAfterDeadLine()
        startDate = Calendar.current.date(byAdding: .minute, value: beforeDeadLine, to: startDate)!
        endDate = Calendar.current.date(byAdding: .minute, value: afterDeadLine, to: endDate)!
        let currentDate = Date()
        if currentDate.isBetween(startDate, and: endDate) {
            return true
        }
        return false
    }
    
    static func checkForNextReservation(booking: Booking) -> Bool {
        guard let safeStartDate = booking.startTime else { return false }
        let startDate = self.convertDateStringToDate(dateString: safeStartDate)
        
        guard let currentDateStart = Calendar.current.date(byAdding: .minute, value: 20, to: Date()) else { return false }
        guard let currentDateEnd = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) else { return false }
        
        if startDate.isBetween(currentDateStart, and: currentDateEnd) {
            return true
        }
        return false
    }
    
    static func convertToDateAndHourRangeString(startDate: Date, endDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateStartHourFormat
        let initDate = dateFormatter.string(from: startDate)
        let dateFormmaterHour = DateFormatter()
        dateFormmaterHour.dateFormat = Constants.DateTimeFormat.timeFormat
        let finishDate = dateFormmaterHour.string(from: endDate)
        return "\(initDate) to \(finishDate)"
    }
    
    static func qrAlreadyValidatedString(booking: Booking, buildingTimezone: String) -> (title: String, body: String) {
        let startTime = self.convertDateStringToDate(dateString: booking.startTime ?? "")
        let endTime = self.convertDateStringToDate(dateString: booking.endTime ?? "")
        let bookingHourRange = self.convertToDateAndHourRangeString(startDate: startTime, endDate: endTime)
        let title = "Booking already validated"
        let body = "You have already validated this reservation at \(booking.assetCode ?? "") - \(booking.areaName ?? "") \(bookingHourRange)  (\(buildingTimezone))"
        return (title, body)
    }
    
    static func qrNextBookingString(booking: Booking, buildingTimezone: String) -> (title: String, body: String) {
        let startTime = self.convertDateStringToDate(dateString: booking.startTime ?? "")
        let endTime = self.convertDateStringToDate(dateString: booking.endTime ?? "")
        let bookingHourRange = self.convertToDateAndHourRangeString(startDate: startTime, endDate: endTime)
        let startValidation = Calendar.current.date(byAdding: .minute, value: -20, to: startTime) ?? Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.timeFormat
        let startValidationHour = dateFormatter.string(from: startValidation)
        let title = "Can't validate booking"
        let body = "Your next reservation is \(booking.assetCode ?? "") - \(booking.areaName ?? "") for \(bookingHourRange) that you can validate starting at \(startValidationHour) (\(buildingTimezone))"
        return (title, body)
    }
    
    static func setCrashlyticsKey(_ key: String, as value: String?) {
        Crashlytics.crashlytics().setCustomValue(value ?? Constants.Crashlytics.nullValue, forKey: key)
    }
    
    static func getMonthDaysNumber(month: Int, year: Int) -> Int {
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents) ?? Date()
        guard let interval = calendar.dateInterval(of: .month, for: date) else { return 1 }
        guard let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day else { return 1 }
        return days
    }
    
    static func getMonthOrdinalDays(month: Int, year: Int = Calendar.current.component(.year, from: Date())) -> [String] {
        let monthDays = getMonthDaysNumber(month: month, year: year)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .ordinal
        numberFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        let daysArray = Array(1...monthDays).map { numberFormatter.string(from: NSNumber(value: $0)) ?? "" }
        return daysArray
    }
    
    static func getDayInMonthPosition(day: Int, month: Int, year: Int = Calendar.current.component(.year, from: Date())) -> (dayName: String, dayPosition: Int) {
        var selectedDate = DateComponents()
        selectedDate.day = day
        selectedDate.month = month
        selectedDate.year = year
        let date = Calendar.current.date(from: selectedDate) ?? Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dayNameFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        let weekDay = dateFormatter.string(from: date)
        
        var dayRepeated:[Int] = []
        let monthDays = getMonthDaysNumber(month: month, year: year)
        for monthDay in 1...monthDays {
            var dateComponents = DateComponents()
            dateComponents.day = monthDay
            dateComponents.month = month
            dateComponents.year = year
            let monthDayDate = Calendar.current.date(from: dateComponents) ?? Date()
            let monthDayName = dateFormatter.string(from: monthDayDate)
            if monthDayName == weekDay {
                dayRepeated.append(monthDay)
            }
        }
        let dayPosition = dayRepeated.indices.filter { dayRepeated[$0] == day }.first ?? 0
        return (weekDay, dayPosition)
    }
    
    static func getDayName(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dayNameFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        return dateFormatter.string(from: date)
    }
    
    static func getDateToHourLabel(date: String) -> String {
        let dateFormatted = convertDateStringToDate(dateString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.hourMinuteTimeFormat
        return dateFormatter.string(from: dateFormatted)
    }
    
    static func getWidgetDateLabel(date: String) -> String {
        let dateFormatted = convertDateStringToDate(dateString: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.dateFormat
        dateFormatter.locale = NSLocale(localeIdentifier: Constants.LocalIdentifier.enUS) as Locale
        var dateLabel = ""
        if dateFormatter.string(from: dateFormatted).elementsEqual(dateFormatter.string(from: Date())) {
            dateLabel = "Today"
        } else {
            dateLabel = dateFormatter.string(from: dateFormatted)
        }
        return dateLabel
    }
}
