//
//  Constants.swift
//  Bureau
//
//  Created by user on 6/2/21.
//

import Foundation
import Kingfisher
import UIKit

struct Constants {

    struct StoryBoard {
        static let mainStoryBoardID = "Main"
        static let loginViewControllerID = "loginView"
        static let mainViewControllerID = "mainView"
        static let splitViewControllerID = "splitViewController"
        static let workstationViewControllerID = "workstationView"
        static let workstationDetailsViewControllerID = "workstationDetailsView"
        static let qrReaderViewControllerID = "qrReaderView"
        static let bookingConfirmationModalViewControllerID = "bookingConfirmationView"
        static let myBookingsViewControllerID = "myBookingsView"
        static let buildingListControllerID = "homeView"
    }
    
    struct TabBar {
        static let book = "Book"
        static let myBookings = "My Bookings"
        static let scan = "Scan"
        static let favorites = "Favorites"
        static let profile = "Profile"
    }
    
    struct SplitView {
        static let sideMenuTitle = "Buildings"
    }

    struct Api {
        static let urlBaseDemo = "URLBaseDemo"
        static let urlBase = "URLBase"
    }
    
    struct Crashlytics {
        static let keyNoEmail = "no_email"
        static let keyBuildingId = "building_id"
        static let keyAreaId = "area_id"
        static let keyAssetId = "asset_id"
        static let keyDateValue = "date_filter"
        static let keyStartTimeValue = "start_time_filter"
        static let keyEndTimeValue = "end_time_filter"
        static let keyTimeZoneValue = "time_zone_abbreviation"
        static let prefsError = "preferences_error"
        static let nullValue = "null_value"
    }
    
    struct Appearance {
        static let subtitle = "Select your Bureau's appearance mode"
        static let option = "Select your Mode"
        static let mode = "KeyMode"
        static let unspecifiedMode = "Use system setting"
        static let lightMode = "Light Mode"
        static let darkMode = "Dark Mode"
        static let dark = UIUserInterfaceStyle.dark
        static let light = UIUserInterfaceStyle.light
        static let unspecified = UIUserInterfaceStyle.unspecified
    }
    
    struct VersionNumber {
        static let versionNumberBundleKey = "CFBundleShortVersionString"
        static let versionNumberIsStaging = Constants.Demo.isInDemoMode ? "- Staging" : ""
    }

    struct Colors {
        static let surfaceLow = "surfaceLow"
        static let surfaceMedium = "surfaceMedium"
        static let surfaceHigh = "surfaceHigh"
        static let surfaceDisable = "surfaceDisable"
        static let surfaceConflict = "surfaceConflict"
        static let surfaceSelected = "surfaceSelected"
        static let textHigh = "textHigh"
        static let textMedium = "textMedium"
        static let textLow = "textLow"
        static let textGray = "textGray"
        static let textYellow = "textYellow"
        static let textGreen = "textGreen"
        static let textBuildingSelected = "textBuildingSelected"
        static let workstationReservedColor = UIColor(red: 0.871, green: 0.314, blue: 0.408, alpha: 0.3)
        static let overlayBackground = "overlayBackground"
        static let primaryLight = "primaryLight"
        static let primaryMain = "primaryMain"
        static let interactiveDark = "interactiveDark"
        static let interactiveLight = "interactiveLight"
        static let secondaryLight = "secondaryLight"
        static let secondaryMain = "secondaryMain"
        static let secondaryDark = "secondaryDark"
        static let textSection = "textSection"
        static let cancel = "Cancel"
        static let rowHighlighted = "rowHighlighted"
        static let textAreaSelected = "textAreaSelected"
        static let textAreaNotSelected = "textAreaNotSelected"
        static let selectedBuilding = "selectedBuilding"
    }
    
    struct Fonts {
        static let bold = "Roboto Bold"
        static let regular = "Roboto Regular"
    }

    struct Images {
        static let pointerBlue = "pointerBlue"
        static let pointerGreen = "pointerGreen"
        static let pointerGray = "pointerGray"
        static let qrCodeCheck = "QRCodeCheck"
        static let qrCodeLess = "QRCodeLess"
        static let defaultProfilePicture = UIImage(named: "profile")
        static let expandIcon = UIImage(systemName: "arrow.up.left.and.arrow.down.right")
        static let shrinkIcon = UIImage(systemName: "arrow.down.right.and.arrow.up.left")
    }

    struct NibNames {
        static let areaCell = "AreaCell"
        static let mainMenu = "MainMenu"
        static let mainMenuPad = "MainMenu_ipad"
        static let topLogoView = "TopLogoView"
        static let calendarDay = "CalendarDay"
        static let areaListView = "AreaListView"
        static let workstationView = "WorkstationView"
        static let workstationDetailView = "WorkstationDetailView"
        static let bookingCell = "BookingTableViewCell"
        static let buildingCell = "BuildingTableViewCell"
        static let padBuildingCell = "PadBuildingTableViewCell"
        static let assetTypeCell = "AssetTypeCell"
        static let assetFeatureCell = "AssetFeatureCell"
        static let searchUserCell = "SearchUserCell"
        static let recurrenceWeeklyCell = "RecurrenceWeeklyCell"
        static let favoriteCell = "FavoriteCell"
        static let searchUserTableCell = "SearchUserTableViewCell"
    }

    struct ViewControllers {
        static let workstation = "Workstation"
    }

    struct PopupBuildingPiker {
        static let title = "Select a building"
        static let content = "contentViewController"
    }

    struct NoConnectedToNetwork {
        static let title = "You don't have internet connection"
        static let message = "We're having difficulty connecting to the server. Check your connection and try again."
    }

    struct AuhtenticationFailed {
        static let title = "Authentication failed"
        static let message = "The operation couldn’t be completed. Please try again later."
    }

    struct InternalError {
        static let title = "Oops!"
        static let message = "An unexpected internal error occurred. Please try again later."
    }

    struct ServerConnectionError {
        static let title = "Error"
        static let message = "An unexpected error occurred while connecting to Bureau Server. Please try again later."
    }

    struct BookingConfirmationError {
        static let title = "Oops!"
        static let message = "Booking couldn’t be validated. Please try again later."
    }

    struct LoggingOut {
        static let title = "Are you sure you want to logout?"
        static let message = "You will be returned to the login screen."
    }

    struct ForcedLogout {
        static let title = "Session Expired"
        static let message = "Please log into the app again to reconnect with Bureau."
    }

    struct NormalLogout {
        static let title = "Logout Successful"
        static let message = "You have successfully logged out."
    }

    struct ReadQrCodeError {
        static let title = "Error"
        static let message = "An unexpected error occurred while reading QR Code. Please try again later."
    }

    struct DecisionActionsText {
        static let cancel = "Cancel"
        static let select = "Select"
        static let accept = "Accept"
        static let positiveAnswer = "Yes"
        static let done = "Done"
    }

    struct PopupAreaPicker {
        static let title = "Select an area"
        static let content = "contentViewController"
    }

    struct DateTimeFormat {
        static let utcFormat = "UTC"
        static let dateFormat = "EEE, MMM. d"
        static let dateBookingFormat = "EEE, MMM. d yyyy"
        static let timeFormat = "hh:mm a"
        static let hourMinuteTimeFormat = "HH:mm"
        static let completeDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        static let completeDateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        static let optionalDateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        static let timeForBooking = "yyyy-MM-dd'T'HH:mmZ"
        static let areaLayoutDateTime = "yyyy-MM-dd'T'HH:mm'Z'"
        static let content = "contentViewController"
        static let dateFormatForAPI = "yyyy-MM-dd"
        static let hourFormatForAPI = "HH:mm"
        static let hourFormatWithSeconds = "HH:mm:ss"
        static let dateFormatWithHour = "MMM dd, YYYY'T'h:mma"
        static let dateForAPI = "yyyy-MM-dd'T'HH:mm:ss"
        static let hourFormat = "h:mm a"
        static let completeLiteralDate = "MMM dd, YYYY' from 'h:mm a"
        static let dateFormatToConvert = "yyyy-MM-dd'T'HH:mm"
        static let dateStartHourFormat = "MMMM dd, yyyy' from 'h:mm a"
        static let oneDayInSeconds = 60 * 60 * 24
        static let dayNameFormat = "EEEE"
    }

    struct MainMenuStrings {
        static let selectDate = "Select date"
        static let selectTime = "Select time"
        static let selectArea = "Select area"
    }

    struct JalaLogin {
        static let issuer = "Issuer"
        static let issuerDemo = "IssuerDemo"
        static let clientId = "bureau"
        static let clientSecretDemo = "b6a739e0-a434-4983-b782-214fb51e026f"
        static let clientSecret = "5cd491e5-0a29-4239-b0ce-686fa6459282"
        static let redirectURL = "RedirectURL"
        static let kAppAuthBureauAuthStateKey = "authState"
        static let suiteName = "group.net.openid.appauth.Example"
        static let logoutFlag = "logoutFlag"
        static let endSessionUrl = "endSessionUrl"
        static let idToken = "idToken"
    }

    struct AuthorizationHeader {
        static let fieldName = "Authorization"
        static let fieldValue = "Bearer"
    }

    struct ImageReference {
        static let checkMark = "checkmark"
    }

    struct Layout {
        static let imageName = "layout"
        static let noAreaSelected = "No Area Selected"
        static let noBuildingSelected = "No Building Selected"
    }

    struct AreaCard {
        static let cornerRadius: CGFloat = 10.0
        static let areaSpacecornerRadius: CGFloat = 15.0
        static let shadowOpacity: Float = 0.4
        static let shadowRadius: CGFloat = 7.0
        static let clipsToBounds = true
        static let masksToBounds = true
        static let areaName = "Area"
        static let buildingName = "Building"
    }

    struct WorkstationCard {
        static let cornerRadius: CGFloat = 10.0
        static let height: CGFloat = 117.0
        static let width: CGFloat = 193.0
    }

    struct ScannerNotSupported {
        static let title = "Scanning not supported"
        static let message = "Your device does not support scanning a code from an item. Please use a device with a camera."
    }

    struct CameraPermissionDenied {
        static let title = "Camera Access Permission Required"
        static let message = "Please grant camera access to Bureau in Settings."
    }

    struct EmptyWorkstationId {
        static let title = "Error"
        static let message = "The field Workstation ID cannot be empty."
    }
    
    struct InvalidTime {
        static let title = "Error"
        static let message = "The hour entered is invalid."
    }
    
    struct NotAvailableSpaces {
        static let title = "Oops!"
        static let message = "This area does not have available spaces. Please select another one."
    }
    
    struct BookingNotFound {
        static let title = "Oops!"
        static let message = "You don't have a booking available. Would you like to book right now?"
    }
    
    struct NoBookingAvailable {
        static let title = "Oops!"
        static let message = "You don't have a booking available for validation right now."
    }

    struct ServerError {
        static let title = "Server Error"
    }
    
    struct TypeWorksationCode {
        static let title = "Booking confirmation"
        static let message = "Enter the workstation identifier."
        static let textFieldPlaceholder = "Type the Workstation ID"
    }

    struct BookingConfirmationModal {
        static let yDragVelocity: CGFloat = 500.0
        static let withDuration: Double = 0.3
        static let timeZone = 0
        static let workstationCodeLabel = "Workstation"
        static let bookingDateLabelTable = "DATE STARTHOUR to ENDHOUR"
        static let bookingDateLabel = "DATE from STARTHOUR to ENDHOUR"
        static let code = "CODE"
        static let date = "DATE"
        static let startHour = "STARTHOUR"
        static let endHour = "ENDHOUR"
        static let completeHour = "STARTHOUR - ENDHOUR"
        static let validableLabel = "Validate from STARTHOUR to ENDHOUR"
        static let bookingConfirmed = "Booking Confirmed!"
    }

    struct BookingPolicies {
        static let empty = ""
        static let bookingPolicy = "Booking Policy"
        static let permanentBooking = "Fixed Resource Assigned To:"
        static let bookingDetailPermanent = "This is a permanent asset assigned to you"
        static let scheduleBookingId = "ScheduleBookingId"
        static let permanentBookingCell = "Permanent Booking"
    }
    
    struct PresentationControllerModal {
        static let initX: CGFloat = 0.0
        static let cornerRadius: CGFloat = 10.0
    }

    struct NotificationNameKeys {
        static let timersChanged = "timersChanged"
        static let dateChanged = "dateChanged"
        static let goBackAreaList = "goBackAreaList"
        static let isAClash = "isAClash"
        static let clashOfSchedules = "clashOfSchedules"
        static let presentDateCalendarPicker = "presentDateCalendarPicker"
        static let presentTimePickers = "presentTimePickers"
        static let enableWorkstationFeatures = "enableWorkstationFeatures"
        static let selectWorkstationFeatures = "selectWorkstationFeatures"
        static let updateButtonFiltersLabel = "updateButtonFiltersLabel"
        static let recurrenceRepeatChanged = "recurrenceRepeatChanged"
        static let deleteFavoriteAsset = "deleteFavoriteAsset"
        static let updateFavoritesAsset = "updateFavoritesAsset"
        static let updateLayout = "updateLayout"
        static let updateBookingList = "updateBookingList"
    }
    
    struct Notifications {
        static let bodyNotification = "Your reservation of %s starts in 15 min\n%s"
    }

    struct UserDefaultsKeys {
        static let user = "userData"
        static let bookingInfo = "bookingInfo"
        static let bookingRecent = "bookingRecent"
        static let buildingId = "buildingId"
        static let buildingName = "buildingName"
        static let buildingSwitch = "buildingSwitch"
        static let currentURL = "currentURL"
        static let userEmail = "userEmail"
        static let userAuth = "userAuth"
        static let limitAfterDeadLine = "limitAfterDeadLine"
        static let limitBeforeDeadLine = "limitBeforeDeadLine"
        static let recurrentBookingsPermission = "recurrentBookingsPermission"
        static let bookForOthersPermission = "bookForOthersPermission"
        static let buildingTimezone = "buildingTimezone"
    }

    struct MainMenuTags {
        static let btnStartTimeTag = 1
        static let btnEndTimeTag = 2
    }

    struct BookingCard {
        static let cornerRadius: CGFloat = 10.0
        static let workstationAreaLabel = " - Area "
        static let bookingValidated = "Validated"
        static let bookingNotValidated = "Validate"
        static let shadowOpacity: Float = 0.1
        static let shadowRadius: CGFloat = 2.0
        static let borderWidth: CGFloat = 2.0
        static let borderColor = CGColor(red: 0.968, green: 0.968, blue: 0.968, alpha: 1.0)
    }
    
    struct CancelBooking {
        static let cancelBooking: String = "Workstation"
    }
    
    struct EmptyData {
        static let noBookingsAvailable = "Currently you don't have any bookings."
        static let noBookingsForSection = "No bookings found for this section."
        static let noFavoritesAvailable = "Currently you don't have any favorite spaces."
        static let noOwner = "Owner not specified"
        static let noType = "unspecified"
    }
    
    struct ImagesNames {
        static let brandAndLetters = "JalasoftLogoWhite.svg"
    }
    
    struct SpaceAvailability {
        static let space = "Space"
        static let spaces = "Spaces"
        static let noSpaces = "No spaces"
    }
    
    struct WorkstationDetailsBottomSheet {
        static let initHeight: CGFloat = 250.0
        static let minHeight: CGFloat = 100.0
        static let initialAlpha: CGFloat = 0.1
        static let alphaStrength: CGFloat = 0.5
        static let topHeight: CGFloat = 0.9
        static let topSpace: CGFloat = 40
    }
    
    struct MyBookingsListKeys {
        static let today = "Today"
        static let upcoming = "Upcoming"
    }
    
    struct MyBookingsValidate {
        static let validate = "Validate"
        static let validated = "Validated"
    }
    
    struct CreateBookingLabel {
        static let bookNow = "Book now"
        static let cancelBooking = "Cancel booking"
        static let hasOwner = "Has Owner"
    }
    
    struct PushNotification {
        static let minutesTimeBefore = -60 * 1 // Change the last number
        static let minutesBeforeCancelation = 10
        static let title = "Your booking is comming."
        static let titleCancellation = "Booking cancelation warning."
        static let categoryIdentifier = "BookingNotification"
        static let openBooking = "openBooking"
        static let cancelBooking = "cancelBooking"
        static let openBookingTitle = "Open booking"
        static let cancelBookingTitle = "Cancel booking"
        static let userInfo = "Booking"
        static func setBadgeToZero() {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    struct AssetTypeDefault {
        static let label = "Workstation"
    }
    
    struct QRBookingValidation {
        static let bookingToValidate = "bookingToValidate"
        static let nextBooking = "nextBooking"
        static let newQRBookingDescription = "This Booking is already validated, no need to scan the QR again."
    }
    
    struct DatePickerSetType {
        static let date = "date"
        static let tempDate = "tempDate"
        static let endDate = "endDate"
    }
    
    struct LocalIdentifier {
        static let enUS = "en_US"
    }
    
    struct RecurrentOptions {
        static let repeatOptions = ["Never", "Daily", "Weekly", "Monthly", "Yearly"]
        static let weekDaysName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        static let dayPosition = ["First", "Second", "Third", "Fourth", "Last"]
        static let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        static let never = "never"
        static let daily = "daily"
        static let weekly = "weekly"
        static let monthly = "monthly"
        static let yearly = "yearly"
    }
    
    struct Favorites {
        static let available = "Available"
        static let unavailable = "Unavailable"
        static let permanentAsset = "Unavailable / Permanently booked"
    }
    
    struct AlertPicker {
        static let contentKey = "contentViewController"
    }
    
    struct UserValidation {
        static let userSelected = "userSelected"
        static let emailUndefined = "Email undefined"
        static let emptyName = "Empty Name"
        static let emptyText = ""
        static let enterValidEmail = "Enter a valid email"
        static let forExample = "For example myEmail@domain.com"
        static let bookForThis = "Book for this email"
    }
    
    struct Schemes {
        static let schemeBooking = "scheme://booking"
    }
    
    struct UnavailableAssets {
        static let emptyMap = "There are not available spaces to book at the moment"
        static let mismatchedFilters = "Your current filter settings does not contain results"
    }
    
    struct QueryParameters {
        static let excludeUserCancelled = "status.state ne string(UserCancelled)"
        static let excludeSystemCancelled = "status.state ne string(SystemCancelled)"
    }
    
    struct BookingState {
        static let pending = "Pending"
        static let validated = "Validated"
        static let userCancelled = "UserCancelled"
        static let systemCancelled = "SystemCancelled"
    }
}
