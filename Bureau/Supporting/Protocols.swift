//
//  Protocols.swift
//  Bureau
//
//  Created by Victor Arana on 6/15/21.
//

import Foundation
import AppAuth

protocol APIServiceProtocol {
    // MARK: - Properties
    var selectedArea: Area! { get set }
    var selectedBuilding: Building! { get set }
    var buildingData: [Building]! { get set }
    var buildingId: String! { get set }
    var selectedAssetType: AssetType! { get set }
    var selectedWorkstationFeatures: [WorkstationFeature] { get set }
    var currentDate: Date { get set }
    var startHourDate: Date { get set }
    var endHourDate: Date { get set }
    var showOnlyAvailableSpaces: Bool { get set }
    var enabledFilters: Int { get set }
    var tempCurrentDate: Date { get set }
    var recurrenceRule : RecurrenceRule { get set }
    var tempRecurrenceRule :  RecurrenceRule { get set }
    var isRecurrent: Bool { get set }
    var selectedUser: User! { get set }
    var sessionUser: User! { get set }

    // MARK: - Functions
    func apiToGetAreaData(completion: @escaping (Result<[Area], CustomError>) -> Void )
    func apiToGetAreasNumberOfBuilding(buildingId: String?, completion: @escaping (Result<Int, CustomError>) -> Void )
    func apiToGetBuildingData(completion : @escaping (Result<[Building], CustomError>) -> Void )
    func apiToGetWorkstationDataFromAnArea(completion : @escaping (Result<[Asset], CustomError>) -> Void )
    func apiToGetLayoutDataFromAnArea(completion : @escaping (Result<Area, CustomError>) -> Void )
    func apiToConfirmABooking(bookingId: String, completion : @escaping (Result<Booking, CustomError>) -> Void )
    func apiToGetWorkstationBookings(workstationID: String?, completion : @escaping (Result<[Booking], CustomError>) -> Void )
    func apiToGetUserBookings(email: String, bookingsTime: BookingsTime, completion : @escaping (Result<[Booking], CustomError>) -> Void )
    func apiToDeleteBooking(bookingID:String, completion : @escaping (Result<Bool, CustomError>) -> Void )
    func apiToCreateABooking(assetId: String, buildingId: String, completion: @escaping (Result<Booking, CustomError>) -> Void)
    func apiToGetAssetBookings(assetId: String, startTime: String, endTime: String, completion : @escaping (Result<[Booking], CustomError>) -> Void )
    func apiToGetAssetWithID(assetId: String, completion:@escaping(Result<Asset, CustomError>) -> Void)
    func apiToGetAssetTypes(completion:@escaping(Result<[TypeData],CustomError>) -> Void)
    func apiToGetWorkstationFeatures(completion:@escaping(Result<[Feature],CustomError>) -> Void)
    func apiToGetWorkstationDataFromABuilding(idList: String, completion: @escaping (Result<[Asset], CustomError>) -> Void)
    func apiToGetAreaBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void)
    func apiToGetAllWorkstationDataFromAnArea(completion : @escaping (Result<[Asset], CustomError>) -> Void )
    func apiToGetUserProfileInfo(completion: @escaping (Result<UserProfile, CustomError>) -> Void)
    func apiToGetUserFavorites(assets: [String], completion: @escaping (Result<[Asset], CustomError>) -> Void)
    func apiToDeletefavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void)
    func apiToCreateFavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void)
    func apiToGetUsers(completion: @escaping (Result<[User], CustomError>) -> Void )
    func apiToGetUserGroups(groupIdArray: [String], completion: @escaping (Result<[Group], CustomError>) -> Void )
    func apiToGetOverlappedBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void)
    func apiToGetUserPermanentAssets(email: String, completion : @escaping (Result<[Asset], CustomError>) -> Void )
    func apiToGetAreaInfo(areaId: String, completion : @escaping (Result<Area, CustomError>) -> Void )
    func setRecurrentEndDate()
}

protocol UserServiceProtocol {

    // MARK: - Functions
    func apiToGetUserData(completion : @escaping (Result<User, CustomError>) -> Void )
}

protocol AuthServiceProtocol {

    // MARK: - Functions
    func setAuthState(_ authState: OIDAuthState?, _ completionHandler: @escaping (Result<Bool, CustomError>) -> Void )
    func saveState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void )
    func loadState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void )
    func getOIDAuthorizationRequest(completion: @escaping (Result<OIDAuthorizationRequest, CustomError>) -> Void )
    func getAuthState() -> OIDAuthState?
    func getUserInformationEndpoint() -> URL?
    func signOut(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void)
    func getIslogoutPendingFlag() -> Bool
}

protocol WorkstationViewControllerDelegate: AnyObject {

    // MARK: - Functions
    func goToWorkstationDetails(workstationID: String)
    func showError(error: CustomError)
}

protocol AuthServiceDelegate: AnyObject {

    // MARK: - Functions
    func setAuthState(authState: OIDAuthState?)
}

protocol QrReaderViewControllerDelegate: AnyObject {

    // MARK: - Functions
    func displayBookingConfirmation(bookingConfirmation: Booking)
    func displayBookingConfirmationAndValidation(bookingValidation: Booking)
}

protocol BookingDetailsControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func dismissBookingDetailsView()
    func showCancelBookingError(title: String, message: String)
}

protocol BookingCreatedViewControllerDelegate: AnyObject {

    // MARK: - Functions
    func displayBookingCreated(bookingCreated: Booking)
    func startQrReader()
}

protocol FiltersViewControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func presentDateCalendarPicker(pickerSetType: String)
    func presentTimePickers()
    func presenFiltersPage()
    func presentBookingOptions()
}

protocol UpdateContentViewControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func executeNewRequest()
}

protocol AreaListViewControllerDeletage: AnyObject {
    
    // MARK: - Functions
    func updateLayout()
    func showBuildingsMenuButton()
}

protocol AreaLayoutViewControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func updateCoverViewBackground(alpha: CGFloat)
    func updateLayout(onlyMarker update: Bool)
}

protocol MyBookingsViewControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func updateList()
}

protocol UserDefaultsProtocol: AnyObject {
    
    // MARK: - Functions
    func saveUserInfo(user: User, completion: @escaping (Result<Bool, CustomError>) -> Void)
    func getUserInfo(completion: @escaping (Result<User, CustomError>) -> Void)    
}

protocol FavoriteViewControllerDelegate: AnyObject {
    
    // MARK: - Functions
    func updateFavoritesView()
}
