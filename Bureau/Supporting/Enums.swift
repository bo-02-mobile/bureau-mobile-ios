//
//  Enums.swift
//  Bureau
//
//  Created by Victor Arana on 6/7/21.
//

import Foundation

enum CustomError: Error, Equatable {
    case internalError
    case connectionError
    case bookingConfirmationError
    case noInternetConnection
    case authenticationFailed
    case cameraPermissionDenied
    case scannerNotSupported
    case readQrCodeError
    case emptyWorkstationId
    case invalidTime
    case notAvailableSpaces
    case bookingNotFound
    case serverError(message: String)
    case noBookingAvailable
}

enum IconSize {
    case normal
    case big
}

enum ModalType: Float {
    case datePicker = 455.0
    case timePicker = 554.0
    case confirmationBooking = 378.0
}

enum ActionType: String, CustomStringConvertible {
    case scanQrAction
    case newBookingAction
    case myBookingsAction
    
    var description : String {
        switch self {
        case .scanQrAction: return "Validate with the QR"
        case .newBookingAction: return "Go to my bookings"
        case .myBookingsAction: return "Book a Space"
        }
    }
    
    var id: Int {
        switch self {
        case .scanQrAction:
            return 2
        case .newBookingAction:
            return 0
        case .myBookingsAction:
            return 1
        }
    }
}

enum ApiErrorType {
    case bookingConfirmation
    case serverError
    case defaultError
}

enum BookingsTime {
    case today
    case upcoming
}
