//
//  Extensions.swift
//  Bureau
//
//  Created by user on 6/3/21.
//

import Foundation
import UIKit

@IBDesignable extension UIButton {

    // MARK: - Variables
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
    
    public func setTime(hour: Int, min: Int, sec: Int, timeZone: TimeZone? = nil) -> Date? {
        let set: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second, .timeZone]
        let cal = Calendar.current
        var components = cal.dateComponents(set, from: self)
        
        components.hour = hour
        components.minute = min
        components.second = sec
        
        if let timeZone = timeZone {
            components.timeZone = timeZone
        }
        
        return cal.date(from: components)
    }
    
    func convertToTimeZone(timeZoneAbbreviation: String) -> Date {
        guard let timeZone = TimeZone(abbreviation: timeZoneAbbreviation) else {
            return self
        }
        let initTimeZone = TimeZone.current
        let delta = TimeInterval(initTimeZone.secondsFromGMT(for: self) - timeZone.secondsFromGMT(for: self))
        return addingTimeInterval(delta)
    }
}

extension UIViewController {
    
    func showErrorMessage(_ title: String, _ message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: Constants.DecisionActionsText.accept, style: .default, handler: handler)
        alert.addAction(acceptAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showErrorMessage(_ title: String, _ message: String, handler: ((UIAlertAction) -> Void)? = nil, cancelHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .destructive, handler: cancelHandler))
        alert.addAction(UIAlertAction(title: Constants.DecisionActionsText.positiveAnswer, style: .default, handler: handler))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textColor = UIColor(named: Constants.Colors.textHigh)
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont(name: Constants.Fonts.regular, size: 16)
        messageLabel.sizeToFit()
        messageLabel.frame.size.width = self.frame.width
        let container = UIView()
        container.addSubview(messageLabel)
        self.backgroundView = container
    }

    func restore() {
        self.backgroundView = nil
    }
}

private let badChars = CharacterSet.alphanumerics.inverted

let firstPart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
let serverPart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
let emailRegex = firstPart + "@" + serverPart + "[A-Za-z]{2,8}"
let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)

extension String {
    var uppercasingFirst: String {
        return prefix(1).uppercased() + dropFirst()
    }

    var lowercasingFirst: String {
        return prefix(1).lowercased() + dropFirst()
    }

    var camelized: String {
        guard !isEmpty else {
            return ""
        }

        let parts = self.components(separatedBy: badChars)

        let first = String(describing: parts.first!).lowercasingFirst
        let rest = parts.dropFirst().map({String($0).uppercasingFirst})

        return ([first] + rest).joined(separator: "")
    }
    
    var getAssetIdFromURL: String {
        guard !isEmpty else {
            return ""
        }
        var code = ""
        let arr = self.components(separatedBy: "/")
        if (arr.indices.contains(4)) {
            code = arr[4]
        }
        return code
    }
    
    func isValidEmail() -> Bool {
        return emailPredicate.evaluate(with: self)
    }
    
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.DateTimeFormat.timeForBooking
        return dateFormatter.date(from: self)
    }
    
}

extension Data {
    func toString() -> String? {
        return String(data: self, encoding: .utf8)
    }
}

extension URLRequest {
    func log() {
        print("\(httpMethod ?? "") \(self)")
        print("BODY \n \(httpBody?.toString() ?? "no body")")
        print("HEADERS \n \(allHTTPHeaderFields ?? ["error": "no headers"])")
    }
}

extension URL {
    /// Returns a new URL by adding the query items, or nil if the URL doesn't support it.
    /// URL must conform to RFC 3986.
    mutating func appending(_ queryItems: [URLQueryItem]) {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            // URL is not conforming to RFC 3986 (maybe it is only conforming to RFC 1808, RFC 1738, and RFC 2732)
            return
        }
        urlComponents.queryItems = (urlComponents.queryItems ?? []) + queryItems
        self = urlComponents.url!
    }
}

extension Array {
    func convertToIdString() -> String {
        let ids = self.map { id in
            "id(\(id))"
        }
        return ids.joined(separator: ",")
    }
}
