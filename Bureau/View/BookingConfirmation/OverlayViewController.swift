//
//  OverlayViewController.swift
//  Bureau
//
//  Created by Victor Arana on 6/21/21.
//

import Foundation
import UIKit

class OverlayViewController: GestureSupport {

    // MARK: - Variables
    var viewModel = OverlayViewModel()
    var hasSetPointOrigin = false
    var isNewBooking = false
    var isNewBookingQR = false
    var assetId = ""
    
    weak var overlayViewDelegate: BookingDetailsControllerDelegate!

    // MARK: - Outlets
    @IBOutlet var closeImage: UIImageView!
    @IBOutlet var workstationCode: UILabel!
    @IBOutlet var bookingDate: UILabel!
    @IBOutlet var bookingTitle: UILabel!
    @IBOutlet var bookingRecomentation: UILabel!
    @IBOutlet var confirmedMessage: UILabel!
    @IBOutlet weak var descriptionMessage: UILabel!
    @IBOutlet var checkImage: UIImageView!
    @IBOutlet var imageQR: UIImageView!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewGestureRecongnizer()
        setupCloseGestureRecognizer()
        bindWorkstationCodeLabel()
        bindBookingDateLabel()
        bindError()
        viewModel.constructWorkstationCodeLabel()
        viewModel.constructBookingDateLabel()
        if isNewBooking {
            if isNewBookingQR {
                descriptionMessage.text = Constants.QRBookingValidation.newQRBookingDescription
            }
            bookingTitle.text = Constants.BookingConfirmationModal.bookingConfirmed
            confirmedMessage.isHidden = false
            checkImage.isHidden = true
            imageQR.image = #imageLiteral(resourceName: "checkValidation")
            imageQR.tintColor = UIColor(named: Constants.Colors.secondaryMain)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        goToMyBookings()
        if isBeingDismissed && isNewBooking {
            overlayViewDelegate.dismissBookingDetailsView()
        }
    }

    // MARK: - Functions
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            originPoint = self.view.frame.origin
        }
    }

    func bindWorkstationCodeLabel() {
        viewModel.bindWorkstationCodeViewModelToController = { [weak self] (workstationCode) in
            self?.workstationCode.text = workstationCode
        }
    }

    func bindBookingDateLabel() {
        viewModel.bindBookingDateViewModelToController = { [weak self] (bookingDate) in
            self?.bookingDate.text = bookingDate
        }
    }

    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }

    func setBookingConfirmation(bookingConfirmation: Booking) {
        viewModel.setBookingConfirmation(bookingConfirmation: bookingConfirmation)
    }

    func setupViewGestureRecongnizer() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
    }

    func setupCloseGestureRecognizer() {
        let panGesture = UITapGestureRecognizer(target: self, action: #selector(closeViewRecognizerAction))
        closeImage.isUserInteractionEnabled = true
        closeImage.addGestureRecognizer(panGesture)
    }
    
    func goToMyBookings() {
        let navVC = BottomTabBar.currentInstance?.viewControllers![1] as? UINavigationController
        let viewController = navVC?.topViewController as? MyBookingsViewController
        viewController?.highlightedBooking = true
        viewController?.highlightedBookingId = assetId
        if (BottomTabBar.currentInstance?.selectedIndex == 1) {
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateBookingList), object: nil)
        } else {
            BottomTabBar.currentInstance?.selectedIndex = 1
        }
    }
}
