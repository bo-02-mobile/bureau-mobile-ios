//
//  PresentationController.swift
//  Bureau
//
//  Created by Victor Arana on 6/21/21.
//

import Foundation
import UIKit

class PresentationController: UIPresentationController {

    // MARK: - Variables
    let backgroundView: UIView!
    var modalHeight: CGFloat = 0.0
    var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer()
    override var frameOfPresentedViewInContainerView: CGRect {
        CGRect(origin: CGPoint(x: Constants.PresentationControllerModal.initX,
                               y: self.containerView!.frame.height - self.modalHeight),
               size: CGSize(width: self.containerView!.frame.width,
                            height: self.modalHeight))
    }

    // MARK: - Constructors
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        backgroundView = UIView()
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissController))
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.backgroundView.isUserInteractionEnabled = true
        self.backgroundView.addGestureRecognizer(tapGestureRecognizer)
    }

    // MARK: - Functions
    override func presentationTransitionWillBegin() {
        self.backgroundView.backgroundColor = .clear
        self.containerView?.addSubview(backgroundView)
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) in
            self.backgroundView.backgroundColor = UIColor(named: Constants.Colors.overlayBackground)
        }, completion: nil)
    }

    override func dismissalTransitionWillBegin() {
        self.presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (_) in
            self.backgroundView.backgroundColor = .clear
        }, completion: { (_) in
            self.backgroundView.removeFromSuperview()
        })
    }

    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        presentedView!.roundCorners([.topLeft, .topRight], radius: Constants.PresentationControllerModal.cornerRadius)
    }

    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
        backgroundView.frame = containerView!.bounds
    }

    @objc func dismissController() {
        self.presentedViewController.dismiss(animated: true, completion: nil)
    }
}
