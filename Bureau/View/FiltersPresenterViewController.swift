//
//  FiltersPresenterViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import UIKit

class FiltersPresenterViewController: ModalPresenterViewController, FiltersViewControllerDelegate, UpdateContentViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func presentDateCalendarPicker(pickerSetType: String) {
        self.modalHeight = Helpers.getDatePickerHeight()
        let slideVC = DatePickerViewController(pickerSetType: pickerSetType)
        slideVC.viewControllerDelegate = self
        slideVC.modalPresentationStyle = .custom
        slideVC.transitioningDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
    
    func presentTimePickers() {
        self.modalHeight = Helpers.getTimePickerHeight()
        let slideVC = TimePickerViewController()
        slideVC.viewControllerDelegate = self
        slideVC.modalPresentationStyle = .custom
        slideVC.transitioningDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
    
    func presenFiltersPage() {
        let filtersViewController = FiltersViewController()
        filtersViewController.viewControllerDelegate = self
        self.present(filtersViewController, animated: true, completion: nil)
    }
    
    func executeNewRequest() {
        preconditionFailure("This method must be overridden")
    }
    
    func presentBookingOptions() {
        let slideVC = BookingOptionsController()
        slideVC.viewControllerDelegate = self
        self.present(slideVC, animated: true, completion: nil)
    }
}
