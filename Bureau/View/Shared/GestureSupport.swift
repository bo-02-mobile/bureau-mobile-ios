//
//  GestureSupport.swift
//  Bureau
//
//  Created by Joshua Ugarte on 17/6/22.
//

import UIKit

class GestureSupport: UIViewController {
    var originPoint: CGPoint?
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        guard translation.y >= Constants.PresentationControllerModal.initX else { return }
        guard let safePointOrigin = self.originPoint else { return }
        view.frame.origin = CGPoint(x: Constants.PresentationControllerModal.initX, y: safePointOrigin.y + translation.y)
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= Constants.BookingConfirmationModal.yDragVelocity {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: Constants.BookingConfirmationModal.withDuration) {
                    self.view.frame.origin = safePointOrigin
                }
            }
        }
    }

    @objc func closeViewRecognizerAction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}
