//
//  FiltersViewController.swift
//  Bureau
//
//  Created by Victor Arana on 8/25/21.
//

import UIKit

class FiltersViewController: UIViewController {
    
    // MARK: - Variables
    let viewModel = FiltersViewModel()
    weak var viewControllerDelegate: UpdateContentViewControllerDelegate?
    
    // MARK: - Outlets
    @IBOutlet var spaceTypeCollectionView: UITableView!
    @IBOutlet var assetFeaturesCollectionView: UITableView!
    @IBOutlet var showAvailableSpacesOnly: UISwitch!
    
    // MARK: - Constants
    let notificationCenter = NotificationCenter.default
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.bindAssetTypes()
        self.bindWorkstationFeatures()
        self.viewModel.callFuncToGetAssetTypes()
        self.configureObservers()
        self.showAvailableSpacesOnly.isOn = viewModel.getShowOnlyAvailableSpacesValue()
    }
    
    // MARK: - Functions
    func configureTableView() {
        spaceTypeCollectionView.register(UINib(nibName: Constants.NibNames.assetTypeCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.assetTypeCell)
        spaceTypeCollectionView.dataSource = self
        spaceTypeCollectionView.separatorColor = .clear
        assetFeaturesCollectionView.register(UINib(nibName: Constants.NibNames.assetFeatureCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.assetFeatureCell)
        assetFeaturesCollectionView.dataSource = self
        assetFeaturesCollectionView.separatorColor = .clear
    }
    
    func bindAssetTypes() {
        viewModel.bindAssetTypesToViewController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                self?.viewModel.callFuncToGetWorkstationFeatures()
                strongSelf.spaceTypeCollectionView.reloadData()
            }
        }
    }
    
    func bindWorkstationFeatures() {
        self.viewModel.bindWorstationFeaturesToViewController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.assetFeaturesCollectionView.reloadData()
            }
        }
    }
    
    func configureObservers() {
        self.notificationCenter.addObserver(self, selector: #selector(FiltersViewController.enableWorkstationFeatures),
                                            name: Notification.Name(Constants.NotificationNameKeys.enableWorkstationFeatures), object: nil)
        self.notificationCenter.addObserver(self, selector: #selector(FiltersViewController.selectWorkstationFeatures),
                                            name: Notification.Name(Constants.NotificationNameKeys.selectWorkstationFeatures), object: nil)
    }
    
    @objc func enableWorkstationFeatures(notification: NSNotification) {
        if let assetType = notification.userInfo?[Constants.NotificationNameKeys.enableWorkstationFeatures] as? AssetType {
            viewModel.setSelectedAssetType(assetType: assetType)
        }
    }
    
    @objc func selectWorkstationFeatures(notification: NSNotification) {
        if let workstationFeature = notification.userInfo?[Constants.NotificationNameKeys.selectWorkstationFeatures] as? WorkstationFeature {
            viewModel.setWorkstationFeature(workstationFeature: workstationFeature)
        }
    }
    
    @IBAction func applyFilters(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.viewModel.applyFilters()
            self.updateFiltersButtonLabel()
            self.viewControllerDelegate?.executeNewRequest()
        }
    }
    
    @IBAction func closeFilters(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if self.viewModel.getEnabledFiltersNumber() == 0 {
                self.updateFiltersButtonLabel()
            }
        }
    }
    
    @IBAction func clearFilters(_ sender: UIButton) {
        self.showAvailableSpacesOnly.isOn = false
        self.viewModel.clearFilters()
    }
    
    @IBAction func showAvailableSpaces(_ sender: UISwitch) {
        viewModel.setShowOnlyAvailableSpacesValue(isOn: sender.isOn)
    }
    
    func updateFiltersButtonLabel() {
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateButtonFiltersLabel),
                                        object: nil,
                                        userInfo: [Constants.NotificationNameKeys.updateButtonFiltersLabel : viewModel.getEnabledFiltersNumber()])
    }
    
}

extension FiltersViewController: UITableViewDataSource {
    
    // MARK: - Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.spaceTypeCollectionView {
            return viewModel.getAssetTypesLenght()
        } else {
            return viewModel.getWorkstationFeaturesLenght()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.spaceTypeCollectionView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.assetTypeCell, for: indexPath) as? AssetTypeCell else {
                return UITableViewCell()
            }
            cell.updateContent(assetType: viewModel.getAssetTypeByIndex(index: indexPath.row))
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.assetFeatureCell, for: indexPath) as? AssetFeatureCell else {
                return UITableViewCell()
            }
            cell.updateContent(workstationFeature: viewModel.getWorkstationFeatureByIndex(index: indexPath.row))
            return cell
        }
    }
}
