//
//  FavoriteViewController.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 23/3/22.
//
import UIKit

class FavoriteViewController: UIViewController {
 
    var viewModel: FavoriteViewModel = FavoriteViewModel()
    let refreshControl = UIRefreshControl()
    var assets: [Asset] = []
    @IBOutlet weak var favoriteCountLabel: UILabel!
    @IBOutlet weak var favoriteTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.navigationController?.navigationBar.isHidden = true
        }
        configureTableView()
        configureRefresh()
        configureObservers()
        bindViewModelToViewController()
        configureNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        viewModel.getUserProfileInfo()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func configureNavigationBar() {
        let logo = UIImage(named: Constants.ImagesNames.brandAndLetters)
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        NSLayoutConstraint.activate([imageView.widthAnchor.constraint(equalToConstant: 150)])
        self.navigationItem.titleView = imageView
    }
    
    func configureTableView() {
        favoriteTableView.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        favoriteTableView.register(UINib(nibName: Constants.NibNames.favoriteCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.favoriteCell)
        // setting up table view
        favoriteTableView.dataSource = self
        favoriteTableView.delegate = self
        // start activityIndicator animation
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
    }
    
    func configureObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(deleteFavoriteAsset(notification:)), name: Notification.Name(Constants.NotificationNameKeys.deleteFavoriteAsset), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTableView), name: Notification.Name(Constants.NotificationNameKeys.updateFavoritesAsset), object: nil)
    }
    
    func configureRefresh() {
        refreshControl.addTarget(self, action: #selector(refreshTableView), for: .valueChanged)
        favoriteTableView.addSubview(refreshControl)
    }
    
    @objc func deleteFavoriteAsset(notification: NSNotification) {
        if let assetId = notification.userInfo?[Constants.NotificationNameKeys.deleteFavoriteAsset] as? String {
            self.viewModel.deleteFavoriteAsset(assetId: assetId)
        }
    }
    
    @objc func refreshTableView() {
        viewModel.getUserProfileInfo()
    }
    
    func bindViewModelToViewController() {
        viewModel.bindUserProfileInfoToViewController = { userProfileInfo in
            guard let favoriteAssets = userProfileInfo.favoriteAssets else { return }
            self.viewModel.getAllUserFavorites(favoriteAssets: favoriteAssets)
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.refreshControl.endRefreshing()
                if favoriteAssets.isEmpty {
                    strongSelf.activityIndicator.stopAnimating()
                    strongSelf.activityIndicator.isHidden = true
                    strongSelf.favoriteTableView.setEmptyMessage(Constants.EmptyData.noFavoritesAvailable)
                    strongSelf.assets = []
                    strongSelf.favoriteTableView.reloadData()
                } else {
                    strongSelf.favoriteTableView.restore()
                }
            }
        }
        
        viewModel.bindAssetsToViewController = { [weak self] favoriteAssets in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.activityIndicator.isHidden = true
                strongSelf.assets = favoriteAssets
                strongSelf.favoriteTableView.reloadData()
            }
        }
    }

}

extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.favoriteCountLabel.text = "(\(assets.count))"
        return assets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = favoriteTableView.dequeueReusableCell(withIdentifier: Constants.NibNames.favoriteCell, for: indexPath) as? FavoriteCell else {
            return UITableViewCell()
        }
        let asset = assets[indexPath.row]
        cell.updateContent(asset: asset)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedAsset = self.assets[indexPath.row]
        let workstationDetailsVC = WorkstationDetailsViewController()
        // setting up workstationDetail
        workstationDetailsVC.isfromFavoriteViewController = true
        workstationDetailsVC.favoriteListener = self
        workstationDetailsVC.viewModel.selectedAsset = selectedAsset
        let height = UIScreen.main.bounds.height * Constants.WorkstationDetailsBottomSheet.topHeight
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.present(workstationDetailsVC, animated: true, completion: nil)
        } else {
            self.addPullUpController(workstationDetailsVC, initialStickyPointOffset: height, animated: true)
        }
    }
}

extension FavoriteViewController: FavoriteViewControllerDelegate {
    
    func updateFavoritesView() {
        viewModel.getUserProfileInfo()
    }
}
