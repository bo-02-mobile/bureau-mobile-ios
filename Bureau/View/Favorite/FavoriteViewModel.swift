//
//  FavoriteViewModel.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 24/3/22.
//

import Foundation

class FavoriteViewModel: ErrorViewModel {
    
    var apiSharedService: APIServiceProtocol = ApiService.shared
    var bindUserProfileInfoToViewController: ((UserProfile) -> Void) = { _ in
        // will retreave all the user's favorite spaces and executed on its view controller
    }
    var bindAssetsToViewController: (([Asset]) -> Void) = { _ in
        // will retreave all assets details and executed on its view controller
    }
    
    private (set) var userProfileInfo: UserProfile! {
        didSet {
            self.bindUserProfileInfoToViewController(userProfileInfo)
        }
    }
    
    private (set) var assets: [Asset]! {
        didSet {
            self.bindAssetsToViewController(assets)
        }
    }
    
    func getUserProfileInfo() {
        apiSharedService.apiToGetUserProfileInfo { (result) in
            switch (result) {
            case .success(let userProfileInfo):
                self.userProfileInfo = userProfileInfo
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            
        }
    }
    
    func getAllUserFavorites(favoriteAssets: [String]) {
        apiSharedService.apiToGetUserFavorites(assets: favoriteAssets) { result in
            switch (result) {
            case .success(let myAssets):
                let buildings = ApiService.shared.buildingData
                var filteredAssets: [Asset] = []
                buildings?.forEach { buildingItem in
                    filteredAssets.append(contentsOf: myAssets.filter { assetItem in assetItem.buildingID == buildingItem.id })
                }
                self.assets = filteredAssets.sorted(by: {$0.properties?.owner == nil && $1.properties?.owner != nil})
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
            
        }
    }
    
    func getOverlappedAssetsByDate(assetId: String, completion: @escaping ([Booking]) -> Void) {
        var startDateFormat = Helpers.convertDateFormat(inputDate: ApiService.shared.currentDate, inputHour: ApiService.shared.startHourDate)
        startDateFormat = Helpers.convertLocalToUTCFormat(stringDate: startDateFormat, isForQueryParam: true)
        var endDateFormat = Helpers.convertDateFormat(inputDate: ApiService.shared.currentDate, inputHour: ApiService.shared.endHourDate)
        endDateFormat = Helpers.convertLocalToUTCFormat(stringDate: endDateFormat, isForQueryParam: true)
        apiSharedService.apiToGetAssetBookings(assetId: assetId, startTime: startDateFormat, endTime: endDateFormat) { result in
            switch result {
            case .success(let bookings):
                completion(bookings)
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func deleteFavoriteAsset(assetId: String) {
        apiSharedService.apiToDeletefavorite(assetId: assetId) { (result) in
            switch result {
            case .success(let userProfileInfo):
                self.userProfileInfo = userProfileInfo
            case .failure(let error):
                self.displayErrorInView(error: error)
            }
        }
    }
    
    func isAvailable(workstation: Asset) -> Bool {
        return checkWorkstationSchedules(workstation: workstation) &&
            checkBookingWindow(workstation: workstation) &&
            checkAssetMaxMinUsage(workstation: workstation) &&
            checkBookingPermanent(workstation: workstation)
    }
    
    func checkWorkstationSchedules(workstation: Asset) -> Bool {
        guard let schedules = workstation.policies?.schedule else { return true }
        let timezone = ApiService.shared.buildingData
            .first(where: { $0.id == workstation.buildingID })?.timezone ?? TimeZone.current.identifier
        let today = apiSharedService.currentDate
        let startString = Helpers.convertDateFormatUTC(inputDate: today, inputHour: apiSharedService.startHourDate)
        let endString = Helpers.convertDateFormatUTC(inputDate: today, inputHour: apiSharedService.endHourDate)
        let startHourSelected = Helpers.convertDateStringToDate(dateString: startString).convertToTimeZone(timeZoneAbbreviation: timezone)
        var endHourSelected = Helpers.convertDateStringToDate(dateString: endString).convertToTimeZone(timeZoneAbbreviation: timezone)
        let oneDay = Double(Constants.DateTimeFormat.oneDayInSeconds)
        
        for schedule in schedules {
            guard let startTime = schedule.startTime, let endTime = schedule.endTime else { continue }
            var scheduleStartDate = Helpers.convertDateStringToDate(dateString: startTime)
            var scheduleEndDate = Helpers.convertDateStringToDate(dateString: endTime)
            let startDateString = Helpers.convertDateFormatUTC(inputDate: today, inputHour: scheduleStartDate)
            let endDateString = Helpers.convertDateFormatUTC(inputDate: today, inputHour: scheduleEndDate)
            scheduleStartDate = Helpers.convertDateStringToDate(dateString: startDateString)
            scheduleEndDate = Helpers.convertDateStringToDate(dateString: endDateString)
            // adding 1 day if schedule goes over midnight
            if scheduleEndDate < scheduleStartDate {
                scheduleEndDate = scheduleEndDate.addingTimeInterval(oneDay)
            }
            if endHourSelected < startHourSelected {
                endHourSelected = endHourSelected.addingTimeInterval(oneDay)
            }
            if (!(startHourSelected >= scheduleStartDate && endHourSelected <= scheduleEndDate)) {
                return false
            }
        }
        return true
    }
    
    func checkBookingWindow(workstation: Asset) -> Bool {
        let assetBookingWindow = workstation.policies?.bookingWindow ?? 0
        var dayComponent = DateComponents()
        dayComponent.day = assetBookingWindow
        let fromDate = Calendar.current.startOfDay(for: Date())
        let toDate = Calendar.current.startOfDay(for: apiSharedService.currentDate)
        let dateDifference = Calendar.current.dateComponents([.day], from: fromDate, to:  toDate).day ?? 0
        if(dateDifference >= assetBookingWindow && assetBookingWindow != 0) {
            return false
        }
        return true
    }
    
    // Function is not being use. Remains here in case of future need
    func checkAssetMaxMinUsage(workstation: Asset) -> Bool {
        guard let assetMin = workstation.policies?.min else { return true }
        guard let assetMax = workstation.policies?.max else { return true }
        guard let minutesDifference = Calendar.current.dateComponents([.minute], from: apiSharedService.startHourDate, to: apiSharedService.endHourDate)
            .minute else { return true }
        if (minutesDifference < assetMin && minutesDifference >= 0) || (minutesDifference > assetMax && minutesDifference >= 0) {
            return false
        }
        return true
    }
        
    func checkBookingPermanent(workstation: Asset) -> Bool {
        if let ownerName = workstation.properties?.owner?.preferredUsername, !ownerName.isEmpty {
            return false
        }
        if let ownerName = workstation.properties?.owner?.email, !ownerName.isEmpty {
            return false
        }
        return true
    }
    
}
