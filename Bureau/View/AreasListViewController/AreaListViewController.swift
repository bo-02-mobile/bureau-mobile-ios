//
//  AreasListViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import UIKit

class AreaListViewController: FiltersPresenterViewController {
    
    // MARK: - Variables
    var viewModel = AreaListViewModel()

    // MARK: - Constants
    // let notificationCenter = NotificationCenter.default

    // MARK: - Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var mainMenu: MainMenu!
    @IBOutlet var numberOfAreas: UILabel!
    @IBOutlet weak var areasTitleLabel: UILabel!
    
    // MARK: - Outlets Tablet
    @IBOutlet var iPadMenu: MainMenu!
    @IBOutlet weak var buildingsMenu: UIButton!
    
    @IBOutlet var tableToPadMenu: NSLayoutConstraint!
    @IBOutlet var padHeight: NSLayoutConstraint!
    @IBOutlet var padMenuToLabel: NSLayoutConstraint!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        mainMenu.viewControllerDelegate = self
        SplitViewController.shared?.areaListDelegate = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.navigationController?.navigationBar.isHidden = true
            mainMenu.isHidden = true
            iPadMenu.isHidden = false
            if SplitViewController.shared?.displayMode != .twoDisplaceSecondary {
                buildingsMenu.isHidden = false
            }
            self.areasTitleLabel.font = areasTitleLabel.font.withSize(34)
            buildingsMenu.addTarget(self, action: #selector(onShowBuildingsPressed), for: .touchUpInside)
            padMenuToLabel.constant = 12
            iPadMenu.viewControllerDelegate = self
            padHeight.constant = 100
            tableToPadMenu.constant = 12
            tableView.layer.cornerRadius = 10
            iPadMenu.layer.cornerRadius = 10
            self.view.layoutIfNeeded()
        }
        configureTableView()
        bindList()
        bindError()
        viewModel.callFuncToGetUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        executeNewRequest()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func configureTableView() {
        tableView.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        tableView.register(UINib(nibName: Constants.NibNames.buildingCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.buildingCell)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorColor = .clear
    }

    // Update Area Data
    func bindList() {
        viewModel.bindAreaViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.tableView.reloadData()
                strongSelf.activity.stopAnimating()
                strongSelf.activity.isHidden = true
                strongSelf.numberOfAreas.text = "(\(strongSelf.viewModel.dataArea.count))"
                if UIDevice.current.userInterfaceIdiom == .pad {
                    if strongSelf.viewModel.apiServiceShared.selectedArea == nil && !strongSelf.viewModel.dataArea.isEmpty {
                        strongSelf.viewModel.setArea(index: 0)
                        SplitViewController.shared?.bindAreaLayoutUpdate()
                    }
                    guard let index = strongSelf.viewModel.dataArea?
                            .firstIndex(where: { $0.area?.id == strongSelf.viewModel.getArea().id }) else {
                        return
                    }
                    strongSelf.tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
                }
            }
        }
    }
    
    func bindError() {
        viewModel.bindErrorViewModelToController = { title, message in
            self.showErrorMessage(title, message)
            self.activity.stopAnimating()
        }
    }

    func configureView() {
        self.activity.isHidden = false
        self.activity.startAnimating()
        if viewModel.apiServiceShared.buildingId == nil {
            self.numberOfAreas.text = "(\(Constants.Layout.noBuildingSelected))"
            self.activity.isHidden = true
        } else {
            self.viewModel.callFuncToGetOverlappedBookings()
        }
    }
    
    override func executeNewRequest() {
        configureView()
    }
    
    @objc func onShowBuildingsPressed() {
        SplitViewController.shared?.show(.primary)
        self.buildingsMenu.isHidden = true
    }
}

extension AreaListViewController: UITableViewDelegate, UITableViewDataSource {

    // MARK: - Functions TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArea?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.buildingCell, for: indexPath) as? BuildingTableViewCell else {
            return UITableViewCell()
        }
        cell.updateContentAsArea(areaName: viewModel.dataArea[indexPath.row].area?.name ?? Constants.AreaCard.areaName,
                                 buildingName: ApiService.shared.buildingData.first(where: { $0.id == ApiService.shared.buildingId })?.name ?? Constants.AreaCard.buildingName,
                                 space: self.viewModel.getAvaliableSpaceFromArea(index: indexPath.row))
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            cell.separatorLine.isHidden = false
            tableView.backgroundColor = UIColor(named: Constants.Colors.surfaceHigh)
            cell.layer.cornerRadius = 10
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.setArea(index: indexPath.row)
        Helpers.setCrashlyticsKey(Constants.Crashlytics.keyAreaId, as: viewModel.getArea().id)
        if UIDevice.current.userInterfaceIdiom == .pad {
            let cell = tableView.cellForRow(at: indexPath) as? BuildingTableViewCell
            let previousCell = tableView.cellForRow(at: IndexPath(row: indexPath.row - 1, section: 0)) as? BuildingTableViewCell
            SplitViewController.shared?.bindAreaLayoutUpdate()
            SplitViewController.shared?.hide(.primary)
            previousCell?.separatorLine.isHidden = true
            cell?.buildingName.textColor = .white
            cell?.buildingAddress.textColor = UIColor(named: Constants.Colors.textAreaSelected)
            cell?.space.textColor = .white
        } else {
            let areaLayoutVC = AreaLayoutViewController()
            areaLayoutVC.title = viewModel.getArea().code
            self.navigationController?.pushViewController(areaLayoutVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            let cell = tableView.cellForRow(at: indexPath) as? BuildingTableViewCell
            let previousCell = tableView.cellForRow(at: IndexPath(row: indexPath.row - 1, section: 0)) as? BuildingTableViewCell
            previousCell?.separatorLine.isHidden = false
            let space = self.viewModel.getAvaliableSpaceFromArea(index: indexPath.row)
            if space > 0 {
                cell?.space.textColor = UIColor(named: Constants.Colors.textGreen)
            } else {
                cell?.space.textColor = UIColor(named: Constants.Colors.textHigh)?.withAlphaComponent(0.3)
            }
        }
    }

    func onPickerUpdate() {
        self.viewModel.callFuncToGetAreaData()
    }
}

extension AreaListViewController: AreaListViewControllerDeletage {
    
    // MARK: - Functions
    func updateLayout() {
        self.title = ApiService.shared.selectedBuilding.name
        self.bindList()
        self.executeNewRequest()
    }
    
    func showBuildingsMenuButton() {
        self.buildingsMenu?.isHidden = false
    }
}
