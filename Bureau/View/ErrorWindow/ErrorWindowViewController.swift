//
//  ErrorWindowViewController.swift
//  Bureau
//
//  Created by Julio Gabriel Tobares on 24/01/2022.
//

import UIKit

class ErrorWindowViewController: UIViewController {

    // MARK: - Variables
    var viewModel = OverlayViewModel()
    var hasSetPointOrigin = false
    var originPoint: CGPoint?
    var onCloseModal: Void?
    var errorMessage: String?
    var dateBooking: String?
    
    // MARK: - Outlets
    @IBOutlet var lbDateBooking: UILabel!
    @IBOutlet var lbErrorMessage: UILabel!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
    }
 
    // MARK: - Functions
    @IBAction func btCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            originPoint = self.view.frame.origin
        }
    }
    
    func setView() {
        self.lbErrorMessage.text = errorMessage
        self.lbDateBooking.text = dateBooking
    }
    
    func bindMessage(errorMessage: String) {
        self.errorMessage = errorMessage
    }
    
    func bindBookingDateLabel() {
        viewModel.constructBookingDateLabel()
        self.dateBooking = viewModel.bookingDate
    }
}
