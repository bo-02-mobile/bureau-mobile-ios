//
//  SplitViewController.swift
//  Bureau
//
//  Created by Fernando Guerrero on 3/28/22.
//

import UIKit

class SplitViewController: UISplitViewController {
    
    static var shared: SplitViewController?
    var hideBuildingsFlag = false
    
    weak var areaLayoutDelegate: AreaLayoutViewControllerDelegate?
    weak var areaListDelegate: AreaListViewControllerDeletage?
    
    var bindAreaListUpdate: (() -> Void) = {
        // this method will update the area list
    }
    var bindAreaLayoutUpdate : (() -> Void) = {
        // this method will update the area layout
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SplitViewController.shared = self
        self.show(.primary)
        self.bindAreaListUpdate = {
            DispatchQueue.main.async {
                self.areaListDelegate?.updateLayout()
            }
        }
        self.bindAreaLayoutUpdate = {
            DispatchQueue.main.async {
                self.areaLayoutDelegate?.updateLayout(onlyMarker: false)
            }
        }
    }
}
