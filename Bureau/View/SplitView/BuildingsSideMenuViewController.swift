//
//  BuildingsSideMenuViewController.swift
//  Bureau
//
//  Created by Fernando Guerrero on 3/29/22.
//

import UIKit

class BuildingsSideMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var viewModel = BuildingViewModel()
    var userDefaults = UserDefaultsService()
    @IBOutlet var buildingsTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var numberBuildings: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.checkSavedBuilding()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constants.SplitView.sideMenuTitle
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        configureTableView()
        bindBuildings()
        bindError()
        viewModel.callFuncToGetBuildingData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SplitViewController.shared?.areaListDelegate?.showBuildingsMenuButton()
    }
    
    func bindBuildings() {
        viewModel.bindBuildingViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.buildingsTableView.reloadData()
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.activityIndicator.isHidden = true
                strongSelf.checkSavedBuilding()
            }
        }
    }
    
    func bindAreasNumbers() {
        viewModel.bindAreasNumberViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                self?.buildingsTableView.reloadData()
            }
        }
    }
    
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    
    func checkSavedBuilding() {
        let buildingInfo: [String] = userDefaults.getBuildingInfo()
        if buildingInfo != [] {
            let buildingId = buildingInfo[0]
            guard let index = self.viewModel.dataBuilding?.firstIndex(where: { $0.id == buildingId }) else {
                return
            }
            if SplitViewController.shared?.hideBuildingsFlag == false {
                self.buildingsTableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .middle)
                SplitViewController.shared?.hide(.primary)
            } else {
                self.buildingsTableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
            }
        }
        SplitViewController.shared?.hideBuildingsFlag = true
    }
    
    func configureTableView() {
        buildingsTableView.backgroundColor = .clear
        buildingsTableView.register(UINib(nibName: Constants.NibNames.padBuildingCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.padBuildingCell)
        buildingsTableView.dataSource = self
        buildingsTableView.delegate = self
        buildingsTableView.separatorColor = .clear
        buildingsTableView.layer.cornerRadius = 10
    }

    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataBuilding?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.padBuildingCell, for: indexPath) as? PadBuildingTableViewCell else {
            return UITableViewCell()
        }
        if viewModel.dataBuilding[indexPath.row].areasNumber == -1 {
            self.viewModel.callFuncToGetAreasNumberOfBuilding(buildingId: viewModel.dataBuilding[indexPath.row].id)
        }
        cell.updateContent(buildingName: viewModel.dataBuilding[indexPath.row].name!, areasNumber: viewModel.dataBuilding[indexPath.row].areasNumber)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let building = viewModel.dataBuilding[indexPath.row]
        viewModel.setBuilding(building: building)
        viewModel.setBuildingId(buildingID: building.id!)
        Helpers.setCrashlyticsKey(Constants.Crashlytics.keyBuildingId, as: building.id)
        if userDefaults.getBuildingSwitchState() {
            userDefaults.saveBuildingInfo(building: building)
            userDefaults.saveBuildingTimezone(buildingTimezone: building.timezone ?? Constants.DateTimeFormat.utcFormat)
        }
        SplitViewController.shared?.bindAreaListUpdate()
    }
}
