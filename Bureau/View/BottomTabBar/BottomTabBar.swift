//
//  BottomTabBar.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 15/3/22.
//

import UIKit
class BottomTabBar: UITabBarController, UITabBarControllerDelegate {
    
    let mainViewModel = MainViewModel()
    static private(set) var currentInstance: BottomTabBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BottomTabBar.currentInstance = self
        BottomTabBar.currentInstance?.delegate = self
        getBookingsConfiguration()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.navigationController?.navigationBar.isHidden = true
        }
        mainViewModel.callFuncToGetUserProfile()
    }
    
    func getBookingsConfiguration() {
        ApiService.shared.getBookingConfiguration { result in
            switch result {
            case .success(let bookingConfiguration):
                // if limitAfterDeadLine or limitBeforeDeadLine are not retreaved
                // its default value will be 90 min for limitAfterDeadLine and 20 min for limitBeforeDeadLine
                let limitAfterDeadLine = bookingConfiguration.limitAfterDeadLine ?? 90
                let limitBeforeDeadLine = bookingConfiguration.limitBeforeDeadLine ?? 20
                UserDefaultsService.shared.saveLimitAfterDeadLine(after: limitAfterDeadLine)
                UserDefaultsService.shared.saveLimitBeforeDeadLine(before: limitBeforeDeadLine)

                NotificationManager.shared.initNotitications()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if UIDevice.current.userInterfaceIdiom == .pad {
            if viewController.tabBarItem.title == Constants.TabBar.scan {
                let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoard.mainStoryBoardID, bundle: nil)
                    if let qrReaderView = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoard.qrReaderViewControllerID) as? QrReaderViewController {
                        qrReaderView.isButtonHidden = false
                        self.present(qrReaderView, animated: true, completion: nil)
                    }
                    return false
            }
            let index = tabBarController.viewControllers?.firstIndex(of: viewController)
            return ((index != 0) || (index != tabBarController.selectedIndex))
        }
        return true
    }
}
