//
//  ProfileViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/19/21.
//

import UIKit
import Kingfisher
import DropDown

class ProfileViewController: UIViewController {
    
    // MARK: - Constans
    let viewModel = ProfileViewModel()
    var userDefaults = UserDefaultsService()
    let dropDown = DropDown()
    let modeArray = [Constants.Appearance.unspecifiedMode,
                     Constants.Appearance.lightMode,
                     Constants.Appearance.darkMode]
    let typeArray = [Constants.Appearance.unspecified,
                     Constants.Appearance.light,
                     Constants.Appearance.dark]
    let current = UNUserNotificationCenter.current()
    
    // MARK: - Outlets
    @IBOutlet var userPicture: UIImageView!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userEmailLabel: UILabel!
    @IBOutlet var vwDropDown: UIView!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var btDropDown: UIButton!
    @IBOutlet var toggleBuilding: UISwitch!
    @IBOutlet weak var versionLabel: UILabel!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appVersion = Bundle.main.object(forInfoDictionaryKey: Constants.VersionNumber.versionNumberBundleKey) as? String
        self.versionLabel.text = "Version Number: \(appVersion ?? "0.1.0") \(Constants.VersionNumber.versionNumberIsStaging)"
        self.userPicture.image = Constants.Images.defaultProfilePicture
        self.userPicture.tintColor = .gray
        self.userPicture.layer.cornerRadius = 25
        self.userPicture.layer.borderWidth = 2
        self.userPicture.layer.borderColor = #colorLiteral(red: 0.8870649934, green: 0.2483777106, blue: 0.3568376899, alpha: 1)
        bindUser()
        bindLogOut()
        bindError()
        viewModel.callFuncToGetUserData()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.navigationController?.navigationBar.isHidden = true
        }
        configureNavigationBar()
        configureAppereanceDropDown()
        toggleBuilding.setOn(self.userDefaults.getBuildingSwitchState(), animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Functions
    func configureNavigationBar() {
        let logo = UIImage(named: Constants.ImagesNames.brandAndLetters)
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        NSLayoutConstraint.activate([imageView.widthAnchor.constraint(equalToConstant: 150)])
        self.navigationItem.titleView = imageView
    }
    
    func bindUser() {
        viewModel.bindUserToViewController = { user in
            self.userNameLabel.text = user.name
            self.userEmailLabel.text = user.email
            guard let userPicturePath = user.picture else { return }
            DispatchQueue.main.async {
                self.setUserPicture(userPicturePath: userPicturePath)
            }
        }
    }

    private func setUserPicture(userPicturePath: String) {
        self.userPicture.kf.setImage(with: URL(string: userPicturePath), completionHandler: { _, error, _, _  in
            guard error != nil else { return }
            self.userPicture.image = Constants.Images.defaultProfilePicture
        })
    }
    
    func bindLogOut() {
        viewModel.bindLogOutToViewController = {
            Helpers.redirectToLoginPage(isForcedLogout: false)
        }
    }
    
    private func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    
    func configureAppereanceDropDown() {
        if #available(iOS 13.0, *) {
            btDropDown.setTitle("", for: .normal)
            self.lbTitle.text = (UserDefaults.standard.string(forKey: Constants.Appearance.mode) != nil) ? UserDefaults.standard.string(forKey: Constants.Appearance.mode) :  Constants.Appearance.option
            dropDown.anchorView = vwDropDown
            dropDown.dataSource = modeArray
            dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
            dropDown.direction = .bottom
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                self.lbTitle.text = modeArray[index]
                let scenes = UIApplication.shared.connectedScenes
                let windowScene = scenes.first as? UIWindowScene
                let window = windowScene?.windows.first
                window?.overrideUserInterfaceStyle = typeArray[index]
                UserDefaults.standard.set(item, forKey: Constants.Appearance.mode)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func logOutTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: Constants.LoggingOut.title,
                                      message: Constants.LoggingOut.message,
                                      preferredStyle: UIAlertController.Style.alert)
        let clearAuthAction = UIAlertAction(title: Constants.DecisionActionsText.accept, style: .destructive) { _ in
            NotificationManager.shared.removeAllNotifications()
            self.userDefaults.deleteBuildingInfo()
            self.viewModel.callFuncToLogOut()
        }
        alert.addAction(clearAuthAction)
        let cancelAction = UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func showOptions(_ sender:Any) {
        dropDown.show()
    }
    
    @IBAction func saveBuilding(_ sender: Any) {
        self.userDefaults.saveBuildingSwitchState(!self.userDefaults.getBuildingSwitchState())
        toggleBuilding.setOn(self.userDefaults.getBuildingSwitchState(), animated: true)
    }
}
