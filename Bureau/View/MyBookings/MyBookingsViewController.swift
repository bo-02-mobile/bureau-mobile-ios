//
//  MyBookingsViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/2/21.
//

import UIKit
import UserNotifications
import FirebaseCrashlytics

class MyBookingsViewController: UIViewController {
    
    // MARK: - Variables
    var viewModel = MyBookingsViewModel()
    weak var qrReaderDelegate: QrReaderViewControllerDelegate?
    var data = [Constants.MyBookingsListKeys.today: [Booking](),
                Constants.MyBookingsListKeys.upcoming: [Booking]()]
    var keys: [String] = []
    var highlightedBooking: Bool?
    var highlightedBookingId: String?
    // MARK: - Constants
    let refreshControl = UIRefreshControl()
    // MARK: - Outlets
    @IBOutlet var bookingsTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var numberBookings: UILabel!
    
    // MARK: - Constructor
    override func viewDidLoad() {
        super.viewDidLoad()
        bindBookingsWithUserEmail()
        bindBookings()
        configureView()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.navigationController?.navigationBar.isHidden = true
        }
        configureNavigationBar()
        configurePullToRefresh()
        configureObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        viewModel.callFuncToGetUserData()
    }
    
    // MARK: - Functions
    func configurePullToRefresh() {
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        bookingsTableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    func configureObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name(Constants.NotificationNameKeys.updateBookingList), object: nil)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.refreshData()
    }
    
    func refreshData() {
        viewModel.callFuncToGetUserData()
        bindBookingsWithUserEmail()
        bookingsTableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func configureNavigationBar() {
        let logo = UIImage(named: Constants.ImagesNames.brandAndLetters)
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        NSLayoutConstraint.activate([imageView.widthAnchor.constraint(equalToConstant: 150)])
        self.navigationItem.titleView = imageView
    }
    
    func configureView() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        configureTableView()
        bindError()
    }
    
    func bindBookingsWithUserEmail() {
        viewModel.bindUserToViewController = { (user) in
            DispatchQueue.main.async {
                guard let safeUserEmail = user.email else { return }
                self.loadUserBookings(userEmail: safeUserEmail)
            }
        }
    }
    
    func loadUserBookings(userEmail: String) {
        viewModel.callFuncToGetAllBookings(email: userEmail)
    }
    
    func bindBookings() {
        viewModel.bindBookingsViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.numberBookings.text = "(\(strongSelf.viewModel.dataBookings.count))"
                strongSelf.separateDates()
                strongSelf.assignKeys()
                strongSelf.setEmptyMessage()
                strongSelf.bookingsTableView.reloadData()
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.activityIndicator.isHidden = true
            }
        }
    }
    
    func separateDates() {
        self.data[Constants.MyBookingsListKeys.today]?.removeAll()
        self.data[Constants.MyBookingsListKeys.upcoming]?.removeAll()
        for booking in viewModel.dataBookings {
            if let safeStringStartTime = booking.startTime {
                let isPermanentBooking = booking.isPermanent ?? false
                if (Helpers.dateStringIsToday(dateString: safeStringStartTime) || isPermanentBooking) {
                    self.data[Constants.MyBookingsListKeys.today, default: []].append(booking)
                } else {
                    self.data[Constants.MyBookingsListKeys.upcoming, default: []].append(booking)
                }
            }
        }
    }
    
    func assignKeys() {
        self.keys.removeAll()
        if !(self.data[Constants.MyBookingsListKeys.today]?.isEmpty ?? true) {
            self.keys.append(Constants.MyBookingsListKeys.today)
        }
        if !(self.data[Constants.MyBookingsListKeys.upcoming]?.isEmpty ?? true) {
            self.keys.append(Constants.MyBookingsListKeys.upcoming)
        }
    }
    
    func setEmptyMessage() {
        if keys.isEmpty {
            self.bookingsTableView.setEmptyMessage(Constants.EmptyData.noBookingsAvailable)
        } else {
            self.bookingsTableView.restore()
        }
    }
    
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    
    func configureTableView() {
        bookingsTableView.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        bookingsTableView.register(UINib(nibName: Constants.NibNames.bookingCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.bookingCell)
        bookingsTableView.dataSource = self
        bookingsTableView.delegate = self
        bookingsTableView.separatorColor = .clear
    }
}

extension MyBookingsViewController: MyBookingsViewControllerDelegate {
    func updateList() {
        refreshData()
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateLayout), object: nil)
    }
}

extension MyBookingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Functions UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return keys[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = keys[section]
        return data[key]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.bookingCell, for: indexPath) as? BookingTableViewCell else {
            return UITableViewCell()
        }
        
        let key = keys[indexPath.section]
        cell.qrValidation.isHidden = true
        cell.notValidableView.isHidden = true
        
        let building = ApiService.shared.buildingData?.first(where: { $0.id == data[key]?[indexPath.row].buildingId })
        let timezone = building?.timezone ?? Constants.DateTimeFormat.utcFormat
        var startDateFormat = Helpers.convertDateFormat(inputDate: ApiService.shared.currentDate, inputHour: ApiService.shared.startHourDate)
        startDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: startDateFormat, isForQueryParam: true, timezone: timezone)
        if((highlightedBooking ?? false) && startDateFormat == data[key]?[indexPath.row].startTime && highlightedBookingId == data[key]?[indexPath.row].assetId) {
            cell.cardView.backgroundColor = UIColor(named: Constants.Colors.rowHighlighted)
            cell.qrValidation.backgroundColor = UIColor(named: Constants.Colors.rowHighlighted)
            cell.notValidableView.backgroundColor = UIColor(named: Constants.Colors.rowHighlighted)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                cell.cardView.backgroundColor = UIColor(named: Constants.Colors.surfaceHigh)
                cell.qrValidation.backgroundColor = UIColor(named: Constants.Colors.surfaceHigh)
                cell.notValidableView.backgroundColor = UIColor(named: Constants.Colors.surfaceHigh)
            })
            highlightedBooking = false
        }
        if let booking = data[key]?[indexPath.row] {
            cell.updateCotent(booking: booking)
            cell.validateButton.addTarget(self, action: #selector(goToQRScanner), for: .touchUpInside)
            cell.validateQR.addTarget(self, action: #selector(goToQRScanner), for: .touchUpInside)
        }
        
        return cell
    }
    
    @objc func goToQRScanner() {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoard.mainStoryBoardID, bundle: nil)
        if let qrReaderView = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoard.qrReaderViewControllerID) as? QrReaderViewController {
            qrReaderView.isButtonHidden = false
            self.present(qrReaderView, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookingDetailVC = BookingDetailsViewController()
        bookingDetailVC.myBookingsDelegate = self
        let key = keys[indexPath.section]
        if let object = data[key]?[indexPath.row] {
            bookingDetailVC.bookingSelected = object
        }
                
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.present(bookingDetailVC, animated: true, completion: nil)
        } else {
            self.navigationController?.pushViewController(bookingDetailVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        
        let sectionText = UILabel()
        sectionText.frame = CGRect.init(x: 5, y: 5, width: sectionHeader.frame.width-10, height: sectionHeader.frame.height-10)
        sectionText.text = keys[section].uppercased()
        sectionText.font = .systemFont(ofSize: 13, weight: .regular )
        sectionText.textColor = UIColor(named: Constants.Colors.textSection)
        sectionHeader.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        
        sectionHeader.addSubview(sectionText)
        
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
