//
//  CancelBookingViewController.swift
//  Bureau
//
//  Created by user on 7/6/21.
//

import UIKit
import FirebaseAnalytics
import UserNotifications

class CancelBookings: GestureSupport {

    // MARK: - Variables
    let viewModel = CancelBookingViewModel()
    var hasSetPointOrigin = false
    var onCloseModal: Void?
    weak var bookingDetailsDelegate: BookingDetailsControllerDelegate?
    let current = UNUserNotificationCenter.current()
    
    // MARK: - Outlets
    @IBOutlet var closeImage: UIImageView!
    @IBOutlet var workstationCode: UILabel!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewGestureRecongnizer()
        setupCloseGestureRecognizer()
        bindWorkstationCodeLabel()
        bindError()
        bindCancelBookingResponse()
    }
 
    // MARK: - Functions
    @IBAction func confirmCancel(_ sender: UIButton) {
        viewModel.callFuncToDeleteBooking()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            self.originPoint = self.view.frame.origin
        }
    }

    func bindWorkstationCodeLabel() {
        guard let workstation = viewModel.workstationCode else {
            return
        }
        self.workstationCode.text = "\(Constants.CancelBooking.cancelBooking) \(workstation)"
    }

    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            self?.bookingDetailsDelegate?.showCancelBookingError(title: errorTitle, message: errorMessage)
        }
    }
    
    func bindCancelBookingResponse() {
        viewModel.bindCancelBookingResponseToController = { [weak self] in
            if let bookingId = self?.viewModel.bookingID {
                NotificationManager.shared.removeNotification(bookingId: bookingId, bookingDeadLine: .beforeDeadLine)
                NotificationManager.shared.removeNotification(bookingId: bookingId, bookingDeadLine: .afterDeadLine)
            }
            DispatchQueue.main.async { [weak self] in
                self?.bookingDetailsDelegate?.dismissBookingDetailsView()
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateBookingList), object: nil)
            }
        }
    }

    func setBottomSheetData(workstationCode:String, bookingID:String) {
        viewModel.initViewModel(workstationCode: workstationCode, bookingID: bookingID)
    }

    func setupViewGestureRecongnizer() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
    }

    func setupCloseGestureRecognizer() {
        let panGesture = UITapGestureRecognizer(target: self, action: #selector(closeViewRecognizerAction))
        self.closeImage.isUserInteractionEnabled = true
        self.closeImage.addGestureRecognizer(panGesture)
    }
}
