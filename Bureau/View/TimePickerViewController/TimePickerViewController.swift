//
//  TimePickerViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/15/21.
//

import UIKit

class TimePickerViewController: UIViewController {
    
    let viewModel = TimePickerViewModel()
    var startTime = TimeHour()
    var endTime = TimeHour()
    weak var viewControllerDelegate: UpdateContentViewControllerDelegate?

    @IBOutlet var startHour: UIDatePicker!
    @IBOutlet var endHour: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureValues()
        startHour.becomeFirstResponder()
    }
    
    func configureValues() {
        startTime = viewModel.getStartTime()
        endTime = viewModel.getEndTime()
        let calendar = Calendar.current
        var components = DateComponents(hour: startTime.hour, minute: startTime.minute)
        startHour.setDate(calendar.date(from: components)!, animated: false)
        components = DateComponents(hour: endTime.hour, minute: endTime.minute)
        endHour.setDate(calendar.date(from: components)!, animated: false)
    }

    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func doneBtnTapped(_ sender: UIButton) {
        self.viewModel.setStartTimeAndEndTime(startHour: startHour.date, endHour: endHour.date)
        self.dismiss(animated: true) {
            self.viewControllerDelegate?.executeNewRequest()
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
        }
    }
    
    @IBAction func btnNowPress(_ sender: Any) {
        let startDate = NSDate() as Date
        let endDate = Calendar.current.date(byAdding: .hour, value: 1, to: startDate) ?? Date()
        startHour.setDate(startDate, animated: true)
        endHour.setDate(endDate, animated: true)
        
        UIView.animate(withDuration: 1.0) {
            self.endHour.subviews.first?.subviews.last?.backgroundColor = UIColor.orange
            self.endHour.subviews.first?.subviews.last?.backgroundColor = UIColor.systemGray5
        }
    }
}
