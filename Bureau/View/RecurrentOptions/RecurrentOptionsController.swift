//
//  RecurrentOptionsController.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 4/3/22.
//

import Foundation
import UIKit

class RecurrentOptionsController: UIViewController {
    
    @IBOutlet weak var btnRecurrence: UIButton!
    @IBOutlet weak var containerOptions: UIView!
    
    let viewModel = RecurrenceOptionsViewModel()
    let recurrenceDailyOption = RecurrenceDailyOption()
    let recurrenceWeeklyOption = RecurrenceWeeklyOption()
    let recurrenceMonthlyOption = RecurrenceMonthlyOption()
    let recurrenceYearlyOption = RecurrenceYearlyOption()
    var emptyVC = UIViewController()
    let recurrenceOptions = Constants.RecurrentOptions.repeatOptions
    let recurrencePicker = UIPickerView()
    let alertVC = UIViewController()
    var selectedRecurrence = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitValues()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(selectedRecurrence == 0) {
            viewModel.setRecurrenceRules()
        }
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.recurrenceRepeatChanged), object: nil)
    }
    
    func setInitValues() {
        viewModel.updateRecurrenceEndDate()
        let recurrenceRules = viewModel.getTempRecurrenceRule()
        let recurrentItem = recurrenceRules.type.capitalized
        let recurrentIndex = recurrenceOptions.firstIndex(of: recurrentItem) ?? 0
        setRecurrentOptions(index: recurrentIndex, item: recurrentItem)
        self.selectedRecurrence = recurrentIndex
    }
    
    func configurePickerView(picker: UIPickerView, dataSource: [String]) {
        alertVC.view.subviews.forEach({ $0.removeFromSuperview() })
        let screenHeight = UIScreen.main.bounds.height / 5
        let screenWidth = UIDevice.current.userInterfaceIdiom == .pad ? self.view.bounds.width*0.5 : UIScreen.main.bounds.width - 20
        alertVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        picker.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        picker.dataSource = self
        picker.delegate = self
        picker.selectRow(selectedRecurrence, inComponent: 0, animated: false)
        alertVC.view?.addSubview(picker)
        picker.centerXAnchor.constraint(equalTo: alertVC.view.centerXAnchor).isActive = true
        picker.centerYAnchor.constraint(equalTo: alertVC.view.centerYAnchor).isActive = true
        let editRadiousAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        editRadiousAlert.setValue(alertVC, forKey: Constants.AlertPicker.contentKey)
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.done, style: .default, handler: {_ in
            let index = picker.selectedRow(inComponent: 0)
            let item = dataSource[picker.selectedRow(inComponent: 0)]
            self.setRecurrentOptions(index: index, item: item)
            self.selectedRecurrence = index
        }))
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .cancel, handler: nil))
        if UIDevice.current.userInterfaceIdiom == .pad, let popoverController = editRadiousAlert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = [] 
        }
        self.present(editRadiousAlert, animated: true)
    }
    
    func setRecurrentOptions(index: Int, item: String) {
        switch(index) {
        case 0:
            addView(viewController: emptyVC)
        case 1:
            addView(viewController: recurrenceDailyOption)
        case 2:
            addView(viewController: recurrenceWeeklyOption)
        case 3:
            addView(viewController: recurrenceMonthlyOption)
        case 4:
            addView(viewController: recurrenceYearlyOption)
        default:
            addView(viewController: emptyVC)
        }
        btnRecurrence.setTitle(item, for: .normal)
    }

    func addView(viewController: UIViewController) {
        containerOptions.subviews.forEach({ $0.removeFromSuperview() })
        addChild(viewController)
        self.containerOptions.addSubview(viewController.view)
        viewController.didMove(toParent: self)
        viewController.view.frame = CGRect(x: 0, y: 0, width: containerOptions.frame.width, height: containerOptions.frame.size.height)
    }
    
    func removeChildrens() {
        if !self.children.isEmpty {
                let childViewControllers: [UIViewController] = self.children
                for childvc in childViewControllers {
                    childvc.willMove(toParent: nil)
                    childvc.view.removeFromSuperview()
                    childvc.removeFromParent()
                }
            }
    }
    
    @IBAction func btnRecurrencePressed(_ sender: Any) {
        configurePickerView(picker: recurrencePicker, dataSource: recurrenceOptions)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        removeChildrens()
        self.dismiss(animated: true, completion: nil)
    }
}

extension RecurrentOptionsController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return recurrenceOptions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return recurrenceOptions[row]
    }
    
}
