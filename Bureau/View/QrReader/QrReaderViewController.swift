//
//  QrReaderViewController.swift
//  Bureau
//
//  Created by Victor Arana on 6/17/21.
//

import UIKit
import AVFoundation

class QrReaderViewController: UIViewController {

    // MARK: - Variables
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    private var qrViewModel = QrReaderViewModel()
    private var bookingsViewModel = MyBookingsViewModel()
    private var buildingsViewModel = BuildingViewModel()
    let workstationDetailsVC = WorkstationDetailsViewController()
    let notificationManager = NotificationManager.shared
    var isButtonHidden = true

    // MARK: - Outlets
    @IBOutlet var topLeftCorner: UIImageView!
    @IBOutlet var topRightCorner: UIImageView!
    @IBOutlet var bottomLeftCorner: UIImageView!
    @IBOutlet var bottomRightCorner: UIImageView!
    @IBOutlet var keyboardView: UIImageView!
    @IBOutlet var qrReaderView: UIView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var coverView: UIView!
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardImageGestureRecognizer()
        topRightCorner.transform = topRightCorner.transform.rotated(by: .pi / 2)
        bottomRightCorner.transform = bottomRightCorner.transform.rotated(by: .pi)
        bottomLeftCorner.transform = bottomLeftCorner.transform.rotated(by: .pi * 1.5)
        self.bindConfirmedBooking()
        self.bindValidatedBooking()
        self.bindBookingsWithUserEmail()
        self.bindError()
        self.requestCameraAccess()
        self.closeButton.isHidden = isButtonHidden
        self.workstationDetailsVC.areaLayoutDelegate = self
    }

    override func viewDidDisappear(_ animated: Bool) {
        self.removePullUpController(workstationDetailsVC, animated: false)
    }
    
    // MARK: - Events
    @IBAction func goBack(_ sender: UIButton) {
        captureSession = nil
        dismiss(animated: true) {
            self.isButtonHidden = true
        }
    }

    func setupKeyboardImageGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(typeQrCode))
        keyboardView.isUserInteractionEnabled = true
        keyboardView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func typeQrCode() {
        let alert = UIAlertController(title: Constants.TypeWorksationCode.title, message: Constants.TypeWorksationCode.message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = Constants.TypeWorksationCode.textFieldPlaceholder
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.DecisionActionsText.accept, style: .default, handler: { [weak alert] (_) in
            guard let textFields = alert?.textFields else {
                let displayError = Helpers.getErrorMessages(error: .internalError)
                self.showErrorMessage(displayError.title, displayError.message)
                return
            }
            guard let code = textFields[0].text else {
                let displayError = Helpers.getErrorMessages(error: .internalError)
                self.showErrorMessage(displayError.title, displayError.message)
                return
            }
            if code.isEmpty {
                let diplayError = Helpers.getErrorMessages(error: .emptyWorkstationId)
                self.showErrorMessage(diplayError.title, diplayError.message)
                return
            }
            self.qrScannerMessages(code: code, fromQrScanner: false)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func bindConfirmedBooking() {
        qrViewModel.bindConfirmateBookingToController = { [weak self] (confirmationResult) in
            self?.notificationManager.removeNotification(bookingId: confirmationResult.id, bookingDeadLine: .afterDeadLine)
            self?.displayBookingConfirmation(bookingConfirmation: confirmationResult)
        }
    }
    
    func bindValidatedBooking() {
        qrViewModel.bindQRBookingValidateToController = { [weak self] (confirmationResult) in
            self?.displayBookingConfirmationAndValidation(bookingValidation: confirmationResult)
        }
    }
    
    func openWorkstationController(assetSelected: Asset) {
        let height = UIScreen.main.bounds.height * Constants.WorkstationDetailsBottomSheet.topHeight
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.removeBookingController))
        self.coverView.isUserInteractionEnabled = true
        self.coverView.addGestureRecognizer(tapGestureRecognizer)
        self.updateCoverViewBackground(alpha: Constants.WorkstationDetailsBottomSheet.alphaStrength)
        self.workstationDetailsVC.directBook = true
        self.workstationDetailsVC.viewModel.selectedAsset = assetSelected
        self.workstationDetailsVC.bookingCreatedViewControllerDelegate = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.present(workstationDetailsVC, animated: true, completion: nil)
        } else {
            self.addPullUpController(self.workstationDetailsVC, initialStickyPointOffset: height, animated: true)
        }
    }
    
    @objc func removeBookingController() {
        removePullUpController(self.workstationDetailsVC, animated: false)
    }
    
    func unsetCover() {
        self.coverView.isUserInteractionEnabled = false
        self.coverView.gestureRecognizers?.removeAll()
        self.updateCoverViewBackground(alpha: 0)
    }

    func bindError() {
        qrViewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }

    func requestCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.setupCaptureSession()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.setupCaptureSession()
                }
            }
        case .denied, .restricted:
            let displayError = Helpers.getErrorMessages(error: CustomError.cameraPermissionDenied)
            self.showErrorMessage(displayError.title, displayError.message)
            return
        @unknown default:
            let displayError = Helpers.getErrorMessages(error: CustomError.internalError)
            self.showErrorMessage(displayError.title, displayError.message)
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func bindBookingsWithUserEmail() {
        bookingsViewModel.bindUserToViewController = { (user) in
            DispatchQueue.main.async {
                guard let safeUserEmail = user.email else { return }
                self.loadUserBookings(userEmail: safeUserEmail)
            }
        }
    }
    
    func loadUserBookings(userEmail: String) {
        qrViewModel.callFuncToGetBookingsData(email: userEmail)
        buildingsViewModel.callFuncToGetBuildingData()
    }
}

extension QrReaderViewController: AVCaptureMetadataOutputObjectsDelegate {

    // MARK: - Functions
    func setupCaptureSession() {
        DispatchQueue.main.async {
            self.qrReaderView.backgroundColor = UIColor.black
        }
        if Constants.Demo.isASimulator {
            return
        }
        captureSession = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            let displayError = Helpers.getErrorMessages(error: CustomError.internalError)
            self.showErrorMessage(displayError.title, displayError.message)
            return
        }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            let displayError = Helpers.getErrorMessages(error: CustomError.internalError)
            self.showErrorMessage(displayError.title, displayError.message)
            return
        }
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            let displayError = Helpers.getErrorMessages(error: CustomError.scannerNotSupported)
            self.showErrorMessage(displayError.title, displayError.message)
            return
        }
        let metadataOutput = AVCaptureMetadataOutput()
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            let displayError = Helpers.getErrorMessages(error: CustomError.scannerNotSupported)
            self.showErrorMessage(displayError.title, displayError.message)
            return
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        DispatchQueue.main.async {
            self.previewLayer.frame = self.qrReaderView.layer.bounds
            self.previewLayer.videoGravity = .resizeAspectFill
            self.qrReaderView.layer.addSublayer(self.previewLayer)
            self.captureSession.startRunning()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bookingsViewModel.callFuncToGetUserData()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {
                let displayError = Helpers.getErrorMessages(error: CustomError.readQrCodeError)
                self.showErrorMessage(displayError.title, displayError.message, handler: { [weak self] _ in
                    self?.captureSession.startRunning()
                })
                return
            }
            guard let data = readableObject.stringValue else {
                let displayError = Helpers.getErrorMessages(error: CustomError.readQrCodeError)
                self.showErrorMessage(displayError.title, displayError.message, handler: { [weak self] _ in
                    self?.captureSession.startRunning()
                })
                return
            }
            let code = data.getAssetIdFromURL
            qrScannerMessages(code: code, fromQrScanner: true)
        }
    }

    func qrScannerMessages(code: String, fromQrScanner: Bool) {
        guard let assetBooked = self.qrViewModel.getTimeValidatedBooking(assetId: code) else {
            self.qrViewModel.setWorkstation(workstationId: code) { response in
                self.checkQrCodeExist(asset: response, fromQrScanner: fromQrScanner)
            }
            return
        }
        let buildingTimezone = self.buildingsViewModel.getBuildingTimezone(buildingId: assetBooked.booking.buildingId ?? "")
        if assetBooked.status == Constants.QRBookingValidation.bookingToValidate && assetBooked.booking.confirmedAt != nil {
            let message = Helpers.qrAlreadyValidatedString(booking: assetBooked.booking, buildingTimezone: buildingTimezone ?? "")
            self.showErrorMessage(message.title, message.body, handler: { [weak self] _ in
                if fromQrScanner {
                    self?.captureSession.startRunning()
                }
            })
            return
        }
        if assetBooked.status == Constants.QRBookingValidation.nextBooking {
            let message = Helpers.qrNextBookingString(booking: assetBooked.booking, buildingTimezone: buildingTimezone ?? "")
            self.showErrorMessage(message.title, message.body, handler: { [weak self] _ in
                if fromQrScanner {
                    self?.captureSession.startRunning()
                }
            })
            return
        }
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        qrViewModel.callFuncToConfirmateABooking(bookingId: assetBooked.booking.id)
    }
    
    func checkQrCodeExist(asset: Asset?, fromQrScanner: Bool) {
        if let safeAsset = asset {
            let diplayError = Helpers.getErrorMessages(error: .bookingNotFound)
            self.showErrorMessage(diplayError.title, diplayError.message, handler: { [weak self] _ in
                self?.qrViewModel.setStartAndEndTime(startHour: Date())
                self?.openWorkstationController(assetSelected: safeAsset)
            }, cancelHandler: { [weak self] _ in
                if fromQrScanner {
                    self?.captureSession.startRunning()
                }
            })
        } else {
            let diplayError = Helpers.getErrorMessages(error: .noBookingAvailable)
            self.showErrorMessage(diplayError.title, diplayError.message, handler: { [weak self] _ in
                if fromQrScanner {
                    self?.captureSession.startRunning()
                }
            })
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

extension QrReaderViewController: QrReaderViewControllerDelegate {

    // MARK: - Functions
    func displayBookingConfirmation(bookingConfirmation: Booking) {
        DispatchQueue.main.async {
            let slideVC = OverlayViewController()
            slideVC.modalPresentationStyle = .custom
            slideVC.transitioningDelegate = self
            slideVC.setBookingConfirmation(bookingConfirmation: bookingConfirmation)
            self.present(slideVC, animated: true, completion: nil)
        }
    }
    
    func displayBookingConfirmationAndValidation(bookingValidation: Booking) {
        DispatchQueue.main.async {
            let slideVC = OverlayViewController()
            slideVC.modalPresentationStyle = .custom
            slideVC.transitioningDelegate = self
            slideVC.overlayViewDelegate = self.workstationDetailsVC
            slideVC.isNewBooking = true
            slideVC.isNewBookingQR = true
            slideVC.assetId = bookingValidation.assetId ?? ""
            slideVC.setBookingConfirmation(bookingConfirmation: bookingValidation)
            self.present(slideVC, animated: true, completion: nil)
        }
    }
}

extension QrReaderViewController: BookingCreatedViewControllerDelegate {
    func startQrReader() {
        captureSession?.startRunning()
    }
    
    // MARK: - Functions
    func displayBookingCreated(bookingCreated: Booking) {
        qrViewModel.callFuncToValidateBookingAutomatically(bookingId: bookingCreated.id)
    }
}

extension QrReaderViewController: UIViewControllerTransitioningDelegate {

    // MARK: - Functions
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = PresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.modalHeight = CGFloat(ModalType.confirmationBooking.rawValue)
        return presentationController
    }
}

extension QrReaderViewController: AreaLayoutViewControllerDelegate {
    func updateCoverViewBackground(alpha: CGFloat) {
        DispatchQueue.main.async {
            self.coverView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: alpha)
        }
    }
    
    func updateLayout(onlyMarker update: Bool) {
        self.unsetCover()
    }
    
}
