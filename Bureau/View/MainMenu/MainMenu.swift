//
//  MainMenu.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 13/6/21.
//

import UIKit

class MainMenu: UIView {

    // MARK: - Variables
    let mainMenuViewModel = MainMenuViewModel()
    weak var viewControllerDelegate: FiltersViewControllerDelegate?

    // MARK: - Constants
    let popUpPickerWidth = UIScreen.main.bounds.width - 10
    let screenWidth = UIScreen.main.bounds.width - 10
    let screenHeight = UIScreen.main.bounds.height / 2

    // MARK: - Outlets
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnSelectDate: UIButton!
    @IBOutlet var btnSelectStartEndTime: UIButton!
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet weak var btnSelectBookOptions: UIButton!
    // MARK: - Constructors
    override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
        getValuesAndShow()
        createObservers()
    }

    // MARK: - Functions
    func initSubviews() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            Bundle.main.loadNibNamed(Constants.NibNames.mainMenuPad, owner: self, options: nil)
            self.contentView.layer.cornerRadius = 10
        } else {
            Bundle.main.loadNibNamed(Constants.NibNames.mainMenu, owner: self, options: nil)
        }
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }

    @objc func getValuesAndShow() {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = DateFormatter.Style.short
        let date = self.mainMenuViewModel.getDate()
        let start = self.mainMenuViewModel.getStartTime()
        let end = self.mainMenuViewModel.getEndTime()
        let enabledFilters = self.mainMenuViewModel.getEnabledFiltersNumber()
        let newLabel = enabledFilters == 0 ? "FILTER" : "FILTER(\(enabledFilters))"
        DispatchQueue.main.async {
            self.btnSelectDate.setTitle(Helpers.getDateLabel(date: date), for: .normal)
            self.btnSelectStartEndTime.setTitle(Helpers.getTimeLabel(startDate: start, endDate: end), for: .normal)
            self.btnFilter.setTitle(newLabel, for: .normal)
            if UIDevice.current.userInterfaceIdiom == .pad {
                self.btnSelectBookOptions.setTitle(Helpers.getDateAndTimeLabel(startDate: start, endDate: end, date: date), for: .normal)
            }
            
        }
    }
    
    @objc func updateButtonFiltersLabel(notification: NSNotification) {
        if let value = notification.userInfo?[Constants.NotificationNameKeys.updateButtonFiltersLabel] as? Int {
            let newLabel = value == 0 ? "FILTER" : "FILTER(\(value))"
            self.btnFilter.setTitle(newLabel, for: .normal)
        }
    }

    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(getValuesAndShow), name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateButtonFiltersLabel), name: Notification.Name(Constants.NotificationNameKeys.updateButtonFiltersLabel), object: nil)
    }

    // MARK: - Events
    @IBAction func datePicker(_ sender: UIButton) {
        viewControllerDelegate?.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.date)
    }

    @IBAction func timePicker(_ sender: UIButton) {
        viewControllerDelegate?.presentTimePickers()
    }
    
    @IBAction func filtersPickers(_ sender: Any) {
        viewControllerDelegate?.presenFiltersPage()
    }
    
    @IBAction func btnBookOptions(_ sender: Any) {
        viewControllerDelegate?.presentBookingOptions()
    }
}
