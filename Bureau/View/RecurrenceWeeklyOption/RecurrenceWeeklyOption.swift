//
//  RecurrenceWeeklyOption.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 7/3/22.
//

import UIKit

class RecurrenceWeeklyOption: FiltersPresenterViewController {
    
    @IBOutlet weak var btnEvery: UIButton!
    @IBOutlet weak var tableWeekDays: UITableView!
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    
    let viewModel = RecurrenceWeeklyViewModel()
    let everyOptions = Array(1...52).map { String($0) }
    let weekDayOptions = Constants.RecurrentOptions.weekDaysName
    let pickerEvery = UIPickerView()
    let alertVC = UIViewController()
    var everySelected = 0
    var weekDaySelected: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        executeNewRequest()
        self.tableWeekDays.register(UINib.init(nibName: Constants.NibNames.recurrenceWeeklyCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.recurrenceWeeklyCell)
        tableWeekDays.delegate = self
        tableWeekDays.dataSource = self
        setInitValues()
    }
    
    func setInitValues() {
        let currentRecurrence = viewModel.getTempRecurrenceRule()
        if(currentRecurrence.type == Constants.RecurrentOptions.weekly) {
            let everyItem = Int(currentRecurrence.every ?? "1") ?? 1
            selectedPicker(index: everyItem - 1, item: String(everyItem))
            weekDaySelected = currentRecurrence.onDays ?? [Helpers.getDayName(date: viewModel.getTempDate())]
        } else {
            weekDaySelected.append(Helpers.getDayName(date: viewModel.getTempDate()))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.setRecurrenceRules(
            every: everyOptions[everySelected],
            onDays: weekDaySelected
        )
    }
    
    func configurePickerView(picker: UIPickerView) {
        alertVC.view.subviews.forEach({ $0.removeFromSuperview() })
        let screenHeight = UIScreen.main.bounds.height / 5
        let screenWidth = UIScreen.main.bounds.width - 20
        alertVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        picker.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        picker.dataSource = self
        picker.delegate = self
        picker.selectRow(everySelected, inComponent: 0, animated: false)
        alertVC.view?.addSubview(picker)
        picker.centerXAnchor.constraint(equalTo: alertVC.view.centerXAnchor).isActive = true
        picker.centerYAnchor.constraint(equalTo: alertVC.view.centerYAnchor).isActive = true
        let editRadiousAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        editRadiousAlert.setValue(alertVC, forKey: Constants.AlertPicker.contentKey)
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.done, style: .default, handler: {_ in
            let index = picker.selectedRow(inComponent: 0)
            let item = self.everyOptions[picker.selectedRow(inComponent: 0)]
            self.selectedPicker(index: index, item: item)
        }))
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .cancel, handler: nil))
        self.present(editRadiousAlert, animated: true)
    }
    
    func selectedPicker(index: Int, item: String) {
        everySelected = index
        btnEvery.setTitle(item, for: .normal)
    }
    
    override func executeNewRequest() {
        let tempStartDate = self.viewModel.getTempDate()
        self.btnStartDate.setTitle(Helpers.getSecondDateLabel(date: tempStartDate), for: .normal)
        let recurrenceRule = self.viewModel.getTempRecurrenceRule()
        self.btnEndDate.setTitle(Helpers.getSecondDateLabel(date: recurrenceRule.endDate), for: .normal)
    }
    
    @IBAction func btnEveryPressed(_ sender: Any) {
        configurePickerView(picker: pickerEvery)
    }
    
    @IBAction func btnStartDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.tempDate)
    }
    
    @IBAction func btnEndDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.endDate)
    }
}

extension RecurrenceWeeklyOption: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDayOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableWeekDays.dequeueReusableCell(withIdentifier: Constants.NibNames.recurrenceWeeklyCell, for: indexPath)
        cell.textLabel?.text = weekDayOptions[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16)
        cell.tintColor = UIColor.label
        cell.selectionStyle = .none
        for day in weekDaySelected where day == weekDayOptions[indexPath.row] {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableWeekDays.cellForRow(at: indexPath)
        if(cell?.accessoryType == UITableViewCell.AccessoryType.none) {
            cell?.accessoryType = .checkmark
            weekDaySelected.append(weekDayOptions[indexPath.row])
        } else if (weekDaySelected.count > 1) {
            cell?.accessoryType = .none
            weekDaySelected = weekDaySelected.filter {$0 != weekDayOptions[indexPath.row]}
        }
    }
}

extension RecurrenceWeeklyOption: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return everyOptions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return everyOptions[row]
    }
}
