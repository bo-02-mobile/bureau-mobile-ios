//
//  WorkstationDetailsViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import UIKit
import PullUpController
import UserNotifications

class WorkstationDetailsViewController: FiltersPresenterViewController {

    // MARK: - Variables
    var viewModel = WorkstationDetailsViewModel()
    var xOffset:CGFloat = 60
    weak var bookingCreatedViewControllerDelegate: BookingCreatedViewControllerDelegate?
    var navBarHeight: CGFloat = 0
    weak var areaLayoutDelegate: AreaLayoutViewControllerDelegate?
    var bookingWasCreated = false
    var workstationHasOwner = false
    var directBook = false
    var isfromFavoriteViewController = false
    weak var favoriteListener : FavoriteViewControllerDelegate!
    
    override var pullUpControllerPreferredSize: CGSize {
        if isfromFavoriteViewController {
            return CGSize(width: UIScreen.main.bounds.width,
                          height: Constants.WorkstationDetailsBottomSheet.minHeight)
        }
        return CGSize(width: UIScreen.main.bounds.width,
                      height: Constants.WorkstationDetailsBottomSheet.initHeight)
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        if directBook {
            return [100,
                    UIScreen.main.bounds.height * 0.50,
                    UIScreen.main.bounds.height - navBarHeight]
        }

        if isfromFavoriteViewController {
            return [0, 0, 0]
        }

        return [Constants.WorkstationDetailsBottomSheet.initHeight,
                UIScreen.main.bounds.height * 0.50,
                UIScreen.main.bounds.height - navBarHeight - Constants.WorkstationDetailsBottomSheet.topSpace]
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
        let value = point - pullUpControllerMiddleStickyPoints[0]
        let total = pullUpControllerMiddleStickyPoints[2] - pullUpControllerMiddleStickyPoints[0]
        let alpha = (value / total) * Constants.WorkstationDetailsBottomSheet.alphaStrength
        self.areaLayoutDelegate?.updateCoverViewBackground(alpha: alpha + Constants.WorkstationDetailsBottomSheet.initialAlpha)
        // This will only be possible when directBook or booking from favorites is true
        if point <= 100 {
            removePullUpController(self, animated: true)
            if isfromFavoriteViewController {
                self.favoriteListener.updateFavoritesView()
            }
        }
    }

    // MARK: - Constants
    let notificationCenter = NotificationCenter.default

    // MARK: - Outlets
    @IBOutlet var workstationCode: UILabel!
    @IBOutlet weak var calendarDay: CalendarDay!
    @IBOutlet var buttonBookNow: UIButton!
    @IBOutlet var topSliderView: UIView!
    @IBOutlet var featuresLabel: UILabel!
    @IBOutlet var btnSelectStartEndTime: UIButton!
    @IBOutlet var assetType: UILabel!
    @IBOutlet var bookingPolicyTitle: UILabel!
    @IBOutlet var bookingWindowLabel: UILabel!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet var buttonHeight: NSLayoutConstraint!
    @IBOutlet var buttonTopToView: NSLayoutConstraint!
    let notificationManager = NotificationManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 10
        self.topSliderView.layer.cornerRadius = 2.5
        self.configureObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.bookingPolicyTitle.text = Constants.BookingPolicies.empty
        self.configureView()
        self.bookingWasCreated = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            let onlyMarker = !self.bookingWasCreated && !self.directBook
            areaLayoutDelegate?.updateLayout(onlyMarker: onlyMarker)
        } else {
            if self.bookingWasCreated || self.directBook {
                areaLayoutDelegate?.updateLayout(onlyMarker: false)
            }
        }
        viewModel.resetRecurrenceRules()
        bookingCreatedViewControllerDelegate?.startQrReader()
    }

    func configureView() {
        self.bindUserInfo()
        self.bindWorkstationDetails()
        self.bindError()
        self.bindBookings()
        self.bindCreateBooking()
        self.bindFavorites()
        self.calculateNavBarHeight()
        self.calendarDay.updateModel(viewModel: viewModel)
        self.executeNewRequest()
        self.setBookingWindow()
        self.validatePermanentBooking()
        self.setHeartTapGestureRecognizer()
    }
    
    func validatePermanentBooking() {
        let ownerName = viewModel.checkBookingPermanent()
        if ownerName == "" {
            self.btnSelectStartEndTime.isHidden = false
            self.buttonHeight.constant = 35
            self.buttonTopToView.constant = 20
        } else {
            self.btnSelectStartEndTime.isHidden = true
            self.buttonHeight.constant = 0
            self.buttonTopToView.constant = 0
            self.calendarDay.isHidden = true
            self.bookingPolicyTitle.text = Constants.BookingPolicies.permanentBooking
            self.bookingWindowLabel.text = ownerName
            self.buttonBookNow.isHidden = true
        }
    }
    
    func setBookingWindow() {
        let assetBookingWindow = viewModel.checkBookingWindow()
        if(assetBookingWindow == "") {
            self.calendarDay.isHidden = false
            self.buttonBookNow.isHidden = false
            bookingPolicyTitle.text = ""
        } else {
            self.calendarDay.isHidden = true
            self.buttonBookNow.isHidden = true
            bookingPolicyTitle.text = Constants.BookingPolicies.bookingPolicy
        }
        bookingWindowLabel.text = assetBookingWindow
    }
    
    @objc override func executeNewRequest() {
        self.viewModel.loadWorkstationDetails()
        self.viewModel.callFuncToGetWorkstationBookings()
        self.getValuesAndShow()
    }
    
    func getValuesAndShow() {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = DateFormatter.Style.short
        let date = self.viewModel.getDate()
        let start = self.viewModel.getStartTime()
        let end = self.viewModel.getEndTime()
        DispatchQueue.main.async {
            self.setBookingWindow()
            self.validatePermanentBooking()
            self.btnSelectStartEndTime.setTitle(Helpers.getDateAndTimeLabel(startDate: start, endDate: end, date: date), for: .normal)
        }
    }
    
    func bindFavorites() {
        viewModel.bindFavoritesToController = { [weak self] isFavorite in
            DispatchQueue.main.async {
                if isFavorite {
                    self?.heartImageView.tintColor = UIColor(named: Constants.Colors.primaryMain)
                    self?.viewModel.isCurrentAssetFavorite = true
                } else {
                    self?.heartImageView.tintColor = UIColor(named: Constants.Colors.surfaceMedium)
                    self?.viewModel.isCurrentAssetFavorite = false
                }
                NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateFavoritesAsset), object: nil)
            }
        }
    }
    
    func bindWorkstationDetails() {
        viewModel.bindWorkstationToController = { [weak self] workstation in
            self?.viewModel.getFavoriteAsset()
            self?.workstationHasOwner = workstation?.properties?.owner != nil
            DispatchQueue.main.async {
                self?.workstationCode.text = workstation?.code
                self?.featuresLabel.text = workstation?.properties?.labels?.joined(separator: " • ")
                self?.assetType.text = Helpers.getAssetTypeLabel(label: workstation?.properties?.type)
            }
        }
    }
    
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] title, message in
            if title == Constants.ServerError.title {
                self?.displayErrorWindow(errorMessage: message)
            }
            self?.showErrorMessage(title, message)
        }
    }
    
    func bindUserInfo() {
        viewModel.callFuncToGetUserData()
    }
    
    func calculateNavBarHeight() {
        var statusBarHeight: CGFloat = 0
        var topPadding: CGFloat = 0
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            topPadding = window?.safeAreaInsets.top ?? 0
            statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
            
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height
        }
        
        let navHeight = self.navigationController?.navigationBar.frame.size.height ?? 0
        self.navBarHeight = navHeight + (statusBarHeight > topPadding ? statusBarHeight : topPadding)
    }

    func configureObservers() {
        self.notificationCenter.addObserver(self, selector: #selector(WorkstationDetailsViewController.enableButtonBookingNew),
                                            name: Notification.Name(Constants.NotificationNameKeys.clashOfSchedules), object: nil)
        self.notificationCenter.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
    }

    @objc func enableButtonBookingNew(notification: NSNotification) {
        if let isAClash = notification.userInfo?[Constants.NotificationNameKeys.isAClash] as? Bool {
            self.buttonBookNow.isEnabled = !isAClash
            self.buttonBookNow.setTitle(Constants.CreateBookingLabel.bookNow, for: .normal)
            if isAClash {
                self.buttonBookNow.backgroundColor = UIColor(named: Constants.Colors.surfaceDisable)!
                if !viewModel.userBookings.isEmpty {
                    setButtonAsCancel()
                }
            } else {
                self.buttonBookNow.backgroundColor = UIColor(named: Constants.Colors.interactiveDark)
            }
        }
    }

    // MARK: - Functions
    func bindBookings() {
        viewModel.bindWorkstationDetailVMToController = { [weak self] () in
            DispatchQueue.main.async { [self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.calendarDay.calendarDayView.reloadData()
                strongSelf.calendarDay.calendarDayView.move(to: strongSelf.viewModel.getDate())
                strongSelf.calendarDay.calendarDayView.scrollTo(hour24: strongSelf.viewModel.getStartHour())
            }
        }
    }
    
    func displayCancelBooking(assetCode: String) {
        DispatchQueue.main.async {
            let slideVC = CancelBookings()
            slideVC.modalPresentationStyle = .custom
            slideVC.transitioningDelegate = self
            slideVC.bookingDetailsDelegate = self
            let userBookingID = self.viewModel.userBookings[0].id
            slideVC.setBottomSheetData(workstationCode: assetCode, bookingID: userBookingID)
            self.modalHeight = CGFloat(ModalType.confirmationBooking.rawValue)
            self.present(slideVC, animated: true, completion: nil)
        }
    }
    
    func displayErrorWindow(errorMessage: String) {
        DispatchQueue.main.async {
            let slideVC = ErrorWindowViewController()
            slideVC.modalPresentationStyle = .custom
            slideVC.transitioningDelegate = self
            slideVC.bindMessage(errorMessage: errorMessage)
            slideVC.bindBookingDateLabel()
            self.modalHeight = CGFloat(ModalType.confirmationBooking.rawValue)
            self.present(slideVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - Actions
    @IBAction func buttonBookNowPressed(_ sender: UIButton) {
        if viewModel.userBookings.isEmpty {
            self.viewModel.callFuncToCreateABooking()
        } else {
            if let assetCode = viewModel.userBookings[0].assetCode {
                self.displayCancelBooking(assetCode: assetCode)
            }
        }
    }

    func bindCreateBooking() {
        viewModel.bindCreateBookingToController = { [weak self] (confirmationResult) in
            DispatchQueue.main.async { [self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.notificationManager.createNotificationForBooking(booking: confirmationResult)
                if confirmationResult.confirmedAt == nil {
                    strongSelf.notificationManager.createCancelledBooking(booking: confirmationResult)
                }
                strongSelf.bookingCreatedViewControllerDelegate?.displayBookingCreated(bookingCreated: confirmationResult)
                if strongSelf.directBook {
                    return
                }
                let slideVC = OverlayViewController()
                slideVC.viewModel.selectedAsset = self?.viewModel.selectedAsset
                slideVC.setBookingConfirmation(bookingConfirmation: confirmationResult)
                slideVC.modalPresentationStyle = .custom
                slideVC.transitioningDelegate = strongSelf
                slideVC.overlayViewDelegate = strongSelf
                slideVC.isNewBooking = true
                slideVC.assetId = self?.viewModel.selectedAsset?.id ?? ""
                self?.modalHeight = CGFloat(ModalType.confirmationBooking.rawValue)
                strongSelf.present(slideVC, animated: true, completion: nil)
            }
        }
    }
    
    func setButtonAsCancel() {
        self.buttonBookNow.setTitle(Constants.CreateBookingLabel.cancelBooking, for: .normal)
        self.buttonBookNow.backgroundColor = UIColor(named: Constants.Colors.cancel)
        self.buttonBookNow.isEnabled = true
    }
        
    @IBAction func timePicker(_ sender: Any) {
        self.presentBookingOptions()
    }
    
    func setHeartTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTappedHeartImageView))
        heartImageView.isUserInteractionEnabled = true
        heartImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func didTappedHeartImageView() {
        viewModel.createOrDeleteFavorite()
        
    }
}

extension WorkstationDetailsViewController : BookingDetailsControllerDelegate {

    // MARK: - Functions
    func dismissBookingDetailsView() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.dismiss(animated: true)
        } else {
            self.calendarDay.removeCurrentEventView()
            self.executeNewRequest()
            self.bookingWasCreated = true
        }
    }
    
    func showCancelBookingError(title: String, message: String) {
        self.showErrorMessage(title, message)
    }
}
