//
//  BuildingListViewController.swift
//  Bureau
//
//  Created by user on 7/18/21.
//

import UIKit
import FirebaseAnalytics

class BuildingListViewController: UIViewController {
    var viewModel = BuildingViewModel()
    var userDefaults = UserDefaultsService()
    @IBOutlet var buildingsTableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var numberBuildings: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        checkSavedBuilding()
        self.configureView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    func configureView() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        configureTableView()
        bindBuildings()
        bindError()
        viewModel.callFuncToGetBuildingData()
        configureNavigationBar()
    }
    
    func configureNavigationBar() {
        let logo = UIImage(named: Constants.ImagesNames.brandAndLetters)
        let imageView = UIImageView(image:logo)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        NSLayoutConstraint.activate([imageView.widthAnchor.constraint(equalToConstant: 150)])
        self.navigationItem.titleView = imageView
    }
    
    func bindBuildings() {
        viewModel.bindBuildingViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.buildingsTableView.reloadData()
                strongSelf.activityIndicator.stopAnimating()
                strongSelf.activityIndicator.isHidden = true
                
                strongSelf.numberBuildings.text = "(\(strongSelf.viewModel.dataBuilding!.count))"
            }
        }
    }
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.activityIndicator.isHidden = true
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    func configureTableView() {
        buildingsTableView.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        buildingsTableView.register(UINib(nibName: Constants.NibNames.buildingCell, bundle: nil), forCellReuseIdentifier: Constants.NibNames.buildingCell)
        buildingsTableView.dataSource = self
        buildingsTableView.delegate = self
        buildingsTableView.separatorColor = .clear
    }
    
    func checkSavedBuilding() {
        let buildingInfo: [String] = userDefaults.getBuildingInfo()
        if buildingInfo != [] {
            let areasVC = AreaListViewController()
            areasVC.title = buildingInfo[1]
            viewModel.setBuildingId(buildingID: buildingInfo[0])
            self.navigationController?.pushViewController(areasVC, animated: true)
        }
        // Navigate to AreaList if using iPad
        else if UIDevice.current.userInterfaceIdiom == .pad {
            let areasVC = AreaListViewController()
            self.navigationController?.pushViewController(areasVC, animated: false)
        }
    }
}
extension BuildingListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataBuilding?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.buildingCell, for: indexPath) as? BuildingTableViewCell else {
            return UITableViewCell()
        }
        cell.updateContent(buildingName: viewModel.dataBuilding[indexPath.row].name!, buildingAddress: viewModel.dataBuilding[indexPath.row].address!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let building = viewModel.dataBuilding[indexPath.row]
        viewModel.setBuilding(building: building)
        viewModel.setBuildingId(buildingID: building.id!)
        Helpers.setCrashlyticsKey(Constants.Crashlytics.keyBuildingId, as: building.id)
        if userDefaults.getBuildingSwitchState() {
            userDefaults.saveBuildingInfo(building: building)
            userDefaults.saveBuildingTimezone(buildingTimezone: building.timezone ?? Constants.DateTimeFormat.utcFormat)
        }
        let areasVC = AreaListViewController()
        areasVC.title = building.name
        self.navigationController?.pushViewController(areasVC, animated: true)
    }
}
