//
//  CalendarDay.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 14/6/21.
//

import UIKit
import CalendarKit

class CalendarDay: UIView, EventDataSource {

    // MARK: - Variables
    var viewModel: WorkstationDetailsViewModel!
    var viewModelUser = BookingOptionsViewModel()
    var calendarDayViewModel = CalendarDayViewModel()
    var newBooking: Event = Event()
    var currentEventView: EventView?
    var user: User!
    
    // MARK: - Constants
    let defaults = UserDefaults.standard

    // MARK: - Outtlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var calendarDayView: DayView!

    // MARK: - Constructors
    override func awakeFromNib() {
        super.awakeFromNib()
        initSubviews()
        configureCalendar()
        createObservers()
        refreshNewBooking()
    }

    // MARK: - Functions
    func initSubviews() {
        Bundle.main.loadNibNamed(Constants.NibNames.calendarDay, owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }

    func configureCalendar() {
        calendarDayView.dataSource = self
        calendarDayView.isHeaderViewVisible = false
        calendarDayView.delegate = self
    }

    func updateModel(viewModel: WorkstationDetailsViewModel) {
        self.viewModel = viewModel
    }

    func refreshNewBooking() {
        let currentDate = calendarDayViewModel.getDate()
        var startTime = calendarDayViewModel.getStartTime()
        var endTime = calendarDayViewModel.getEndTime()
        let componentsDate = Calendar.current.dateComponents([.year, .month, .day], from: currentDate)
        var componentsStartTime = Calendar.current.dateComponents([.hour, .minute], from: startTime)
        var componentsEndTime = Calendar.current.dateComponents([.hour, .minute], from: endTime)
        componentsStartTime.year = componentsDate.year
        componentsStartTime.month = componentsDate.month
        componentsStartTime.day = componentsDate.day
        componentsEndTime.year = componentsDate.year
        componentsEndTime.month = componentsDate.month
        componentsEndTime.day = componentsDate.day
        startTime = Calendar.current.date(from: componentsStartTime)!
        endTime = Calendar.current.date(from: componentsEndTime)!
        self.newBooking.startDate = Helpers.dateLocalToUTC(dateLocal: startTime)
        self.newBooking.endDate = Helpers.dateLocalToUTC(dateLocal: endTime)
    }

    func isAClashOfSchedules() -> Bool {
        var isAClash = false
        for booking in viewModel.dataBooking {
            let permanentBooking = booking.isPermanent ?? false
            if (permanentBooking) {
                break
            }
            guard let timezone = ApiService.shared.buildingData
                    .first(where: { $0.id == booking.buildingId })?.timezone else { break }
            var startDate = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: booking.startTime!))
            var endDate = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: booking.endTime!))
            startDate = Helpers.dateUTCToLocal(dateUTC: startDate.addingTimeInterval(1))
            endDate = Helpers.dateUTCToLocal(dateUTC: endDate.addingTimeInterval(-1))
            startDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startDate, timezoneAbbreviation: timezone)
            endDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: endDate, timezoneAbbreviation: timezone)
            if startDate.isBetween(newBooking.startDate, and: newBooking.endDate) {
                isAClash = true
                break
            }
            if endDate.isBetween(newBooking.startDate, and: newBooking.endDate) {
                isAClash = true
                break
            }
            if newBooking.startDate.isBetween(startDate, and: endDate) {
                isAClash = true
                break
            }
        }
        let clashOfSchedules = Notification.Name(Constants.NotificationNameKeys.clashOfSchedules)
        NotificationCenter.default.post(name: clashOfSchedules, object: nil, userInfo: [Constants.NotificationNameKeys.isAClash :isAClash])

        return isAClash
    }

    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateStartEndTime), name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateStartEndTime), name: Notification.Name(Constants.UserValidation.userSelected), object: nil)
    }

    @objc func updateStartEndTime() {
        DispatchQueue.main.async {
            self.refreshNewBooking()
            self.updateText()
        }
    }

    func updateText() {
        user = viewModelUser.getSelectedUser()
        guard let name = user.name else { return }
        if (name == Constants.UserValidation.emptyText) {
            self.newBooking.text = user.email ?? Constants.UserValidation.emailUndefined
        } else {
            self.newBooking.text = name
        }
    }
    
    func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events: [Event] = []

        let colorWorkstationReserved = UIColor(named: Constants.Colors.primaryLight)!
        let colorLetterCalendar = UIColor(named: Constants.Colors.textGray)!
        let colorScheduleBooking = UIColor(named: Constants.Colors.surfaceDisable)!

        if (viewModel != nil ) {
            for booking in viewModel.dataBooking {
                let event: Event = Event()

                guard let safeStartTime = booking.startTime else { break }
                guard let safeEndTime = booking.endTime else { break }
                guard let owner = booking.owner else { break }
                guard let ownerName = owner.preferredUsername else { break }
                let timezone = ApiService.shared.buildingData
                        .first(where: { $0.id == booking.buildingId })?.timezone ?? Constants.DateTimeFormat.utcFormat

                let starDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: safeStartTime))
                let endDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: safeEndTime))
                
                if (starDatetUTC == endDatetUTC) {
                    continue
                }
                
                event.startDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: starDatetUTC.addingTimeInterval(1), timezoneAbbreviation: timezone)
                event.endDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: endDatetUTC.addingTimeInterval(-1), timezoneAbbreviation: timezone) // To avoid bookings overlapping add/remove 1 second
                
                if booking.id == Constants.BookingPolicies.scheduleBookingId {
                    event.backgroundColor = colorScheduleBooking
                    event.color = colorScheduleBooking
                } else {
                    event.backgroundColor = colorWorkstationReserved
                    event.color = colorWorkstationReserved
                }
                event.textColor = colorLetterCalendar
                event.text = ownerName
                event.userInfo = booking
                events.append(event)
            }

            if newBooking.startDate < newBooking.endDate {
                if isAClashOfSchedules() {
                    newBooking.backgroundColor = UIColor(named: Constants.Colors.surfaceConflict)!
                    newBooking.color = UIColor(named: Constants.Colors.surfaceConflict)!
                } else {
                    newBooking.backgroundColor = UIColor(named: Constants.Colors.interactiveLight)!
                    newBooking.color = UIColor(named: Constants.Colors.interactiveLight)!
                }
                newBooking.textColor = UIColor(named: Constants.Colors.interactiveDark)!
                calendarDayViewModel.getUserName { (result) in
                    switch (result) {
                    case .success(let userName):
                        if (self.user != nil) {
                            self.updateText()
                        } else {
                            self.newBooking.text = userName
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
                events.append(newBooking)
            }
        }
        return events
    }
    
    func removeCurrentEventView() {
        self.currentEventView = nil
    }
}

extension CalendarDay: DayViewDelegate {
    
    func dayViewDidSelectEventView(_ eventView: EventView) {
        guard let event = eventView.descriptor as? Event else {
            return
        }
        guard let safeBooking = event.userInfo as? Booking else {
            self.currentEventView?.color = Constants.Colors.workstationReservedColor
            self.currentEventView?.backgroundColor = Constants.Colors.workstationReservedColor
            self.viewModel.checkBookingExists()
            _ = self.isAClashOfSchedules()
            return
        }
        calendarDayViewModel.getUserEmail { (result) in
            switch (result) {
            case .success(let userEmail):
                self.currentEventView?.color = Constants.Colors.workstationReservedColor
                self.currentEventView?.backgroundColor = Constants.Colors.workstationReservedColor
                if userEmail == safeBooking.owner?.email {
                    eventView.color = UIColor(named: Constants.Colors.secondaryLight)!
                    eventView.backgroundColor = UIColor(named: Constants.Colors.secondaryLight)!
                    self.currentEventView = eventView
                    self.viewModel.setUserBooking(booking: safeBooking)
                    NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.clashOfSchedules), object: nil,
                                                    userInfo: [Constants.NotificationNameKeys.isAClash: true])
                } else {
                    self.viewModel.checkBookingExists()
                    _ = self.isAClashOfSchedules()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func dayViewDidLongPressEventView(_ eventView: EventView) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didTapTimelineAt date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didLongPressTimelineAt date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayViewDidBeginDragging(dayView: DayView) {
        // MARK: - Default Implementation
    }
    
    func dayViewDidTransitionCancel(dayView: DayView) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, willMoveTo date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didMoveTo date: Date) {
        DispatchQueue.main.async {
            if Calendar.current.component(.day, from: date) != Calendar.current.component(.day, from: self.viewModel.getDate()) {
                self.calendarDayView.move(to: self.viewModel.getDate())
            } else {
                self.calendarDayView.scrollTo(hour24: self.calendarDayViewModel.getStartHour())
            }
        }
    }
    
    func dayView(dayView: DayView, didUpdate event: EventDescriptor) {
        // MARK: - Default Implementation
    }
    
}
