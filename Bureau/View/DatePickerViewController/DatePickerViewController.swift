//
//  DatePickerViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/14/21.
//

import UIKit

class DatePickerViewController: UIViewController {
    
    var selectedDate = Date()
    let datePickerViewModel = DatePickerViewModel()
    weak var viewControllerDelegate: UpdateContentViewControllerDelegate?
    let pickerSetType: String
    
    init(pickerSetType: String) {
        self.pickerSetType = pickerSetType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("DatePickerController coder not initilized")
    }
    
    @IBOutlet var dateCalendarPicker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()
        setCalendarDate()
        dateCalendarPicker.addTarget(self, action: #selector(DatePickerViewController.handler(sender:)), for: UIControl.Event.valueChanged)
    }
    
    func setCalendarDate() {
        switch(pickerSetType) {
        case Constants.DatePickerSetType.date:
            selectedDate = self.datePickerViewModel.getDate()
        case Constants.DatePickerSetType.tempDate:
            selectedDate = self.datePickerViewModel.getTempDate()
        case Constants.DatePickerSetType.endDate:
            selectedDate = self.datePickerViewModel.getEndDate()
        default:
            return
        }
        self.dateCalendarPicker.date = selectedDate
    }

    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func doneButtonTapped(_ sender: Any) {
        self.datePickerViewModel.setDate(date: selectedDate, pickerSetType: pickerSetType)
        self.dismiss(animated: true) {
            self.viewControllerDelegate?.executeNewRequest()
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
        }
    }
    
    @objc func handler(sender: UIDatePicker) {
        self.selectedDate = dateCalendarPicker.date
    }

}
