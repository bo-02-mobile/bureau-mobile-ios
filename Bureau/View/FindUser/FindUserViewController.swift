//
//  FindUserViewController.swift
//  Bureau
//
//  Created by Julio Gabriel Tobares on 16/03/2022.
//

import UIKit

class FindUserViewController: UIViewController {
    
    var viewModel = FindUserViewModel()
    var filteredData: [User]!
    var searchText: String = ""
    var validatedEmail: String = ""
    var isValidated: Bool = false
    
    @IBOutlet var searchUser: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        searchUser.delegate = self
        filteredData = []
        bindUsers()
        bindError()
        registerTableViewCells()
        viewModel.callFuncToGetUserData()
    }
    
    private func registerTableViewCells() {
        let cellNib = UINib(nibName: Constants.NibNames.searchUserTableCell, bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: Constants.NibNames.searchUserCell)
    }
    
    func bindUsers() {
        viewModel.bindFindUserViewModelToController = { [weak self] () in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FindUserViewController: UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.NibNames.searchUserCell, for: indexPath) as? SearchUserTableViewCell else { return UITableViewCell() }
        cell.updateContent(userName: filteredData[indexPath.row].name ?? "",
                           userEmail: filteredData[indexPath.row].email!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var userObj:User = filteredData[indexPath.row]
        if (userObj.name == Constants.UserValidation.bookForThis) {
            if (!self.isValidated) {
                return
            } else {
                userObj.name = Constants.UserValidation.emptyText
                userObj.email = self.validatedEmail
            }
        }
        viewModel.setUser(user: userObj)
        NotificationCenter.default.post(name: Notification.Name(Constants.UserValidation.userSelected), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let safeUsers = viewModel.dataUser else { return }
        filteredData = searchText.isEmpty ? safeUsers : safeUsers.filter { (item: User) -> Bool in
            return item.email?.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        if (filteredData.isEmpty) {
            self.isValidated = searchText.isValidEmail()
            var user = User()
            if (!self.isValidated) {
                user.name = Constants.UserValidation.enterValidEmail
                user.email = Constants.UserValidation.forExample
            } else {
                self.validatedEmail = searchText
                user.name = Constants.UserValidation.bookForThis
                user.email = "\(self.validatedEmail)"
            }
            filteredData = [user]
        }
        tableView.reloadData()
    }
}
