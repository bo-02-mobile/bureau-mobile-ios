//
//  LoginViewController.swift
//  Bureau
//
//  Created by Victor Arana on 6/5/21.
//

import UIKit
import AppAuth

class LoginViewController: UIViewController {

    // MARK: - Variables
    private var authState: OIDAuthState?
    private var loginViewModel = LoginViewModel()

    // MARK: - Outlets
    @IBOutlet var loginButton: UIButton!

    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 24
        configureView()
        loginViewModel.tryLogout(thenLogin: false)
    }

    // MARK: - Events
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() {
            loginViewModel.tryLogout(thenLogin: true)
        } else {
            let diplayError = Helpers.getErrorMessages(error: CustomError.noInternetConnection)
            self.showErrorMessage(diplayError.title, diplayError.message)
        }
    }

    // MARK: - Functions
    private func configureView() {
        self.bindOIDAuthorizationRequest()
        self.bindError()
        self.bindAuthState()
    }
}

// MARK: - Extensions
extension LoginViewController {

    // MARK: - Functions
    func doAuthWithAutoCodeExchange(request: OIDAuthorizationRequest) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            self.showErrorMessage(Constants.InternalError.title, Constants.InternalError.message)
            return
        }
        appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) { authState, error in
            if error != nil {
                self.loginViewModel.callFuncToSetAuthState(authState: nil)
                let diplayError = Helpers.getErrorMessages(error: CustomError.authenticationFailed)
                self.showErrorMessage(diplayError.title, diplayError.message)
                return
            }
            guard let authStateResponse = authState else {
                self.loginViewModel.callFuncToSetAuthState(authState: nil)
                let diplayError = Helpers.getErrorMessages(error: CustomError.authenticationFailed)
                self.showErrorMessage(diplayError.title, diplayError.message)
                return
            }
            self.loginViewModel.callFuncToSetAuthState(authState: authStateResponse)
        }
    }

    func bindOIDAuthorizationRequest() {
        loginViewModel.bindAuthRequestViewModelToController = { [weak self] (request) in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.doAuthWithAutoCodeExchange(request: request)
            }
        }
    }

    private func bindError() {
        loginViewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }

    func bindAuthState() {
        loginViewModel.bindLoginViewModelToController = { [weak self] (_) in
            DispatchQueue.main.async {
                guard let strongSelf = self else {
                    return
                }
                strongSelf.redirectToMainPage()
            }
        }
    }

    func redirectToMainPage() {
        // Save Building Switch ON by default when logging in
        let userDefaults = UserDefaultsService()
        userDefaults.saveBuildingSwitchState(true)
        
        let viewControllerID: String
        if UIDevice.current.userInterfaceIdiom == .pad {
            viewControllerID = Constants.StoryBoard.splitViewControllerID
        } else {
            viewControllerID = Constants.StoryBoard.mainViewControllerID
        }
        let viewController = SceneDelegate.mainStoryboard.instantiateViewController(withIdentifier: viewControllerID)
        UIApplication.shared.windows.first?.rootViewController = viewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
