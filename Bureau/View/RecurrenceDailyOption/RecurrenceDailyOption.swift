//
//  RecurrenceDailyOption.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 7/3/22.
//

import UIKit

class RecurrenceDailyOption: FiltersPresenterViewController {

    @IBOutlet weak var btnEvery: UIButton!
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    
    let viewModel = RecurrenceDailyViewModel()
    let everyOptions = Array(1...99).map { String($0) }
    let pickerEvery = UIPickerView()
    let alertVC = UIViewController()
    var everySelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitValues()
        executeNewRequest()
    }
    
    func setInitValues() {
        let currentRecurrence = viewModel.getTempRecurrenceRule()
        if(currentRecurrence.type == Constants.RecurrentOptions.daily) {
            let everyItem = Int(currentRecurrence.every ?? "1") ?? 1
            pickedSelected(index: everyItem - 1, item: String(everyItem))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.setRecurrenceRules(
            every: everyOptions[everySelected]
        )
    }
    
    func configurePickerView(picker: UIPickerView) {
        alertVC.view.subviews.forEach({ $0.removeFromSuperview() })
        let screenHeight = UIScreen.main.bounds.height / 5
        let screenWidth = UIScreen.main.bounds.width - 20
        alertVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        picker.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        picker.dataSource = self
        picker.delegate = self
        picker.selectRow(everySelected, inComponent: 0, animated: false)
        alertVC.view?.addSubview(picker)
        picker.centerXAnchor.constraint(equalTo: alertVC.view.centerXAnchor).isActive = true
        picker.centerYAnchor.constraint(equalTo: alertVC.view.centerYAnchor).isActive = true
        let editRadiousAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        editRadiousAlert.setValue(alertVC, forKey: Constants.AlertPicker.contentKey)
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.done, style: .default, handler: {_ in
            let index = picker.selectedRow(inComponent: 0)
            let item = self.everyOptions[picker.selectedRow(inComponent: 0)]
            self.pickedSelected(index: index, item: item)
        }))
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .cancel, handler: nil))
        self.present(editRadiousAlert, animated: true)
    }
    
    func pickedSelected(index: Int, item: String) {
        everySelected = index
        btnEvery.setTitle(item, for: .normal)
    }

    override func executeNewRequest() {
        let tempStartDate = self.viewModel.getTempDate()
        self.btnStartDate.setTitle(Helpers.getSecondDateLabel(date: tempStartDate), for: .normal)
        let recurrenceRule = self.viewModel.getTempRecurrenceRule()
        self.btnEndDate.setTitle(Helpers.getSecondDateLabel(date: recurrenceRule.endDate), for: .normal)
    }
    
    @IBAction func btnEveryPressed(_ sender: Any) {
        configurePickerView(picker: pickerEvery)
    }
    
    @IBAction func btnStartDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.tempDate)
    }
    
    @IBAction func btnEndDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.endDate)
    }
}

extension RecurrenceDailyOption: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return everyOptions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return everyOptions[row]
    }
}
