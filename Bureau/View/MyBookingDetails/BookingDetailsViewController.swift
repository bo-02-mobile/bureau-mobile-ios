//
//  BookingDetailsViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/2/21.
//

import UIKit
import CalendarKit

class BookingDetailsViewController: UIViewController, EventDataSource {
    
    // MARK: - Variables
    var viewModel: BookingDetailViewModel = BookingDetailViewModel()
    var myBookingsviewModel = MyBookingsViewModel()
    var bookingSelected: Booking!
    var isCancelDirectly: Bool = false
    weak var myBookingsDelegate: MyBookingsViewControllerDelegate?
    var isPermanentBooking = false
    
    // MARK: - Outlets
    @IBOutlet var assetCode: UILabel!
    @IBOutlet var dateBookingLabel: UILabel!
    @IBOutlet var calendarDay: DayView!
    @IBOutlet var assetType: UILabel!
    @IBOutlet var assetLocalitation: UILabel!
    @IBOutlet var assetLabels: UILabel!
    @IBOutlet var validateButton: UIButton!
    @IBOutlet weak var validationLabel: UILabel!
    @IBOutlet var qrButton: UIButton!
    @IBOutlet weak var btnCancelBooking: UIButton!
    
    // MARK: - Constructor
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAssetData()
        bindAssetData()
        isPermanentBooking = bookingSelected.isPermanent ?? false
        if(!isPermanentBooking) {
            loadBookings()
            bindBookings()
            configureCalendar()
        }
        bindBookingData()
        if isCancelDirectly {
            showCancelPopUp()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            myBookingsDelegate?.updateList()
        }
    }
    
    func bindBookingData() {
        guard let safeStartTime = bookingSelected.startTime else {
            return
        }
        
        assetCode.text = bookingSelected.assetCode
        assetType.text = Helpers.getAssetTypeLabel(label: bookingSelected.assetType)
        assetLocalitation.text = bookingSelected?.areaName
        if (!Helpers.dateStringIsToday(dateString: safeStartTime) && !isPermanentBooking) {
            qrButton.isHidden = true
            validateButton.isHidden = true
        } else if (bookingSelected.confirmedAt != nil || isPermanentBooking) {
            qrButton.setImage(UIImage(named: Constants.Images.qrCodeCheck), for: .normal)
            qrButton.isEnabled = false
            validateButton.setTitle(Constants.MyBookingsValidate.validated, for: .normal)
            validateButton.setTitleColor(UIColor(named: Constants.Colors.textGreen), for: .normal)
            validateButton.isHidden = false
            validateButton.isEnabled = false
        } else if Helpers.isBookingInTimeForValidation(booking: bookingSelected) {
            validateButton.isHidden = false
            qrButton.setImage(UIImage(named: Constants.Images.qrCodeLess), for: .normal)
            qrButton.isEnabled = true
        } else {
            qrButton.isHidden = true
            validateButton.isHidden = true
            validationLabel.isHidden = false
            let timezone = ApiService.shared.buildingData.first(where: { $0.id == bookingSelected.buildingId })?.timezone ?? Constants.DateTimeFormat.utcFormat
            var startDate = Helpers.convertDateStringToDate(dateString: safeStartTime)
            startDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startDate, timezoneAbbreviation: timezone)
            let beforeDeadLine = UserDefaultsService.shared.getLimitBeforeDeadLine()
            let afterDeadLine = UserDefaultsService.shared.getLimitAfterDeadLine()
            let initValidationDate = Helpers.addMinutesToDate(date: startDate, minutes: beforeDeadLine * -1)
            let endValidationDate = Helpers.addMinutesToDate(date: startDate, minutes: afterDeadLine)
            validationLabel.text = Helpers.constructLabelForValidation(initDate: initValidationDate, endDate: endValidationDate)
        }
        if(isPermanentBooking) {
            calendarDay.isHidden = true
            btnCancelBooking.isHidden = true
            dateBookingLabel.text = Constants.BookingPolicies.bookingDetailPermanent
        } else {
            dateBookingLabel.text = (Helpers.getAssetTypeLabel(label: bookingSelected.reservationMessage))
        }
        title = bookingSelected.assetCode
    }
    
    func configureCalendar() {
        calendarDay.dataSource = self
        calendarDay.isHeaderViewVisible = false
        calendarDay.delegate = self
    }
    
    func eventsForDate(_ date: Date) -> [EventDescriptor] {
        var events: [Event] = []
        let colorWorkstationReserved = UIColor(named: Constants.Colors.primaryLight)!
        let colorBookingSelected = UIColor(named: Constants.Colors.interactiveLight)!
        let colorInteractiveDark = UIColor(named: Constants.Colors.interactiveDark)!
        for booking in viewModel.dataBooking {
            if let startTime = booking.startTime, let endTime = booking.endTime {
                let ownerName = booking.owner?.preferredUsername ?? booking.owner?.email ?? Constants.EmptyData.noOwner
                let event: Event = Event()
                guard let timezone = ApiService.shared.buildingData
                        .first(where: { $0.id == booking.buildingId })?.timezone else { break }

                let startDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: startTime))
                let endDatetUTC = Helpers.convertDateStringToDate(dateString: Helpers.correctDateFormatFromApi(date: endTime))
                event.startDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startDatetUTC, timezoneAbbreviation: timezone)
                event.endDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: endDatetUTC, timezoneAbbreviation: timezone)
                if booking.owner?.email == bookingSelected.owner?.email {
                    event.color = colorBookingSelected
                    event.textColor = colorInteractiveDark
                } else {
                    event.color = colorWorkstationReserved
                }
                
                event.text = ownerName
                events.append(event)
            }
        }
        return events
    }
    
    func loadAssetData() {
        guard let safeAssetId = bookingSelected.assetId else { return }
        self.viewModel.callFuncToGetAssetWithID(assetId: safeAssetId)
    }
    
    func bindAssetData() {
        viewModel.bindAssetViewModelToController = { [weak self] () in
            DispatchQueue.main.async { [self] in
                guard let strongSelf = self else {
                    return
                }
                guard let safeAsset = strongSelf.viewModel.asset else {
                    return
                }
                guard let safeProperties = safeAsset.properties else {
                    return
                }
                guard let safeLabels = safeProperties.labels else {
                    return
                }
                strongSelf.assetLabels.text = safeLabels.map({$0.uppercasingFirst}).joined(separator: " • ")
            }
        }
    }
    
    func bindBookings() {
        viewModel.bindBookingDetailViewModelToController = { [weak self] () in
            DispatchQueue.main.async { [self] in
                guard let strongSelf = self else {
                    return
                }
                guard let startTime = strongSelf.bookingSelected.startTime else {
                    return
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = Constants.DateTimeFormat.optionalDateFormat
                if let date = dateFormatter.date(from: startTime) {
                    strongSelf.calendarDay.scrollTo(hour24: 07.0)
                    strongSelf.calendarDay.move(to: date)
                    strongSelf.calendarDay.reloadData()
                }
            }
        }
    }
    
    func loadBookings() {
        guard let safeStartTime = bookingSelected.startTime else { return }
        let correctStartTime = Helpers.correctDateFormatFromApi(date: safeStartTime)
        let date = Helpers.convertDateStringToDate(dateString: correctStartTime)
        guard let startTime = date.setTime(hour: 0, min: 0, sec: 0), let endTime = date.setTime(hour: 23, min: 59, sec: 59) else { return
        }
        let stringStartTime = Helpers.convertDateToDateString(date: startTime)
        let stringEndTime = Helpers.convertDateToDateString(date: endTime)
        viewModel.callFuncToGetAssetBookings(assetId: bookingSelected.assetId!, startTime: stringStartTime, endTime: stringEndTime)
    }
    
    func displayBookingConfirmation(assetCode: String) {
        DispatchQueue.main.async {
            let slideVC = CancelBookings()
            slideVC.modalPresentationStyle = .custom
            slideVC.transitioningDelegate = self
            slideVC.bookingDetailsDelegate = self
            
            slideVC.setBottomSheetData(workstationCode: assetCode, bookingID: self.bookingSelected.id)
            self.present(slideVC, animated: true, completion: nil)
        }
    }
    
    func showCancelPopUp() {
        guard let safeAssetCode = bookingSelected.assetCode else { return }
        self.displayBookingConfirmation(assetCode: safeAssetCode)
    }
    
    // MARK: - Actions
    @IBAction func cancelBooking(_ sender: Any) {
        showCancelPopUp()
    }
    
    @IBAction func validateBooking(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: Constants.StoryBoard.mainStoryBoardID, bundle: nil)
        if let qrReaderView = storyBoard.instantiateViewController(withIdentifier: Constants.StoryBoard.qrReaderViewControllerID) as? QrReaderViewController {
            qrReaderView.isButtonHidden = false
            self.present(qrReaderView, animated: true, completion: nil)
        }
    }
}

extension BookingDetailsViewController: BookingDetailsControllerDelegate {

    // MARK: - Functions
    func dismissBookingDetailsView() {
        DispatchQueue.main.async {
            if UIDevice.current.userInterfaceIdiom == .pad {
                self.dismiss(animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func showCancelBookingError(title: String, message: String) {
        self.showErrorMessage(title, message)
    }
}

extension BookingDetailsViewController: UIViewControllerTransitioningDelegate {
    
    // MARK: - Functions
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = PresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.modalHeight = CGFloat(ModalType.confirmationBooking.rawValue)
        return presentationController
    }
}

extension BookingDetailsViewController: DayViewDelegate {
    func dayViewDidSelectEventView(_ eventView: EventView) {
        // MARK: - Default Implementation
    }
    
    func dayViewDidLongPressEventView(_ eventView: EventView) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didTapTimelineAt date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didLongPressTimelineAt date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayViewDidBeginDragging(dayView: DayView) {
        // MARK: - Default Implementation
    }
    
    func dayViewDidTransitionCancel(dayView: DayView) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, willMoveTo date: Date) {
        // MARK: - Default Implementation
    }
    
    func dayView(dayView: DayView, didMoveTo date: Date) {
        let currentDate = Helpers.convertDateStringToDate(dateString: self.bookingSelected.startTime!)
        if Calendar.current.component(.day, from: date) != Calendar.current.component(.day, from: currentDate) {
            DispatchQueue.main.async {
                self.calendarDay.move(to: currentDate)
            }
        }
    }
    
    func dayView(dayView: DayView, didUpdate event: EventDescriptor) {
        // MARK: - Default Implementation
    }
}
