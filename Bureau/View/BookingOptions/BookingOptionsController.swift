//
//  BookingParametersController.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 2/3/22.
//

import UIKit

class BookingOptionsController: FiltersPresenterViewController {
    
    let viewModel = BookingOptionsViewModel()
    var startTime = TimeHour()
    var endTime = TimeHour()
    weak var viewControllerDelegate: UpdateContentViewControllerDelegate?
    
    @IBOutlet weak var startHour: UIDatePicker!
    @IBOutlet weak var endHour: UIDatePicker!
    @IBOutlet weak var btnPickDate: UIButton!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet var btnBookFor: UIButton!
    @IBOutlet weak var repeatContainerView: UIView!
    @IBOutlet weak var bookForContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureValues()
        createObservers()
        startHour.becomeFirstResponder()
        btnRepeat.setTitle(viewModel.getTempRecurrencetype().capitalized, for: .normal)
        bookingExtraPermissions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        viewModel.setTempDate()
        viewModel.setRecurrenceWhenDismiss()
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.recurrenceRepeatChanged), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabel), name: Notification.Name(Constants.UserValidation.userSelected), object: nil)
    }
    
    func configureValues() {
        startTime = viewModel.getStartTime()
        endTime = viewModel.getEndTime()
        let date = viewModel.getDate()
        let calendar = Calendar.current
        var components = DateComponents()
        components.hour = startTime.hour
        components.minute = startTime.minute
        startHour.setDate(calendar.date(from: components)!, animated: false)
        components.hour = endTime.hour
        components.minute = endTime.minute
        endHour.setDate(calendar.date(from: components)!, animated: false)
        self.btnPickDate.setTitle(Helpers.getDateLabel(date: date), for: .normal)
        self.updateLabel()
    }
    
    @objc override func executeNewRequest() {
        let tempDate = self.viewModel.getTempDate()
        self.btnPickDate.setTitle(Helpers.getDateLabel(date: tempDate), for: .normal)
        let recurrenceType = viewModel.getTempRecurrencetype()
        self.btnRepeat.setTitle(recurrenceType.capitalized, for: .normal)
    }
    
    @objc func updateLabel() {
        let user = viewModel.getSelectedUser()
        if (user.name == Constants.UserValidation.emptyText) {
            self.btnBookFor.setTitle(user.email, for: .normal)
        } else {
            self.btnBookFor.setTitle(user.name, for: .normal)
        }
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnTapped(_ sender: UIButton) {
        let tempDate = self.viewModel.getTempDate()
        self.viewModel.setStartTimeAndEndTime(startHour: startHour.date, endHour: endHour.date)
        self.viewModel.setDate(date: tempDate)
        self.viewModel.setRecurrenceRule()
        self.dismiss(animated: true) {
            self.viewControllerDelegate?.executeNewRequest()
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.updateLayout), object: nil)
        }
    }
    
    @IBAction func btnPickDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.tempDate)
    }
    
    @IBAction func btnRecurrentOptionsPressed(_ sender: Any) {
        let viewController = RecurrentOptionsController()
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.setNavigationBarHidden(true, animated: true)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func btnBookFor(_ sender: Any) {
        let viewController = FindUserViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.setNavigationBarHidden(true, animated: true)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func btnNowPressed(_ sender: Any) {
        let startDate = NSDate() as Date
        let endDate = Calendar.current.date(byAdding: .hour, value: 1, to: startDate) ?? Date()
        startHour.setDate(startDate, animated: true)
        endHour.setDate(endDate, animated: true)
        self.viewModel.setStartTimeAndEndTime(startHour: startDate, endHour: endDate)
        
        UIView.animate(withDuration: 1.0) {
            self.endHour.subviews.first?.subviews.last?.backgroundColor = UIColor.orange
            self.endHour.subviews.first?.subviews.last?.backgroundColor = UIColor.systemGray5
        }
    }
    
    func bookingExtraPermissions() {
        let recurrencePremission: Bool = UserDefaultsService.shared.getRecurrentBookingsPermission()
        let bookForOthersPermission: Bool = UserDefaultsService.shared.getBookForOthersPermission()
        if(recurrencePremission) {
            repeatContainerView.heightAnchor.constraint(equalToConstant: CGFloat(56)).isActive = true
            repeatContainerView.isHidden = false
        } else {
            repeatContainerView.heightAnchor.constraint(equalToConstant: CGFloat(0)).isActive = true
            repeatContainerView.isHidden = true
        }
        if(bookForOthersPermission) {
            bookForContainerView.isHidden = false
        } else {
            bookForContainerView.isHidden = true
        }
    }
}
