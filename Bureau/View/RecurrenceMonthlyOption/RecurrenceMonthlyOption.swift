//
//  RecurrenceMonthlyOption.swift
//  Bureau
//
//  Created by Juan Pablo Lozada Chambilla on 8/3/22.
//

import UIKit

class RecurrenceMonthlyOption: FiltersPresenterViewController {

    @IBOutlet weak var btnEvery: UIButton!
    @IBOutlet weak var btnMonthDay: UIButton!
    @IBOutlet weak var btnDayPosition: UIButton!
    @IBOutlet weak var btnWeekDay: UIButton!
    @IBOutlet weak var switchOnDay: UISwitch!
    @IBOutlet weak var switchOnThe: UISwitch!
    @IBOutlet weak var containerOnDay: UIView!
    @IBOutlet weak var containerOnThe: UIView!
    @IBOutlet weak var btnStartDate: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    
    let viewModel = RecurrenceMonthlyViewModel()
    let pickerEvery = UIPickerView()
    let pickerMonthDay = UIPickerView()
    var pickerDayPosition = UIPickerView()
    let pickerWeekDay = UIPickerView()
    let everyOptions = Array(1...11).map { String($0) }
    var monthDayOptions = [""]
    let dayPositionOptions = Constants.RecurrentOptions.dayPosition
    let weekDayOptions = Constants.RecurrentOptions.weekDaysName
    let alertVC = UIViewController()
    var everySelected = 0
    var monthDaySelected = 0
    var dayPositionSelected = 0
    var weekDaySelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let currentRecurrence = viewModel.getTempRecurrenceRule()
        if(currentRecurrence.type == Constants.RecurrentOptions.monthly) {
            setInitValues()
        } else {
            executeNewRequest()
            setSwitchEnable(onDay: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.setRecurrenceRules(
            every: everyOptions[everySelected] ,
            onDate: String(monthDaySelected + 1),
            onWeek: String(dayPositionSelected + 1),
            onDays: weekDayOptions.filter {$0 == weekDayOptions[weekDaySelected]},
            onSelected: switchOnDay.isOn
        )
    }
    
    func configurePickerView(picker: UIPickerView, dataSource: [String], itemSelected: Int) {
        alertVC.view.subviews.forEach({ $0.removeFromSuperview() })
        let screenHeight = UIScreen.main.bounds.height / 5
        let screenWidth = UIScreen.main.bounds.width - 20
        alertVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        picker.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        picker.dataSource = self
        picker.delegate = self
        picker.selectRow(itemSelected, inComponent: 0, animated: false)
        alertVC.view?.addSubview(picker)
        picker.centerXAnchor.constraint(equalTo: alertVC.view.centerXAnchor).isActive = true
        picker.centerYAnchor.constraint(equalTo: alertVC.view.centerYAnchor).isActive = true
        let editRadiousAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        editRadiousAlert.setValue(alertVC, forKey: Constants.AlertPicker.contentKey)
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.done, style: .default, handler: {_ in
            let index = picker.selectedRow(inComponent: 0)
            let item = dataSource[picker.selectedRow(inComponent: 0)]
            self.selectedPicker(index: index, item: item, picker: picker)
        }))
        editRadiousAlert.addAction(UIAlertAction(title: Constants.DecisionActionsText.cancel, style: .cancel, handler: nil))
        self.present(editRadiousAlert, animated: true)
    }
    
    func selectedPicker(index: Int, item: String, picker: UIPickerView) {
        switch(picker) {
        case pickerEvery:
            everySelected = index
            btnEvery.setTitle(item, for: .normal)
        case pickerMonthDay:
            monthDaySelected = index
            btnMonthDay.setTitle(item, for: .normal)
        case pickerDayPosition:
            dayPositionSelected = index
            btnDayPosition.setTitle(item, for: .normal)
        case pickerWeekDay:
            weekDaySelected = index
            btnWeekDay.setTitle(item, for: .normal)
        default:
            return
        }
    }
    
    override func executeNewRequest() {
        let tempStartDate = self.viewModel.getTempDate()
        self.btnStartDate.setTitle(Helpers.getSecondDateLabel(date: tempStartDate), for: .normal)
        let recurrenceRule = self.viewModel.getTempRecurrenceRule()
        self.btnEndDate.setTitle(Helpers.getSecondDateLabel(date: recurrenceRule.endDate), for: .normal)
        let dateYear = Calendar.current.component(.year, from: tempStartDate)
        let dateMonth = Calendar.current.component(.month, from: tempStartDate)
        let dateDay = Calendar.current.component(.day, from: tempStartDate)
        monthDayOptions = Helpers.getMonthOrdinalDays(month: dateMonth, year: dateYear)
        btnMonthDay.setTitle(monthDayOptions[dateDay - 1], for: .normal)
        monthDaySelected = dateDay - 1
        let dayInMonth = Helpers.getDayInMonthPosition(day: dateDay, month: dateMonth, year: dateYear)
        btnDayPosition.setTitle(dayPositionOptions[dayInMonth.dayPosition], for: .normal)
        dayPositionSelected = dayInMonth.dayPosition
        btnWeekDay.setTitle(dayInMonth.dayName, for: .normal)
        weekDaySelected = weekDayOptions.indices.filter { weekDayOptions[$0] == dayInMonth.dayName }.first ?? 0
    }
    
    func setInitValues() {
        let currentRecurrence = viewModel.getTempRecurrenceRule()
        let tempStartDate = self.viewModel.getTempDate()
        self.btnStartDate.setTitle(Helpers.getSecondDateLabel(date: tempStartDate), for: .normal)
        self.btnEndDate.setTitle(Helpers.getSecondDateLabel(date: currentRecurrence.endDate), for: .normal)
        let everyItem = Int(currentRecurrence.every ?? "1") ?? 1
        selectedPicker(index: everyItem - 1, item: String(everyItem), picker: pickerEvery)
        let dateMonth = Calendar.current.component(.month, from: tempStartDate)
        let dateYear = Calendar.current.component(.year, from: tempStartDate)
        monthDayOptions = Helpers.getMonthOrdinalDays(month: dateMonth, year: dateYear)
        let monthDayItem = Int(currentRecurrence.onDate ?? "1") ?? 1
        selectedPicker(index: monthDayItem - 1, item: monthDayOptions[monthDayItem - 1], picker: pickerMonthDay)
        let dayPositionItem = Int(currentRecurrence.onWeek ?? "1") ?? 1
        selectedPicker(index: dayPositionItem - 1, item: dayPositionOptions[dayPositionItem - 1], picker: pickerDayPosition)
        let weekDayItem = weekDayOptions.firstIndex(of: currentRecurrence.onDays?.first ?? "1") ?? 1
        selectedPicker(index: weekDayItem, item: weekDayOptions[weekDayItem], picker: pickerWeekDay)
        if(currentRecurrence.onDate == nil) {
            setSwitchEnable(onDay: false)
        } else {
            setSwitchEnable(onDay: true)
        }
    }
    
    func setSwitchEnable(onDay: Bool) {
        if(onDay) {
            switchOnDay.isOn = true
            switchOnThe.isOn = false
            containerOnThe.alpha = 0.4
            containerOnThe.isUserInteractionEnabled = false
            containerOnDay.alpha = 1
            containerOnDay.isUserInteractionEnabled = true
        } else {
            switchOnThe.isOn = true
            switchOnDay.isOn = false
            containerOnDay.alpha = 0.4
            containerOnDay.isUserInteractionEnabled = false
            containerOnThe.alpha = 1
            containerOnThe.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func btnEveryPressed(_ sender: Any) {
        configurePickerView(picker: pickerEvery, dataSource: everyOptions, itemSelected: everySelected)
    }
    
    @IBAction func btnMonthDayPressed(_ sender: Any) {
        configurePickerView(picker: pickerMonthDay, dataSource: monthDayOptions, itemSelected: monthDaySelected)
    }
    
    @IBAction func btnDayPositionPressed(_ sender: Any) {
        configurePickerView(picker: pickerDayPosition, dataSource: dayPositionOptions, itemSelected: dayPositionSelected)
    }
    
    @IBAction func btnWeekDayPressed(_ sender: Any) {
        configurePickerView(picker: pickerWeekDay, dataSource: weekDayOptions, itemSelected: weekDaySelected)
    }
    
    @IBAction func switchOnDayPressed(_ sender: UISwitch) {
        if(sender.isOn) {
            setSwitchEnable(onDay: true)
        } else {
            setSwitchEnable(onDay: false)
        }
    }
    
    @IBAction func switchOnThePressed(_ sender: UISwitch) {
        if(sender.isOn) {
            setSwitchEnable(onDay: false)
        } else {
            setSwitchEnable(onDay: true)
        }
    }
    
    @IBAction func btnStartDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.tempDate)
    }
    
    @IBAction func btnEndDatePressed(_ sender: Any) {
        self.presentDateCalendarPicker(pickerSetType: Constants.DatePickerSetType.endDate)
    }
}

extension RecurrenceMonthlyOption: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(pickerView) {
        case pickerEvery:
            return everyOptions.count
        case pickerMonthDay:
            return monthDayOptions.count
        case pickerDayPosition:
            return dayPositionOptions.count
        case pickerWeekDay:
            return weekDayOptions.count
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(pickerView) {
        case pickerEvery:
            return everyOptions[row]
        case pickerMonthDay:
            return monthDayOptions[row]
        case pickerDayPosition:
            return dayPositionOptions[row]
        case pickerWeekDay:
            return weekDayOptions[row]
        default:
            return ""
        }
    }
}
