//
//  AreaLayoutViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import UIKit
import PullUpController
import TTGSnackbar

class AreaLayoutViewController: FiltersPresenterViewController {

    // MARK: - Variables
    var viewModel = AreaLayoutViewModel()
    var imageScrollView: LayoutScrollView!
    var cardsCollectionView: UICollectionView!
    var currentFocusCard: Int = 0
    var lastDisplayedCard: Int = 0
    var selectedIndex: Int = -1
    let workstationDetailsVC = WorkstationDetailsViewController()

    // MARK: - Constants

    // MARK: - Outlets
    @IBOutlet var scrollContainerView: UIView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var mainMenu: MainMenu!
    @IBOutlet var coverView: UIView!
    @IBOutlet weak var emptyLayoutMessage: UILabel!
    
    // MARK: - Outlets Tablet
    @IBOutlet weak var areaTitleLabel: UILabel!
    @IBOutlet weak var expandMapButton: UIButton!
    
    // MARK: - Constructors
    override func viewDidLoad() {
        super.viewDidLoad()
        workstationDetailsVC.areaLayoutDelegate = self
        SplitViewController.shared?.areaLayoutDelegate = self
        createObservers()
        if UIDevice.current.userInterfaceIdiom != .pad {
            mainMenu.viewControllerDelegate = self
        } else {
            self.navigationController?.navigationBar.isHidden = true
            self.areaTitleLabel.text = viewModel.getArea()?.name ?? Constants.Layout.noAreaSelected
            self.expandMapButton.setTitle("", for: .normal)
            self.expandMapButton.addTarget(self, action: #selector(onExpandMapPressed), for: .touchUpInside)
        }
        self.title = viewModel.getArea()?.name ?? Constants.Layout.noAreaSelected
        configureView(reloadMarkers: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    // MARK: - Functions

    func noConnectedToNetwork() {
        let alert = UIAlertController(title: Constants.NoConnectedToNetwork.title, message: Constants.NoConnectedToNetwork.message , preferredStyle: .alert)
        let accept = UIAlertAction(title: Constants.DecisionActionsText.accept, style: .default, handler: { _ in
            self.configureView(reloadMarkers: false)
        })
        alert.addAction(accept)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }

    func createObservers() {
        if UIDevice.current.userInterfaceIdiom != .pad {
            NotificationCenter.default.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.updateLayout), object: nil)
        } else {
            NotificationCenter.default.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(executeNewRequest), name: Notification.Name(Constants.NotificationNameKeys.updateButtonFiltersLabel), object: nil)
        }
    }

    func setupImageScrollView() {
        imageScrollView = LayoutScrollView(frame: scrollContainerView.bounds)
        if UIDevice.current.userInterfaceIdiom != .pad {
            imageScrollView.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
        } else {
            imageScrollView.backgroundColor = .systemBackground
        }
        imageScrollView.workstationDelegate = self
        scrollContainerView.addSubview(imageScrollView)
        setupImageScrollViewConstraints()
    }

    func setupImageScrollViewConstraints() {
        imageScrollView.translatesAutoresizingMaskIntoConstraints = false
        imageScrollView.topAnchor.constraint(equalTo: scrollContainerView.topAnchor).isActive = true
        imageScrollView.bottomAnchor.constraint(equalTo: scrollContainerView.bottomAnchor).isActive = true
        imageScrollView.trailingAnchor.constraint(equalTo: scrollContainerView.trailingAnchor).isActive = true
        imageScrollView.leadingAnchor.constraint(equalTo: scrollContainerView.leadingAnchor).isActive = true
    }

    func configureView(reloadMarkers: Bool) {
        if(!reloadMarkers) {
            self.setupImageScrollView()
        }
        if Reachability.isConnectedToNetwork() {
            activity.isHidden = false
            activity.startAnimating()
            bindLayout(reloadMarkers: reloadMarkers)
            bindError()
            if self.title != Constants.Layout.noAreaSelected {
                viewModel.callFuncToGetAllWorkstationForArea()
            }
        } else {
            self.noConnectedToNetwork()
        }
    }

    func bindLayout(reloadMarkers: Bool) {
        viewModel.bindLayoutViewModelToController = { [weak self] (dataLayout) in
            DispatchQueue.main.async {
                self?.showUnavailableAssetsMessages(dataLayout: dataLayout)
                self?.activity.stopAnimating()
                self?.activity.isHidden = true
                if(reloadMarkers) {
                    self?.imageScrollView.imageZoomView.subviews.forEach { $0.removeFromSuperview() }
                    self?.imageScrollView.markers = dataLayout.markers ?? []
                    self?.imageScrollView.setMarkers(reloadMarkers: true)
                } else {
                    guard let safeImagePath = dataLayout.background?.url, !safeImagePath.isEmpty else {
                        self?.emptyLayoutMessage.isHidden = false
                        return
                    }
                    self?.emptyLayoutMessage.isHidden = true
                    self?.imageScrollView.set(imagePath: safeImagePath, markers: dataLayout.markers ?? [])
                }
            }
        }
    }
    
    // Show Error
    func bindError() {
        viewModel.bindErrorViewModelToController = { [weak self] (errorTitle, errorMessage) in
            DispatchQueue.main.async {
                self?.showErrorMessage(errorTitle, errorMessage)
            }
        }
    }
    
    @objc override func executeNewRequest() {
        configureView(reloadMarkers: true)
    }
    
    @objc func onExpandMapPressed() {
        if SplitViewController.shared?.displayMode == .secondaryOnly {
            SplitViewController.shared?.show(.supplementary)
            self.expandMapButton.setImage(Constants.Images.expandIcon, for: .normal)
        } else {
            SplitViewController.shared?.hide(.supplementary)
            self.expandMapButton.setImage(Constants.Images.shrinkIcon, for: .normal)
        }
    }

}

// MARK: - Extensions
extension AreaLayoutViewController: WorkstationViewControllerDelegate {

    // MARK: - Functions WorkstationViewControllerDelegate
    func goToWorkstationDetails(workstationID: String) {
        let index = getMarkerIndex(workstationID: workstationID)
        if index == -1 {
            self.showError(error: .internalError)
            return
        }
        if selectedIndex != index {
            let workstation = self.viewModel.dataWorkstation[index]
            workstationDetailsVC.viewModel.selectedAsset = workstation
            Helpers.setCrashlyticsKey(Constants.Crashlytics.keyAssetId, as: workstation.id)
            removePullUpController(workstationDetailsVC, animated: false) { _ in
                self.updateCoverViewBackground(alpha: Constants.WorkstationDetailsBottomSheet.initialAlpha)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    self.present(self.workstationDetailsVC, animated: true, completion: nil)
                } else {
                    self.addPullUpController(self.workstationDetailsVC,
                                             initialStickyPointOffset: Constants.WorkstationDetailsBottomSheet.initHeight,
                                             animated: true)
                }
                self.selectedIndex = index
            }
        } else {
            removePullUpController(workstationDetailsVC, animated: true)
            self.updateCoverViewBackground(alpha: 0.0)
            selectedIndex = -1
        }
    }

    func getMarkerIndex(workstationID: String) -> Int {
        if viewModel.dataWorkstation.isEmpty {
            return -1
        }
        for index in 0...(viewModel.dataWorkstation.count - 1) where workstationID == viewModel.dataWorkstation[index].id {
            return index
        }
        return -1
    }
    
    func showError(error: CustomError) {
        let displayError = Helpers.getErrorMessages(error: error)
        self.showErrorMessage(displayError.title, displayError.message)
    }

}

extension AreaLayoutViewController: AreaLayoutViewControllerDelegate {
    
    // MARK: - Functions
    func updateCoverViewBackground(alpha: CGFloat) {
        DispatchQueue.main.async {
            self.coverView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: alpha)
        }
    }
    
    func updateLayout(onlyMarker update: Bool) {
        if (update) {
            self.imageScrollView.updateMarkers(index: selectedIndex)
            selectedIndex = -1
        } else {
            self.title = viewModel.getArea()?.name ?? Constants.Layout.noAreaSelected
            self.areaTitleLabel?.text = viewModel.getArea()?.name ?? Constants.Layout.noAreaSelected
            self.configureView(reloadMarkers: false)
        }
    }
    
    func showUnavailableAssetsMessages(dataLayout: Area) {
        var snackbar: TTGSnackbar?
        if(self.viewModel.getEnabledFiltersNumber() > 0 && dataLayout.markers?.filter { $0.isAvailable }.count ?? 0 == 0) {
            snackbar = TTGSnackbar(message: Constants.UnavailableAssets.mismatchedFilters, duration: .middle)
        } else if(dataLayout.markers?.filter { $0.isAvailable }.count ?? 0 == 0) {
            snackbar = TTGSnackbar(message: Constants.UnavailableAssets.emptyMap, duration: .middle)
        }
        snackbar?.contentInset = UIEdgeInsets.init(top: 8, left: 12, bottom: 8, right: 12)
        snackbar?.show()
    }
}
