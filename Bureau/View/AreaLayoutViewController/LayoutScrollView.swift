//
//  LayoutScrollView.swift
//  Bureau
//
//  Created by Victor Arana on 6/15/21.
//

import UIKit
import Kingfisher

class LayoutScrollView: UIScrollView {

    // MARK: - Variables
    var imageZoomView: UIImageView!
    lazy var zoomingTap: UITapGestureRecognizer = {
        let zoomingTap = UITapGestureRecognizer(target: self, action: #selector(handleZoomingTap))
        zoomingTap.numberOfTapsRequired = 2
        return zoomingTap
    }()
    let iconWidth = 48.0
    let iconHeight = 55.0
    var markers: [Marker] = []
    var currentMarker = -1
    var minXpos = 0.0
    var maxXpos = 100.0
    var minYpos = 0.0
    var maxYpos = 100.0
    weak var workstationDelegate: WorkstationViewControllerDelegate?
    
    let viewModel = LayoutScrollViewModel()

    // MARK: - Outlets

    // MARK: - Constructors
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .gray
        self.delegate = self
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.decelerationRate = UIScrollView.DecelerationRate.fast
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.centerImage()
    }

    // MARK: - Functions

    func set(imagePath: String, markers: [Marker]) {
        self.markers = markers
        imageZoomView?.removeFromSuperview()
        imageZoomView = nil
        imageZoomView = UIImageView(image: UIImage(named: Constants.Layout.imageName))
        self.imageZoomView.kf.setImage(with: URL(string: imagePath)!,
                                       options: [],
                                       completionHandler: { [ weak self] image, _, _, _ in
                                        self?.configureView(image: image)
                                       }
        )
        self.addSubview(imageZoomView)
    }

    func configureView(image: UIImage?) {
        guard let safeImage = image else {
            workstationDelegate?.showError(error: CustomError.connectionError)
            return
        }
        self.getMinAndMaxXYValues()
        self.imageZoomView.frame.size = CGSize(width: safeImage.size.width, height: safeImage.size.height)
        self.configurateFor(imageSize: image!.size)
        self.setMarkers(reloadMarkers: false)
    }

    func configurateFor(imageSize: CGSize) {
        self.contentSize = imageSize
        setCurrentMaxAndMinZoomScale()
        self.zoomScale = self.minimumZoomScale
        self.imageZoomView.addGestureRecognizer(self.zoomingTap)
        self.imageZoomView.isUserInteractionEnabled = true
    }

    func setCurrentMaxAndMinZoomScale() {
        let boundSize = self.bounds.size
        let imageSize = self.imageZoomView.bounds.size
        let xScale = boundSize.width / imageSize.width
        let yScale = boundSize.height / imageSize.height
        let minScale = min(xScale, yScale)
        var maxScale: CGFloat = 1.0
        if minScale < 0.1 {
            maxScale = 0.3
        }
        if minScale >= 0.1  && minScale <= 0.5 {
            maxScale = 0.7
        }
        if minScale >= 0.5 {
            maxScale = max(1.0, minScale)
        }
        self.minimumZoomScale = minScale
        self.maximumZoomScale = maxScale
    }

    func centerImage() {
        if self.imageZoomView == nil {
            return
        }
        let boundSize = self.bounds.size
        var frameToCenter = self.imageZoomView.frame
        if frameToCenter.size.width < boundSize.width {
            frameToCenter.origin.x = (boundSize.width - frameToCenter.size.width) / 2
        } else {
            frameToCenter.origin.x = 0.0
        }
        frameToCenter.origin.y = 0.0
        imageZoomView.frame = frameToCenter
    }

    func getMinAndMaxXYValues() {
        minXpos = Double(self.bounds.width) / 2
        maxXpos = Double(imageZoomView.bounds.width) - Double(self.bounds.width)
        minYpos = Double(self.bounds.height) / 2
        maxYpos = Double(imageZoomView.bounds.height) - Double(self.bounds.height)
    }

    func setMarkers(reloadMarkers: Bool) {
        for (index, marker) in markers.enumerated() {
            if let safeXpos = marker.xPos, let safeYpos = marker.yPos, marker.isPublic ?? false {
                createMarker(xPos: safeXpos, yPos: safeYpos, isAvailable: marker.isAvailable, index: index)
            }
        }
        if (!reloadMarkers) {
            focusFirstCard()
        } else if (currentMarker >= 0) {
            changeMarkerSize(index: currentMarker, iconSize: .big)
        }
    }

    func createMarker(xPos: Double, yPos: Double, isAvailable: Bool, index: Int) {
        let safeXpos = xPos * Double(imageZoomView.bounds.width)
        let safeYpos = yPos * Double(imageZoomView.bounds.height)
        var imageName = Constants.Images.pointerGreen
        if !isAvailable {
            imageName = Constants.Images.pointerGray
        }
        guard let safeImage = UIImage(named: imageName) else {
            return
        }
        let imageView = UIImageView(image: safeImage)
        imageView.frame = createIconFrame(xPos: safeXpos, yPos: safeYpos, iconSize: IconSize.normal)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapImageView(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        if !isAvailable && viewModel.getShowOnlyAvailableSpaces() {
            imageView.isHidden = true
        }
        imageView.tag = index
        self.imageZoomView.addSubview(imageView)
    }

    func focusFirstCard() {
        if markers.isEmpty {
            return
        }
        guard let firstMarkerView = imageZoomView.subviews[0] as? UIImageView else {
            return
        }
        let firstMaker = markers[0]
        if var safeXpos = firstMaker.xPos, var safeYpos = firstMaker.yPos {
            safeXpos *= Double(imageZoomView.bounds.width)
            safeYpos *= Double(imageZoomView.bounds.height)
            if zoomScale == minimumZoomScale {
                zoom(point: CGPoint(x: safeXpos, y: safeYpos), animated: true)
            } else {
                let offset = calculateContentOffset(point: firstMarkerView.center)
                setContentOffset(CGPoint(x: offset.0 * Double(zoomScale), y: offset.1 * Double(zoomScale)), animated: true)
            }
        }
    }

    func getMarkerIndex(selectedId: String) -> Int {
        if markers.isEmpty {
            return -1
        }
        for index in 0..<markers.count where selectedId == markers[index].workstationId {
            return index
        }
        return -1
    }
    
    func getSubView(index: Int) -> UIImageView? {
        let subView = imageZoomView.subviews[index]
        guard let safeSubView = subView as? UIImageView else {
            return nil
        }
        return safeSubView
    }
    
    func focusMarker(index: Int) {
        guard let subView = getSubView(index: index) else {
            return
        }
        let coordinates = getCoordinates(index: index)
        if coordinates.xPos < 0 || coordinates.yPos < 0 {
            return
        }
        if zoomScale == minimumZoomScale {
            zoom(point: CGPoint(x: coordinates.xPos, y: coordinates.yPos), animated: true)
        } else {
            let offset = calculateContentOffset(point: subView.center)
            setContentOffset(CGPoint(x: offset.0 * Double(zoomScale), y: offset.1 * Double(zoomScale)), animated: true)
        }
    }
    
    func getCoordinates(index: Int) -> (xPos: Double, yPos: Double) {
        let marker = markers[index]
        if var safeXpos = marker.xPos, var safeYpos = marker.yPos {
            safeXpos *= Double(imageZoomView.bounds.width)
            safeYpos *= Double(imageZoomView.bounds.height)
            return (safeXpos, safeYpos)
        }
        return (-1, -1)
    }
    
    func changeMarkerSize(index: Int, iconSize: IconSize) {
        guard let subView = getSubView(index: index) else {
            return
        }
        let coordinates = getCoordinates(index: index)
        if coordinates.xPos < 0 || coordinates.yPos < 0 {
            return
        }
        if iconSize == .normal {
            subView.frame = createIconFrame(xPos: coordinates.xPos, yPos: coordinates.yPos, iconSize: IconSize.normal)
            var imageName = Constants.Images.pointerGreen
            if !markers[index].isAvailable {
                imageName = Constants.Images.pointerGray
            }
            subView.image = UIImage(named: imageName)
        } else {
            subView.frame = createIconFrame(xPos: coordinates.xPos, yPos: coordinates.yPos, iconSize: IconSize.big)
            subView.image = UIImage(named: Constants.Images.pointerBlue)
        }
    }

    func updateMarkers(index: Int) {
        if index < 0 {
            return
        }
        if currentMarker < 0 {
            changeMarkerSize(index: index, iconSize: .big)
            focusMarker(index: index)
            currentMarker = index
            return
        }
        if currentMarker == index {
            changeMarkerSize(index: index, iconSize: .normal)
            focusMarker(index: index)
            currentMarker = -1
            return
        }
        changeMarkerSize(index: currentMarker, iconSize: .normal)
        changeMarkerSize(index: index, iconSize: .big)
        focusMarker(index: index)
        currentMarker = index
    }

    func calculateContentOffset(point: CGPoint) -> (Double, Double) {
        var offsetXpos = Double(point.x)
        var offsetYpos  = Double(point.y)
        offsetXpos -= minXpos
        if offsetXpos < 0.0 {
            offsetXpos = 0.0
        } else if offsetXpos > maxXpos {
            offsetXpos = maxXpos
        }
        offsetYpos -= minYpos
        if offsetYpos < 0.0 {
            offsetYpos = 0.0
        } else if offsetYpos > maxYpos {
            offsetYpos = maxYpos
        }
        return (offsetXpos, offsetYpos)
    }

    // MARK: - Objc Functions
    @objc func didTapImageView(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let tappedImage = tapGestureRecognizer.view as? UIImageView else {
            return
        }
        self.updateMarkers(index: tappedImage.tag)
        workstationDelegate?.goToWorkstationDetails(workstationID: markers[tappedImage.tag].workstationId)
    }

    @objc func handleZoomingTap(sender: UITapGestureRecognizer) {
        let location = sender.location(in: sender.view)
        self.zoom(point: location, animated: true)
    }

    func createIconFrame(xPos: Double, yPos: Double, iconSize: IconSize) -> CGRect {
        switch iconSize {
        case .normal:
            return CGRect(x: xPos - (iconWidth / 2), y: yPos - iconHeight, width: iconWidth, height: iconHeight)
        case .big:
            return CGRect(x: xPos - iconWidth, y: yPos - (iconHeight * 2), width: iconWidth * 2, height: iconHeight * 2)
        }
    }
}

// MARK: - Extensions
extension LayoutScrollView: UIScrollViewDelegate {

    // MARK: - Functions UIScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageZoomView
    }

    func zoom(point: CGPoint, animated: Bool) {
        let currentScale = self.zoomScale
        let minScale = self.minimumZoomScale
        let maxScale = self.maximumZoomScale
        if (minScale == maxScale && minScale > 1) {
            return
        }
        let toScale = maxScale
        let finalScale = (currentScale == minScale) ? toScale : minScale
        let zoomRect = self.zoomRect(scale: finalScale, center: point)
        self.zoom(to: zoomRect, animated: animated)
    }

    func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        let bounds = self.bounds
        zoomRect.size.width = bounds.size.width / scale
        zoomRect.size.height = bounds.size.height / scale
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2)
        return zoomRect
    }
}
