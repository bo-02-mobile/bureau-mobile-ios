//
//  ModalPresentationViewController.swift
//  Bureau
//
//  Created by Victor Arana on 7/21/21.
//

import UIKit
import PullUpController

class ModalPresenterViewController: PullUpController, UIViewControllerTransitioningDelegate {
    
    var modalHeight: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Functions
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController =  PresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.modalHeight = self.modalHeight
        return presentationController
    }

}
