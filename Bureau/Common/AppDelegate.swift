//
//  AppDelegate.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 1/6/21.
//

import UIKit
import AppAuth
import Firebase
import FirebaseMessaging
import FirebaseCore

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Variables
    var currentAuthorizationFlow: OIDExternalUserAgentSession?
    
    // MARK: - Functions
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        center.requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
        )
        
        center.getPendingNotificationRequests(completionHandler: { requests in
            for request in requests {
                print(request)
            }
        })
        
        application.registerForRemoteNotifications()
        
        self.registerCustomActions()
        
        // Navbar and tabbar config for iOS 15
        if #available(iOS 15, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithDefaultBackground()
            navigationBarAppearance.backgroundColor = UIColor(named: Constants.Colors.primaryMain)
            navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            if UIDevice.current.userInterfaceIdiom == .pad {
                navigationBarAppearance.backgroundColor = UIColor(named: Constants.Colors.surfaceLow)
                navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor(named: Constants.Colors.textLow) as Any]
            }
            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
            
            let tabAppearance = UITabBarAppearance()
            tabAppearance.configureWithDefaultBackground()
            tabAppearance.backgroundColor = .systemBackground
            UITabBar.appearance().standardAppearance = tabAppearance
            UITabBar.appearance().scrollEdgeAppearance = tabAppearance
        }
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Sends the URL to the current authorization flow (if any) which will
        // process it if it relates to an authorization response.
        if let authorizationFlow = self.currentAuthorizationFlow,
           authorizationFlow.resumeExternalUserAgentFlow(with: url) {
            self.currentAuthorizationFlow = nil
            return true
        }
        // Your additional URL handling (if any)
        return false
    }
}

extension AppDelegate:  UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .badge, .sound])
    }
        
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let bookingData = userInfo[Constants.PushNotification.userInfo] as? Data {
            do {
                let decoder = JSONDecoder()
                _ = try decoder.decode(Booking.self, from: bookingData)
                
                if let actionTypeValue = ActionType(rawValue: response.actionIdentifier) {
                    BottomTabBar.currentInstance?.selectedIndex = actionTypeValue.id
                } else {
                    BottomTabBar.currentInstance?.selectedIndex = ActionType.newBookingAction.id
                }
            } catch {
                print(error)
            }
        }
    }
    
    private func registerCustomActions() {
        let validateQR = UNNotificationAction(
            identifier: ActionType.scanQrAction.rawValue,
            title: String(describing: ActionType.scanQrAction),
            options: .foreground)
        let myBookings  = UNNotificationAction(
            identifier: ActionType.myBookingsAction.rawValue,
            title: String(describing: ActionType.myBookingsAction),
            options: .foreground)
        let bookNow  = UNNotificationAction(
            identifier: ActionType.newBookingAction.rawValue,
            title: String(describing: ActionType.newBookingAction),
            options: .foreground)
        
        let category = UNNotificationCategory.init(identifier: Constants.PushNotification.categoryIdentifier,
                                                   actions: [validateQR, myBookings, bookNow],
                                                   intentIdentifiers: [], options: [])

        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
}
