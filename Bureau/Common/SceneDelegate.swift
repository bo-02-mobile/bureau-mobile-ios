//
//  SceneDelegate.swift
//  Bureau
//
//  Created by Cristian Misael Almendro Lazarte on 1/6/21.
//

import UIKit
import AppAuth
import FirebaseCrashlytics
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: - Variables
    var window: UIWindow?
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: Constants.StoryBoard.mainStoryBoardID, bundle: Bundle.main)
    }
    var shortcutItemToProcess: UIApplicationShortcutItem?

    // MARK: - Functions
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        var viewControllerID = Constants.StoryBoard.loginViewControllerID
        
        AuthService.shared.loadState { (result) in
            switch (result) {
            case .success(_):
                if UIDevice.current.userInterfaceIdiom == .pad {
                    viewControllerID = Constants.StoryBoard.splitViewControllerID
                } else {
                    viewControllerID = Constants.StoryBoard.mainViewControllerID
                }
                self.getUserInfo()
            case .failure(_):
                viewControllerID = Constants.StoryBoard.loginViewControllerID
            }
            let viewController = SceneDelegate.mainStoryboard.instantiateViewController(withIdentifier: viewControllerID)
            self.window = UIWindow(windowScene: windowScene)
            self.window?.rootViewController = viewController
            
            // If open with a shortcut save it for later
            if let shortcutItem = connectionOptions.shortcutItem {
                self.shortcutItemToProcess = shortcutItem
            }
            
            // Appearance Mode saved
            self.checkAppereanceMode()
            
            self.window?.makeKeyAndVisible()
        }
    }
    
    func getUserInfo() {
        UserDefaultsService.shared.getUserInfo { (result) in
            switch (result) {
            case .success(let user):
                if let email = user.email {
                    Crashlytics.crashlytics().setUserID(email)
                } else {
                    Helpers.setCrashlyticsKey(Constants.Crashlytics.keyNoEmail, as: Constants.Crashlytics.prefsError)
                }
            case .failure(_):
                Helpers.setCrashlyticsKey(Constants.Crashlytics.keyNoEmail, as: Constants.Crashlytics.prefsError)
            }
        }
    }
    
    func checkAppereanceMode() {
        if #available(iOS 13.0, *) {
            if let mode = UserDefaults.standard.string(forKey: Constants.Appearance.mode) {
                switch mode {
                case Constants.Appearance.unspecifiedMode  :
                    self.window?.overrideUserInterfaceStyle = Constants.Appearance.unspecified
                case Constants.Appearance.lightMode:
                    self.window?.overrideUserInterfaceStyle = Constants.Appearance.light
                case Constants.Appearance.darkMode:
                    self.window?.overrideUserInterfaceStyle = Constants.Appearance.dark
                default:
                    self.window?.overrideUserInterfaceStyle = Constants.Appearance.unspecified
                }
            } else {
                self.window?.overrideUserInterfaceStyle = Constants.Appearance.unspecified
            }
        } else {
            self.window?.overrideUserInterfaceStyle = Constants.Appearance.unspecified
        }
    }
    
    func scene(_ scene: UIScene, openURLContexts urlContexts: Set<UIOpenURLContext>) {
        if let item = urlContexts.first {
            let url = item.url
            if "\(url)" == Constants.Schemes.schemeBooking {
                BottomTabBar.currentInstance?.selectedIndex = 0
            }
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func windowScene(_ windowScene: UIWindowScene, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        shortcutItemToProcess = shortcutItem
        completionHandler(true)
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        Constants.PushNotification.setBadgeToZero()
        if let shortcutItem = shortcutItemToProcess {
            handleShortcutItem(shortcutItem: shortcutItem)
        }
        // Reset to today date
        ApiService.shared.currentDate = Date()
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.dateChanged), object: nil)
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func handleShortcutItem(shortcutItem: UIApplicationShortcutItem) {
        if let actionTypeValue = ActionType(rawValue: shortcutItem.type) {
            BottomTabBar.currentInstance?.selectedIndex = actionTypeValue.id
        }
    }
}
