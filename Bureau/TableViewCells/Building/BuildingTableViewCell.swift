//
//  BuildingTableViewCell.swift
//  Bureau
//
//  Created by user on 7/18/21.
//

import UIKit

class BuildingTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContainer: UIView!
    @IBOutlet var buildingName: UILabel!
    @IBOutlet var buildingAddress: UILabel!
    @IBOutlet var space: UILabel!
    @IBOutlet var chevron: UIImageView!
    @IBOutlet weak var separatorLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chevron.tintColor = UIColor(named: Constants.Colors.textHigh)?.withAlphaComponent(0.3)
        if UIDevice.current.userInterfaceIdiom == .pad {
            chevron.widthAnchor.constraint(equalToConstant: CGFloat(0)).isActive = true
            chevron.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if UIDevice.current.userInterfaceIdiom != .pad {
            return
        }
        if selected {
            buildingName.textColor = .white
            buildingAddress.textColor = UIColor(named: Constants.Colors.textAreaSelected)
            space.textColor = .white
            cellContainer.backgroundColor =  UIColor(named: Constants.Colors.primaryMain)
            cellContainer.layer.cornerRadius = 10
            separatorLine.isHidden = true
        } else {
            buildingName.textColor = UIColor(named: Constants.Colors.textHigh)
            buildingAddress.textColor = UIColor(named: Constants.Colors.textLow)
            cellContainer.backgroundColor =  UIColor(named: Constants.Colors.surfaceHigh)
            cellContainer.layer.cornerRadius = 0
            separatorLine.isHidden = false
        }
    }
    
    override var canBecomeFocused: Bool {
        return false
    }
    
    func updateContent(buildingName: String, buildingAddress: String) {
        self.buildingName.text = buildingName
        self.buildingAddress.text = buildingAddress
    }
    
    func updateContentAsArea(areaName: String, buildingName: String, space: Int = 0) {
        self.buildingName.text = areaName
        self.buildingAddress.text = buildingName
        self.space.text = Constants.SpaceAvailability.noSpaces
        if space > 0 {
            if space == 1 {
                self.space.text = "\(space) \(Constants.SpaceAvailability.space)"
            } else {
                self.space.text = "\(space) \(Constants.SpaceAvailability.spaces)"
            }
            self.space.textColor = UIColor(named: Constants.Colors.textGreen)
        } else {
            self.space.textColor = UIColor(named: Constants.Colors.textHigh)?.withAlphaComponent(0.3)
        }
    }
    
}
