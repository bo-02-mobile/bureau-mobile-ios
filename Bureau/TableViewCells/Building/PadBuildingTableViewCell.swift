//
//  PadBuildingViewCell.swift
//  Bureau
//
//  Created by admin on 3/30/22.
//

import UIKit

class PadBuildingTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var buildingLabel: UILabel!
    @IBOutlet weak var areasNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let bgView = UIView()
        bgView.backgroundColor = UIColor(named: Constants.Colors.surfaceSelected)
        bgView.layer.cornerRadius = 10
        self.selectedBackgroundView = bgView
        if selected {
            self.buildingLabel.textColor = UIColor(named: Constants.Colors.textBuildingSelected)
        } else {
            self.buildingLabel.textColor = .label
        }
    }
    
    func updateContent(buildingName: String, areasNumber: Int) {
        self.buildingLabel.text = buildingName
        self.areasNumberLabel.text = "\(areasNumber < 0 ? 0 : areasNumber)"
    }
}
