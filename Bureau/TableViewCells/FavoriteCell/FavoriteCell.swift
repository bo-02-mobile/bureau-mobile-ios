//
//  FavoriteCell.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 23/3/22.
//

import UIKit

class FavoriteCell: UITableViewCell {

    @IBOutlet weak var workstationNameLabel: UILabel!
    @IBOutlet weak var workstationFeaturesLabel: UILabel!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet weak var worstationAvailabilityLabel: UILabel!
    var viewModel = FavoriteViewModel()
    var assetId: String = ""
        
    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateContent(asset: Asset) {
        self.worstationAvailabilityLabel.text = ""
        self.assetId = asset.id
        self.workstationNameLabel.text = asset.name
        self.workstationFeaturesLabel.text = "\(asset.properties?.type ?? Constants.EmptyData.noType) | \(asset.properties?.labels?.joined(separator: " • ") ?? "")"
        if(asset.properties?.owner != nil) {
            self.worstationAvailabilityLabel.text = Constants.Favorites.permanentAsset
            self.worstationAvailabilityLabel.textColor = UIColor(named: Constants.Colors.textGray)
            return
        }
        var available = viewModel.isAvailable(workstation: asset)
        viewModel.getOverlappedAssetsByDate(assetId: assetId) { bookings in
            available = available && bookings.isEmpty
            DispatchQueue.main.async {
                self.worstationAvailabilityLabel.text = available ? Constants.Favorites.available : Constants.Favorites.unavailable
                self.worstationAvailabilityLabel.textColor = available ? UIColor(named: Constants.Colors.textGreen) : UIColor(named: Constants.Colors.textGray)
            }
        }
    }
    
    func configureViews() {
        // add gesture recognizer for heart imageview
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTappedHeartImageView))
        self.heartImageView.isUserInteractionEnabled = true
        self.heartImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func didTappedHeartImageView() {
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.deleteFavoriteAsset), object: nil, userInfo: [Constants.NotificationNameKeys.deleteFavoriteAsset : assetId])
    }
    
}
