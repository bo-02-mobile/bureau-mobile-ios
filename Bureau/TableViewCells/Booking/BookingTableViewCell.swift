//
//  BookingTableViewCell.swift
//  Bureau
//
//  Created by Victor Arana on 7/1/21.
//

import UIKit

class BookingTableViewCell: UITableViewCell {
    
    @IBOutlet var cardView: UIView!
    @IBOutlet var bookingLocation: UILabel!
    @IBOutlet var bookingDate: UILabel!
    @IBOutlet var validationLabel: UILabel!
    @IBOutlet var qrValidation: UIView!
    @IBOutlet var notValidableView: UIView!
    @IBOutlet var validateButton: UIButton!
    @IBOutlet var validateQR: UIButton!
    @IBOutlet var assetType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: true)
    }
    
    func updateCotent(booking: Booking) {
        guard let safeWorkstationCode = booking.assetCode else {
            return
        }
        
        qrValidation.isHidden = true
        notValidableView.isHidden = true
        isValidatedUI(booking: booking)
        self.bookingLocation.text = safeWorkstationCode
        assetType.text = Helpers.getAssetTypeLabel(label: booking.assetType)
        let isPermanentBooking = booking.isPermanent ?? false
        if(isPermanentBooking) {
            bookingDate.text = Constants.BookingPolicies.permanentBookingCell
        } else {
            bookingDate.text = (Helpers.getAssetTypeLabel(label: booking.reservationMessage))
        }
    }
    
    func isValidatedUI(booking: Booking) {
        guard let safeStartTime = booking.startTime else {
            return
        }
        let isPermanentBooking = booking.isPermanent ?? false
        if  (Helpers.dateStringIsToday(dateString: safeStartTime) || isPermanentBooking) {
            let isValidated = booking.confirmedAt != nil
            if (isValidated || isPermanentBooking) {
                qrValidation.isHidden = false
                validateQR.setImage(UIImage(named: Constants.Images.qrCodeCheck), for: .normal)
                validateButton.setTitle(Constants.MyBookingsValidate.validated, for: .normal)
                validateButton.setTitleColor(UIColor(named: Constants.Colors.textGreen), for: .normal)
                validateQR.isEnabled = false
                validateButton.isEnabled = false
                return
            }
            if Helpers.isBookingInTimeForValidation(booking: booking) {
                qrValidation.isHidden = false
                validateQR.setImage(UIImage(named: Constants.Images.qrCodeLess), for: .normal)
                validateButton.setTitle(Constants.MyBookingsValidate.validate, for: .normal)
                validateButton.setTitleColor(UIColor(named: Constants.Colors.interactiveDark), for: .normal)
                validateQR.isEnabled = !isValidated
                validateButton.isEnabled = !isValidated
                return
            }
            notValidableView.isHidden = false
            let timezone = ApiService.shared.buildingData.first(where: { $0.id == booking.buildingId })?.timezone ?? Constants.DateTimeFormat.utcFormat
            var startDate = Helpers.convertDateStringToDate(dateString: safeStartTime)
            startDate = Helpers.offsetUTCDateToGivenTimezone(dateUTC: startDate, timezoneAbbreviation: timezone)
            let beforeDeadLine = UserDefaultsService.shared.getLimitBeforeDeadLine()
            let afterDeadLine = UserDefaultsService.shared.getLimitAfterDeadLine()
            let initValidationDate = Helpers.addMinutesToDate(date: startDate, minutes: beforeDeadLine * -1)
            let endValidationDate = Helpers.addMinutesToDate(date: startDate, minutes: afterDeadLine)
            validationLabel.text = Helpers.constructLabelForValidation(initDate: initValidationDate, endDate: endValidationDate)
        }
    }
    
    func isDayOfBookingValidation(booking: Booking) -> Bool {
        return true
    }
}
