//
//  AssetFeatureCell.swift
//  Bureau
//
//  Created by Victor Arana on 8/27/21.
//

import UIKit

class AssetFeatureCell: UITableViewCell {

    @IBOutlet var assetFeatureName: UILabel!
    @IBOutlet var assetFeatureSelected: UISwitch!
    
    var workstationFeature: WorkstationFeature!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateContent(workstationFeature: WorkstationFeature) {
        self.assetFeatureName.text = workstationFeature.name
        self.assetFeatureSelected.isOn = workstationFeature.isSelected
        self.workstationFeature = workstationFeature
    }
    
    @IBAction func enableFeature(_ sender: UISwitch) {
        guard let safeWorkstationFeature = workstationFeature else { return }
        NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.selectWorkstationFeatures),
                                        object: nil,
                                        userInfo: [Constants.NotificationNameKeys.selectWorkstationFeatures : safeWorkstationFeature])
    }
    
}
