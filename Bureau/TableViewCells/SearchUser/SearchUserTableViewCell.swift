//
//  SearchUserTableViewCell.swift
//  Bureau
//
//  Created by Julio Gabriel Tobares on 09/03/2022.
//

import UIKit

class SearchUserTableViewCell: UITableViewCell {
    
    @IBOutlet var userName: UILabel!
    @IBOutlet var userEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateContent(userName: String, userEmail: String) {
        self.userName.text = userName
        self.userEmail.text = userEmail
    }
}
