//
//  AssetTypeCell.swift
//  Bureau
//
//  Created by Victor Arana on 8/26/21.
//

import UIKit

class AssetTypeCell: UITableViewCell {
    
    var assetType: AssetType!

    @IBOutlet var assetTypeImageView: UIImageView!
    @IBOutlet var assetTypeName: UILabel!
    @IBOutlet var checkAccessoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            self.checkAccessoryImage.isHidden = !self.checkAccessoryImage.isHidden
            guard let safeAssetType = assetType else { return }
            NotificationCenter.default.post(name: Notification.Name(Constants.NotificationNameKeys.enableWorkstationFeatures),
                                            object: nil,
                                            userInfo: [Constants.NotificationNameKeys.enableWorkstationFeatures : safeAssetType])
        }
    }
    
    func updateContent(assetType: AssetType) {
        self.assetTypeName.text = assetType.name ?? Constants.AssetTypeDefault.label
        if assetType.isSelected {
            self.checkAccessoryImage.isHidden = false
        } else {
            self.checkAccessoryImage.isHidden = true
        }
        self.assetType = assetType
    }
    
}
