//
//  UserDefaultsService.swift
//  Bureau
//
//  Created by Victor Arana on 8/16/21.
//

import Foundation

class UserDefaultsService: UserDefaultsProtocol {
    
    // MARK: - Constants
    static let shared = UserDefaultsService()
    let defaults = UserDefaults()
    let defaultsWidget = UserDefaults(suiteName: "group.com.jalasoft.bureauapps")

    // MARK: - Functions
    func saveUserInfo(user: User, completion: @escaping (Result<Bool, CustomError>) -> Void) {
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(user)
            UserDefaults.standard.set(data, forKey: Constants.UserDefaultsKeys.user)
            completion(.success(true))
        } catch {
            completion(.failure(.internalError))
        }
    }
    
    func getUserInfo(completion: @escaping (Result<User, CustomError>) -> Void) {
        if let data = UserDefaults.standard.data(forKey: Constants.UserDefaultsKeys.user) {
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                UserDefaultsService.shared.defaultsWidget?.setValue(user.email, forKey: Constants.UserDefaultsKeys.userEmail)
                completion(.success(user))
            } catch {
                completion(.failure(.internalError))
            }
        } else {
            completion(.failure(.internalError))
        }
    }

    func saveBuildingInfo(building: Building) {
        UserDefaults.standard.set(building.id, forKey: Constants.UserDefaultsKeys.buildingId)
        UserDefaults.standard.set(building.name, forKey: Constants.UserDefaultsKeys.buildingName)
        UserDefaults.standard.synchronize()
    }
    
    func saveBuildingTimezone(buildingTimezone: String) {
        UserDefaults.standard.set(buildingTimezone, forKey: Constants.UserDefaultsKeys.buildingTimezone)
    }
    
    func getBuildingInfo() -> [String] {
        guard let buildingName = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.buildingName) else { return [] }
        guard let buildingId = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.buildingId) else { return [] }
        if getBuildingSwitchState() {
            return [buildingId, buildingName]
        }
        return []
    }
    
    func getBuildingTimezone() -> String? {
        guard let buildingTimezone = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.buildingTimezone) else { return nil }
        if getBuildingSwitchState() {
            return buildingTimezone
        }
        return nil
    }
    
    func saveLimitAfterDeadLine(after limit: Int) {
        UserDefaults.standard.setValue(limit, forKey: Constants.UserDefaultsKeys.limitAfterDeadLine)
    }
    
    func getLimitAfterDeadLine() -> Int {
        let limitAfterDeadLine = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKeys.limitAfterDeadLine)
        return limitAfterDeadLine
    }
    
    func saveLimitBeforeDeadLine(before limit: Int) {
        UserDefaults.standard.setValue(limit, forKey: Constants.UserDefaultsKeys.limitBeforeDeadLine)
    }
    
    func getLimitBeforeDeadLine() -> Int {
        let limitAfterDeadLine = UserDefaults.standard.integer(forKey: Constants.UserDefaultsKeys.limitBeforeDeadLine)
        return limitAfterDeadLine
    }
    
    func deleteBuildingInfo() {
        UserDefaults.standard.removeObject(forKey: Constants.UserDefaultsKeys.buildingId)
        UserDefaults.standard.removeObject(forKey: Constants.UserDefaultsKeys.buildingName)
        UserDefaults.standard.synchronize()
    }
    
    func saveBuildingSwitchState(_ state: Bool) {
        UserDefaults.standard.set(state, forKey: Constants.UserDefaultsKeys.buildingSwitch)
        UserDefaults.standard.synchronize()
    }
    
    func getBuildingSwitchState() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.UserDefaultsKeys.buildingSwitch)
    }

    open func setStructArray<T: Codable>(_ value: [T], forKey defaultName: String) {
        let data = value.map { try? JSONEncoder().encode($0) }
        defaultsWidget?.set(data, forKey: defaultName)
    }

    open func getStructArray<T>(_ type: T.Type, forKey defaultName: String) -> [T] where T : Decodable {
        guard let encodedData = defaultsWidget?.array(forKey: defaultName) as? [Data] else {
            return []
        }

        // swiftlint:disable force_try
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
    
    func saveRecurrentBookingsPermission(_ recurrentBookingsPermission: Bool) {
        UserDefaults.standard.setValue(recurrentBookingsPermission, forKey: Constants.UserDefaultsKeys.recurrentBookingsPermission)
    }
    
    func getRecurrentBookingsPermission() -> Bool {
        let limitAfterDeadLine = UserDefaults.standard.bool(forKey: Constants.UserDefaultsKeys.recurrentBookingsPermission)
        return limitAfterDeadLine
    }
    
    func saveBookForOthersPermission(_ bookForOthersPermission: Bool) {
        UserDefaults.standard.setValue(bookForOthersPermission, forKey: Constants.UserDefaultsKeys.bookForOthersPermission)
    }
    
    func getBookForOthersPermission() -> Bool {
        let limitAfterDeadLine = UserDefaults.standard.bool(forKey: Constants.UserDefaultsKeys.bookForOthersPermission)
        return limitAfterDeadLine
    }
}
