//
//  AuthService.swift
//  Bureau
//
//  Created by Victor Arana on 6/7/21.
//

import Foundation
import AppAuth

class AuthService: AuthServiceProtocol {

    // MARK: - Variables
    private var authState: OIDAuthState?
    private var authServiceConfiguration: OIDServiceConfiguration?

    // MARK: - Constants
    static let shared = AuthService()
    let issuer: String = Constants.Demo.isInDemoMode
                        ? Bundle.main.object(forInfoDictionaryKey: Constants.JalaLogin.issuerDemo)! as! String
                        : Bundle.main.object(forInfoDictionaryKey: Constants.JalaLogin.issuer)! as! String
    let redirectURL: String = Bundle.main.object(forInfoDictionaryKey: Constants.JalaLogin.redirectURL)! as! String

    // MARK: - Functions
    func getOIDAuthorizationRequest(completion: @escaping (Result<OIDAuthorizationRequest, CustomError>) -> Void ) {
        guard let safeIssuer = URL(string: issuer) else {
            completion(.failure(CustomError.internalError))
            return
        }
        OIDAuthorizationService.discoverConfiguration(forIssuer: safeIssuer) { configuration, error in
            if error != nil {
                completion(.failure(CustomError.connectionError))
                return
            }
            guard let config = configuration else {
                completion(.failure(CustomError.connectionError))
                return
            }
            self.authServiceConfiguration = config
            guard let redirectURI = URL(string: self.redirectURL) else {
                completion(.failure(CustomError.internalError))
                return
            }
            let request = OIDAuthorizationRequest(configuration: config,
                                                  clientId: Constants.JalaLogin.clientId,
                                                  clientSecret: Constants.Demo.isInDemoMode ? Constants.JalaLogin.clientSecretDemo : Constants.JalaLogin.clientSecret,
                                                  scopes: [OIDScopeOpenID, OIDScopeProfile, "offline_access"],
                                                  redirectURL: redirectURI,
                                                  responseType: OIDResponseTypeCode,
                                                  additionalParameters: ["prompt": "login"])
            completion(.success(request))
        }
    }
    
    func signOut(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void) {
        var endSessionUrl: URL?
        
        if let idToken = self.authState?.lastTokenResponse?.idToken {
            endSessionUrl = self.authServiceConfiguration?.endSessionEndpoint
            let queryItems = [URLQueryItem(name: "id_token_hint", value: idToken)]
            endSessionUrl?.appending(queryItems)
        } else {
            let savedUrlString = self.getEndSessionVariables()
            endSessionUrl = savedUrlString != nil ? URL(string: savedUrlString ?? "") : nil
        }
        
        if let saveURL = endSessionUrl {
            let urlRequest = URLRequest(url: saveURL)
            let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
                if let response = response as? HTTPURLResponse, error == nil, (200...299).contains(response.statusCode) {
                    self.setIslogoutPendingFlag(value: false)
                    return
                } else {
                    self.setIslogoutPendingFlag(value: true)
                    self.setEndSessionVariables(endSessionUrl: saveURL.absoluteString)
                }
                if let safeData = data, !safeData.isEmpty {
                    print("Logout response: \(String(describing: String(data: safeData, encoding: .utf8)))")
                }
            }
            task.resume()
        }
        
        self.setAuthState(nil, completionHandler)
    }

    func setAuthState(_ authState: OIDAuthState?, _ completionHandler: @escaping (Result<Bool, CustomError>) -> Void ) {
        if (self.authState == authState) {
            return
        }
        self.authState = authState
        self.saveState(completionHandler)
    }

    func saveState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void ) {
        var data: Data?
        if let safeAuthState = self.authState {
            do {
                data = try NSKeyedArchiver.archivedData(withRootObject: safeAuthState as Any, requiringSecureCoding: true)
            } catch {
                completionHandler(.failure(CustomError.internalError))
            }
        }
        if let userDefaults = UserDefaults(suiteName: Constants.JalaLogin.suiteName) {
            userDefaults.set(data, forKey: Constants.JalaLogin.kAppAuthBureauAuthStateKey)
            userDefaults.synchronize()
            completionHandler(.success(true))
            return
        }
        completionHandler(.failure(CustomError.internalError))
    }
    
    func setIslogoutPendingFlag(value: Bool) {
        if let userDefaults = UserDefaults(suiteName: Constants.JalaLogin.suiteName) {
            userDefaults.set(value, forKey: Constants.JalaLogin.logoutFlag)
            userDefaults.synchronize()
        }
    }
    
    func setEndSessionVariables(endSessionUrl: String) {
        if let userDefaults = UserDefaults(suiteName: Constants.JalaLogin.suiteName) {
            userDefaults.set(endSessionUrl, forKey: Constants.JalaLogin.endSessionUrl)
            userDefaults.synchronize()
        }
    }
    
    func getIslogoutPendingFlag() -> Bool {
        guard let isLogoutPending = UserDefaults(suiteName: Constants.JalaLogin.suiteName)?.object(forKey: Constants.JalaLogin.logoutFlag) as? Bool else {
            return false
        }
        return isLogoutPending
    }
    
    func getEndSessionVariables() -> String? {
        let url = UserDefaults(suiteName: Constants.JalaLogin.suiteName)?.object(forKey: Constants.JalaLogin.endSessionUrl) as? String
        return url
    }

    func loadState(_ completionHandler: @escaping (Result<Bool, CustomError>) -> Void ) {
        guard let data = UserDefaults(suiteName: Constants.JalaLogin.suiteName)?.object(forKey: Constants.JalaLogin.kAppAuthBureauAuthStateKey) as? Data else {
            completionHandler(.failure(CustomError.internalError))
            return
        }
        if let authState = NSKeyedUnarchiver.unarchiveObject(with: data) as? OIDAuthState {
            self.authState = authState
            completionHandler(.success(true))
            return
        }
        completionHandler(.failure(CustomError.internalError))
    }

    func getAuthState() -> OIDAuthState? {
        guard let safeAuthState = self.authState else {
            let viewController = SceneDelegate.mainStoryboard.instantiateViewController(withIdentifier: Constants.StoryBoard.loginViewControllerID)
            UIApplication.shared.windows.first?.rootViewController = viewController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
            return nil
        }
        return safeAuthState
    }

    func getUserInformationEndpoint() -> URL? {
        guard let userinfoEndpoint = self.authState?.lastAuthorizationResponse.request.configuration.discoveryDocument?.userinfoEndpoint else {
            return nil
        }
        return userinfoEndpoint
    }
}
