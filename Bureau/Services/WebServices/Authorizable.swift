//
//  Authorizable.swift
//  Bureau
//
//  Created by Joshua Ugarte on 15/6/22.
//

import Foundation
import AppAuth

class Authorizable: AuthServiceDelegate {
    
    func setAuthState(authState: OIDAuthState?) {
         AuthService.shared.setAuthState(authState) { (result) in
              switch result {
              case .success(_):
                   return
              case .failure(_):
                   Helpers.redirectToLoginPage(isForcedLogout: true)
              }
         }
    }
    
    func verifyAccessToken(_ currentAccessToken: String?,_ accessToken: String?,_ error: Error?) -> (Bool,String) {
         guard let authState = AuthService.shared.getAuthState() else {
              return (false,"")
         }
         if error != nil {
              self.setAuthState(authState: nil)
              Helpers.redirectToLoginPage(isForcedLogout: true)
              return (false,"")
         }
         guard let safeAccessToken = accessToken else {
              return (false,"")
         }
         if currentAccessToken != safeAccessToken {
              self.setAuthState(authState: authState)
         }
         UserDefaultsService.shared.defaultsWidget?.setValue(safeAccessToken, forKey: Constants.UserDefaultsKeys.userAuth)
         return (true, safeAccessToken)
    }
}
