//
//  UserService.swift
//  Bureau
//
//  Created by Victor Arana on 7/7/21.
//

import Foundation
import AppAuth
import FirebaseCrashlytics

class UserService: Authorizable, UserServiceProtocol {

    // MARK: - Constants
    static let shared = UserService()
        
    func apiToGetUserData(completion: @escaping (Result<User, CustomError>) -> Void) {
        guard let authState = AuthService.shared.getAuthState() else {
            return
        }
        let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
        authState.performAction { (accessToken, _, error) in
            let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
            if !verifyAccessTokenTuple.0 {
                completion(.failure(CustomError.internalError))
                return
            }
            guard let userinfoEndpoint = AuthService.shared.getUserInformationEndpoint() else {
                completion(.failure(CustomError.internalError))
                return
            }
            var request = URLRequest(url: userinfoEndpoint)
            request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    completion(.failure(CustomError.connectionError))
                    return
                }
                guard let safeResponse = response as? HTTPURLResponse else {
                    completion(.failure(CustomError.connectionError))
                    return
                }
                if safeResponse.statusCode != 200 {
                    completion(.failure(CustomError.connectionError))
                    return
                }
                guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let user = try decoder.decode(User.self, from: json)
                    if let email = user.email {
                        Crashlytics.crashlytics().setUserID(email)
                    }
                    completion(.success(user))
                } catch {
                    completion(.failure(CustomError.internalError))
                }
            }.resume()
        }
    }
}
