//
//  APIService.swift
//  Bureau
//
//  Created by user on 6/2/21.
//

import Foundation

class ApiService: Authorizable, APIServiceProtocol{
     
     // MARK: - Constants
     static let shared = ApiService()
     var currentDate = Date() {
          didSet {
               Helpers.setCrashlyticsKey(Constants.Crashlytics.keyDateValue, as: currentDate.description)
          }
     }
     var startHourDate = Date() {
          didSet {
               Helpers.setCrashlyticsKey(Constants.Crashlytics.keyStartTimeValue, as: startHourDate.description)
          }
     }
     var endHourDate = Date() {
          didSet {
               Helpers.setCrashlyticsKey(Constants.Crashlytics.keyEndTimeValue, as: endHourDate.description)
          }
     }
     var sessionUser: User!
     var selectedUser: User!
     var selectedBuilding: Building!
     var buildingData: [Building]! = []
     var buildingId: String!
     var selectedArea: Area!
     var selectedAssetType: AssetType!
     var selectedWorkstationFeatures: [WorkstationFeature] = []
     var showOnlyAvailableSpaces: Bool = false
     var enabledFilters = 0
     var urlBase = Constants.Demo.isInDemoMode
     ? Bundle.main.object(forInfoDictionaryKey: Constants.Api.urlBaseDemo)!
     : Bundle.main.object(forInfoDictionaryKey: Constants.Api.urlBase)!
     var tempCurrentDate: Date = Date() {
          didSet {
               self.setRecurrentEndDate()
          }
     }
     var recurrenceRule : RecurrenceRule = RecurrenceRule(isCustom: false, type: "never", endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
     var tempRecurrenceRule: RecurrenceRule = RecurrenceRule(isCustom: false, type: "never", endDate: Date(), onDays: [], onMonth: nil, onWeek: nil, onDate: nil, every: nil)
     var isRecurrent: Bool = false
     
     // MARK: - Constructors
     private override init() {
          Helpers.setCrashlyticsKey(Constants.Crashlytics.keyDateValue, as: currentDate.description)
          Helpers.setCrashlyticsKey(Constants.Crashlytics.keyTimeZoneValue, as: TimeZone.current.abbreviation())
          startHourDate = Calendar.current.date(bySettingHour: 8, minute: 30, second: 0, of: Date())!
          Helpers.setCrashlyticsKey(Constants.Crashlytics.keyStartTimeValue, as: startHourDate.description)
          UserDefaultsService.shared.defaultsWidget?.setValue(urlBase, forKey: Constants.UserDefaultsKeys.currentURL)
          if let safeEndHour = Calendar.current.date(byAdding: .hour, value: 10, to: startHourDate) {
               endHourDate = safeEndHour
               Helpers.setCrashlyticsKey(Constants.Crashlytics.keyEndTimeValue, as: endHourDate.description)
          }
     }
     
     func setRecurrentEndDate() {
          let diff = Calendar.current.dateComponents([.day], from: self.tempRecurrenceRule.endDate, to: tempCurrentDate)
          if diff.day ?? -1 >= 0 {
               self.tempRecurrenceRule.endDate = Calendar.current.date(byAdding: .month, value: 1, to: tempCurrentDate) ?? Date()
          }
     }
     
     // MARK: - Functions
     func apiToGetAreaData(completion: @escaping (Result<[Area], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState(), let buildingID = buildingId else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               
               let urlSwagger = "\(self.urlBase)areas?filters=buildingId%20in%20array(id(\(buildingID)))&filters=public%20eq%20bool(true)"
               guard let url = URL(string: urlSwagger) else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let areaData = try decoder.decode([Area].self, from: json)
                         completion(.success(areaData))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetAreasNumberOfBuilding(buildingId: String?, completion: @escaping (Result<Int, CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState(), let buildingID = buildingId else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               
               let urlSwagger = "\(self.urlBase)areas?filters=buildingId%20in%20array(id(\(buildingID)))&filters=public%20eq%20bool(true)"
               guard let url = URL(string: urlSwagger) else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let areaData = try decoder.decode([Area].self, from: json)
                         completion(.success(areaData.count))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetBuildingData(completion : @escaping (Result<[Building], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)buildings") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let buildingList = try decoder.decode([Building].self, from: json)
                         completion(.success(buildingList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetWorkstationDataFromAnArea(completion: @escaping (Result<[Asset], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState(), let areaID = selectedArea?.id else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    self.setAuthState(authState: nil)
                    Helpers.redirectToLoginPage(isForcedLogout: true)
                    return
               }
               guard let safeAccessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               if currentAccessToken != safeAccessToken {
                    self.setAuthState(authState: authState)
               }
               var assetTypeSelected = ""
               if self.selectedAssetType != nil && self.selectedAssetType.id != "" {
                    assetTypeSelected = "&filters=properties.type%20eq%20Id(\(self.selectedAssetType.id!))"
               }
               
               var workstationFeaturesSelected = ""
               if !self.selectedWorkstationFeatures.isEmpty {
                    workstationFeaturesSelected += self.selectedWorkstationFeatures.map({
                       "&filters=properties.labels%20in%20array(id(\($0.id!)))"
                    }).joined()
               }
               guard let url = URL(string: "\(self.urlBase)assets?filters=areaId%20in%20array%28id%28\(areaID)%29%29" + assetTypeSelected + workstationFeaturesSelected) else {
                    completion(.failure(CustomError.internalError))
                    return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(safeAccessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let workstationList = try decoder.decode([Asset].self, from: json)
                         completion(.success(workstationList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetAllWorkstationDataFromAnArea(completion: @escaping (Result<[Asset], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState(), let areaID = selectedArea?.id else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    self.setAuthState(authState: nil)
                    Helpers.redirectToLoginPage(isForcedLogout: true)
                    return
               }
               guard let safeAccessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               if currentAccessToken != safeAccessToken {
                    self.setAuthState(authState: authState)
               }
               guard let url = URL(string: "\(self.urlBase)assets?filters=areaId%20in%20array%28id%28\(areaID)%29%29") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(safeAccessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetAllWorkstationDataFromAreaRequest(request: request, completion: completion)
          }
     }

     private func makeGetAllWorkstationDataFromAreaRequest(request: URLRequest, completion: @escaping(Result<[Asset], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let workstationList = try decoder.decode([Asset].self, from: json)
                    completion(.success(workstationList))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetLayoutDataFromAnArea(completion: @escaping (Result<Area, CustomError>) -> Void) {
          guard let area = selectedArea else {
               completion(.failure(.internalError))
               return
          }
          completion(.success(area))
     }
     
     func apiToConfirmABooking(bookingId: String, completion: @escaping (Result<Booking, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)bookings/\(bookingId)") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var request = URLRequest(url: url)
               let body = [["op": "replace",
                           "path": "/ConfirmedAt",
                           "value": Helpers.convertDateToDateString(date: Date())]]
               request.httpMethod = "PATCH"
               do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
               } catch {
                    completion(.failure(CustomError.internalError))
                    return
               }
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.bookingConfirmation], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let bookingConfirmation = try decoder.decode(Booking.self, from: json)
                         completion(.success(bookingConfirmation))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetWorkstationBookings(workstationID: String?, completion : @escaping (Result<[Booking], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let safeWorkstationId = workstationID else { return }
               var urlSwagger = "\(self.urlBase)bookings/overlapped?filters=assetId%20in%20array%28id%28\(safeWorkstationId)%29%29"
               let startHour = Helpers.constructDate(hour: "0", minute: "0", isAm: true)
               let endHour = Helpers.constructDate(hour: "11", minute: "59", isAm: false)
               let startDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: startHour)
               let endDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: endHour)
               urlSwagger += "&StartTime=\(startDateFormat)&EndTime=\(endDateFormat)"
               guard var url = URL(string: urlSwagger) else { return }
               let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
               let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
               url.appending(queryNotUserCancelled)
               url.appending(queryNotSystemCancelled)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let bookingList = try decoder.decode([Booking].self, from: json)
                         completion(.success(bookingList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToCreateFavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               completion(.failure(.internalError))
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)users/favorites/\(assetId)") else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               request.httpMethod = "POST"
               self.makeCreateFavoriteRequest(request: request, completion: completion)
          }
     }

     private func makeCreateFavoriteRequest(request: URLRequest, completion: @escaping(Result<UserProfile, CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let userProfileInfo = try decoder.decode(UserProfile.self, from: json)
                    completion(.success(userProfileInfo))
               } catch {
                    completion(.failure(.internalError))
               }
          }.resume()
     }
     
     func apiToDeletefavorite(assetId: String, completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               completion(.failure(.internalError))
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)users/favorites/\(assetId)") else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               request.httpMethod = "DELETE"
               self.makeDeleteFavoriteRequest(request: request, completion: completion)
          }
     }

     private func makeDeleteFavoriteRequest(request: URLRequest, completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let jsonData = data else {
                    completion(.failure(.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let userProfileInfo = try decoder.decode(UserProfile.self, from: jsonData)
                    completion(.success(userProfileInfo))
               } catch {
                    completion(.failure(.internalError))
               }
          }.resume()
     }
     
     func apiToGetUserFavorites(assets: [String], completion: @escaping (Result<[Asset], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               completion(.failure(.internalError))
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               let assetArray = assets.convertToIdString()
               guard let url = URL(string: "\(self.urlBase)assets?filters=_id%20in%20array(\(assetArray)&filters=public%20eq%20bool(true)&sorts=asc(name)")
               else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetFavoriteRequest(request: request, completion: completion)
          }
     }

     private func makeGetFavoriteRequest(request: URLRequest, completion: @escaping (Result<[Asset], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let myAssets = try decoder.decode([Asset].self, from: json)
                    completion(.success(myAssets))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetUserProfileInfo(completion: @escaping (Result<UserProfile, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)users/me") else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request ) { (data, _, error) in
                    if error != nil {
                         completion(.failure(CustomError.connectionError))
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let userProfileInfo = try decoder.decode(UserProfile.self, from: json)
                         completion(.success(userProfileInfo))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetUserBookings(email: String, bookingsTime: BookingsTime, completion : @escaping (Result<[Booking], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard var url = URL(string: "\(self.urlBase)bookings/groupByRecurrence?filters=owner.email%20eq%20string(\(email))") else { return }
               
               let calendar = Calendar.current
               if (bookingsTime == .today) {
                    let startOfDate: Date = calendar.startOfDay(for: Date())
                    let endOfDate: Date = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) ?? Date()
                    let startOfDateFormat: String = Helpers.convertDateTimeToDateString(date: startOfDate)
                    let endOfDateFormat: String = Helpers.convertDateTimeToDateString(date: endOfDate)
                    let queryStartOfDay = [URLQueryItem(name: "filters", value: "startTime gte date(\(startOfDateFormat))")]
                    let queryEndOfDay = [URLQueryItem(name: "filters", value: "startTime lte date(\(endOfDateFormat))")]
                    url.appending(queryStartOfDay)
                    url.appending(queryEndOfDay)
               } else if (bookingsTime == .upcoming) {
                    var dayComponent = DateComponents()
                    dayComponent.day = 1
                    let nextDay: Date = calendar.date(byAdding: dayComponent, to: Date()) ?? Date()
                    let startDate: Date = calendar.startOfDay(for: nextDay)
                    let startDateFormat: String = Helpers.convertDateTimeToDateString(date: startDate)
                    let queryStartDay = [URLQueryItem(name: "filters", value: "startTime gte date(\(startDateFormat))")]
                    url.appending(queryStartDay)
               }
               let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
               let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
               url.appending(queryNotUserCancelled)
               url.appending(queryNotSystemCancelled)
               
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetUserBookingsRequest(request: request, completion: completion)
          }
     }

     private func makeGetUserBookingsRequest(request: URLRequest, completion : @escaping (Result<[Booking], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, _ , error) in
               if error != nil {
                    completion(.failure(CustomError.connectionError))
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let bookingList = try decoder.decode([Booking].self, from: json)
                    completion(.success(bookingList))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToDeleteBooking(bookingID:String, completion : @escaping (Result<Bool, CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)bookings/\(bookingID)") else { return }
               var request = URLRequest(url: url)
               request.httpMethod = "DELETE"
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, _ , error) in
                    if error != nil {
                         completion(.failure(CustomError.connectionError))
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         _ = try decoder.decode(Booking.self, from: json)
                         completion(.success(true))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToCreateABooking(assetId: String, buildingId: String, completion: @escaping (Result<Booking, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else { return }
          if buildingId.isEmpty { return }

          guard let building = self.buildingData?.first(where: { $0.id == buildingId }) else {
               completion(.failure(.internalError))
               return
          }

          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)

               guard let url = URL(string: "\(self.urlBase)Bookings/"),
                     verifyAccessTokenTuple.0 else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var request = URLRequest(url: url)
               let timezone = building.timezone ?? Constants.DateTimeFormat.utcFormat
               var startDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.startHourDate)
               var endDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.endHourDate)
               var recurrentEndDateFormat = Helpers.convertDateFormat(inputDate: self.recurrenceRule.endDate, inputHour: self.endHourDate)
               startDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: startDateFormat, isForQueryParam: false, timezone: timezone)
               endDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: endDateFormat, isForQueryParam: false, timezone: timezone)
               recurrentEndDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: recurrentEndDateFormat, isForQueryParam: false, timezone: timezone)
               var recurrenceRules: [String : Any] = [:]
               var owner: [String : Any] = [:]
               let state: String = Constants.BookingState.pending
               if(self.isRecurrent) {
                    recurrenceRules = [
                         "isCustom": "\(self.recurrenceRule.isCustom)",
                         "type": "\(self.recurrenceRule.type)",
                         "endDate": recurrentEndDateFormat,
                         "onDays": self.recurrenceRule.onDays,
                         "onMonth": self.recurrenceRule.onMonth ?? "",
                         "onWeek": self.recurrenceRule.onWeek ?? "",
                         "onDate": self.recurrenceRule.onDate ?? "",
                         "every": self.recurrenceRule.every ?? ""
                    ]
               }
               if(self.selectedUser != nil) {
                    if (self.selectedUser.email != self.sessionUser.email) {
                         owner = [
                              "name": self.selectedUser.name!,
                              "email": self.selectedUser.email!
                         ]
                    } else {
                         owner = [:]
                    }
               }
               let body: [String : Any] = [
                    "startTime": startDateFormat,
                    "endTime": endDateFormat,
                    "isRecurrent": "\(self.isRecurrent)",
                    "recurrenceRule": recurrenceRules,
                    "owner": owner,
                    "assetId": assetId,
                    "status": ["state": state]
               ]
               request.httpMethod = "POST"
               do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
               } catch {
                    completion(.failure(CustomError.internalError))
                    return
               }
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
               self.makeCreateBookikngRequest(request: request, completion: completion)
          }
     }

     private func makeCreateBookikngRequest(request: URLRequest, completion: @escaping (Result<Booking, CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               let errorTypes: [ApiErrorType] = [.bookingConfirmation, .serverError]
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: errorTypes, completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    var booking = try decoder.decode(Booking.self, from: json)
                    booking.startTime = Helpers.calendarFormatDateTimeString(dateStr: booking.startTime!)
                    booking.endTime = Helpers.calendarFormatDateTimeString(dateStr: booking.endTime!)
                    completion(.success(booking))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetWorkstationDataFromABuilding(idList: String, completion: @escaping (Result<[Asset], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState(), let buildingID = buildingId else {
               completion(.failure(.internalError))
               return
          }
          authState.performAction { (accessToken, _, error) in
               guard let accessToken = accessToken,
                    error == nil else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var assetTypeSelected = ""
               if self.selectedAssetType != nil && self.selectedAssetType.id != "" {
                    assetTypeSelected = "&filters=properties.type%20eq%20id(\(self.selectedAssetType.id!))"
               }
               
               var workstationFeaturesSelected = ""
               if !self.selectedWorkstationFeatures.isEmpty {
                    workstationFeaturesSelected += self.selectedWorkstationFeatures.map({
                       "&filters=properties.labels%20in%20array(id(\($0.id!)))"
                    }).joined()
               }
               guard var url = URL(string: "\(self.urlBase)assets?filters=buildingId%20eq%20id(\(buildingID))" + assetTypeSelected + workstationFeaturesSelected)
               else { return }

               if(idList != "") {
                    let queryBookingsIds = [URLQueryItem(name: "filters", value: "_id nin array(\(idList))")]
                    url.appending(queryBookingsIds)
               }
               let queryPublicTrue = [URLQueryItem(name: "filters", value: "public eq bool(true)")]
               let querySortName = [URLQueryItem(name: "sorts", value: "asc(name)")]
               url.appending(queryPublicTrue)
               url.appending(querySortName)
               
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetWorkStationFromBuildingRequest(request: request, completion: completion)
          }
     }

     private func makeGetWorkStationFromBuildingRequest(request: URLRequest, completion: @escaping (Result<[Asset], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let layoutList = try decoder.decode([Asset].self, from: json)
                    completion(.success(layoutList))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetAssetBookings(assetId: String, startTime: String, endTime: String, completion : @escaping (Result<[Booking], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard var url = URL(string: "\(self.urlBase)bookings/overlapped?StartTime=\(startTime)&EndTime=\(endTime)&filters=assetId%20eq%20id(\(assetId)") else { return }
               let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
               let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
               url.appending(queryNotUserCancelled)
               url.appending(queryNotSystemCancelled)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let bookingList = try decoder.decode([Booking].self, from: json)
                         completion(.success(bookingList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetAssetWithID(assetId: String, completion:@escaping(Result<Asset,CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)assets/\(assetId)") else {
                    completion(.failure(CustomError.internalError))
                    return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let asset = try decoder.decode(Asset.self, from: json)
                         completion(.success(asset))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetAssetTypes(completion: @escaping (Result<[TypeData], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard var url = URL(string: "\(self.urlBase)assets/types") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               let queryItems = [URLQueryItem(name: "sorts", value: "asc(name)")]
               url.appending(queryItems)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let assetTypes = try decoder.decode([TypeData].self, from: json)
                         completion(.success(assetTypes))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetWorkstationFeatures(completion: @escaping (Result<[Feature], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)assets/labels") else {
                    completion(.failure(CustomError.internalError))
                    return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let workstationFeatures = try decoder.decode([Feature].self, from: json)
                         completion(.success(workstationFeatures))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetAreaBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState(), let areaId = selectedArea.id else {
               return
          }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let accessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               let timezone = self.selectedBuilding.timezone ?? Constants.DateTimeFormat.utcFormat
               var startDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.startHourDate)
               var endDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.endHourDate)
               startDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: startDateFormat, isForQueryParam: true, timezone: timezone)
               endDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: endDateFormat, isForQueryParam: true, timezone: timezone)
               guard var url = URL(string: "\(self.urlBase)bookings/overlapped?StartTime=\(startDateFormat)" +
                                    "&EndTime=\(endDateFormat)&filters=areaId%20in%20array%28id%28\(areaId)%29%29") else {
                    completion(.failure(.internalError))
                    return
               }
               let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
               let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
               url.appending(queryNotUserCancelled)
               url.appending(queryNotSystemCancelled)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let bookingList = try decoder.decode([Booking].self, from: json)
                         completion(.success(bookingList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func apiToGetUsers(completion: @escaping (Result<[User], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               let verifyAccessTokenTuple = self.verifyAccessToken(currentAccessToken, accessToken, error)
               if !verifyAccessTokenTuple.0 {
                    completion(.failure(CustomError.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)users") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(verifyAccessTokenTuple.1)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                         return
                    }
                    guard let json = data else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    do {
                         let decoder = JSONDecoder()
                         let userList = try decoder.decode([User].self, from: json)
                         completion(.success(userList))
                    } catch {
                         completion(.failure(CustomError.internalError))
                    }
               }.resume()
          }
     }
     
     func getBookingConfiguration(completion: @escaping (Result<BookingConfiguration, CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else { return }
          authState.performAction { (accessToken, _, error) in
               guard let accessToken = accessToken,
                    error == nil else {
                    completion(.failure(.internalError))
                    return
               }
               guard let url = URL(string: "\(self.urlBase)bureausettings/initialconfig") else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetBookingConfigurationRequest(request: request, completion: completion)
          }
     }

     private func makeGetBookingConfigurationRequest(request: URLRequest, completion: @escaping(Result<BookingConfiguration, CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let bookingConfiguration = try decoder.decode(BookingConfiguration.self, from: json)
                    completion(.success(bookingConfiguration))
               } catch {
                    completion(.failure(.internalError))
               }
          }.resume()
     }
     
     func apiToGetUserGroups(groupIdArray: [String], completion: @escaping (Result<[Group], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else { return }
          authState.performAction { (accessToken, _, error) in
               guard let accessToken = accessToken,
                    error == nil else {
                    completion(.failure(.internalError))
                    return
               }
               let userGroupsId = groupIdArray.convertToIdString()
               guard let url = URL(string: "\(self.urlBase)groups?filters=_id%20in%20array(\(userGroupsId))") else { return }
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetUserGroupsRequest(request: request, completion: completion)
          }
     }

     private func makeGetUserGroupsRequest(request: URLRequest, completion: @escaping(Result<[Group], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let bookingConfiguration = try decoder.decode([Group].self, from: json)
                    completion(.success(bookingConfiguration))
               } catch {
                    completion(.failure(.internalError))
               }
          }.resume()
     }
     
     func apiToGetOverlappedBookings(completion: @escaping (Result<[Booking], CustomError>) -> Void) {
          guard let authState = AuthService.shared.getAuthState() else { return }
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                         completion(.failure(CustomError.internalError))
                         return
                    }
                    guard let accessToken = accessToken else {
                         completion(.failure(CustomError.internalError))
                         return
                    }
               let timezone = UserDefaultsService().getBuildingTimezone() ?? Constants.DateTimeFormat.utcFormat
               var startDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.startHourDate)
               var endDateFormat = Helpers.convertDateFormat(inputDate: self.currentDate, inputHour: self.endHourDate)
               startDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: startDateFormat, isForQueryParam: true, timezone: timezone)
               endDateFormat = Helpers.convertDatetimeToUTCFormat(stringDate: endDateFormat, isForQueryParam: true, timezone: timezone)
               guard var url = URL(string: "\(self.urlBase)bookings/overlapped?StartTime=\(startDateFormat)" +
                                    "&EndTime=\(endDateFormat)") else {
                    completion(.failure(.internalError))
                    return
               }
               let queryNotUserCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeUserCancelled)]
               let queryNotSystemCancelled = [URLQueryItem(name: "filters", value: Constants.QueryParameters.excludeSystemCancelled)]
               url.appending(queryNotUserCancelled)
               url.appending(queryNotSystemCancelled)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(accessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetOverlappedBookingsRequest(request: request, completion: completion)
          }
     }

     private func makeGetOverlappedBookingsRequest(request: URLRequest, completion: @escaping(Result<[Booking], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let bookingList = try decoder.decode([Booking].self, from: json)
                    completion(.success(bookingList))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetUserPermanentAssets(email: String, completion: @escaping (Result<[Asset], CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    self.setAuthState(authState: nil)
                    Helpers.redirectToLoginPage(isForcedLogout: true)
                    return
               }
               guard let safeAccessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               if currentAccessToken != safeAccessToken {
                    self.setAuthState(authState: authState)
               }
               guard var url = URL(string: "\(self.urlBase)assets") else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               let permanentAssets = [URLQueryItem(name: "filters", value: "properties.owner.email eq string(\(email)")]
               url.appending(permanentAssets)
               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(safeAccessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetUserPermanentAssetsRequest(request: request, completion: completion)
          }
     }

     private func makeGetUserPermanentAssetsRequest(request: URLRequest, completion: @escaping(Result<[Asset], CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let assetList = try decoder.decode([Asset].self, from: json)
                    completion(.success(assetList))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func apiToGetAreaInfo(areaId: String, completion: @escaping (Result<Area, CustomError>) -> Void ) {
          guard let authState = AuthService.shared.getAuthState() else {
               completion(.failure(.internalError))
               return
          }
          let currentAccessToken: String? = authState.lastTokenResponse?.accessToken
          authState.performAction { (accessToken, _, error) in
               if error != nil {
                    self.setAuthState(authState: nil)
                    Helpers.redirectToLoginPage(isForcedLogout: true)
                    return
               }
               guard let safeAccessToken = accessToken else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               if currentAccessToken != safeAccessToken {
                    self.setAuthState(authState: authState)
               }
               guard let url = URL(string: "\(self.urlBase)areas/\(areaId)") else {
                    completion(.failure(CustomError.internalError))
                    return
               }

               var request = URLRequest(url: url)
               request.setValue("\(Constants.AuthorizationHeader.fieldValue) \(safeAccessToken)", forHTTPHeaderField: Constants.AuthorizationHeader.fieldName)
               self.makeGetAreaInfoRequest(request: request, completion: completion)
          }
     }

     private func makeGetAreaInfoRequest(request: URLRequest, completion: @escaping(Result<Area, CustomError>) -> Void) {
          URLSession.shared.dataTask(with: request) { (data, response, error) in
               if (self.hasErrors(data: data, response: response, error: error, apiErrorType: [.defaultError], completion: completion)) {
                    return
               }
               guard let json = data else {
                    completion(.failure(CustomError.internalError))
                    return
               }
               do {
                    let decoder = JSONDecoder()
                    let areaInfo = try decoder.decode(Area.self, from: json)
                    completion(.success(areaInfo))
               } catch {
                    completion(.failure(CustomError.internalError))
               }
          }.resume()
     }
     
     func hasErrors<T>(data: Data?, response: URLResponse?, error: Error?, apiErrorType: [ApiErrorType], completion: @escaping (Result<T, CustomError>) -> Void) -> Bool {
          if error != nil {
               completion(.failure(CustomError.connectionError))
               return true
          }
          guard let safeResponse = response as? HTTPURLResponse else {
               completion(.failure(CustomError.connectionError))
               return true
          }
          if safeResponse.statusCode == 404 && apiErrorType.contains(.bookingConfirmation) {
               completion(.failure(CustomError.bookingConfirmationError))
               return true
          }
          if safeResponse.statusCode == 500 && apiErrorType.contains(.serverError), let json = data {
               do {
                    let decoder = JSONDecoder()
                    let errorMessage = try decoder.decode(ErrorMessage.self, from: json)
                    completion(.failure(CustomError.serverError(message: errorMessage.error)))
                    return true
               } catch {
                    completion(.failure(CustomError.internalError))
                    return true
               }
          }
          if safeResponse.statusCode != 200 {
               completion(.failure(CustomError.connectionError))
               return true
          }
          return false
     }
}
