//
//  BookingNotification.swift
//  Bureau
//
//  Created by Christian Torrico Avila on 2/4/22.
//

import Foundation
import UserNotifications
import UIKit

class NotificationManager {
    
    static let shared = NotificationManager()
    let current = UNUserNotificationCenter.current()
    var todayBookingsList: [Booking] = []
    var upcomingBookingsList: [Booking] = []
    
    // MARK: - Create Notification
    func createNotificationForBooking(booking: Booking) {
        if let timezone = ApiService.shared.buildingData.first(where: { $0.id == booking.buildingId })?.timezone,
           let date = Helpers.calendarFormatDateWithTimezone(dateStr:  booking.startTime!, timezone: TimeZone.current.description) {
            guard let startTime = booking.startTime, let endTime = booking.endTime, let assetCode = booking.assetCode else {
                return
            }
            let dateFormat = Helpers.formatDate(startTime: startTime,
                                                  endTime: endTime,
                                                  format: Constants.BookingConfirmationModal.bookingDateLabelTable,
                                                timezone: timezone)
            let beforeDeadLine = UserDefaultsService.shared.getLimitBeforeDeadLine() *  -60
            let date = date.addingTimeInterval(TimeInterval(beforeDeadLine))
            let dateCompo = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
            
            let content = UNMutableNotificationContent()
            content.categoryIdentifier = Constants.PushNotification.categoryIdentifier
            content.title = Constants.PushNotification.title
            let min = UserDefaultsService.shared.getLimitBeforeDeadLine()
            content.body = "Your reservation of \(assetCode) starts in \(min) min\n\(dateFormat) (\(timezone))."
            content.sound = UNNotificationSound.default
            
            do {
                let encoder = JSONEncoder()
                let dataBookings = try encoder.encode(booking)
                content.userInfo = [Constants.PushNotification.userInfo : dataBookings]
            } catch {
                print(error)
            }
            let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateCompo, repeats: false)
            let request = UNNotificationRequest(identifier: booking.id, content: content, trigger: trigger)
            
            current.add(request) {(error) in
                if let error = error {
                    print(error)
                }
            }
        }
    }
    
    // MARK: - Create Cancel Notification
    func createCancelledBooking(booking: Booking) {
        if let date = Helpers.calendarFormatDate(dateStr: booking.startTime!) {
            guard let assetCode = booking.assetCode else {
                return
            }
            let minutesBefore = Constants.PushNotification.minutesBeforeCancelation
            let afterDeadLine = (UserDefaultsService.shared.getLimitAfterDeadLine() - minutesBefore ) * 60
            let date = date.addingTimeInterval(TimeInterval(afterDeadLine))
            let dateCompo = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
            
            let notificationId = "\(booking.id)cancell"
            let content = UNMutableNotificationContent()
            content.categoryIdentifier = Constants.PushNotification.categoryIdentifier
            content.title = Constants.PushNotification.titleCancellation
            content.body = "Your reservation \(assetCode) has not been validated and will be cancelled in \(minutesBefore) minutes if the QR code is not scanned."
            content.sound = UNNotificationSound.default
            
            do {
                let encoder = JSONEncoder()
                let dataBookings = try encoder.encode(booking)
                content.userInfo = [Constants.PushNotification.userInfo : dataBookings]
            } catch {
                print(error)
            }
            let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateCompo, repeats: false)
            let request = UNNotificationRequest(identifier: notificationId, content: content, trigger: trigger)
            
            current.add(request) {(error) in
                if let error = error {
                    print(error)
                }
            }
        }
    }
    
    // this function will delete all pending notifications, and will fetch its current bookings and will recreate
    // all before and after dead line notifications
    // MARK: - Init Notification
    func initNotitications() {
        UserDefaultsService.shared.getUserInfo { result in
            switch result {
            case .success(let user):
                guard let email = user.email else { return }
                self.removeAllNotifications()
                self.getUserBookings(email: email)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK: - Get Bookings from user
    func getUserBookings(email: String) {
        let group: DispatchGroup = DispatchGroup()
        self.todayBookingsList.removeAll()
        self.upcomingBookingsList.removeAll()
        group.enter()
        ApiService.shared.apiToGetUserBookings(email: email, bookingsTime: .today) { (result) in
            switch (result) {
            case .success(let todayBookings):
                self.todayBookingsList = todayBookings
            case .failure(let error):
                print(error)
            }
            group.leave()
        }
        group.enter()
        ApiService.shared.apiToGetUserBookings(email: email, bookingsTime: .upcoming) { (result) in
            switch (result) {
            case .success(let upcomingBookings):
                self.upcomingBookingsList = upcomingBookings
            case .failure(let error):
                print(error)
            }
            group.leave()
        }
        group.notify(queue: DispatchQueue.main) {
            let bookings = self.todayBookingsList + self.upcomingBookingsList
            for booking in bookings {
                self.createNotificationForBooking(booking: booking)
                if booking.confirmedAt == nil {  // if the booking has not been confirmed yet
                    self.createCancelledBooking(booking: booking)
                }
            }
        }
    }
    
    // MARK: - Remove All Notifications
    func removeNotification(bookingId: String, bookingDeadLine: BookingDeadLine ) {
        switch bookingDeadLine {
        case .beforeDeadLine:
            current.removePendingNotificationRequests(withIdentifiers: [bookingId])
        case .afterDeadLine:
            let notificationId = "\(bookingId)cancell"
            current.removePendingNotificationRequests(withIdentifiers: [notificationId])
        }
    }
    
    func removeAllNotifications() {
        current.removeAllPendingNotificationRequests()
        current.removeAllDeliveredNotifications()
    }
    
}
